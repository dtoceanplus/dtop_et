# This file is for creating Python package.

# Packaging Python Projects: https://packaging.python.org/tutorials/packaging-projects/
# Setuptools documentation: https://setuptools.readthedocs.io/en/latest/index.html

import setuptools

setuptools.setup(
    name="dtop_energytransf",
    version="0.0.1",
    packages=setuptools.find_packages(where="src"),
    package_dir={"": "src"},
    install_requires=[
        "flask",
        "flask-babel",
        "flask-cors",
        "flask-marshmallow<0.12",
        "marshmallow-sqlalchemy",
        "requests",
        "pytest",
        "numpy",
        "pandas",
        "scipy",
        "matplotlib",
        "pytest-cov",
        "flask-sqlalchemy",
        "scipy",
    ],
    # data_files=[("Lib/site-packages/dtop_energytransf/Databases", ["src/dtop_energytransf/Databases/ET_DB_test.db"])],
    include_package_data=True,
    zip_safe=False,
)
