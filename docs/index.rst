.. _et-home:

Energy Transformation
=====================

Introduction 
------------
The Energy Transformation ET module computes the transformation of energy from 
the power captured to the electrical output of each device in an array of 
Ocean Energy Systems (OES). In the energy conversion chain as illustrated 
in the figure below, the Energy Transformation module is run after Energy Capture (EC) 
and Machine characterization (MC) and before Energy Delivery (ED). 

.. figure:: figures/ET-flow.svg

      Energy flow representation in the ET module


Structure
---------

This module's documentation is divided into four main sections:

- :ref:`et-tutorials` to give step-by-step instructions on using ET for new users. 

- Several :ref:`et-how-to` that show how to achieve specific outcomes using ET. 

- A section on :ref:`background, theory and calculation methods <et-explanation>` that describes how ET works and aims to give confidence in the tools. 

- The :ref:`API reference <et-reference>` section documents the code of modules, classes, API, and GUI. 

.. toctree::
   :hidden:
   :maxdepth: 1

   tutorials/index
   how_to/index
   explanation/index
   reference/index


Functionalities
---------------  

The main purpose of the Energy Transformation module is to design the 
different energy transformation steps: 

•	Hydrodynamic to Mechanic (Mechanical Transformation);

•	Mechanic to Electric (Electrical Transformation) and Control;

•	Electric to Grid (Grid Conditioning).

ET allows the study of several Power Take-Off (PTO) systems both for tidal and wave 
energy converters. The main outputs are costs, efficiency, reliability and bill of materials 
of the three energy transformation steps. The module can either be run in simplified mode at 
each transformation step, which corresponds to complexity 1, or in advanced mode, in case of 
complexity 2 and 3.

The main outputs are costs, efficiency, reliability and bill of materials of 
the three energy transformation steps. The module can either be run in simplified
mode at each transformation step, which corresponds to complexity 1, or in advanced mode, 
in case of complexity 2 and 3.

From a User perspective, there is no substantial difference in the ET module 
computation between complexity 2 and 3: the variation is that at complexity 2, 
the data are considered from a series of existing items in the DTOceanPlus 
Catalogue, while at complexity 3, the User can introduce his own inputs in 
the ET module, updating so the DTOceanPlus Catalogue. 

This allows the User to run the tool considering different complexities 
at each transformation steps. Depending on the complexity levels selected, 
a global complexity level (called ET Cpx) will be assigned considering 
the following practices: 

• ET Cpx 1: At least one of three transformation steps has complexity 1
• ET Cpx 2: all the three transformation steps have complexity 2
• ET Cpx 3: At least one of three transformation steps has complexity 3
• Complexity 1 at one transformation step is not compatible with complexity 3 at any another transformation step. Therefore, if the User selects complexity 1 at least for one transformation step of the three, it is not be possible to run the module at ET Cpx 2 or 3.

Knowing the technology and its main components (either user-defined or default ones), the tool gives a component list with all the attributes needed for the assessment tools (performance, reliability, environmental and cost).



Workflow for using the ET module
--------------------------------
The workflow for using the Energy Transformation module can be summarised as 
1) provide inputs, 2) perform a design, and 3) view the results, as shown in the figure below. 

.. figure:: figures/ET-work-flow.svg

      The workflow for using the ET module

Overview of ET data requirements
--------------------------------
This section summarises the types of input data required to run the Energy Transformation module.
Full details and data specifications are given in how-to-guide on preparing data.

ET module will obtain inputs from 3 different sources:

•	External modules (Site Characterisation, Energy Capture, Machine Characterisation)
•	User inputs from the GUI
•	Component Database (Catalogue)

Therefore, the required inputs to run the module are summarised in the following tables. 

Inputs from External modules
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Depending on the complexity level and the technology, different inputs will be needed:

•	The resource from the Site Characterisation module 
•	The absorbed energy and the device motion from the Energy Capture module
•	The device characteristics from the Machine Characterisation module

In standalone mode, these inputs are uploaded to the ET study through 3 independent external json files. 
For more information about the format of the inputs check the how-to-guide section.


User inputs from the GUI  
~~~~~~~~~~~~~~~~~~~~~~~~
The user will set basic information about the ET study and provide the main inputs of each transformation stage depending on the complexity level and technology

•	**Study**: Name, description and standalone mode (yes/no)
•	**General inputs**: Parallel PTOs and shutdown flag
•	**Mechanical inputs**: Main mechanical transformation parameters as power, type of conversion, transformation ratio, etc.
•	**Electrical inputs**: Main generator parameters like rated power, voltage, frequency, etc.
•	**Grid inputs**: Main power electronics parameters like rated power, DC-link voltage, switching frequency, etc.
•	**Control inputs**: Control type, basic control variables (n sigma and bins).

The amount of parameters needed will depend on the technology and complexity. The following rule will be applied in this section:
 | **Name of the input**: Description. *[w/t]. [1/2/3]*
 
Where *w* means that this input is for wave technology, *t* for tidal technology, and *1* , *2* , *3* the complexity levels at which it is needed

In all the cases there will always appear a value by default where the format of the input parameter is shown. The user can let the value by default as a valid approach.

**General inputs**
^^^^^^^^^^^^^^^^^^

•	**Parallel PTOs**: *[w].[1/2/3]*. Number of power-take offs (PTOs) per device. In tidal, the number of rotors comes from external modules. 
•	**Device Shutdown Flag**: *[w/t].[1/2/3]*. Minimum number of PTOs required to be active for the device to be operational.     


**Mechanical inputs**
^^^^^^^^^^^^^^^^^^^^^^

•     **Mechanical object from catalogue**: *[w/t].[3]* A pop-up showing all the catalogue objects for the mechanical transformation stage. The user must select the object whose parameters wants to use.		
•     **Mechanical transformation type**: *[w/t].[2/3]*. Airturbine, Hydraulic, Linear to rotational motion system or a Gearbox in Tidal
•	**Air turbine type**: *[Airturbine].[w].[2/3]*. Type of air turbine ["Wells" or "Impulse"]. 
•	**Direct-drive option?**: *[Not Linear to rotational].[w/t].[1/2/3]* Check if direct-drive option is desired. In *wave* if *Direct Drive option* is not selected, a gearbox will be used in addition to the mechanical transformation type selected. This option is not available if a "Linear to rotational" mechanical transformation object is selected. In *tidal*, *Direct drive* will imply there is no mechanical transformation stage. No losses, damage, cost or mass will be computed.    
•	**Rated power [kW]**: *[Gearbox].[w/t].[2/3]*. Rated power (mechanical conversion size) in kW. 
•	**Mechanical transformation ratio [-]**: *[Airturbine/Hydraulic].[w].[2/3]*. Relationship between the mechanical rotation obtained from the mechanical transformation and the speed of the generator shaft [dimensionless]. Only available when *direct drive* option is NOT selected. Consider that the output rotational speed will be the input to the electrical generator.
•     **Transmission ratio [-]**: This input will be available in two circumstances
     
      •     *[w/t].[1]*. Relationship between the speed of the device and rotating speed for a simplified mechanical transformation assumption [dimensionless]. In complexity 1, this ratio is automatically calculated as the nominal electrical speed divided by the mean mechanical speed in order to match both speeds and provide a better performance.
     
      •     *[Gearbox/Linear to rotational/not direct drive].[w/t].[2/3]* Gearbox transmission ratio. Relationship between the speed of the device and rotating speed of the generator shaft [dimensionless]. 

•	**Turbine diameter [m]**: *[Airturbine].[w].[2/3]* Air turbine diameter [m] 
•	**Surface water level area [m2]**: *[Airturbine].[w].[2/3]*. internal surface water level area of each Air turbine [m2]. If multiple PTOs, the whole area will be the sum of each turbine area
•	**motor size [m3/rad]**: *[Hydraulic].[w].[2/3]*. Hydraulic motor size [m3/rad]
•	**Cross section piston [m2]**: *[Hydraulic].[w].[2/3]*.Cross section piston [m2]
•	**Flow [%]**: *[Hydraulic].[w].[2/3]*. Flow portion allowed to pass through the hydraulic motor



**Electrical inputs**
^^^^^^^^^^^^^^^^^^^^^^

•     **Electrical object from catalogue**: *[w/t].[3]*. A pop-up showing all the catalogue objects for the electrical transformation stage. The user must select the object whose parameters wants to use.
•	**Electrical transformation type**: *[w/t].[2/3]*. Squirrel Cage Induction Generator (SCIG) or Permanent Magnet Synchronous Generator (PMSG). If Direct-drive option is selected, only PMSG is elegible. Note that this selection is independent from the selection of the object from the catalogue in complexity 3. The type selected will decide which equations (for SCIG or PMSG) will be used in the internal calculations. But the parameters will be the ones selected in the GUI and in the catalogue object.
•     **Rated power [kW]**: *[w/t].[1/2/3]*. Electrical conversion rated power in kW. Maximum power will be incremented by the Torque and Voltage maximum to nominal ratios
•	**Rated rms Voltage [V]**: *[w/t].[2/3]*. Electrical conversion rated voltage in Volts
•	**Nominal frequency [Hz]**: *[w/t].[2/3]*. Electrical conversion nominal frequency in Hz
•	**Generator inductance [Hr]**: *[w/t].[2/3]*. Electrical conversion inductance in Hr
•     **Generator resistance [ohm]**: *[w/t].[2/3]*. Electrical conversion resistance in ohm	
•     **Generator pole pairs**: *[w/t].[2/3]*. Electrical conversion number of pairs of poles. In case of SCIG this variable will be limited to 7. Induction generators with high pole count have big magnetic losses. In addition, an indication of the maximum cos phi of the generator at full load with this pole pair number will be shown as information
•	**Peak to nominal Torque**: *[w/t].[2/3]*. Ratio of peak to nominal Torque to calculate the admisible torque peak (above the nominal. A value of 1 will imply that no peaks are admisible. Otherwise, values >1 must be selected)
•	**Peak to nominal Voltage**: *[w/t].[2/3]*. Ratio of peak to nominal Voltage to calculate the admisible voltage peak (above the nominal. A value of 1 will imply that no peaks are admisible. Otherwise, values >1 must be selected). The same ratio applies to obtain the maximum frequency and speed
•     **Electrical conversion class**: *[w/t].[2/3]*. Generator class for life calculation. The winding insulation class of the generator describes the hability of the winding insulation to handle heat. The most common four insulation classes can be selected. A, B, F, and H. 

      - **Class A**: Maximum Temperature Rise: 60ºC
                 
                        Hot-spot Over Temperature Allowance: 5ºC
                 
                        Maximum Winding Temperature: 105ºC 

      - **Class B**: Maximum Temperature Rise: 80ºC

                        Hot-spot Over Temperature Allowance: 10ºC

                        Maximum Winding Temperature: 130ºC 

      - **Class F**: Maximum Temperature Rise: 105ºC

                        Hot-spot Over Temperature Allowance: 10ºC

                        Maximum Winding Temperature: 155ºC 

      - **Class H**: Maximum Temperature Rise: 125ºC

                        Hot-spot Over Temperature Allowance: 15ºC

                        Maximum Winding Temperature: 180ºC   

      The *maximum winding temperature* is the sum of the *ambient temperature* (40ºC)  and the *allowed temperature rise*. The *allowable temperature rise* is made up of two parts: the *maximum temperature rise* for the insulation class plus a *hot-spot over temperature allowance*                                

With the selected inputs, the **generator nominal speed** will be shown for information. If the obtained outputs are not optimal, check that the mean mechanical speed shown in the outputs is not very different from the generator nominal speed. Otherwise, the generator will not have good performance.	


**Grid conditioning inputs**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

•     **Grid conditioning object from catalogue**: *[w/t].[3]*. A pop-up showing all the catalogue objects for the grid conditioning stage (power converter).	The user must select the object whose parameters wants to use.
•     **Rated power (grid) [kW]**: *[w/t].[1/2/3]*. Power converter rated power in kW
•     **DC Link voltage [V]**: *[w/t].[2/3]*. DC link (bus) voltage
•     **Switching frequency [Hz]**: *[w/t].[2/3]*. Power converter switching frequency
•     **Grid rms voltage [V]**: *[w/t].[2/3]*. Grid rms voltage in volts
•     **Resistance [ohm]**: *[w/t].[2/3]*. Grid connection resistance
•     **Inductance [Hr]**: *[w/t].[2/3]*. Grid connection inductance
•     **Required cosfi**: *[w/t].[2/3]*. Required cosfi in the power injected into grid. Cosfi=1 will imply all the generated power will be active power inyected into grid. Lower cosfi will imply reactive power.
•     **Grid frequency [Hz]**: *[w/t].[2/3]*. Frequency of the grid


**Control inputs**
^^^^^^^^^^^^^^^^^^

•     **Control type**: Control type selection. Depends on mechanical complexity level (1-2: Passive, 3: Passive/User defined)
•     **Control object from catalogue**: *[User defined].[w/t].[3]*. A pop-up showing all the catalogue objects for the control is shown.	The user must select the object whose parameters wants to use.
•     **n sigma**: [w/t].[1/2/3]. Number of velocity standar deviations to consider in the solution
•     **bins**: [w/t].[1/2/3]. Nº of points to represent each sea state. The bigger the higher precision but slower computation

.. tip::
    If the computation is very slow, try reducing the bins number.
