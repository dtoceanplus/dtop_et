.. _Gearbox:

Gearbox
=======

Gearboxes are used to reduce speed and increase torque or reduce torque and increase speed between the PTO and the generator. 
They can be used in wave and tidal devices, although they are the most common mechanical transformation method for horizontal tidal turbines.

**Performance**

Power loss in the gearbox is mostly due to friction, which generates heat. In miniature gearboxes, 
heat is not much of a problem because the power losses and the absolute amounts of power involved are 
relatively small. However, large gearboxes use oil coolers and pumps to compensate for gearbox inefficiency [Faulhaber15]_ . 

Thus, gearbox efficiency depends on friction. This in turn depends on the quality of the gearing, the number 
of tooth engagements (how many times one-wheel drives another) and the load torque (how much "moment" the gearbox has to deliver).

Most manufacturers will specify an intended gearbox operating point. Gearbox efficiencies in a spur gearbox at a 16-mm diameter vary from about 87% at a gear ratio of 6.3:1 to about 40% at a ratio of 10,683:1. 
A basic rule that designers use for spur gears is a 10% loss per engagement. One gear wheel in contact with another is defined as an engagement and the loss in that engagement is approximately 10%.

A general rule is the lighter the load and the higher the ratio, the less likely it is that the gearbox will actually reach the manufacturers' specified efficiency. Light loading and high ratios tend to produce poor gearbox efficiencies. 
But with heavy loading and high ratios, the gearbox will approach its theoretical efficiency.

In this tool, for the moment, a simplified method has been approached using the following efficiency curve for 1 engagement spur gearbox as shown in the next picture.

.. figure:: ../figures/efficiency-gearbox.svg

    Spur gearbox efficiency vs. power load

Transmission relation is calculated as:

  .. math:: i=ω_2/ω_1 

As a design rule, transmission relation of a pair of wheels should remain within the range 1/8 - 8. 

.. note::
    As a future improvement, in this case, w_2 > w_1 so a maximum of i=8 should be accepted. For higher transmission relations, a gear train must be used, more wheels. 
    The number of engagements will be:
      .. math:: n_{eng}=\frac{i}{8}+1
    As a simplified calculation, if more than one engagement is used, the efficiency should reduce in 10% for each extra engagement. 


**Reliability**

The approach to calculate the reliability of the gearbox is similar to the one taken in the rest of mechanical objects.
First, the admissible number of cycles at each environmental state are calculated. The calculous is modified with respect to other components. Attention should be paid to how fatigue strength is introduced in the database. The steps are as follows:

- Obtain the equivalent load assuming tan(alpha)=1

  .. math:: r_{shaft}= 0.5 · shaftD

  .. math:: J = \frac{π}{2} · r_shaft^4

  .. math:: tao= \frac{2000·Mech_T}{2·r_{shaft}·1000}·√2

- Obtain the corresponding fatigue life curves from the DB for the two sections. Note that the fatigue-life curves can have the following format: [[3.3,26.47],[3.3,26.47]]  
  
  .. math:: first.sect = fatigue.life[0]
  
  .. math:: second.sect = fatigue.life[1]       
  
- Manual correction for small torques, which almost doesn't consume any life
  
  .. math:: tao[tao<10^{(second.sect[1]-9)/second.sect[0]}] = 10^{\frac{second.sect[1]-9}{second.sect[0]}}
  
  .. math:: ultimate-stress = \frac{10^{\frac{fatigue.life[0][1] - 3}{fatigue.life[0][0]}}}{0.9}  (MPa)

- Life of the tension ranges within the first section of the fatigue curves: 

  .. math:: sigma_{lim} = 10 ^{(7 - first.sect[1])· (-1 / first.sect[0])}

  .. math:: inds_{first} = abs(tao) > sigma_{lim}

  .. math:: N_{life}[inds_{first}] = 10^{first.sect[1] - first.sect[0]·log10(abs(tao[inds_{first}]))}

- Life of the tension ranges within the second section of the fatigue curves:

  .. math:: inds_{second} = abs(tao) < sigma_{lim}

  .. math:: N_{life}[inds.second] = 10 ^{second.sect[1]- second.sect[0]·log10(abs(tao[inds.second]))}

- Manual assigment of ONE cycle life to those tension ranges higher than the tension range corresponding to 1000 cycles (low cycle region, out of the scope of this work)
  
  .. math:: sigma_{max} = 10^{(3 - first.sect[1]) · (-1 / first_sect[0])}
  
  .. math:: N_life[tao > sigma_{max}] = 1  

- Admissible number of cycles for the stress ranges at the shaft
  
  .. math:: N = N_{life}  

- Now the damage subject to a specific environmental condition is computed during the total time, the same way as the rest of mechanical objects

**Cost assessment**

Based on a query to suppliers, the default cost has been defined as 55.88 € / kW

This can be modified in complexity 3 through the catalogue. (NOTE that in the catalogue €/W is introduced).


**Environmental Impact**

The following equation has been considered to obtain the mass of the gearbox:

  .. math:: Mass=6,9948\cdot P_{nom}+1647,5

This parameters can be changed through the catalogue in complexity 3. Note that P\ :sub:`nom` is in W.


**Catalogue**

The following table shows the paramenters included in the catalogue concenrning the Gearbox:

.. list-table:: 
   :width: 100%
   :header-rows: 1
   :name: _Catalogue of Gearbox

   * - Element
     - Description
   * - Power loads norm
     - Power load normalized to obtain different efficiencies
   * - Eff levels
     - Efficiency per different power loads
   * - Speed levels 
     - Speed levels normalized with the nominal power to obtain different life levels
   * - Life levels
     - Life levels per normalized speed
   * - Cost
     - Cost per nominal power
   * - Mass
     - Mass per nominal power
   * - Failure rate cpx1
     - Failure rate at complexity level 1
