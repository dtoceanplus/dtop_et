.. _Control:

Control Strategies
====================

The control strategy in the Energy Transformation deployment tool consists in defining the probability distribution of the captured power and the corresponding loads, maintaining the same mean captured power and representing the PTO damping through the operational conditions of the mechanical transformation object.

The power distribution is needed to enable the computation of the efficiency of each transformation stage subject to each environmental condition. The loads in the mechanical conversion stage is subsequently used to provide an annual probability density function to RAMS so that the survivability can be computed. The load ranges are used in the ET module to compute the fatigue damage in each transformation stage.

The most simplistic approach is the passive control in which the PTO damping is assumed to be represented by a purely linear system. Some other control strategies are herein represented that produce different probability distributions and consequently modifying the efficiency of the energy transformation stages.

**Passive Control**

The passive control assumes that the PTO force is applied as a purely linear damping force. 

Therefore, the motion probability density function (PDF) is represented by the same PDF of the excitation force. It is here assumed that the wave elevation (and the excitation force since only linear excitation force is considered) follows a Gaussian distribution with a mean zero value and so will be the motion of the wave energy converter.

The produced power with the passive control strategy is computed as:

    .. math:: P_{(t)}=F_{PTO}(t)·v(t)=C_{PTO}·v(t)^2

It is known that the square of a Gaussian distributed variable is distributed as a chi-square (χ^2) distribution of one degree of freedom, and, since the power is proportional to the square of the velocity it will also be χ^2 distributed. 

Next figure shows an example of power distribution function for a specific sea states.

.. figure:: ../figures/power-distribution-control.svg

The force is assumed to be rectified by the mechanical transformation stage which makes the force distributed as the absolute value of the velocity. Therefore, it follows a folded-normal distribution with zero mean velocity. Next figure shows an example of torque distribution function for a specific sea state.

.. figure:: ../figures/torque-control.svg

Also, since the velocity is Gaussian distributed, it is straightforward to assume that the force ranges follow a Rayleigh distribution.Next figure shows an example of probability torque range distribution for a sea state:

.. figure:: ../figures/probability-torque-control.svg






