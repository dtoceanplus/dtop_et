.. _et-explanation:

***************************************************************
Composition of the Business Logic and Background Theory
***************************************************************

This section of the documentation provides some background on energy trasformation 
stages and different configurations, which is the baseline of the bussines logic of ET.
This is followed by an explanation of the calculation methods used for the simple (CPX1) and full (CPX2/CPX3) design process.

The business logic of Energy Trasformation ET module is composed of 8 main objects 
which depend on one another. They are following described:

.. toctree::
    :maxdepth: 2

    Array
    Device
    PTO
    Mechanical_Transformation
    Electrical_Transformation
    Grid_conditioning
    Control
    

Glossary of key terms used
---------------------------
Energy Transformation
        The Energy Transformation module converts the energy available on a fluid into electrical energy adapted to be inyected into the grid. It is composed of a mechanical stage, electrical generator and grid conditioning stage.

PTO Power Take-Off 
        The Power Take-Off subsystem converts the captured wave energy (by the
        hydrodynamic subsystem) into electricity. The PTO systems can
        be based on different principles, of which some of the most common are
        hydraulic PTO, direct drive mechanical PTO, linear generators, air turbine and
        low head water turbine.

Mechanical Transformation
        Stage of the energy transformation where the hydrodynamic to mechanic transformation is computed. The output will be a rotary or linear movement.
        In the ET module, Mechanical Transformation stage designs the mechanical parts and performs the calculation of the PTO mechanical efficiency loads estimation (reliability), weight and cost estimation.

Electrical Transformation
        Stage of the energy transformation where the mechanical movements are converted into electrical power.
        In the ET module, Electrical Transformation stage is the electrical generator. It computes its efficiency, reliability cost and environmental issues.

Grid Conditioning
        Stage of the energy transformation where the output of the electrical generator is adapted to the voltage and current required for the inyection into the grid.
        In the ET module, Grid Conditioning stage designs the components of the power converter, computes its efficiency,  reliability cost and environmental issues.

Control Strategy
        Stage of the energy transformation where captured power and the corresponding loads are controlloed to maximaze the power output.
        The control strategy in the Energy Transformation deployment tool consists in defining the probability distribution of the captured power and the corresponding loads, maintaining the same mean captured power and representing the PTO damping through the operational conditions of the mechanical transformation object.

Array Annual Transformed Energy AATE 
        AATE is a metrics of the total Energy tranformated in a typical year accountig dot he tranformation of the whole array system.
        It is the result of the different transformation stages for each PTO, in each device of the array.

Total PTO Array Cost [kWh/€]
        Total PTO Array Cost represents the power transformed at array level for unit of PTO cost, expressed as [kWh/€].
        It is calculated through the equation:

        .. math:: Cost_{Ratio} = \frac{AATE [kWh]}{PTO_{Cost} [€]}

Total PTO Array Damage [kWh·yr]
        Total PTO Array Damage represents the power transformed at array level for unit of nnual failure rate, expressed as [kWh·yr].
        It is calculated through the equation:
        
        .. math:: Reliability_{Ratio} = \frac{AATE [kWh]}{Failure Rate[\frac{1}{year}]}

Air Turbine
        An air turbine is a turbine driven by airflow.
        In the ET module, two types of Air turbines are accounted: Wells and Impulse.



References
---------------------------

.. [Falcão4]
        A. F. O. Falcão, L. M. C. Gato, «Air Turbines», In Book: Comprehensive Renewable Energy (pp.111-149), dec. 2012.
        doi: 10.1016/B978-0-08-087872-0.00805-2

.. [Falcão5]
        A. F. O. Falcão, L. M. C. Gato, y E. P. A. S. Nunes, «A novel radial self-rectifying air turbine for use in wave energy converters. Part 2. Results from model testing», 
        Renewable Energy, vol. 53, pp. 159-164, may 2013, 
        doi: 10.1016/j.renene.2012.11.018

.. [Falcão6]
        A. F. de O. Falcão y R. J. A. Rodrigues, «Stochastic modelling of OWC wave power plant performance», 
        Applied Ocean Research, vol. 24, n.o 2, pp. 59-71, abr. 2002, 
        doi: 10.1016/S0141-1187(02)00022-6.

.. [DNV-RP-C203] 
        Fatigue Design of Offshore Steel Structures», p. 176, 2011.
        https://www.dnv.com/oilgas/download/dnvgl-rp-c203-fatigue-design-of-offshore-steel-structures.html

.. [Falcão8] 
        A. F. de O. Falcão, «Stochastic modelling in wave power-equipment optimization: maximum energy production versus maximum profit», Ocean Engineering, vol. 31, n.o 11-12, pp. 1407-1421, ago. 2004. 
        doi: 10.1016/j.oceaneng.2004.03.004.

.. [Islam_Rubel9]	
        R. Islam Rubel, Md. Kamal Uddin, Md. Zahidul Islam, y Md. Rokunuzzaman, «Numerical and Experimental Investigation of Aerodynamics Characteristics of NACA 0015 Aerofoil», 
        International Journal of Engineering Technologies IJET, vol. 2, n.o 4, pp. 132-141, abr. 2017, 
        doi: 10.19072/ijet.280499.

.. [Falcão10]
        A. F. O. Falcão, L. M. C. Gato, y E. P. A. S. Nunes, «A novel radial self-rectifying air turbine for use in wave energy converters», 
        Renewable Energy, vol. 50, pp. 289-298, feb. 2013,
        doi: 10.1016/j.renene.2012.06.050.

.. [Ricci11]
        «Ricci et al., 2011, «Control strategies for a wave energy converter converters.pdf»

.. [Ricci12]_
        P. Ricci et al., «Control strategies for a wave energy converter connected to a hydraulic power take-off», 
        IET Renew. Power Gener., vol. 5, n.o 3, p. 234, 2011, 
        doi: 10.1049/iet-rpg.2009.0197.

.. [Wang14]
        Wang y Lu, «Report on Energy Storage systems.pdf»
        
.. [Faulhaber15]
        Fritz Faulhaber, «A second look at Gearbox efficiencies», Machine Design, p. 2, jun. 2002.

.. [skf_friccion19]
        «Fricción». Retrived: Oct. 28, 2019.
        Available at: https://www.skf.com/es/products/seals/industrial-seals/power-transmission-seals/radial-shaft-seals/friction/index.html?WT.oss=p%C3%A9rdidas&WT.z_oss_boost=0&tabname=Todas&WT.z_oss_rank=7.

.. [ABB20]
        «ABB_GPCast Iron Motors EN 02_2009.pdf»

.. [Catalogue21]
        «Catalogue GenPurpMotors_GB_12_2004 RevA.pdf». Accessed: feb. 26, 2020. [Available online] https://library.e.abb.com/public/367c91cdc1dee017c1257b130057111e/Catalogue%20GenPurpMotors_GB_12_2004%20RevA.pdf.        

.. [Tamura22]
        J. Tamura, «Calculation Method of Losses and Efficiency of Wind Generators», 
        Wind Energy Conversion Systems, S. M. Muyeen, Ed. London: Springer London, 2012, pp. 25-51.

.. [Pyrhönen23]
        J. Pyrhönen, T. Jokinen, V. Hrabovcová, y H. Niemelä, «Design of rotating electrical machines», Reprinted. Chichester: Wiley, 2010.

.. [Electrical_Steels24]
        «Selection of electrical steels for magnetic cores.pdf»
        Avaiable at: https://www.brown.edu/Departments/Engineering/Courses/ENGN1931F/mag_cores_dataAKSteel-very%20good.pdf

.. [Wang25]
        D. Wang y K. Lu, «Report on Energy Storage systems», p. 33. 

.. [ABB26]
        «ABB Library». 
        Available at: https://library.abb.com/ (Retrived on: oct. 28, 2019)   

.. [Roberts28]
        D. Roberts y University of Liverpool, «The application of an induction motor thermal model to motor protection and other functions», 1986

.. [EPD29]
        «EPD AMG France.pdf»

.. [EPD30]
        «EPD for AMS.pdf». 
        Available at: https://library.e.abb.com/public/4246fc4b437847fdc1256d63003f681c/EPD%20for%20AMS.pdf. Retrived on: Feb 26, 2020.

.. [Graovac31]
        D. D. Graovac y M. Pürschel, «IGBT Power Losses Calculation Using the Data-Sheet Parameters», p. 17

.. [ABB32]
        «ABB 5SNE 0800M170100 IGBT module datasheet»

.. [Ciappa34]
        Ciappa M., «Selected failure mechanisms of modern power module.pdf», March 2002.
        doi: https://doi.org/10.1016/S0026-2714(02)00042-2

.. [Bertsche35]
        B. Bertsche, Reliability in Automotive and Mechanical Engineering: Determination of Component and System Reliability. 
        Berlin Heidelberg: Springer-Verlag, 2008.

..[Hadziselmovic]
        M. HADŽISELIMOVIĆ, M. MLAKAR, B. ŠTUMBERGER, «Impact of Pole Pair Number on the Efficiency of an Induction Generator for a Mini Hydro Power Plant» 
        PRZEGLĄD ELEKTROTECHNICZNY, ISSN 0033-2097, R. 89 NR 2b/2013                    


TO BE DELETED

Background
==========

This is some background theory to XXXXX. 


Calculation methods
===================

Detail any calculation methods here


Assumptions
===========

List any assumptions here
