.. _Device:

Device
==========

Device objects are built as composed of a number of PTOs in order to provide 
all transformed power, energy, cost and mass at device level. It is the most 
relevant object in the business logic since it takes all the environmental 
conditions passed by its Array object and assesses all PTOs of the device, 
depending on whether the device is a Wave or Tidal device.

**INPUTS**:

Device_ID: This a database generated identifier for the device object.

Device_name: Human readable device identifier.

Device inputs: Generic information at device level. It is a dictionary with all the required fields that makes a device from the ET perspective, described here:

>	dof_PTO: These are the number of device -mostly relative- motions used to generate power, e.g. a Pelamis device with three hinges would have a dof_PTO=2 or an Orbital device with two turbines would have a dof_PTO=2.

>	Parallel_PTOs: This represents the number of PTOs set in parallel to extract energy from each dof_PTO, e.g. in each Pelamis hinge two independent PTO sets (MechT, ElectT and GridC) working in parallel would have a Parallel_PTO=2 or in a MARMOK type device (one chamber dof_PTO=1) with two air turbines would have a Parallel_PTO=2.

>	Shutdown_flag: As already introduced in the hierarchy, this a device level variable, an indicates the minimum number of PTOs to have the device ON out of the total PTOs within it.

>	Cut in / cut out: It is a list with two positions indicating the minimum and maximum Hs/Vc (depending on whether it is a wave or a tidal case) that defines the range within which the device is assumed to generate power.

>	Device Performance Inputs: It is a dictionary with the required fields to specify the captured power by the device that is gathered from Machine Characterization (MC) and Energy Capture modules (EC). It consists of the following fields depending on the device technology:

    -- Wave devices:

        -	Sigma_v: It represents the standard deviation of the velocity of the device in which the PTO acts.

        -   Cpto: It is the PTO damping applied in the corresponding degree of freedom and sea state.

        -	These fields assume that the system is linear (or linearized) and that PTO damping is not changed within the sea state. Therefore, the power and PTO force are computed as specified in equations below:

            .. math:: P = C_{PTO}\cdot \int_{0}^{\propto}|H_{v}(\omega)^{2}\cdot S_{\eta}(\omega)\cdot d\omega = C_{PTO}\cdot \sigma_{v^{2}}

            .. math:: F_{PTO(t)} = C_{PTO}\cdot v_{(t)}
    
    -- Tidal devices:

        -   σ_v: It represents the standard deviation of the current speed for a given mean current speed

        -   Cpto: It is the factor that multiplied by the square of the current speed provides the captured power

        -   C_(t_tidal): Is the inverse of the mean rotational speed of the turbine rotor

        -   V: speed of the device

        -   V_curr : Current speed

            .. math:: P = C_{PTO}\cdot V_{curr(t)}^{2} = V_{curr}\cdot \frac{1}{2}\cdot C_{p}\cdot \rho_{water}\cdot \pi\cdot R^{2}\cdot V_{curr(t)}^{2}

            .. math:: F_{PTO(t)} = C_{t-tidal}\cdot C_{PTO}\cdot V_{curr(t)^{2}}

        It should be noted that the presented equation of power for tidal devices is not exactly proportional to the cubic of the current speed but to the square times the mean current speed. In order to provide theoretical probability density functions of the power, it has been assumed that the rotational speed of the rotor is constant per mean current speed, and therefore the power turns out as the provided expression.

    -- Environmental Conditions: The environmental conditions are provided exactly as provided at array level. However, in the tidal case, each mean speed must be modified accounting for the current speed field as provided by the Energy Capture module

