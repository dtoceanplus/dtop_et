.. _Hydraulic:

Hydraulic Systems
=================

A typical energy conversion system for energy converters, based on hydraulic power take-off (PTO) system, 
has been implemented throughout a linearization of equivalent force, consisting in a double-acting cylinder, 
a hydraulic motor and two or more accumulators. Next picture shows a typical configuration with 2 extra control accumulators [Ricci11]_ .

.. figure:: ../figures/hydraulic-system.svg
   :scale: 20 %

When the device moves upwards, the pressure into the cylinder chamber p\ :sub:`A` \and the pressure into the HP (High Pressure) accumulator are both the same. 
Furthermore, the pressure p\ :sub:`B` is equal to the pressure in the LP (Low Pressure) accumulator. However, when the movement is downwards it is the pressure p\ :sub:`B` 
which takes the HP accumulator pressure value and p\ :sub:`A` the one of the LP accumulators. The valve prevents the fluid of going into the wrong accumulator.
The model has been linearized from the following typical equations that represent a hydraulic PTO [Ricci12]_ .

The motor pressure difference is modelled as used in this equation: 

	.. math:: ∆p_m=p_{HP}-p_{LP}=p_A-p_B-K·(A_p·x ̇ )^2

if the displacement of the sphere is upwards. And with this equation if the movement is downwards:

	.. math:: ∆p_m=p_{HP}-p_{LP}=p_B-p_A-K·(A_p·x ̇ )^2

Where K·(A\ :sub:`p`·x ) :sup:`2` represents the pressure losses through the system. K is a coefficient of loss pressure due to friction along the circuit, A\ :sub:`p` is the cylinder area and x ̇is the heave motion velocity.
To define the pressure value in the accumulators, an ideal gas and a polytropic process is assumed so it could be applied:

	.. math:: P_{gHP}=P_{0_{gHP}}·(\frac{V_{0_{gHP}}}{V_{gHP}})^\gamma

	.. math:: P_{gLP}=P_{0_{gLP}}·(\frac{V_{0_{gLP}}}{V_{gLP}})^\gamma

Where 

	P\ :sub:`0gHP`, P\ :sub:`0gLP`, V\ :sub:`0gHP`, V\ :sub:`0gLP`  are the initial conditions of gas pressure and gas volume inside the accumulators and γ is the adiabatic index.

In order to calculate the pressure value in each accumulator, a previous operation is needed to find the gas volume:

	.. math:: V_{gHP}=V-V_{fHP}

	.. math:: V_{gLP}=V-V_{fLP}

V is the volume of the accumulators and V\ :sub:`fHP`, V\ :sub:`fLP` are the hydraulic fluid values inside each accumulator. 

To figure out these values, it is required to solve the next Ordinary Differential Equations (ODE):

	.. math:: V_{fLP} =Q_{m}-A_{p}·x ̇

	.. math:: V_{fLP}=A_{p}·x ̇-Q_{m}

Where:

•	∆ :sub:`pm` difference of pressure at the accumulator
•	p\ :sub:`A` Pressure piston size A
•	p\ :sub:`B`	Pressure piston size B 
•	K	Losses coefficient
•	A\ :sub:`p`	Piston area 
•	x ̇Vertical speed
•	p\ :sub:`B`	Pressure accumulator B
•	p\ :sub:`A`	Pressure accumulator A
•	V\ :sub:`gHP`	Volume high pressure accumulator
•	V\ :sub:`gLP`	Volume low pressure accumulator
•	Q\ :sub:`m` Flux at the motor
•	γ	 Isentropic parameter


**Cost assessment** 

The cost assessment of the Hydraulic transformation has been defined through a cost function as follows:

    .. math:: Cost = C_{0}·size+C_1

Where *size* is measured in m\ :sup:`3`/rad and can be inserted by the user in the GUI, and C\ :sub:`0` and C\ :sub:`1` are cost values modificable in the catalogue in complexity 3.


**Environmental impact**

The environmental impact for the Hydraulic consists in calculating the total required mass of its main material. It will depend on the *size* of the hydraulic in m\ :sup:`3`/rad.

    .. math:: Mass = mass_{unit}·size
	

**Reliability**

For the reliability estimation, torsion torque stress on the shaft has been assumed through the formulation:

	.. math:: τ_{shaft}=\frac{T·D_{shaft}}{2·J_{shaft}}

Where T is the mechanical torque transmitted to the turbine shaft, D is the hydraulic motor diameter/ generator shaft and J is the polar moment of inertia of the shaft.

The stress on the shaft is used to compute the corresponding strength in terms of number of cycles of the corresponding stress range through the recommended curves provided in [DNV-RP-C203]_. 

All curves are made up of two sections for a number of cycles lower and higher than 10^7 respectively. Each section is defined by the following equation:

	.. math:: log(N)=log(a)-m·log(∆σ)

In order to be conservative, the considered curve for this component has been the W3. The life obtained with the fatigue strength is afterwards used to compute the damage at each sea state or current velocity accounting for the probability attributed to each stress range.


**Efficiency**

The efficiency estimation of the system has been obtained considering the volumetric efficiency and the torque efficiency [Wang14]_ .

Q\ :sub:`m` is the motor flow obtained from:

	.. math: Q_m=α·D_ω·ω·K_q

α refers to the motor fractional displacement, D\ :sub:`ω` is the motor displacement, ω is the motor angular velocity which to be obtained requires to solve another ODE and K\ :sub:`q` is the volumetric efficiency.

The equation to be solved to define the angular velocity is:

	.. math:: ω ̇= \frac{T_m-T_{res}}{I}

T\ :sub:`m` represents the motor torque which formula is presented below, T\ :sub:`res` refers to a resistive torque and I is the motor inertia.

	.. math:: T_m=α·D_ω·∆p_m·K_t

Where K\ :sub:`T` is a torque efficiency.

Both efficiencies (K\ :sub:`q` , K\ :sub:`t`) are obtained as follows from Deliverable D3.1 Energy Storage Systems (SDWED):

	.. math:: K_q=\frac{1}{1+\frac{C_s}{|α|·S}+\frac{∆p_m}{β}+\frac{C_{st}}{|α|·σ}}

	.. math:: K_t=1+\frac{C_s·S}{|α|}-\frac{C_f}{|α|}-C_h·α^2·σ^2

Where:

Friction loss coefficients:
	•	C\ :sub:`f` is the friction coefficient
	•	C\ :sub:`v` is the viscous coefficient
	•	C\ :sub:`h` is the hydrodynamic coefficient 

End Oil properties:
	•	β is the oil bulk modulus
	•	μ is the oil viscosity
	•	ρ is the oil density
    
Leakage coefficients:
	•	C\ :sub:`s` is the laminar flow coefficient
	•	C\ :sub:`st` is the turbulent flow coefficient


