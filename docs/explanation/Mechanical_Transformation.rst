.. _Mechanical_Transformation:

Mechanical Transformation
===========================

For this module, the design of the hydrodynamic to mechanical transformation is completed through calculation of the PTO mechanical efficiency, loads estimation (reliability), weight and cost estimation. 

For this issue it is necessary to consider the following information: 

-	The PTO technology from the User
-	The resource from the Site Characterization module 
-	The absorbed energy and the device motion from the Energy Capture tool
-	The control strategy
-	The component database

Regarding PTO technologies, three different types have been implemented:

.. toctree::
    :maxdepth: 1

    Air_turbine
    Hydraulic-system
    Gearbox

In the case of wave energy devices, a **Linear to mechanical transformation** is also considered. This object will have the same format as the gearbox for Tidal devices.

When air turbine or hydraulic mechanical transformation are selected, a **speed transformation ratio** is considered to adapt the output speed of the mechanical transformation to the input of the electrical generator. 

The selection of **direct drive** option is also available which means no speed adaptation is required. This direct drive option is only possible in Wave devices when air turbine or hydraulic transformations are selected; and will mean no gearbox in Tidal devices.