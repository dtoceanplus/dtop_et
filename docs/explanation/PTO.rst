.. _PTO:

PTO
==========

The business logic can be used in three modes, early stage (cmpx1), middle stage (cmpx2) and late stage (cmpx3) for the different energy transformation steps and the 9 options are compatible one another.

Regarding simplified models, the reliability model has been done in the same way for the three transformation levels: A fatigue curve has been developed considering that if the element works at the 50% of each capacity one year, the device will need a maintenance in one year. And if the device works 10 times at the capacity the device durability will be the 10% of the annual cycles. This refers to the cost of the device (based on internal knowledge and consults with suppliers): 500€, 100€ and 100€ per dimension and nominal power has been considered and referring to the weight 120kg, 10kg and 10kg per kW installed. 

The module has been developed considering three different objects, PTO level, Device Level and Array level. At the main threat the array level object is initialized and respectively inside object device and the PTO. 
