.. _Grid_conditioning:

Grid Conditioning
====================

A back to back two-level power converter (BTB2L) has been considered. The topology is shown in the next figure. 
It consists of two equal Insulated Gate Bipolar Transistor (IGBT) bridges connected through a DC-bus (capacitance). 

.. figure:: ../figures/grid_conditioning.svg

    Back-to-back 2 level power converter topology

**Performance**

In complexity 1, a normalized efficiency curve has been used as shown in the following figure.

.. figure:: ../figures/converter_eff_cmpx1.svg

    Back-to-back 2 level power converter efficiency in complexity 1


In complexities 2 and 3, the efficiency calculation method consists in calculating the losses of each of the converters and subtracting them from the input power. 

The losses considered in this model are the following:

•	IGBT switching losses

•	IGBT conduction losses 

•	Diode switching losses 

•	Diode conduction losses

•	Joule losses in the filter  

Blocking (leakage) losses have been neglected. Total losses will be the sum of the set of previous losses. 

Each converter has 6 IGBTs and 6 diodes, thus, the switching and conduction losses of one single device are calculated and then multiplied by 6 in each converter. 

**Power modules from datasheet**

For the power losses calculation, the most suitable semiconductor modules must be selected depending on the input nominal parameters. 

The following modules have been used and their data extracted for each input power range.

.. list-table:: Power Modules for Efficiency Calculation (from Manufacturers Datasheet)
   :width: 100%
   :header-rows: 1
   :name: _Power Modules for Efficiency Calculation (from Manufacturers Datasheet)

   * - /
     - Vce
     - Vce Design
     - Ic nom
     - Ic Design
     - P max
   * - INFINEON FF150R17KE4
     - 1700 V
     - 1200 V
     - 150 A
     - 75 A
     - 110 kW
   * - INFINEON FF450R17IE4
     - 1700 V
     - 1200 V
     - 450 A
     - 225 A
     - 330 kW
   * - ABB 5SNE 0800M170100
     - 1700 V
     - 1200 V
     - 800 A
     - 400 A
     - 587 kW
   * - ABB 5SNA 1600N170100
     - 1700 V
     - 1200 V
     - 1600 A
     - 800 A
     - 1175 kW

For safety reasons, a conservative approach has been adopted. The maximum current and voltage data defined as *Ic design* and *VCE design* are considered valid and will correspond to the maximum current and the maximum bus voltage value admitted.

For calculating conduction and switching losses, the methodology shown in [Graovac31]_ has been followed. As an example on how to find the needed parameters from a datasheet, the power module ABB 5SNE 0800M170100 [ABB32]_ has been taken.

**IGBT Looses**

**IGBT Conduction Losses**

The IGBT conduction losses are calculated with the following equation:

  .. math:: P_{ci}=\frac{1}{2}(V_{CEO}·\frac{i}{π}+r_{CE}·\frac{i^2}{4})+m·cos∅·(V_{CEO}·\frac{i}{8}+r_{CE}·{i^2})

Where:

- P_ci are the conduction losses of a single IGBT;

- V_CEO is the on-state zero-current collector-emitter voltage of the IGBT;

- r_CE is the collector emitter on-state resistance;

- m is the modulation index of the power converter;

- cos∅ is the power factor;

- i is the current.

These parameters can be read directly from the IGBT Datasheet as shown in the figure below. 

In order to take the parameter variation into account, and thus to have a conservative calculation, V_ceo value read from the diagram has to be scaled with (Vcemax/Vcetyp) value. 
Those exact values can be read from the datasheet tables, but for an engineering calculation a typical safety margin value of (1.1-1.2) can be used. Usually, the highest temperature curve is used.

.. figure:: ../figures/IGBT_conduction_losses.svg
    
    Reading the Vceo and rce from the datasheet diagram [ABB32]_

Usually, the highest temperature curve is used.   


**IGBT Switching losses**

The switching losses in the IGBT are the product of switching energies and the switching frequency (fsw). The switching energies are the sum of switch-on and switch-off energies. 

  .. math:: P_{swi}=f_{sw}·(E_{oni}+E_{offi})

Information about the switch on and switch off energies can be found in the manufacturer datasheet as in next figure.

.. figure:: ../figures/IGBT_switching_losses.svg
    
    Typical switching energies per pulse vs. collector current [ABB32]_

Parameters a, b and c, are the coefficients of the approximated curve. If manufacturer does not provide them, they can be calculated from the curve. 

The switching losses are then calculated from the following equation:

.. math:: P_{swi}=f_{sw}·(\frac{a}{2}+\frac{b·i}{π}+\frac{c·i^2}{4})·\frac{V_{DC}}{V_{nom}} 

Where:

•	P_swi are the switching losses of a single IGBT
•	V_DC is the voltage of the DC link
•	f_sw is the switching frequency
•	V_nom is the nominal voltage 
•	i ̂ is the current

**Diode Losses**

**Diode Conduction losses**

Using the same approximation for the anti-parallel diode, the conduction losses are calculated from the following equation:

 .. math:: P_{cd}=\frac{1}{2}(V_{FO}·\frac{i}{π}+r_{T}·\frac{i^2}{4})-m·cos∅·(V_{FO}·\frac{i}{8}+\frac{1}{3π}·r_{T}·{i^2})

Where:
	•	P_cd are the conduction losses of a single diode
	•	V_FO is the zero-current forward voltage of the IGBT
	•	r_T is the forward resistance
	•	m is the modulation index of the power converter
	•	cos∅ is the power factor

Again, the main parameters are obtained from the manufacturer datasheet as shown in the figure below

.. figure:: ../figures/diode_conduction_losses.svg
    
    Obtaining V_Fo and r_T parameters from the datasheet [ABB32]_

**Diode Switching losses**

The following equation represents the diode switching losses: 

  .. math:: P_{swd}=f_{sw}·(E_{ond}+E_{offd})

But the switch off losses of the diode can be neglected. The turn-on energy in the diode consists mostly of the reverse-recovery energy (Erec) and the curve can again be found on the datasheet.

Again, the same methodology as for the IGBT is used giving the following equation:


  .. math:: P_{swd}=f_{sw}·(\frac{a}{2}+\frac{b·i}{π}+\frac{c·i ̂^2}{4})·V_{DC}/V_{nom} 

Where:

- P_swd are the switching losses of a single diode

- V_DC is the voltage of the DC link

- f_sw is the switching frequency

- V_nom is the nominal voltage 

- i ̂ is the current


.. figure:: ../figures/diode_switching_losses.svg

    Typical reverse recovery characteristics vs. forward current [ABB32]_

In the case of diodes, switch off losses are usually neglected, the switching losses are the product of switch on energy and the switching frequency (fsw).



**Total Losses and Efficiency calculation**

As the current that circulates for each power converter is different, the losses of the generator-side converter (rectifier) and grid-side converter (inverter) are calculated separately. 

In both cases the equation will be:

  .. math:: P_{(r,i)}=6·(P_{ci}+P_{cd}+P_{swi}+P_{swd} )

Where P_r  are the power losses of the rectifier (generator-side converter)

But each of the losses must be calculated with the proper current. The output current from the generator (rectifier) or the current injected to the grid (inverter).

The output current from the generator is obtained from the input power and input generator voltage. For calculating the grid side current, we must consider the power loss in both converters that will make the grid side current be lower. As we don’t know yet the power losses of the inverter, we assume for the current calculation that they are the same as in the rectifier.

  .. math:: P_{grid} ≈ P_{gen}-P_r-P_r

Where P_grid is the output power from the B2B converter, thus, the power injected to grid. Therefore, the current injected to grid that will be used for the losses calculation in the inverter is:

  .. math:: i_{grid}=\frac{√2\cdot P_{grid}}{√3\cdot V_{grid}}

Considering that V_grid  is rms and i_grid  is peak amplitude.

Then, the efficiency of the whole B2B converter will be:

  .. math:: Eff_{B2B}=\frac{P_{gen}-P_r-P_i}{P_{gen}} 


**Reliability**

The lifetime data is usually given in two lifetime curves. One is for a slow cycle period (tcycle=2min) and other one for a fast cycle (tcycle=2s), these curves are valid for all the power converter modules. The lifetime curves represent the critical joins, each of which fail due to different failure mechanisms [Ciappa34]_.

The lifetime of power converter modules is assessed by power cycling experiments, where a given temperature is repetitively applied to a module until it fails. The failure criterium is defined as a 5% increase in VCB.

The temperature cycles are defined as the maximum-minimum values of a cycle and a Weibull distribution (two parameters) is used to define the lifecycle B10 (number of cycles where 10% of the modules fail) [Bertsche35]_. The typical life models used are based on Coffin-Manson law and fatigue due a plastic deformation. 

Three models can describe de lifetime:

- Chip solder joint

- Solder joints of the conductor

- Wire bonds

For this study taking into account that the model has been developed in frequency domain, the supposition of time independent has been adopted, considering the averages ranges obtained from catalogues (cycle period Tcycle=30s ,minimum temperature Tcmin =40º, absolute maximum temperature Tj,max=125º). 

Next figure shows B10 lifetime data for joins conductor, solder points and wire bonds. For this study, the worst scenario has been considered, that is **joins conductor**.

.. figure:: ../figures/lifetime-conditioning.svg

    B10 lifetime data (joins conductor, solder joins and wire bonds)


**Cost assessment**

A cost of 100 k€/MW has been selected for the Converter (default value selected based on queries with developers). The user can modify this value at complexity level 3).


**Environmental Impact**

Regarding environmental issues next Table represents the relation between Kg of material and power of the converter:

.. list-table:: 
   :width: 100%
   :header-rows: 1
   :name: _RELATION OF KG OF MATERIAL AND POWER CONVERTER

   * - Type of Material
     - kg/kW
   * - Aluminium
     - 0.012
   * - Copper
     - 0.137
   * - Plastic
     - 0.060
   * - Steel	
     - 0.326
   * - Iron
     - 0.005
   * - Zinc
     - 0.002
   * - Other	
     - 0.061


The user can modify the unitary mass of each material in complexity 3.

