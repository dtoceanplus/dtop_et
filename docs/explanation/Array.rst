.. _Array:

Array
==========

Array objects have been created in order to store the performance of all devices 
within it, representing in the most realistic way all aggregated information 
at array level needed by the consumer modules, e.g. mean power, energy, cost, mass.


**INPUTS**:

Array_ID: This a database generated identifier for the array object

Array_name: Human readable array identifier

Array inputs: Generic information at array level

Technology: Whether it is wave or tidal

Number of devices

Complexity Level: This variable defines the complexity level of the array, 
that depends on the models used in the lowest level objects (Mechanical Transformation, 
Electrical Transformation and Grid Conditioning). Each transformation stage can be carried out with a ‘Simplified’ object, a ‘Realistic’ object 
or a ‘Realistic-User_Defined’ object. Each stage is related to complexity 1, 2 and 3 respectively. 

Since there are three transformation stages, multiple combinations arise, however, the next is assumed:

>	Complexity Level 1: At least one transformation is carried out with the corresponding ‘Simplified’ object and none of them can be ‘Realistic-User_Defined’.

>	Complexity Level 2: The three transformation stages must be carried out with ‘Realistic’ objects.

>	Complexity Level 3: At least one transformation is carried out with the corresponding ‘Realistic-User_Defined’ object and none of them can be ‘Simplified’.

>	Device Inputs: Information related to the configuration of the devices making up the array, utilised to initialise all devices. It is a dictionary defining one device and the initialisation of the array assumes all devices within the array are exactly equal.

>	PTO_inputs: Information related to the configuration of the PTOs making up the corresponding device, utilised to initialise all PTOs. It is a list of four dictionaries defining each of the three transformation objects as well as the control one.

>	Array_performance_inputs: This variable is a list of dictionaries with as many elements as devices in the array, representing the captured power of each of them along with the corresponding force/speed.

>	Environmental Conditions: It is a dictionary representing the environmental conditions the array is subjected to. It is made up of three fields, different depending on the technology. Each field is a list defining the number of environmental conditions the array is subjected to. The outputs at array level are reset for each set of environmental conditions. Therefore, if the same array is analysed with different sets of environmental conditions, intermediate savings in the database must be performed and will be treated as independent arrays in terms of storage.


**OUTPUTS**:

Bill of Materials: It summarizes the objects used for the transformation, the corresponding catalogue position as well as its unit and total cost.

Hierarchy: It collects all objects within an array setting the relations between components. Such relations establish to which device belongs each PTO and to which array belongs each device. Additionally, the failure rate of the lowest level objects is provided (Mechanical Transformation, Electrical Transformation and Grid Conditioning) based on the corresponding reliability models. Also, the Shutdown flag is collected from the GUI so that LMO can take it into consideration to design the maintenance strategies. The Shutdown flag indicates the minimum number of PTOs to have the device ON (i.e. in operation) out of the total PTOs within it.

Array Cost: It is the total cost of all PTOs and devices within the array. Its value corresponds with the sum of the last column of the Bill of Materials.

Array Mass: This variable is broken down into the main materials of the transformation stages. Since each transformation stage is assigned with a main material, it is broken down into three fields, e.g. copper, aluminium, steel. And the total array mass of each material is provided.

Array Annual Mean Mechanical Power: A list of the mean Mechanical power (after the Mechanical Transformation) with as many positions as environmental conditions the array is subject to. It corresponds with the Power Matrix and the Power curve, for wave and tidal cases respectively, of the mechanical power on the shaft connecting the Mechanical Transformation and Electrical Transformation objects.

Array Annual Mean Electrical Power: A list of the mean Electrical power (after the Electrical Transformation) with as many positions as environmental conditions the array is subject to. It corresponds with the Power Matrix and the Power curve, for wave and tidal cases respectively, of the electrical power on the cables connecting the Electrical Transformation and the Grid Conditioning object.

Array Annual Mean Grid Conditioned active/reactive Power: these are provided as two separate variables for the active and reactive power. Each of the variables is a list of the mean Active/Reactive Grid Conditioned power (after the Grid Conditioning) with as many positions as environmental conditions the array is subject to. It corresponds with the Power Matrix and the Power curve, for wave and tidal cases respectively, and the Grid Conditioned active/reactive power on the cable interfacing the device with the Energy Delivery module.

Array Annual Transformed Mechanical/Electrical/Grid Conditioned (Active) Energy: These are the transformed energy aggregated at array level after each transformation stage. It is computed as described in the following equation:

.. math:: E_{M/E/Garray} = 24·365·\mathrm{\Sigma}_{n=1}^{EC_{number}}{P}_{M/E/G array_{n}}\cdot Occ_{n}


