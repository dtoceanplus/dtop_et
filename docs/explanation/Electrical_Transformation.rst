.. _Electrical_Transformation:

Electrical Transformation
==============================

The objective of this section is the design of the electrical generator. It computes its efficiency, reliability cost and environmental issues.

Two electrical generator types have been designed: 

•	Squirrel Cage Induction Generator (SCIG)

•	Permanent Magnet Synchronous Generator (PMSG)

.. note::
    For the moment, the only difference between both type of generators is in the performance. In the case of SCIG the cos_fi decreases with the pole pairs and this reduces considerably its efficiency. In addition SCIG will also have *rotor losses* and *brushes losses*. On the contrary, PMSG will add *magnet losses*. Future improvements can be made adding new differences 


**Performance**

The generator model is based on the calculation method for squirrel cage induction generator (SCIG). Its efficiency has been determined by calculating different losses:

•	Mechanical losses

•	Iron losses 

•	Winding losses

•	Capacity losses


In the case of the SCIG, *Brushes losses* and *Rotor losses* must be added.

In the case of PMSG *Magnet losses* are also added.


    **Mechanical losses**

Mechanical losses are consequences of bearing friction, and have been adjusted from the friction losses in the bearings [skf_friccion19]_ . 
In turn, friction losses in the bearings are defined as functions of the rotational speed and the shaft diameter as shown in the next picture.

.. figure:: ../figures/mechanical_losses_bearings.svg

    Mechanical losses in bearings [skf_friccion19]_


Mechanical losses are a function of the shaft diameter, which in case of the ABB generators varies with the nominal power, as described in [ABB20]_ , [Catalogue21]_.

.. figure:: ../figures/mechanical_losses_per_shaft_diameter.svg

    Mechanical losses are a function of the shaft diameter [Catalogue21]_

  
    **Iron Losses**

Iron losses have been calculated per unit of mass, as [Tamura22]_:

.. math:: w_f=B^2·(σ_h·(f/100)+σ_e·d^2·(f/100)^2 )    (W/kg)

Where:

	•	B: Average magnetic flux density in the air gap, considered to be of 0.8 [Pyrhönen23]_

	•	σ\ :sub:`h`: Hysteresis loss coefficient

	•	σ\ :sub:`e`: Eddy current loss coefficient

	•	d: Thickness of the iron core steel plate [mm]

	•	f: Frequency [Hz]


The thickness of the iron core has been considered to be the maximum with which it is manufactured,a 0.64mm [Electrical_Steels24]_ and the frequency is the rotational speed times the pairs of poles of the generator.

Regarding the hysteresis and eddy current loss factors, a fitting has been made of the generator presented in [Wang25]_, having obtained 5.01 and 44.8 factors respectively.

The absolute value of the generator losses must be calculated through the mass of the stator and rotor, which have been assumed to be of 60% of the total mass of the generator.

    **Winding losses**

Winding losses have been derived from the resistance of the stator and the RMS current in the stator (rotor Joule losses will be ignored) as:

  .. math:: P_{(loss,wind)}=I_{est}^2·R_{est}

The resistance in stator has been fitted with available values on [ABB26]_:

.. figure:: ../figures/resistance_per_nom_power.svg

    Resistance depending on the nominal power.



The current along the generator stator is computed as:

  .. math:: I_l=\frac{P_{IN-mech}}{(√3 ·V_{LL}·cos⁡φ )}


Where:

  .. math:: P_{IN-mech}=ω_{shaft}·T_{IN}

The electric voltage considered is the nominal value is 400V [ABB20]_.

Therefore, the efficiency is defined for each nominal power of the motor and its working conditions, ω_shaft and P IN-mech.

In relation with the control of the generator, its voltage is raised up to 690V linearly so that it works without losing torque capacity up to rotational speed larger than that of the nominal speed. For speeds higher than the mentioned value the torque is considered to be lowered with the square of the over speed, as shown below for different number of poles.
 

    **Capacity losses**

2 working ranges have been stablished in the generator:

1. rotor speed is below maximum speed. Where

    .. math:: omega_{max} = \frac{2·pi·f_{max}}{pp}

being *pp* pole pair, and 

    .. math:: f_{max} = rel_{V-maxnom} · f_{nom}

where rel\ :sub:`V-maxnom` is the *ratio of peak to nominal Voltage* introduced in the GUI. 

2. rotor speed is below limit speed. Where

    .. math:: omega_{lim} = omega_{max}·rel_{T-maxnom}

where rel\ :sub:`T-maxnom`  is the *ratio of peak to nominal Torque* introduced in the GUI. 


Capacity losses will appear when the Torque is above certain limits:

In working range 1, when Torque > Torque\ :sub:`max` being:

    .. math:: T_{max} = rel_{T-maxnom} · T_{nom}

In this case:

    .. math:: Capacity-losses = \frac{T-T_{max}}{omega}


In working range 2, capacity losses appear when Torque > Torque\ :sub:`adm` being:

    .. math:: T_{adm} = \frac{T_{max}}{(\frac{omega}{omega_{max}})^2}

In this case:

    .. math:: Capacity-losses = \frac{T-T_{adm}}{omega}


    **Losses in the Brushes** Only for SCIG

The following approach has been taken:

    .. math:: P_{brushes}=Res·cosphi·U_{contact}

Where U_contact=1

.. note::
    The cos_phi value in Induction generators depends on the load factor and pole pairs. For the code, curves from [Hadziselmovic]_ have been adapted

    .. figure:: ../figures/cosfi_scig.svg

        Power factor characteristics of induction generators with different numbers of poles in dependency of turbine power
    
    

**Losses in the Rotor** Only for SCIG

The rotor losses have been obtained as follows [Pyrhönen23]_:

    .. math:: Rotorlosses = I_{rot}^2 · Res

where

    .. math:: I_{rot}= I_{est} · cosphi



    **Losses in the Magnets** Only for PMSG

For calculating the losses in the magnets, *NREL  GeneratorSE sizing Tool for Variable-Speed Wind Turbine Generators* has been used.    

    .. math:: Magnetlosess = pFtm · 2 ·pp · b_m · l_s

where:

•	pFtm: 300 (from NREL)
•	pp: pole pair number
•	b_m: 0.7 * taup;  being taup = 70/1000 (m) from NREL  GeneratorSE
•	l_s: 0.0307 * P_nom\ :sup:`0.4611` from NREL


**Reliability**

The lifetime of a generator can be estimated studying the insulation which are estimated by statistical methods [Pyrhönen23]_ . The machines may withstand temporary, often-repeated high temperatures depending on the duration and height of the temperature peak. A similar shortening of the lifetime applies also to the bearings of the motor, in which heat-resistant grease can be employed. 

The generator life is calculated using the mean temperature of the generator given the stator current. Specifically, the total damage on the generator is computed as the sum of the individual damages for each operational sea state.

Generator life can be defined from the curves presented, which depend on the class of the generator and winding temperature:
 
.. figure:: ../figures/winding_temperature_curves.svg

    Total winding temperature curves obtained from [Roberts28]_.


Curves of the figure above have been adjusted through logarithmic functions, having obtained the following results:  N=e\ :sup:`(KT+K0)`

.. list-table:: 
   :width: 100%
   :header-rows: 1
   :name: _Life assessment curves adjustment

   * - Life assess adjust
     - Class A
     - Class B
     - Class F
     - Class H
   * - k
     - -0,069
     - -0,069
     - -0,069
     - -0,069
   * - ko
     - 17,1288228
     - 18,8974785
     - 20,7619675
     - 22,1243926
   * - T max
     - 105
     - 130
     - 155
     - 180



The relation, k, between the RMS current and temperature has been supposed to be the corresponding value to be working at the maximum permissible temperature and the nominal current.

    .. math:: T_{RMS}=K*I_{RMS}^2


**Cost assessment**

The cost assessment of the Air Turbines has been defined through a cost function as :

    .. math:: Cost= (C_0·P_{nom})^{C1}



**Environmental Impact**

The following Tables show a relation of kg of material for the generator [EPD29]_ [EPD30]_.

.. list-table:: 
   :width: 100%
   :header-rows: 1
   :name: _Environmental Impact

   * - Type of Material
     - kg/kW
   * - Electro steel 
     - 0,980
   * - Normal rolled steel
     - 1.492
   * - Special steel 
     - 0.201
   * - Cast iron
     - 0.056
   * - Aluminium
     - 0.007
   * - Copper
     - 0.350
   * - Insulation material
     - 0.501
   * - Wooden boxes and planks 
     - 0.195
   * - Impregnation resin 
     - 0.025
   * - Normal rolled steel
     - 1.492
   * - Paint 	0.002 
     - 0.002 
   * - TOTAL
     - 3,35

.. list-table:: 
   :width: 100%
   :header-rows: 1
   :name: _Relation of kg of material generator Catalogue

   * - Type of Material
     - Kg/kVA
     - Kg/kVA
   * - -
     - AMG 450 
     - AMG 630
   * - Electrical steel
     - 2.97
     - 2.17
   * - Other steel
     - 2.60
     - 1.44
   * - Copper 
     - 0.59
     - 0.24
   * - Insulation material
     - 0.02
     - 0.05
   * - Impregnation resin
     - 0.03
     - 0.03
   * - Paint
     - 0.01
     - 0.01
   * - Solvent 
     - 0.01
     - 0.01

The following approach has been taken for calculating the mass:

    .. math:: Mass= M_0·P_{nom}+M_{1}

Where units of M\ :sub:`0` and M\ :sub:`1` are in kg/W and kg respectively

