.. _Air_turbine:

Air Turbines: Wells and Impulse
========================================

Two types of self-rectifying air turbines have been considered: the traditional Wells turbine [Falcão4]_ and the Biradial Impulse turbine [Falcão5]_. They are individually described below.

.. figure:: ../figures/airturbines.svg

    Wells turbine (left) and Impulse turbine (right) [Falcão4]_ [Falcão5]_

**Performance**

The non-dimensional performance properties of both turbines have been stored in the DTOceanPlus database. The performance is dimensionalized with the diameter of the turbine and the working rotational speed. Following pictures represent the non-dimensional air flow with the pressure for wells and impulse turbines.

.. figure:: ../figures/Wells-hydrodinamical-performance.svg

    Hydrodynamic non-dimensional properties of the wells turbine

.. figure:: ../figures/Impulse-hydrodinamical-performance.svg

    Hydrodynamic non-dimensional properties of the Impulse turbine

.. figure:: ../figures/Wells-mechanical-performance.svg
    
    Mechanical non-dimensional properties of the wells turbine

.. figure:: ../figures/Impulse-mechanical-performance.svg

    Mechanical non-dimensional properties of the Impulse turbine


Turbine dimensional hydrodynamic and mechanical performance is based on equations:

    .. math:: Ψ= \frac{P}{ρ·ω^2·D^2}

    .. math:: Π =\frac{T}{ρ·ω^2·D^5}

    .. math:: Φ =\frac{S·V}{ω·D^3}

where: 

Ψ: pressure head coefficent,
Π: power coefficent,
Φ: flow coefficient.

Reference data have been gathered from [Falcão5]_ and [Falcão6]_, and it is approximated at the object initialization step with a function of the form:

    .. math:: Ψ= K_p·Φ^{α_p} +K_{p0}     
    
    .. math:: Π= K_t·Φ^{α_t} +K_{t0} 

It applies both to the already included air turbines and to the user defined data at late stage.

Since the module works in the frequency domain, it is assumed that the rotational speed ω is kept constant per sea state. 
It enables linearization of the turbine with a linear function of the form *F_pto = C_pto · V* in an iterative manner so that 
it produces the same power of the equivalent non-linear function. Next picture shows non-dimensional pressure head of a wells 
turbine and the equivalent linearized turbine to capture the same amount of energy. The mechanical torque loss over non-dimensional flow 
of 0-0.03 represents the stall in the Wells turbine.

.. figure:: ../figures/pressure-turbine.svg

    Non-dimensional pressure head of a Wells turbine and the equivalent linearized turbine to capture the same amount of energy


**Reliability**

The reliability of the Air Turbines is based on the stress produced by torque on the shaft contained by the turbine itself for simplicity. The stress is assumed to be produced by the torsion torque, and hence it is a shear stress that is assessed.

    .. math :: τ_{shaft}=\frac{T·D_{shaft}}{2·J_{shaft}}  

Where T is the mechanical torque transmitted to the turbine shaft, D is the turbine shaft and J is the polar moment of inertia of the shaft. It is assumed that the shaft diameter corresponds, in the mid complexity mode, to a 10% the turbine rotor diameter.

The stress on the shaft is used to compute the corresponding strength in terms of number of cycles of the corresponding stress range through the recommended curves provided in [DNV-RP-C203]_.

.. figure:: ../figures/turbine-curves.svg

    Fatigue curves recommended for components in Air [DNV-RP-C203]_.

All curves in the picture above are made up of two sections for a number of cycles lower and higher than 10 :sup:`7` respectively. Each section is defined by the following equation:

    .. math:: log(N)=log(a)-m·log(∆σ)

In order to be conservative, the considered curve for this component has been the W3 in the previous figure. The life obtained with the fatigue strength is afterwards used to compute the damage at each sea state or current velocity accounting for the probability attributed to each stress range.


**Cost assessment**

The cost assessment of the Air Turbines has been defined through a cost function as sugested in [Falcão8]_:

    .. math:: C_{mech(D)}=C_{mech0}(\frac{D^3}{D_0^3})^X

Where the reference values for the equation are C :sub:`mech 0` =330000 € and D\ :sub:`0` \=2.3m and the suggested exponent is X=2/3. The user has the option to modify the cost values at complexity level 3.


**Environmental Impact**

The environmental impact for the Air Turbines consists in calculating the total required mass of its main material. 
Therefore, the calculation is based on the total volume of the blades and the shaft, assuming both components are 
made up of the same material.

    **Wells Turbine**

The Wells turbine is made up of a number of blades whose shape corresponds to the NACA_0015 profile, as specified in [Islam_Rubel9]_.

.. figure:: ../figures/naca0015.svg

    Shape of the section of the NACA 0015.

For the mass computation, the following parameters are assumed, according to [Falcão6]_ :

•	Number of blades: 8
•	Inner to outer radius relation: 0.678
•	Cross section area (chord=1): 0.1027[m\ :sup:`2`]
•	Chord to Diameter relation: 0.212
•	Material density (steel): 7850[m\ :sup:`3`]
•	Shaft length=turbine diameter


    **Impulse Turbine**

To estimate the total mass of the Impulse turbine, the same approach has been considered, however, the volume of the blades is related to the turbine diameter through different parameters. 

.. figure:: ../figures/impulse_blades.svg

    Projected Impulse turbine blades [Falcão10]_

The following parameters have been considered for the mass computation of the turbine [5]:

•	Number of blades: 7
•	Blade thickness to rotor radius: 0.015
•	Blade length (s) to rotor radius: 0.45 (assuming s is projected in 2D for simplicity)
•	Blade breadth to rotor radius: 0.22
•	Material density (steel): 7850[m\ :sup:`3`]
•	Shaft length=turbine diameter





