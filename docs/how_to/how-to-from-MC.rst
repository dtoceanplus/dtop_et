.. _et-how-to-from-MC:

How to prepare data from MC module
===================================

Information about the machine technology must be included in the json file. 

The parameters will depend on the complexity level and technology of the device.

Wave
----
All complexities will need the same input parameters. The only difference is the format of the variable "pto_damping". This will be shown in the Examples:

•	"technology": device technology WEC for wave
•	"complexity": complexity level 1, 2 or 3
•	"id": study id for identification (any number will be accepted)
•	"pto_damping": damping of the device. It will be a list of 1 element in complexities 1 and 2. In complexity 3, it is a 5D array with the following structure:  *[n_hs] x [n_tp] x [n_dir] x [n_dof] x [n_dof]* with zeros in all the positions except for the degrees of freedom of the device. In those positions, the value of the pto damping for each degree of freedom will appear. The value is given in [N·s/m]

An internal function will calculate the number of Degrees of Freedom (DOF) from the *pto_damping* input.

Example in complexities 1 and 2:

``{``
  | ``"technology": "WEC",``    
  | ``"complexity": 1,``
  | ``"id": 1,``
  | ``"pto_damping": [565000]``
``}``

Example in complexity 3 considering 6 sea states and 2 degrees of freedom:

``{``
  | ``"technology": "WEC",``    
  | ``"complexity": 3,``
  | ``"id": 1,``
  | ``"pto_damping": [[[[[132315, 0],[0, 132315]]]],[[[[204371, 0],[0, 204371]]]],[[[[258550, 0],``
  | ``[0, 258550]]]],[[[[301698, 0],[0, 301698]]]],[[[[338093, 0],[0, 338093]]]],[[[[368907, 0],[0, 368907]]]]]``
``}``

Tidal
-----
Input parameters will be different in each complexity:

For Tidal, always 1 Degree of Freedom will be considered. There can be more than one rotor (several PTOs).

Complexity 1
^^^^^^^^^^^^

•	"technology": device technology TEC for tidal
•	"complexity": complexity level 1, 2 or 3
•	"id": study id for identification (any number will be accepted)
•	"cp": power coefficient of the device 
•	"number_rotor": number of rotors of the tidal device
•	"characteristic_dimension": main dimension of the device, would be rotor diameter in [m].

In Tidal complexity 1, Tip Speed Ratio is fixed in TSR=5.8.

Example in complexity 1:

``{``
  | ``"technology": "TEC",``    
  | ``"complexity": 1,``
  | ``"id": 1,``
  | ``"cp": 0.37,``
  | ``"number_rotor": 3,``
  | ``"characteristic_dimension": 20,``  
``}``

Complexity 2
^^^^^^^^^^^^

•	"technology": device technology TEC for tidal
•	"complexity": complexity level 1, 2 or 3
•	"id": study id for identification (any number will be accepted)
•	"cp": power coefficient of the device 
•	"number_rotor": number of rotors of the tidal device
•	"characteristic_dimension": main dimension of the device, would be rotor diameter in [m].
•	"ct": is the inverse of the rotational speed of the device 
•	"tip_speed_ratio": power coefficient of the device 
•	"cut_in_velocity": velocity at which the device starts generating 
•	"cut_out_velocity": velocity at which the device stops generating

``{``
  | ``"technology": "TEC",``    
  | ``"complexity": 2,``
  | ``"id": 1,``
  | ``"cp": 0.37,``
  | ``"number_rotor": 3,``
  | ``"characteristic_dimension": 20,``
  | ``"ct": 0.82,``
  | ``"tip_speed_ratio": 5.8,``
  | ``"cut_in_velocity": 1,``
  | ``"cut_out_velocity": 10,``  
``}``

Complexity 3
^^^^^^^^^^^^
At tidal complexity 3 only one new parameter must be included with regard to complexity 2.  However, more than one sea state are considered and
some of the parameters will be arrays with elements per sea state.

•	"cp_ct_velocity": hub velocity reference values for each cp and ct inputs given (m/s)

``{``
  | ``"technology": "TEC",``    
  | ``"complexity": 3,``
  | ``"id": 1,``
  | ``"cp": [0, 0.3, 1, 1, 0.5, 0],``
  | ``"number_rotor": 3,``
  | ``"characteristic_dimension": 20,``
  | ``"ct": [0, 0.3, 0.3, 0.3, 0.5, 0],``
  | ``"tip_speed_ratio": 5.8,``
  | ``"cut_in_velocity": 1,``
  | ``"cut_out_velocity": 10,``
  | ``"cp_ct_velocity": [0, 0.5, 1, 2, 5, 10],``  
``}``

