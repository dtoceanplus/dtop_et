.. _et-how-to:

*************
How-to Guides
*************

Each *how-to guide* listed below contains step-by-step instructions on how to achieve specific outcomes using the Energy Transformation module. 
These guides are intended for users who have previously completed all the :ref:`Energy Transformation tutorials <et-tutorials>` and have a good knowledge of the features and workings of the Energy Transformation module. 
While the tutorials give an introduction to the basic usage of the module, these *how-to guides* tackle slightly more advanced topics.

This guide summarises the data requirements and specifications for running the Energy Transformation module in full complexity standalone mode. The tool requires inputs from Site Characterisation, Machine Characterisation and Energy Capture. 
The needed data will be different depending on the complexity level and the device technology (Wave or Tidal). For all external modules the input data must be uploaded in a json files.

The following pages shows how to create the inputs file for each external modules:

.. toctree::
   :maxdepth: 1

   how-to-from-EC
   how-to-from-MC
   how-to-from-SC
