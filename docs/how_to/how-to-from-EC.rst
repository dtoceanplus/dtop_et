.. _et-how-to-from-EC:

How to prepare data from EC module
===================================

The following parameters are needed from EC and must be included in the json files.
First, in all the cases the **technology** and **complexity level** must be read. 

Wave
----
All complexities will need the same input parameters. The only difference is that in complexity 1, only one sea state is admitted, 
while in complexities 2 and 3 more than one can be introduced. These will be shown in the Examples:

•	"technology": Device technology WEC for wave and TEC for tidal
•	"complexity": complexity level 1, 2 or 3
•	"id": study id for identification (any number will be accepted)
•	"number_devices": number of devices 
•	"captured_power_per_condition": It will be an array of as much elements as number of devices. Each element will consist of:
   •	"capturedPower": captured power of the device measured in kW
   •	"siteConditionID": site condition id of the device or sea state identifier


Example in complexity 1:

``{``
   | ``"technology": "WEC",`` 
   | ``"complexity": 1,``
   | ``"id": 3,``
   | ``"number_devices": 2,``
   | ``"captured_power_per_condition":[``
   |    ``{"capturedPower": [100],`` 
   |    ``"siteConditionID": [0]},``
   |    ``{"capturedPower": [150],`` 
   |    ``"siteConditionID": [0]}]``
   | ``}``

Example in complexities 2 and 3:

``{``
   | ``"technology": "WEC",``    
   | ``"complexity": 1,``
   | ``"id": 3,``
   | ``"number_devices": 2,``
   | ``"captured_power_per_condition":[``
   |    ``{"capturedPower": [0, 0, 0, 100, 0, 100],`` 
   |    ``"siteConditionID": [1, 2, 3, 4, 5, 6]},``
   |    ``{"capturedPower": [0, 0, 0, 100, 0, 100],`` 
   |    ``"siteConditionID": [1, 2, 3, 4, 5, 6]}]``
   | ``}``


Tidal
-----
All complexities will need the same input parameters. The only difference is that in complexites 1 and 2, only one sea state is admitted, 
while in complexity 3 more than one can be introduced. These will be shown in the Examples:

•	"technology": Device technology WEC for wave and TEC for tidal
•	"complexity": complexity level 1, 2 or 3
•	"id": study id for identification (any number will be accepted)
•	"number_devices": number of devices 
•	"number_rotor": number of rotor per device 
•	"captured_power_per_condition": It will be an array of as much elements as the total number of rotors .So it will have **number_devices** * **number_rotor** elements . Each element will consist of:
   • "deviceID": identifier of the device (not necesarilly in order)
   • "rotorID": identifier of the rotor within the device (not necesarilly in order)
   • "capturedPower": captured power of the rotor measured in kW
   • "siteConditionID": site condition id of the device or sea state identifier
•	"array_velocity_field": It will be an array of as much elements as the total number of rotors .So it will have **number_devices** * **number_rotor** elements . Each element will consist of:
   • "rotorID": identifier of the rotor within the device (not necesarilly in order)
   • "hub_velocity": linear speed in the hub of the rotor (m/s)
   • "siteID": site condition id of the device or sea state identifier

Example in complexities 1 and 2:

``{``
   | ``"technology": "WEC",`` 
   | ``"complexity": 1,``
   | ``"id": 3,``
   | ``"number_devices": 2,``
   | ``"number_rotor": 3,``
   | ``"captured_power_per_condition":[``
   |    ``{"deviceID": 1,`` 
   |     ``"rotorID": 2,``  
   |     ``"capturedPower": [551],`` 
   |     ``"siteConditionID": [0]},`` 
   |    ``{"deviceID": 1,`` 
   |     ``"rotorID": 1,``
   |     ``"capturedPower": [550],`` 
   |     ``"siteConditionID": [0]},``
   |    ``{"deviceID": 2,``
   |     ``"rotorID": 5,``         
   |     ``"capturedPower": [554],``  
   |     ``"siteConditionID": [0]},``
   |    ``{"deviceID": 2,``
   |     ``"rotorID": 4,``         
   |     ``"capturedPower": [553],``  
   |     ``"siteConditionID": [0]},``
   |    ``{"deviceID": 1,``
   |     ``"rotorID": 3,`` 
   |     ``"capturedPower": [552],`` 
   |     ``"siteConditionID": [0]},``
   |    ``{"deviceID": 2,``
   |     ``"rotorID": 6,`` 
   |     ``"capturedPower": [555],``  
   |     ``"siteConditionID": [0]}``                                   
   |    ``]``
   | ``"array_velocity_field": [``
   |    ``{"rotorID": 6,`` 
   |    ``"hub_velocity": [2.0],``
   |    ``"siteID": [0]},`` 
   |    ``{"rotorID": 4,`` 
   |    ``"hub_velocity": [2.4],``
   |    ``"siteID": [0]},`` 
   |    ``{"rotorID": 2,`` 
   |    ``"hub_velocity": [2.2],``
   |    ``"siteID": [0]},``
   |    ``{"rotorID": 3,`` 
   |    ``"hub_velocity": [2.3],``
   |    ``"siteID": [0]},``
   |    ``{"rotorID": 1,`` 
   |    ``"hub_velocity": [2.1],``
   |    ``"siteID": [0]},``
   |    ``{"rotorID": 5,`` 
   |    ``"hub_velocity": [2.5],``
   |    ``"siteID": [0]}``        
   |    ``]``
   | ``}``

Example in complexity 3:

``{``
   | ``"technology": "WEC",`` 
   | ``"complexity": 1,``
   | ``"id": 3,``
   | ``"number_devices": 2,``
   | ``"number_rotor": 3,``
   | ``"captured_power_per_condition":[``
   |    ``{"deviceID": 1,`` 
   |     ``"rotorID": 2,``  
   |     ``"capturedPower": [3.81, 12.86, 59.51, 243.77, 476.11,1046],`` 
   |     ``"siteConditionID": [0, 1, 2, 3, 4, 5]},`` 
   |    ``{"deviceID": 1,`` 
   |     ``"rotorID": 1,``
   |     ``"capturedPower": [3.81, 12.86, 59.51, 243.77, 476.11,1046],`` 
   |     ``"siteConditionID": [0, 1, 2, 3, 4, 5]},``
   |    ``{"deviceID": 2,``
   |     ``"rotorID": 5,``         
   |     ``"capturedPower": [3.81, 12.86, 59.51, 243.77, 476.11,1046],``  
   |     ``"siteConditionID": [0, 1, 2, 3, 4, 5]},``
   |    ``{"deviceID": 2,``
   |     ``"rotorID": 4,``         
   |     ``"capturedPower": [3.81, 12.86, 59.51, 243.77, 476.11,1046],``  
   |     ``"siteConditionID": [0, 1, 2, 3, 4, 5]},``
   |    ``{"deviceID": 1,``
   |     ``"rotorID": 3,`` 
   |     ``"capturedPower": [3.81, 12.86, 59.51, 243.77, 476.11,1046],`` 
   |     ``"siteConditionID": [0, 1, 2, 3, 4, 5]},``
   |    ``{"deviceID": 2,``
   |     ``"rotorID": 6,`` 
   |     ``"capturedPower": [3.81, 12.86, 59.51, 243.77, 476.11,1046],``  
   |     ``"siteConditionID": [0, 1, 2, 3, 4, 5]}``                                   
   |    ``]``
   | ``"array_velocity_field": [``
   |    ``{"rotorID": 6,`` 
   |    ``"hub_velocity": [0.4, 0.6, 1, 1.6, 2, 2.6],``
   |    ``"siteID": [0, 1, 2, 3, 4, 5]},`` 
   |    ``{"rotorID": 4,`` 
   |    ``"hub_velocity": [0.4, 0.6, 1, 1.6, 2, 2.6],``
   |    ``"siteID": [0, 1, 2, 3, 4, 5]},`` 
   |    ``{"rotorID": 2,`` 
   |    ``"hub_velocity": [0.4, 0.6, 1, 1.6, 2, 2.6],``
   |    ``"siteID": [0, 1, 2, 3, 4, 5]},``
   |    ``{"rotorID": 3,`` 
   |    ``"hub_velocity": [0.4, 0.6, 1, 1.6, 2, 2.6],``
   |    ``"siteID": [0, 1, 2, 3, 4, 5]},``
   |    ``{"rotorID": 1,`` 
   |    ``"hub_velocity": [0.4, 0.6, 1, 1.6, 2, 2.6],``
   |    ``"siteID": [0, 1, 2, 3, 4, 5]},``
   |    ``{"rotorID": 5,`` 
   |    ``"hub_velocity": [0.4, 0.6, 1, 1.6, 2, 2.6],``
   |    ``"siteID": [0, 1, 2, 3, 4, 5]}``        
   |    ``]``
   | ``}``