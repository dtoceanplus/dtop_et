.. _et-how-to-from-SC:

How to prepare data from SC module
===================================

The following parameters are needed from SC and must be included in the json files.
The **technology** has been previously read from MC, and the **complexity level** from EC

Wave
----
Complexity 1
^^^^^^^^^^^^
•	"hs": significant wave height (m)

In Wave complexity 1, **Site_id** and **Occurrence** will be set to 1.

Example:

``"hs": {"mean": 0.29880000000000007}``

Complexity 2 and 3
^^^^^^^^^^^^^^^^^^
•	"bins1": variable from SC with values for **significant wave height (m)**
•	"bins2": variable from SC with values for **peak wave period (s)**
•	"pdf": probability density function. For calculating the occurrence of each sea state
•	"id": Sea state identifier 

Example:

``{``
    | ``"bins1": [``
    |    ``"0.0-0.5",``
    |    ``"0.0-0.5",``
    |    ``"0.0-0.5",``
    |    ``"0.5-1.0",``
    |    ``"0.5-1.0",``
    |    ``"0.5-1.0"],``
    | ``"bins2": [``
    |    ``"11.0-12.0",``
    |    ``"11.0-12.0",``
    |    ``"11.0-12.0",``
    |    ``"12.0-13.0",``
    |    ``"12.0-13.0",``
    |    ``"12.0-13.0"],``        
    | ``"id": [1, 2, 3, 4, 5, 6],``
    | ``"pdf": [0, 0, 0, 2, 0, 3]``    
``}``

Tidal
-----
Complexity 1 and 2
^^^^^^^^^^^^^^^^^^
•	No input parameters are needed, but a json file must be uploaded.

**Tz**, **site_id**, and **Occurrence** are set to 1.

Example:

``{``
    | ``"technology": "TEC",``
    | ``"complexity": 1``    
``}``

Complexity  3
^^^^^^^^^^^^^
•	"p": Occurrence of each sea state. The sum of the values is 1
•	"id": Sea state identifier 

**Tz** is set to 1.

Example:

``{``
    | ``"p": [0.1, 0.2, 0.3, 0.2, 0.1, 0.1],``
    | ``"id": [1, 2, 3, 4, 5, 6]``
``}``
