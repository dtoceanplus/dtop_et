.. _create-an-energy-transformation-study: 

Create an Energy Transformation Study
============================================

Once logged into the server, 
the next step is to create a new study within
the Energy Transformation module. 

.. tip::
   Since multiple users across multiple  organisations may be 
   simultaneously accessing the module on the server,
   **please add your organisation’s name in the name of the study you
   create**. This is to ensure that all users work on independent studies
   and are not editing the same study at the same time. 


1. Click on ``Create an Energy Transformation study``.

2. Fill in an appropriate name and description to identify your study.

3.	In the **standalone mode**, the json files of the external modules must be uploaded. Be sure to upload them in the proposed order (EC, MC and SC). MC upload will only be enabled after uploading the EC file. 
   For help on how to upload external modules input json files consult the :ref:`et-how-to`.
   In the **integrated mode**, the external modules data is uploaded directly to the ET module.

4.	Once the necessary data has been completed, the ``Create`` button will be enabled. Click ``Create`` to save these inputs and return to the list of studies.

5.	From the list of studies, click ``Edit`` to update the description of the study or upload a new external module input file, or ``Delete`` to permanently remove a study.

6.	In case of updating the external modules, again, upload the files in the proposed order (EC, MC and SC). If not all the files are to be uploaded, ensure that if EC file is updated, MC is uploaded again even if the file is the same. This is because the tool updates the internal variables during the MC upload. 

After the creation of the study with external modules inputs, the status variable will be 40 %.

