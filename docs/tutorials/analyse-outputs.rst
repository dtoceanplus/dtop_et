.. _analyse-outputs:

Analyse Outputs
=================

Once the study has been run, the results will be available at the ``Outputs`` window.

The outputs are categorized by array, device and PTO. Outputs can be seen in the graphical interface or be downloaded.

Note that the tool is very sensitive, a bad design will lead to a misperformance of the system. If the results are not satisfactory, check if the external modules inputs are the desired and try with other input values.