.. _et-tutorials:

*********
Tutorials
*********

Below is a set of tutorials that guide the user through each of the main functionalities of the Energy Transformation tool.
They are intended for those who are new to the Energy Transformation tool.
It is recommended to follow the tutorials in the suggested order listed below, as certain tutorials are dependent on others.

The use of the Energy Transformation tool is done following 3 steps: 

.. toctree::
   :maxdepth: 1

   create-an-energy-transformation-study
   insert-user-inputs
   analyse-outputs


   
