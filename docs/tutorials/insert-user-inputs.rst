.. _insert-user-inputs:

Insert User Inputs
==============================

After creating a study, the User can continue creating other studies or proceed to insert the inputs to an existing study. 
 
.. tip::
   Since multiple users across multiple  organisations may be 
   simultaneously accessing the module on the server,
   **please add your organisation’s name in the name of the study you
   create**. This is to ensure that all users work on independent studies
   and are not editing the same study at the same time.

1.	From the list of available studies select the study to insert the inputs. A new window ``Analysis Mode Inputs`` is shown with the main study details: Name, description, technology and standalone mode or not.

2.	Five categories of input data appear: General, Mechanical Transformation, Elecrical Transformation, Grid Conditioning, Control. The different inputs are displayed when clicking the name of the category.

3.	Click sequentially the name of each category and insert the necessary inputs in the drop-down menus. Note that the tool will always show a default variable that the User can modify. In mechanical, electrical and grid conditioning categories, select the complexity level. The tool will then display the needed User inputs for the selected complexity level taking into account the technology.

4.	Once all the inputs have been introduced, the buttons ``Save`` and ``Run`` are enabled.
   a.	Click ``Save`` to save the introduced inputs in the database of the study
   b.	Click ``Run`` to save the introduced inputs in the database of the study and run the study. The tool will automatically open the ``Outputs`` window

The Status variable will be 70% when the User inputs are saved, and 100% when the study is run and the outputs generated.
