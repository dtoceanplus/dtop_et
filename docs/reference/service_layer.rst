.. _et-service-layer:

Service Layer
=============

.. toctree::

   service/core
   service/inputs
   service/outputs
   service/studies
   service/integration
   service/representation

Submodules
----------

Errors
^^^^^^

.. automodule:: dtop_energytransf.service.api.errors
   :members:
   :undoc-members:
   :show-inheritance:

Utilities
^^^^^^^^^

.. automodule:: dtop_energytransf.service.api.utils
   :members:
   :undoc-members:
   :show-inheritance:
