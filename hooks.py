import os
import json
import requests
import sys

import traceback
from functools import partial

import dredd_hooks as hooks

from dredd_utils import *

import pickle


@hooks.before("/energy_transf > List the created ET designs > 200 > application/json")
@hooks.before("/energy_transf > List the created ET designs > 404")
@hooks.before("/energy_transf > Add an Energy Transformation (ET) design > 400")
@hooks.before("/energy_transf > Add an Energy Transformation (ET) design > 201")
@hooks.before("/energy_transf/{etId} > Return the Energy Transformation (ET) design > 200 > application/json")
@hooks.before("/energy_transf/{etId} > Return the Energy Transformation (ET) design > 404")
@hooks.before(
    "/energy_transf/{etId} > Return the Energy Transformation (ET) design > 500"
)
@hooks.before("/energy_transf/{etId} > Delete the Energy Transformation Project > 200")
@hooks.before("/energy_transf/{etId} > Delete the Energy Transformation Project > 404")
@hooks.before("/energy_transf/{etId} > Update the Energy Transformation project > 201")
@hooks.before("/energy_transf/{etId} > Update the Energy Transformation project > 400")
@hooks.before("/energy_transf/{etId}/array > Return the Array Outputs > 200 > application/json")
@hooks.before("/energy_transf/{etId}/array > Return the Array Outputs > 404")
@hooks.before("/energy_transf/{etId}/array > Return the Array Outputs > 500")
@hooks.before("/energy_transf/{etId}/devices > Return the Devices Outputs > 200 > application/json")
@hooks.before("/energy_transf/{etId}/devices > Return the Devices Outputs > 404")
@hooks.before("/energy_transf/{etId}/devices > Return the Devices Outputs > 500")
@hooks.before("/energy_transf/{etId}/devices/{deviceId} > Return one Device Outputs > 200 > application/json")
@hooks.before("/energy_transf/{etId}/devices/{deviceId} > Return one Device Outputs > 404")
@hooks.before(
    "/energy_transf/{etId}/devices/{deviceId} > Return one Device Outputs > 500"
)
@hooks.before("/energy_transf/{etId}/devices/{deviceId}/ptos > Return the PTOs Outputs > 200 > application/json")
@hooks.before("/energy_transf/{etId}/devices/{deviceId}/ptos > Return the PTOs Outputs > 404")
@hooks.before(
    "/energy_transf/{etId}/devices/{deviceId}/ptos > Return the PTOs Outputs > 500"
)
@hooks.before("/energy_transf/{etId}/devices/{deviceId}/ptos/{ptoId} > Return one PTO Outputs > 200 > application/json")
@hooks.before("/energy_transf/{etId}/devices/{deviceId}/ptos/{ptoId} > Return one PTO Outputs > 404")
@hooks.before(
    "/energy_transf/{etId}/devices/{deviceId}/ptos/{ptoId} > Return one PTO Outputs > 500"
)
@hooks.before("/representation/{etId} > Export data representation > 200 > application/json")
@hooks.before("/representation/{etId} > Export data representation > 404")
@hooks.before("/representation/{etId} > Export data representation > 500")
def _skip(transaction):
    transaction["skip"] = True


# @hooks.before_each
# @if_not_skipped
# def before_all(transaction):
#     print("\n" + transaction["name"])
#     remove_all_projects(transaction)


# @hooks.before("/energy_transf > Add an Energy Transformation (ET) design > 201")
# @if_not_skipped
# def post_project(transaction):
#     data = json.loads(transaction["request"]["body"])
#     data.pop("etId", None)
#     transaction["request"]["body"] = json.dumps(data)
#     print(transaction["request"]["body"])


# @hooks.before("/energy_transf > Add an Energy Transformation (ET) design > 400")
# @if_not_skipped
# def post_assessment_400(transaction):
#     """
#     Remove one of required parameters when creating a project.
#     """
#     data = json.loads(transaction["request"]["body"])
#     data.pop("Technology")
#     transaction["request"]["body"] = json.dumps(data)


# @hooks.before(
#     "/energy_transf/{etId} > Return the Energy Transformation (ET) design > 200 > application/json"
# )
# @hooks.before(
#     "/energy_transf/{etId}/array > Return the Array Outputs > 200 > application/json"
# )
# @hooks.before(
#     "/energy_transf/{etId}/devices > Return the Devices Outputs > 200 > application/json"
# )
# @hooks.before(
#     "/energy_transf/{etId}/devices/{deviceId} > Return one Device Outputs > 200 > application/json"
# )
# @hooks.before(
#     "/energy_transf/{etId}/devices/{deviceId}/ptos > Return the PTOs Outputs > 200 > application/json"
# )
# @hooks.before(
#     "/energy_transf/{etId}/devices/{deviceId}/ptos/{ptoId} > Return one PTO Outputs > 200 > application/json"
# )
# @if_not_skipped
# def get_project_id(transaction):
#     """
#     Add a correct project path
#     """
#     project = post_a_project(transaction)
#     etId = transaction["_et_id"]
#     transaction["id"] = transaction["id"].replace("123", str(etId))
#     transaction["request"]["uri"] = transaction["request"]["uri"].replace(
#         "123", str(etId)
#     )
#     transaction["fullPath"] = transaction["fullPath"].replace("123", str(etId))


# @hooks.before(
#     "/energy_transf/{etId} > Return the Energy Transformation (ET) design > 404"
# )
# @hooks.before("/energy_transf/{etId}/array > Return the Array Outputs > 404")
# @hooks.before("/energy_transf/{etId}/devices > Return the Devices Outputs > 404")
# @hooks.before(
#     "/energy_transf/{etId}/devices/{deviceId} > Return one Device Outputs > 404"
# )
# @hooks.before(
#     "/energy_transf/{etId}/devices/{deviceId}/ptos > Return the PTOs Outputs > 404"
# )
# @hooks.before(
#     "/energy_transf/{etId}/devices/{deviceId}/ptos/{ptoId} > Return one PTO Outputs > 404"
# )
# @if_not_skipped
# def get_project_id_404(transaction):
#     """
#     Add an incorrect project path
#     """
#     transaction["request"]["uri"] = transaction["request"]["uri"].replace("123", "-1")
#     transaction["fullPath"] = transaction["fullPath"].replace("123", "-1")


# @hooks.before("/energy_transf/{etId} > Delete the Energy Transformation Project > 200")
# @if_not_skipped
# def delete_project_id(transaction):
#     """
#     Add an incorrect project path
#     """
#     project = post_a_project(transaction)
#     etId = transaction["_et_id"]
#     # transaction["id"] = transaction["id"].replace("123", str(speyId))
#     transaction["request"]["uri"] = transaction["request"]["uri"].replace(
#         "123", str(etId)
#     )
#     transaction["fullPath"] = transaction["fullPath"].replace("123", str(etId))


# @hooks.before("/energy_transf/{etId} > Delete the Energy Transformation Project > 404")
# @if_not_skipped
# def delete_project_400(transaction):
#     """
#     Add an incorrect project path
#     """
#     project = post_a_project(transaction)
#     # transaction["id"] = transaction["id"].replace("123", str(speyId))
#     transaction["request"]["uri"] = transaction["request"]["uri"].replace("123", "-1")
#     transaction["fullPath"] = transaction["fullPath"].replace("123", "-1")


# @hooks.before("/energy_transf/{etId} > Update the Energy Transformation project > 201")
# @if_not_skipped
# def put_project_id(transaction):
#     """
#     Update a project by ID
#     """
#     project = post_a_project(transaction)
#     etId = transaction["_et_id"]
#     # transaction["id"] = transaction["id"].replace("123", str(speyId))
#     transaction["request"]["uri"] = transaction["request"]["uri"].replace(
#         "123", str(etId)
#     )
#     transaction["fullPath"] = transaction["fullPath"].replace("123", str(etId))

#     requested = json.loads(transaction["request"]["body"])
#     requested.pop("etId", None)

#     transaction["request"]["body"] = json.dumps(requested)


# @hooks.before("/energy_transf/{etId} > Update the Energy Transformation project > 400")
# @if_not_skipped
# def put_assessment_400(transaction):
#     """
#     Update a project by ID
#     """
#     project = post_a_project(transaction)
#     # transaction["id"] = transaction["id"].replace("123", str(speyId))
#     transaction["request"]["uri"] = transaction["request"]["uri"].replace("123", "-1")
#     transaction["fullPath"] = transaction["fullPath"].replace("123", "-1")
