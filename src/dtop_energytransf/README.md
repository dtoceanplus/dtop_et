This is `dtop_example` package folder.

Python packages should contain `__init__.py` file.
Also it should not contain `-` in name. `_` is recommended.

Packages needed for the Energy Transformation Module:

"See setup.py"
