This folder contains images, icons, CSS and others static resources.

It is better to split all kinds of resources to subdirectories.

Documentation:
- [Static Files](http://flask.pocoo.org/docs/1.0/quickstart/#static-files)