class MyCustomReporter {
    constructor(globalConfig, options) {
        this._globalConfig = globalConfig;
        this._options = options;
    }
    onRunComplete() {
        // eslint-disable-next-line no-console
        console.log('Line coverage: ' + `${(this._globalConfig.coverageThreshold.global.lines).toFixed(0)}%`);
    }
}
module.exports = MyCustomReporter;