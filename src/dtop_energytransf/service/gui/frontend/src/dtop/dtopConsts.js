import { getDomain, getProtocol,getAuth } from "@/dtop/dtopMethods"

export const DTOP_DOMAIN = getDomain();
export const DTOP_PROTOCOL = getProtocol();
export const DTOP_AUTH = getAuth();
