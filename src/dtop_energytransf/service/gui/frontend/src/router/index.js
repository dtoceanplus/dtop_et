import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

export const constantRoutes = [
  // {
  //   path: '/login',
  //   component: () => import('@/views/login/index'),
  //   hidden: true
  // },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: 'Dashboard', icon: 'dashboard' }
    }]
  },
  // {
  //   path: '/assesments',
  //   component: Layout,
  //   redirect: '/assesments/index',
  //   name: 'Assesments',
  //   meta: { title: 'Assesments', icon: 'nested' },
  //   children: [
  //     {
  //       path: 'index',
  //       name: 'Index',
  //       component: () => import('@/views/assesments/index'),
  //       meta: { title: 'Index' },
  //       hidden: true
  //     },
  //     {
  //       path: ':assesment_id',
  //       name: 'View',
  //       component: () => import('@/views/assesments/view/index'),
  //       meta: { title: 'View' },
  //       hidden: true
  //     }
  //   ]
  // },
  {
    path: '/energy_transf',
    component: Layout,
    redirect: '/',
    children: [{
      path: 'index',
      name: 'EtStudies',
      component: () => import('@/views/et_studies/index'),
      meta: { title: 'ET Studies', icon: 'table' },
      hidden: true
    }]
  },
  // {
  //   path: '/projet',
  //   component: Layout,
  //   redirect: '/project/table',
  //   name: 'Project',
  //   meta: { title: 'Project', icon: 'tree' },
  //   children: [
  //     {
  //       path: 'general inputs',
  //       name: 'General Inputs',
  //       component: () => import('@/views/general_inputs/index'),
  //       meta: { title: 'General Inputs', icon: 'table' }
  //     },
  //     // {
  //     //   path: 'array outputs',
  //     //   name: 'Array Outputs',
  //     //   component: () => import('@/views/array_outputs/index'),
  //     //   meta: { title: 'Array Outputs', icon: 'table' }
  //     // },
  //     // {
  //     //   path: 'devive outputs',
  //     //   name: 'Device Outputs',
  //     //   component: () => import('@/views/device_outputs/index'),
  //     //   meta: { title: 'Device Outputs', icon: 'table' }
  //     // }
  //   ]
  // },
  {
    path: '/analysis-mode',
    component: Layout,
    redirect: '/',
    children: [
      {
        path: 'index',
        name: 'AnalysisMode',
        component: () => import('@/views/analysis_mode/index'),
        meta: { title: 'Analysis Mode', icon: 'form' }
      },
      {
        path: ':study_id/other_module_inputs',
        name: 'OtherModuleInputs',
        component: () => import('@/views/analysis_mode/other_modules/index'),
        meta: { title: 'Other Modules Inputs' },
        hidden: true
      },
      {
        path: ':study_id/inputs',
        name: 'AnalysisInputs',
        component: () => import('@/views/analysis_mode/inputs/index'),
        meta: { title: 'Energy Transformation Inputs' },
        hidden: true
      },
      {
        path: ':study_id/outputs',
        name: 'AnalysisOutputs',
        component: () => import('@/views/analysis_mode/outputs/index'),
        meta: { title: 'Analysis Outputs' },
        hidden: true
      }
    ]
  },
  // {
  //   path: '/pto',
  //   component: Layout,
  //   redirect: '/pto/table',
  //   name: 'PTO',
  //   meta: { title: 'Power Take-Off', icon: 'example' },
  //   children: [
  //     {
  //       path: 'inputs',
  //       name: 'Inputs',
  //       component: () => import('@/views/inputs_PTO/index'),
  //       meta: { title: 'Inputs', icon: 'exit-fullscreen' }
  //     },
  //     {
  //       path: 'outputs',
  //       name: 'Outputs',
  //       component: () => import('@/views/outputs_PTO/index'),
  //       meta: { title: 'Outputs', icon: 'fullscreen' }
  //     }
  //   ]
  // },
  // {
  //   path: '/generator',
  //   component: Layout,
  //   redirect: '/generator/table',
  //   name: 'Generator',
  //   meta: { title: 'Generator', icon: 'example' },
  //   children: [
  //     {
  //       path: 'inputs',
  //       name: 'Inputs',
  //       component: () => import('@/views/inputs_generator/index'),
  //       meta: { title: 'Inputs', icon: 'exit-fullscreen' }
  //     },
  //     {
  //       path: 'outputs',
  //       name: 'Outputs',
  //       component: () => import('@/views/outputs_generator/index'),
  //       meta: { title: 'Outputs', icon: 'fullscreen' }
  //     }
  //   ]
  // },
  // {
  //   path: '/power_electronics',
  //   component: Layout,
  //   redirect: '/power_electronics/table',
  //   name: 'Power_Electronics',
  //   meta: { title: 'Power Electronics', icon: 'example' },
  //   children: [
  //     {
  //       path: 'inputs',
  //       name: 'Inputs',
  //       component: () => import('@/views/inputs_PE/index'),
  //       meta: { title: 'Inputs', icon: 'exit-fullscreen' }
  //     },
  //     {
  //       path: 'outputs',
  //       name: 'Outputs',
  //       component: () => import('@/views/outputs_PE/index'),
  //       meta: { title: 'Outputs', icon: 'fullscreen' }
  //     }
  //   ]
  // },
  // {
  //   path: '/outputs',
  //   component: Layout,
  //   redirect: '/outputs',
  //   name: 'Outputs',
  //   meta: { title: 'Outputs', icon: 'example' },
  //   children: [
  //     {
  //       path: 'array',
  //       name: 'Array',
  //       component: () => import('@/views/ET_outputs/array/index'),
  //       meta: { title: 'Array', icon: 'fullscreen' }
  //     },
  //     {
  //       path: 'devices',
  //       name: 'Devices',
  //       component: () => import('@/views/ET_outputs/devices/index'),
  //       meta: { title: 'Devices', icon: 'fullscreen' }
  //     },
  //     {
  //       path: 'pto',
  //       name: 'PTO',
  //       component: () => import('@/views/ET_outputs/ptos/index'),
  //       meta: { title: 'PTO', icon: 'fullscreen' }
  //     }
  //   ]
  // },
  {
    path: 'external-link',
    component: Layout,
    children: [
      {
        path: 'https://dtoceanplus.eu',
        meta: { title: 'DTOcean+', icon: 'link' }
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
