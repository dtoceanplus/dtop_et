export default {
  get() {
      return new Promise((resolve) => {
        if (this.__mockError)
            reject(new Error())
        resolve(this.__result)
    })
  },
  delete() {
      return new Promise((resolve) => {
          if (this.__mockError)
              reject(new Error())
          resolve(true)
      })
  },
  patch() {
      return Promise.resolve(this.__result)
  },
  post() {
      return new Promise((resolve) => {
          if (this.__mockError)
              reject(new Error())
          resolve({ status: 200, data: this.__result.data })
      })
  },
  put() {
      return new Promise((resolve) => {
          if (this.__mockError)
              reject(new Error())
          resolve(true)
      })
  },
  resolveWith(data, mockError = false) {
      this.__mockError = mockError
      this.__result = data
  }
}
