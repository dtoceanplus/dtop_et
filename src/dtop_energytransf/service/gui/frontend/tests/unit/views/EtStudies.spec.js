import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'

import EtStudies from '@/views/et_studies/index.vue'
import ElementUI from 'element-ui';
import EtStudiesJson from '../json/EtStudiesJson.json'

import Vue from 'vue'
import axios from 'axios'

Vue.use(ElementUI);
jest.mock("axios")

describe('index.vue', () => {
    const localVue = createLocalVue()
    localVue.use(VueRouter)

    const $router = {
      push: jest.fn(),
    }
    const wrapper = shallowMount(EtStudies, {
      mocks: {
        $router
      }
    })

    let studyMessage = jest.spyOn(wrapper.vm, "studyMessage");

    it('getStudies', async() => {
      axios.resolveWith(EtStudiesJson)
      await wrapper.vm.getStudies();
      expect(wrapper.vm.et_studies).toEqual(EtStudiesJson.data.et_studies);
    })

    it('addStudy', async() => {
      axios.resolveWith(EtStudiesJson, false)
      await wrapper.vm.addStudy(EtStudiesJson);
      expect(studyMessage).toHaveBeenCalledWith("Study added", "success")
    })

    it('addStudy error', async() => {
      axios.resolveWith(EtStudiesJson, true)
      await wrapper.vm.addStudy(EtStudiesJson);
    })

    it('createBtnDisabled if', () => {
      wrapper.setData({
        studyForm : {
          name: ''
        }
      })
      expect(wrapper.vm.createBtnDisabled()).toEqual(true)
    })

    it('createBtnDisabled else if standalone', () => {
      wrapper.setData({
        studyForm : {
          name: 'test',
          standalone: false,
          sc: null,
          mc: 2,
          ec: 3
        }
      })
      expect(wrapper.vm.createBtnDisabled()).toEqual(true)
    })

    it('createBtnDisabled else if integrated', () => {
      wrapper.setData({
        studyForm : {
          name: 'test',
          standalone: true,
          sc: null,
          mc: 2,
          ec: 3
        }
      })
      expect(wrapper.vm.createBtnDisabled()).toEqual(false)
    })

    it('createBtnDisabled else', () => {
      wrapper.setData({
        studyForm : {
          name: 'test',
          standalone: false,
          sc: 1,
          mc: 2,
          ec: 3
        }
      })
      expect(wrapper.vm.createBtnDisabled()).toEqual(false)
    })

    it('initModuleIds', () => {
      wrapper.setData({
        studyForm: {
          sc: 1,
          mc: 2,
          ec: 3
        }
      })
      wrapper.vm.initModuleIds();
      expect(wrapper.vm.studyForm.sc).toEqual(null)
      expect(wrapper.vm.studyForm.mc).toEqual(null)
      expect(wrapper.vm.studyForm.ec).toEqual(null)
    })

    it('updateStudy', async() => {
      axios.resolveWith(EtStudiesJson, false)
      await wrapper.vm.updateStudy(EtStudiesJson);
      expect(studyMessage).toHaveBeenCalledWith("Study updated", "success")
    })

    it('updateStudy error', async() => {
        axios.resolveWith(EtStudiesJson, true)
        await wrapper.vm.updateStudy(EtStudiesJson);
    })

    it('editStudy', () => {
      let row = {
        id: 1,
        name: 'test',
        description: 'desc',
        standalone: false,
        site_characterisation_study_id: 1,
        machine_characterisation_study_id: 2,
        energy_capture__study_id: 3
      };
      let form = {
        id: 1,
        name: 'test',
        description: 'desc',
        standalone: false,
        sc: 1,
        mc: 2,
        ec: 3
      };
      wrapper.vm.editStudy(row);
      expect(wrapper.vm.studyForm).toEqual(form)
      expect(wrapper.vm.dialogUpdateFormVisible).toEqual(true);
    })

    it('removeStudy', async() => {
        axios.resolveWith(EtStudiesJson, false)
        await wrapper.vm.removeStudy();
        expect(studyMessage).toHaveBeenCalledWith("Study deleted", "success")
    })

    it('removeStudy error', async() => {
        axios.resolveWith(EtStudiesJson, true)
        await wrapper.vm.removeStudy();
    })

    it('deleteStudy', () => {
      let row = {
        id: 1,
        name: 'test',
        description: 'desc',
      };
      let form = {
        id: 1,
        name: 'test',
        description: 'desc',
        standalone: false,
        sc: null,
        mc: null,
        ec: null
      }
      wrapper.setData({ dialogDeleteVisibile: false })
      wrapper.vm.deleteStudy(row);
      expect(wrapper.vm.studyForm).toEqual(form);
      expect(wrapper.vm.dialogDeleteVisible).toEqual(true);
    })

    it('initForm', () => {
      wrapper.setData({
        studyForm: {
          id: 1,
          name: 'test',
          description: 'desc',
          standalone: false,
          sc: 1,
          mc: 1,
          ec: 1
        }
      })
      wrapper.vm.initForm();
      expect(wrapper.vm.studyForm).toEqual({
        id: 1,
        name: '',
        description: '',
        standalone: false,
        sc: null,
        mc: null,
        ec: null
      })
    })

    it('onSubmit', () => {
      let addStudy = jest.spyOn(wrapper.vm, "addStudy");
      let form = {
        name: 'test',
        description: 'desc',
        standalone: false,
        sc: 1,
        mc: 1,
        ec: 1
      };
      let payload = {
        name: 'test',
        description: 'desc',
        standalone: false,
        site_characterisation_study_id: 1,
        machine_characterisation_study_id: 1,
        energy_capture_study_id: 1
      }
      wrapper.setData({
        dialogFormVisible: true,
        studyForm: form
      })
      wrapper.vm.onSubmit();
      expect(wrapper.vm.dialogFormVisible).toEqual(false);
      expect(addStudy).toHaveBeenCalledWith(payload)
    })

    it('onCancel', () => {
      wrapper.vm.onCancel();
      expect(wrapper.vm.dialogFormVisible).toEqual(false);
      expect(wrapper.vm.dialogUpdateFormVisible).toEqual(false);
      expect(wrapper.vm.dialogDeleteVisible).toEqual(false);
    })

    it('onSubmitUpdate', () => {
      let updateStudy = jest.spyOn(wrapper.vm, "updateStudy");
      let form = {
        id: 1,
        name: 'test',
        description: 'desc',
        standalone: false,
        sc: 1,
        mc: 1,
        ec: 1
      };
      let payload = {
        name: 'test',
        description: 'desc',
        standalone: false,
        site_characterisation_study_id: 1,
        machine_characterisation_study_id: 1,
        energy_capture_study_id: 1
      }
      wrapper.setData({
        dialogFormVisible: true,
        studyForm: form
      })
      wrapper.vm.onSubmitUpdate();
      expect(wrapper.vm.dialogUpdateFormVisible).toEqual(false);
      expect(updateStudy).toHaveBeenCalledWith(payload, 1)
    })
})
