# This file should contain create_app() function.
# This function is used by Flask.

import os

from flask import Flask, request, jsonify, render_template
from flask_babel import Babel
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from werkzeug.middleware.proxy_fix import ProxyFix
from dtop_energytransf.storage.config import Config


babel = Babel()
db = SQLAlchemy()
ma = Marshmallow()


def create_app(test_config=None):
    # create and configure the app
    app = Flask(
        __name__,
        instance_relative_config=False,
        # static_folder="../gui/frontend/dist/static",
        # template_folder="../gui/frontend/dist",
    )
    app.config.from_object(Config)
    cors = CORS(app, resources={r"/*": {"origins": "*"}})

    # secret key is needed for session
    # app.secret_key = "dljsaklqk24e21cjn!Ew@@dsa5"

    babel.init_app(app)

    # if test_config is None:
    #     # load the instance config, if it exists, when not testing
    #     app.config.from_pyfile("config.py", silent=True)
    # else:
    #     # load the test config if passed in
    #     app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    db.init_app(app)
    ma.init_app(app)

    from dtop_energytransf.storage import db_utils

    db_utils.init_app(app)

    # Registering Blueprints
    from .api import studies as et_studies
    app.register_blueprint(et_studies.bp, url_prefix="/energy_transf")

    from .api import inputs as et_inputs
    app.register_blueprint(et_inputs.bp, url_prefix="/energy_transf")

    from .api import bp, core,  outputs, representation
    app.register_blueprint(bp)

    app.register_blueprint(core.bp)

    # app.register_blueprint(et_studies.bp_et)

    app.register_blueprint(outputs.bp_et)

    app.register_blueprint(representation.bp_et)

    app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_host=1, x_prefix=1)
    if os.environ.get("FLASK_ENV") == "development":
        from .api.integration import provider_states

        app.register_blueprint(provider_states.bp)

    return app

    # This should be activated IF the front end is integrated without a separate port
    # @app.route('/', defaults={'path': ''})
    # @app.route('/<path:path>')
    # def render_vue(path):
    #     return render_template("index.html")

    # return app


@babel.localeselector
def get_locale():
    return request.accept_languages.best_match(["en", "fr"])
