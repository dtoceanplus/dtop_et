# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import requests
from flask import (
    Blueprint,
    jsonify,
    redirect,
    render_template,
    request,
    url_for,
    make_response,
)
import os
import json
import math

# from dtop_energytransf import business
from dtop_energytransf.business.analysis import RunAnalysis
from dtop_energytransf.business.output_management import AnalysisResults
from dtop_energytransf.service.api.errors import bad_request
from dtop_energytransf.service.api.inputs import send_input_data, get_catalogue
from dtop_energytransf.storage.models.models import *
from dtop_energytransf.storage.schemas.schemas import *
from dtop_energytransf.storage import config
import base64
from pathlib import Path
from dtop_energytransf.utils import get_project_root

root = get_project_root()
PATH_STORAGE = os.path.join(root, Path('storage/'))

bp_et = Blueprint("outputs", __name__)
headers = {"Access-Control-Allow-Headers": "Content-Type"}


@bp_et.route("/energy_transf/<et_id>/outputs", methods=["PUT"])
def run_analysis_and_update_outputs(et_id):
    """
    Update analysis mode inputs, run the analysis and save the results to database.

    First updates the input data.
    If this is successful then runs the analysis and saves the analysis results to the database.
    If the input data cannot be written to the database, returns a bad request error.

    :param str et_id: the ID of the Energy Transformation study
    :return: success message if successful and error message otherwise.
    :rtype: flask.wrappers.Response
    """
    input_data = send_input_data(et_id)
    catalogue_data=get_catalogue(et_id)
    if input_data.get_json()["message"] == "Inputs have been updated": # and catalogue_data.get_json()["message"] == "Catalogue data stored":        
        analysis = RunAnalysis(et_id)
        AnalysisResults(analysis)
        return make_response(jsonify({"message": "Analysis completed"}),201, headers)
    else:
        return bad_request("Error in running Analysis Mode")


# @bp_et.route("/", methods=["GET"])
@bp_et.route("/energy_transf/<et_id>/array", methods=["GET"])
def get_array(et_id):
    """
    Retrieves the array results data from the local database.

    If the retrieval fails, returns a 404 error.

    :param str et_id: the ID of the Energy Transformation study
    :return: diccionary with the array results if successful and HTTP 404 error message otherwise.
    :rtype: flask.wrappers.Response
    """
    try:
        study = EnergyTransformationStudy.query.get(et_id)
        array = study.array_results
        array_schema = ArrayResultsSchema()
        array_dict = array_schema.dump(array)

        # TODO: see if only important information is returned
        return make_response(jsonify(array_dict), 200, headers)
    except:
        return jsonify({"error_message": "error while parsing the projects"}), 404


@bp_et.route("/energy_transf/<et_id>/devices", methods=["GET"])
def get_all_devices(et_id):
    """
    Retrieves the results for all of the devices from the local database.

    If the retrieval fails, returns a 404 error.

    :param str et_id: the ID of the Energy Transformation study
    :return: diccionary with all device results if successful and HTTP 404 error message otherwise.
    :rtype: flask.wrappers.Response
    """
    try:
        study = EnergyTransformationStudy.query.get(et_id)
        device = study.device_results
        devices_schema = DeviceResultsSchema(many=True)
        device_list = devices_schema.dump(device)
        return make_response(jsonify(device_list), 200, headers)
    except:
        return jsonify({"error_message": "error while parsing the projects"}), 404


@bp_et.route("/energy_transf/<et_id>/devices/<dev_id>", methods=["GET"])
def get_device(et_id, dev_id):
    """
    Retreives the results for a single device from the local database.

    If the retrieval fails, returns a 404 error.

    :param str et_id: the ID of the Energy Transformation study  
    :param str dev_id: the ID of the device results entry in the database
    :return: diccionary with the specified device results if successful and HTTP 404 error message otherwise.
    :rtype: flask.wrappers.Response
    """
    try:
        device = DeviceResults.query.filter_by(device_id=dev_id).one()
        device_schema = DeviceResultsSchema()
        device_dict = device_schema.dump(device)
        
        return make_response(jsonify(device_dict), 200, headers)
    except:
        return jsonify({"error_message": "error while parsing the projects"}), 404


@bp_et.route("/energy_transf/<et_id>/devices/pto_results", methods=["GET"])
def get_all_pto_results(et_id):
    """
    Retrieves the results for all of the ptos from the local database.

    If the retrieval fails, returns a 404 error.

    :param str et_id: the ID of the Energy Transformation study
    :return: diccionary with all pto results if successful and HTTP 404 error message otherwise.
    :rtype: flask.wrappers.Response
    """
    try:
        study = EnergyTransformationStudy.query.get(et_id)
        pto = study.pto_results
        pto_schema = PtoResultsSchema(many=True)
        pto_list = pto_schema.dump(pto)
        return make_response(jsonify(pto_list), 200, headers)
    except:
        return jsonify({"error_message": "error while parsing the projects"}), 404

@bp_et.route("/energy_transf/<etId>/devices/<devId>/ptos", methods=["GET"])
def get_all_ptos(etId, devId):
    """
    Returns the list of ptos for a specific device
    
    :param str etId: the ID of the Energy Transformation study
    :param str devId: the ID of the device results entry in the database
    :return: diccionary with the PTO list in the DB if successful and HTTP 404 error message otherwise.
    :rtype: flask.wrappers.Response

    """
    try:
        device = DeviceResults.query.filter_by(et_study_id=etId, device_id=devId).one()
        ptos = device.pto_results
        pto_schema = PtoResultsSchema(many=True)
        pto_list = pto_schema.dump(ptos)
        return make_response(jsonify(pto_list), 200, headers)
    except:
        return jsonify({"error_message": "error while parsing the projects"}), 404


@bp_et.route("/energy_transf/<etId>/devices/<devId>/ptos/<ptoId>", methods=["GET"])
def get_pto(etId, devId, ptoId):
    """
    Returns the outputs for a specific pto

    :param str etId: the ID of the Energy Transformation study
    :param str devId: the ID of the device results entry in the database
    :param str ptoId: the ID of the PTO results entry in the database
    :return: diccionary with the specified PTO results in the DB if successful and HTTP 404 error message otherwise.
    :rtype: flask.wrappers.Response
    """
    try:
        pto = PtoResults.query.filter_by(et_study_id=etId, device_results_id=devId, pto_id=ptoId).one()
        pto_schema = PtoResultsSchema()
        pto_dict = pto_schema.dump(pto)
        # TODO: see if only important information is returned
        return make_response(jsonify(pto_dict), 200, headers)
    except:
        return jsonify({"error_message": "error while parsing the projects"}), 404

@bp_et.route("/energy_transf/<etId>/crossmodule", methods=["GET"])
def get_crossmodule(etId):
    """
    Returns the information of the input data from external modules as saved in the ET database (processed).

    :param str etId: the ID of the Energy Transformation study
    :return: If succesful 3 diccionaries with some selected variables processed from the inputs from EC, MC and SC modules. It also returns the mean value of the mechanical speed as information to the user. HTTP 404 error message otherwise.
    :rtype: flask.wrappers.Response
    """
    try:
        mc_data = MachineCharacterisationData.query.get(etId)
        ec_data = EnergyCaptureData.query.get(etId)
        sc_data = SiteCharacterisationData.query.get(etId)
        technology = mc_data.technology
        cmpx = ec_data.ec_cmpx
        if technology == "Tidal":
            ec_schema = EnergyCaptureSchema(only=("num_devices", "Captured_Power","sigma_v", "siteConditionID"))
            sc_schema = SiteCharacterisationSchema(only=("sc_id", "Occ"))         
            if cmpx>2:
                mc_schema = MachineCharacterisationSchema(only=("Cpto", "dof","cut_in_out", "Cp", "number_rotor","TSR", "Ct_tidal", "cp_ct_velocity", "rotor_diameter"))
            else:
                mc_schema = MachineCharacterisationSchema(only=("Cpto", "dof","cut_in_out", "Cp", "number_rotor","TSR", "Ct_tidal", "rotor_diameter"))
        else:
            ec_schema = EnergyCaptureSchema(only=("num_devices", "Captured_Power", "siteConditionID"))
            sc_schema = SiteCharacterisationSchema(only=("Hs", "Tz", "Occ"))
            mc_schema = MachineCharacterisationSchema(only=("Cpto", "dof","cut_in_out"))
            
        ec_dict = ec_schema.dump(ec_data)
        mc_dict = mc_schema.dump(mc_data)
        sc_dict = sc_schema.dump(sc_data)
        
        if technology == "Wave":
            sea_states = len(ec_dict["Captured_Power"][0])
            if cmpx > 2:
                cpto_mean = 0
                summa = 0
                n_rows = len(mc_dict["Cpto"][0][0][0][0]) # max dof n_rows^2
                num_hs = len(mc_dict["Cpto"])
                num_tp = len(mc_dict["Cpto"][0])
                num_dir = len(mc_dict["Cpto"][0][0])
              
                for cont_hs in range(0, num_hs): 
                    for cont_tp in range(0, num_tp): 
                        for cont_dir in range(0, num_dir): 
                            for cont1 in range(0, n_rows):
                                    for cont2 in range(0, n_rows):
                                        if mc_dict["Cpto"][cont_hs][cont_tp][cont_dir][cont1][cont2] > 0:
                                            cpto_mean =cpto_mean+mc_dict["Cpto"][cont_hs][cont_tp][cont_dir][cont1][cont2]
                                            summa = summa+1
                cpto_mean = cpto_mean/summa  
            else:
                cpto_mean = mc_dict["Cpto"][0]
            Pcapt_mean = 0
            summa = 0
            for cont_dev in range(0, ec_dict["num_devices"]):
                for cont_ss in range (0, sea_states):
                    if ec_dict["Captured_Power"][cont_dev][cont_ss] != 0:
                        Pcapt_mean = Pcapt_mean + ec_dict["Captured_Power"][cont_dev][cont_ss]
                        summa = summa + 1
            Pcapt_mean = Pcapt_mean / summa

            omega_mec_mean = math.sqrt(Pcapt_mean/cpto_mean)

        else: #tidal
            Ct_tidal_mean = 0
            for cont in range(0, ec_dict["num_devices"]*mc_dict["number_rotor"]):
                Ct_tidal_mean = Ct_tidal_mean + mc_dict["Ct_tidal"][cont][0]
            Ct_tidal_mean = Ct_tidal_mean/(ec_dict["num_devices"]*mc_dict["number_rotor"])  
            omega_mec_mean = 1/ Ct_tidal_mean
       
       
        return make_response({"ec_data": ec_dict, "sc_data": sc_dict, "mc_data": mc_dict, "omega_mec_mean":omega_mec_mean}, 200, headers)
    except:
        return jsonify({"error_message": "The module data cannot be fetched"}), 404

@bp_et.route('/energy_transf/<etId>/devices/<devId>/ptos/<ptoId>/sites/<siteId>/getimage', methods=['POST'], strict_slashes=False)
def get_pto_figure(etId, devId, ptoId, siteId):
    """
    Returns the power plot for a specific pto

    :param str etId: the ID of the Energy Transformation study
    :param str devId: the ID of the device results entry in the database
    :param str ptoId: the ID of the PTO results entry in the database
    :param str siteId: Site ID (sea state ID)
    :return: A figure with the captured, mechanical, electrical and grid conditioned power if successful and HTTP 404 error message otherwise.
    :rtype: flask.wrappers.Response
    """    
    try:
        # device = DeviceResults.query.filter_by(et_study_id=etId, device_id=devId).one()
        device = DeviceResults.query.filter_by(device_id=devId).one()
        deviceid = device.id['value']
        pto = PtoResults.query.filter_by(et_study_id=etId, device_results_id=devId, pto_id=ptoId).one()
        ptonameid = pto.id['value']
        path_save = os.path.join(PATH_STORAGE, "figures", etId)
        name_figure = "ET_" + etId + "_Dev_" + deviceid + "_" + ptonameid + "_Site_" + siteId + "_Performance.png"
        path_figure = os.path.join(path_save, name_figure)
        # folder = './src/dtop_energytransf/storage/figures/'
        # figure_name = os.path.join(folder + 'ET_' + etId + '_Dev_' + deviceid + '_' + ptonameid + '_Performance.png')
        # figure_name= './src/dtop_energytransf/storage/figures/ET_1_Dev_0_PTO_0_0_Performance.png'
        data_uri = base64.b64encode(open(path_figure, 'rb').read()).decode('utf-8')
        img_tag1 = data_uri
        return make_response(jsonify(img_tag1), 200, headers)
    except:
        return jsonify({"error_message": "error while parsing the projects"}), 404
    
