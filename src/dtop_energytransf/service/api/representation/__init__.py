# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import requests
from flask import (
    Blueprint,
    jsonify,
    redirect,
    render_template,
    request,
    url_for,
    make_response,
)
import os

import json

from dtop_energytransf.storage.models.models import *
from dtop_energytransf.storage.schemas.schemas import *

headers = {"Access-Control-Allow-Headers": "Content-Type"}

bp_et = Blueprint("representation", __name__)

@bp_et.route("/representation/<et_id>", methods=["GET"])
def get_representation(et_id):
    """
    Retrieves the array results data from the local database.

    If the retrieval fails, returns a 404 error.

    :param str et_id: the ID of the Energy Transformation study
    :return: The serialised response containing the results or a HTTP 404 error.
    :rtype: flask.wrappers.Response
    """
    try:
        study = EnergyTransformationStudy.query.get(et_id)
        representation = study.representation
        representation_schema = RepresentationSchema()
        representation_dict = representation_schema.dump(representation)

        # TODO: see if only important information is returned
        return make_response(jsonify(representation_dict), 200, headers)
    except:
        return jsonify({"error_message": "error while parsing the projects"}), 404
