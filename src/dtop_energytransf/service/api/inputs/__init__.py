# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint, request, jsonify, url_for, make_response

from dtop_energytransf.business.input_management import InputManagement
from dtop_energytransf.storage.models.models import EnergyTransformationStudy  
from dtop_energytransf.storage.schemas.schemas import InputsSchema
from dtop_energytransf.business.cross_module_data import *
from dtop_energytransf.service.api.errors import bad_request, not_found
from ..utils import join_validation_error_messages

bp = Blueprint("api_inputs", __name__)
headers = {"Access-Control-Allow-Headers": "Content-Type"}


@bp.route("/<et_id>/inputs", methods=["GET"])
def get_input_data(et_id):
    """
    Get the input data stored in the DB for a specified Energy Transformation study.

    Returns a not found error if the study does not exist in the database.

    :param str et_id: the ID of the Energy Transformation study if succesful. HTTP 404 error if not.
    :return: the serialised JSON data for the inputs of a single ET study
    :rtype: flask.wrappers.Response
    """
    study = EnergyTransformationStudy.query.get(et_id)
    if study is None:
        return not_found("Energy Transformation study with that ID could not be found")
    try:
        study_inputs_schema = InputsSchema(only=("general_inputs", "mechanical_inputs", "electrical_inputs", "grid_inputs","control_inputs"))
        result = study_inputs_schema.dump(study)
        return make_response(result, 200, headers)
    except: 
        return jsonify({"error_message": "error while parsing the projects"}), 404


@bp.route("/<et_id>/inputs", methods=["PUT"])
def send_input_data(et_id):
    """
    Send the input data from the front-end to the back-end.

    Update the input data for an ET study in the local database.
    Put method because the input database models are created when study is created.
    Returns a not found error if the study does not exist.
    Returns a bad request error if no request body is provided.
    Also returns a bad request error if the validation of the request body does not succeed.

    :param str et_id: the ID of the Energy Transformation study
    :return: success message if successful and error message otherwise.
    :rtype: flask.wrappers.Response
    """
    study = EnergyTransformationStudy.query.get(et_id)
    if study is None:
        return not_found("Energy Transformation study with that ID could not be found")
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")        
    input_management = InputManagement(study, request_body)
    err = input_management.validation_error
    if err:
        msg = join_validation_error_messages(err)
        return bad_request(msg)
    if not input_management.validate_complexity_levels_and_inputs():
        return bad_request('Validation error')
    input_management.update_db()

    return make_response(jsonify({"message": "Inputs have been updated"}), 201, headers)
    


# @bp.route("/<et_id>/inputs/catalogue_objects", methods=["GET"])
# def get_catalogue_objects(et_id):
#     """
#     Get all the catalogue objects names and save them in the local DB

#     It is not a PUT nor POST because the names are obtained from the CATALOGUE API
    
#     Returns a not found error if the study does not exist.
   
#     :param str etId: the ID of the Energy Transformation study
#     :return: success message if successful and error message otherwise.
#     """
#     study = EnergyTransformationStudy.query.get(et_id)
#     if study is None:
#         return not_found("Energy Transformation study with that ID could not be found")
#     try:
#         names = 1
#         ProcessCatalogue(study, names)
#         resp = make_response(jsonify({"message":"Catalogue objects retrieved"}))
#     except ValueError as e:
#         resp = make_response(jsonify({"message": str(e)}), 400)
    
#     return resp   

    
# @bp.route("/<et_id>/inputs/catalogue", methods=["GET"])
def get_catalogue(et_id):
    """
    After the user selection of each object get all the catalogue data and save them in the local DB

    It is not a PUT nor POST because the names are obtained from the CATALOGUE API
    
    Returns a not found error if the study does not exist.
   
    :param str et_id: the ID of the Energy Transformation study
    :return: success message if successful and HTTP 400 error message otherwise.
    :rtype: flask.wrappers.Response
    """
    study = EnergyTransformationStudy.query.get(et_id)
    if study is None:
        return not_found("Energy Transformation study with that ID could not be found")
    try:
        names = 0
        ProcessCatalogue(study, names)
        resp = make_response(jsonify({"message":"Catalogue data stored"}))
    except ValueError as e:
        resp = make_response(jsonify({"message": str(e)}), 400)
    
    return resp     
