# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from flask import jsonify
from werkzeug.http import HTTP_STATUS_CODES


def error_response(status_code, message=None):
    """
    Generate json response for an error message based on provided HTTP status code and a message input argument.

    :param int status_code: the HTTP status code
    :param str message: the desired error message
    :return:
    """
    payload = {"error": HTTP_STATUS_CODES.get(status_code, "Unknown error")}
    if message:
        payload["message"] = message
    response = jsonify(payload)
    response.status_code = status_code

    return response


def bad_request(message):
    """Standard error for HTTP status code 400"""
    return error_response(400, message)


def not_found(message):
    """Standard error for HTTP status code 404"""
    return error_response(404, message)
