# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint, request
from dtop_energytransf import service
from dtop_energytransf.storage.db_utils import init_db

from .data import FULL_INPUT_DATA, STUDY_DATA

import requests



# endpoint_study = "http://spey/spey"
# endpoint_calculation = "http://spey/spey/1/inputs"
bp = Blueprint("provider_states", __name__)


@bp.route("/provider_states/setup", methods=["POST"])
def provider_states_setup():
    """
    Reset database and create data neccessary for contract testing.
    
    Should be available only with `FLASK_ENV=development`.
    """
    init_db()
    consumer = request.json["consumer"]
    state = request.json["state"]

    if state.startswith("et 1 exists"):
        r_study = requests.post(f"{request.url_root}energy_transf", json=STUDY_DATA)
        r_study = requests.put(f"{request.url_root}energy_transf/1/outputs", json=FULL_INPUT_DATA)
        
    return ""
