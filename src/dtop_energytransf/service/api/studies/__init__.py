# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint, request, jsonify, make_response
from marshmallow import ValidationError

from dtop_energytransf.business.et_studies import (
    create_study,
    update_study,
    delete_study,
)
from dtop_energytransf.service.api.errors import bad_request, not_found
from dtop_energytransf.storage.models.models import EnergyTransformationStudy
from dtop_energytransf.storage.schemas.schemas import EnergyTransformationStudySchema, SimpleStudySchema, CrossmoduleSchema
from ..utils import join_validation_error_messages
from dtop_energytransf.business.cross_module_data import *

from sqlalchemy import desc
import os
from pathlib import Path
from dtop_energytransf.utils import get_project_root

root = get_project_root()
PATH_STORAGE = os.path.join(root, Path('storage/'))

bp = Blueprint("api_et_studies", __name__)
headers = {"Access-Control-Allow-Headers": "Content-Type"}


@bp.route("/", methods=["GET"], strict_slashes=False)
def get_et_study_list():
    """
    Flask blueprint route for getting a list of Energy Transformation studies.

    :return: the json response containing the list of ET studies + id of the last study in the DB if successful. HTTP 404 error if not.
    :rtype: flask.wrappers.Response
    """
    try:
        et_studies = EnergyTransformationStudy.query.all()
        et_studies_schema = SimpleStudySchema(many=True)
        result = et_studies_schema.dump(et_studies)
        # last_study = db.session.query(EnergyTransformationStudy).order_by(EnergyTransformationStudy.id.desc()).limit(1).all()
        last_study_id=0
        # for row in (last_study):
        #     last_study_id = row.id
        if db.session.query(EnergyTransformationStudy).order_by(EnergyTransformationStudy.id.desc()).first():
            last_study_id =  db.session.query(EnergyTransformationStudy).order_by(EnergyTransformationStudy.id.desc()).first().id

        return make_response(jsonify({"et_studies": result, "last_study": last_study_id}), 200, headers)
    except:
        return jsonify({"error_message": "error getting the study data"}), 404   


@bp.route("/", methods=["POST"], strict_slashes=False)
def create_et_study():
    """
    Flask blueprint route for creating an Energy Transformation study.

    Validates the request body, ensuring non-empty name is provided.
    Calls the :meth:`~dtop_energytransf.business.et_studies.create_study()` method. If creation of study is
    successful, then sets HTTP status code as 201. If the study name is not provided or already exists for another study
    in the database, then returns a bad request with a HTTP status code of 400.

    In addition, it calls the :meth:`~dtop_energytransf.business.cross_module_data.ProcessCatalogue()` method to obtain the names of the objects available in the catalogue to be shown when entering the inputs.

    :return: the json response containing the created study object if successful, a HTTP 400 error if not
    :rtype: flask.wrappers.Response
    :except ValidationError: if empty name or no name parameter is provided
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    try:
        EnergyTransformationStudySchema().load(request_body)
    except ValidationError as err:
        msg = join_validation_error_messages(err)
        return bad_request(msg)
    et_study = create_study(request_body)
    if type(et_study) is str:
        return bad_request(et_study)
    names = 1
    ProcessCatalogue(et_study, names)
    et_study_schema = SimpleStudySchema()
    result = et_study_schema.dump(et_study)
    response = jsonify(result)
    response.status_code = 201

    return make_response(response, 201, headers)


@bp.route("/<study_id>", methods=["GET"])
def get_et_study(study_id):
    """
    Flask blueprint route for getting a single Energy Transformation study.

    If study_id provided does not exist in the database, returns a 404 error.

    :param str study_id: the ID of the Energy Transformation study
    :return: A diccionary with: the serialised response containing the **selected ET study**, **technology** and **complexity level of Energy Capture** if succesful. A HTTP 404 error if not
    :rtype: flask.wrappers.Response
    """
    study = EnergyTransformationStudy.query.get(study_id)
    if study is None:
        return not_found("Energy Transformation study with that ID could not be found")
    study_schema = SimpleStudySchema()
    study_result = study_schema.dump(study)
    technology = study.machine_characterisation_data.technology
    cmpx = study.energy_capture_data.ec_cmpx
    
    return make_response(jsonify({"study_result": study_result, "technology": technology, "cmpx": cmpx}), 200, headers)



@bp.route("/<study_id>", methods=["PUT"])
def update_et_study(study_id):
    """
    Flask blueprint route for updating an Energy Transformation study.

    Calls the :meth:`~dtop_energytransf.business.et_studies.update_study()` method.
    Returns HTTP 400 error if no request body provided; if study name not provided; or if ET study with same
    name already exists in database.
    Returns 404 error if study ID does not exist.

    :return: the json response containing a successful 'study updated' message and the new study data, if successful
    :rtype: flask.wrappers.Response
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    # try:
    #     EnergyTransformationStudySchema().load(request_body)
    # except ValidationError as err:
    #     return bad_request(err.messages["name"][0])
    s = update_study(request_body, study_id)
    if type(s) is str:
        if "does not exist" in s:
            return not_found(s)
        else:
            return bad_request(s)
    s_schema = SimpleStudySchema()

    response = {"updated_study": s_schema.dump(s), "message": "Study updated."}

    return make_response(jsonify(response), 201, headers)


@bp.route("/<study_id>", methods=["DELETE"])
def delete_et_study(study_id):
    """
    Flask blueprint route for deleting an Energy Transformation study.

    Calls the :meth:`~dtop_energytransf.business.et_studies.delete_study()` method.
    Returns HTTP 400 error if deletion of ET study is unsuccessful.

    :param str study_id: the ID of the Energy Transformation study to be deleted
    :return: serialised response containing 'successfully deleted' message; bad request error otherwise
    :rtype: flask.wrappers.Response
    """
    data = delete_study(study_id)
    if data == "Study deleted":
        path_figures = os.path.join(PATH_STORAGE, "figures",study_id)
        if (os.path.exists(path_figures)):
            src_files = os.listdir(path_figures)
            for file_name in src_files:            
                full_file_name = os.path.join(path_figures, file_name)
                if os.path.isfile(full_file_name):
                    os.remove(full_file_name)
            os.rmdir(path_figures)

        response = {"deleted_study_id": study_id, "message": "Study deleted."}
        return make_response(jsonify(response), 200, headers)
    else:
        return bad_request(data)

# @bp.route("/<study_id>/crossmodule", methods=["GET"])
# def get_crossmodule_data(study_id):
#     """
#     Get the crossmodule data for a specified Energy Transformation study (SC, MC and EC data).

#     Returns a not found error if the study does not exist in the database.

#     :param str study_id: the ID of the Energy Transformation study
#     :return: the serialised JSON data for the crossmodule data of a single ET study
#     :rtype: dict
#     """
#     study = EnergyTransformationStudy.query.get(study_id)
#     if study is None:
#         return not_found("Energy Transformation study with that ID could not be found")
#     study_crossmodule_schema = CrossmoduleSchema(only=("site_characterisation_data", "machine_characterisation_data", "energy_capture_data"))
#     result = study_crossmodule_schema.dump(study)

#     return make_response(result, 200, headers)   

@bp.route("/<study_id>/crossmodule_ec", methods=["PUT"])
def send_ec_data(study_id):
    """
    Send the crossmodule EC data from the front-end to the back-end.

    Update the crossmodule EC data for an ET study in the local database.
    Put method because the crossmodule database models are created when study is created.
    Returns a not found error if the study does not exist.
    Returns a bad request error if no request body is provided.
    Also returns a bad request error if the validation of the request body does not succeed.

    :param str study_id: the ID of the Energy Transformation study
    :return: success message if successful and HTTP 400 error message otherwise.
   :rtype: flask.wrappers.Response
    """
    study = EnergyTransformationStudy.query.get(study_id)
    if study is None:
        return not_found("Energy Transformation study with that ID could not be found")
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    try:
        EnergyCapture = ProcessEnergyCapture(study, request_body)
        resp = make_response(jsonify({"message":"Energy Capture data have been updated"}), 201, headers)
        if EnergyCapture.validation_error:
            raise ValueError(EnergyCapture._ec_results)
    except ValueError as e:
        resp = make_response(jsonify({"message": str(e)}), 400)
    
    return resp        


@bp.route("/<study_id>/crossmodule_mc", methods=["PUT"])
def send_mc_data(study_id):
    """
    Send the crossmodule MC data from the front-end to the back-end.

    Update the crossmodule MC data for an ET study in the local database.
    Put method because the crossmodule database models are created when study is created.
    Returns a not found error if the study does not exist.
    Returns a bad request error if no request body is provided.
    Also returns a bad request error if the validation of the request body does not succeed.

    :param str study_id: the ID of the Energy Transformation study
    :return: success message if successful and HTTP 400 error message otherwise.
    :rtype: flask.wrappers.Response
    """
    study = EnergyTransformationStudy.query.get(study_id)
    if study is None:
        return not_found("Energy Transformation study with that ID could not be found")
    request_body = request.get_json()
    print(request_body)
    if not request_body:
        return bad_request("No request body provided")
    try:
        MachineCharacterisation = ProcessMachineCharacterisation(study, request_body)
        resp = make_response(jsonify({"message":"Machine Characterisation data have been updated"}), 201, headers)
        # error_cmpx = False
        # if (study.energy_capture_data.ec_cmpx != study.site_characterisation_data.sc_cmpx) or (study.energy_capture_data.ec_cmpx != study.machine_characterisation_data.mc_cmpx) or (study.site_characterisation_data.sc_cmpx!=study.machine_characterisation_data.mc_cmpx):
        #     raise ValueError("Complexity levels from external modules must be equal. Try to upload them again")
        if MachineCharacterisation.validation_error:
            raise ValueError(MachineCharacterisation._mc_results)
    except ValueError as e:
        resp = make_response(jsonify({"message": str(e)}), 400)
    
    return resp

    # if error_mc or error_cmpx:
    #     return bad_request(msg)   
    # else:
    #     return {"message": "Machine Characterisation data has been updated", "id": MachineCharacterisation._mc_results["id"]}

@bp.route("/<study_id>/crossmodule_sc", methods=["PUT"])
def send_sc_data(study_id):
    """
    Send the crossmodule SC data from the front-end to the back-end.

    Update the crossmodule SC data for an ET study in the local database.
    Put method because the crossmodule database models are created when study is created.
    Returns a not found error if the study does not exist.
    Returns a bad request error if no request body is provided.
    Also returns a bad request error if the validation of the request body does not succeed.

    :param str study_id: the ID of the Energy Transformation study
    :return: success message if successful and error message otherwise.
    :rtype: flask.wrappers.Response
    """
    study = EnergyTransformationStudy.query.get(study_id)
    if study is None:
        return not_found("Energy Transformation study with that ID could not be found")
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    try:
        SiteCharacterisation=ProcessSiteCharacterisation(study, request_body)
        resp = make_response(jsonify({"message": "Site Characterisation data have been updated"}), 201, headers)
        
        if SiteCharacterisation.validation_error:
            raise ValueError("Error missing or error in SC input")
    except ValueError as e:
        resp = make_response(jsonify({"message": str(e)}), 400)
    except Exception as e:
        resp = make_response(jsonify({"message": f"RunTime Error: {str(e)}"}), 500)
    
    return resp      

