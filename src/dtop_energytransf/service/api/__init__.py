# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import requests
from flask import Blueprint, request

from dtop_energytransf.service.api.errors import bad_request

bp = Blueprint("api_main", __name__)

# TODO: Add route to API/DREDD (can probably skip this transaction until integration phase)
@bp.route("/ec_id", methods=["POST"])
def ec_id():

    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["ec_id"]
    try:
        complexity = requests.get(url).json()["complexity"]
        print(complexity)
        typee = requests.get(url).json()["type"]
        print(typee)
    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to EC failed")

    return str(complexity), 201


# TODO: Add route to API/DREDD (can probably skip this transaction until integration phase)
@bp.route("/ec_farm", methods=["POST"])
def ec_farm():

    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["ec_farm"]
    try:
        number_device = requests.get(url).json()["number_devices"]
        print(number_device)
    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to EC failed")

    return str(number_device), 201

@bp.route("/ec_devices", methods=["POST"])
def ec_devices():
    
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["ec_devices"]
    try:
        capturedPower = requests.get(url).json()[0]["captured_power_per_condition"]["capturedPower"]
        print(capturedPower)
        siteConditionID = requests.get(url).json()[0]["captured_power_per_condition"]["siteConditionID"]
        print(siteConditionID)
    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to EC failed")

    return str(capturedPower), 201

@bp.route("/ec_tec1_hub", methods=["POST"])
def test_ec_tec_cpx1_velocity_hub():

    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["ec_tec1_hub"]
    try:
        deviceID = requests.get(url).json()["array_velocity_field"][0]["rotorID"]
        print(deviceID)
        hub_velocity = requests.get(url).json()["array_velocity_field"][0]["hub_velocity"]
        print(hub_velocity)
    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to EC failed")

    return str(hub_velocity), 201

@bp.route("/ec_tec2_hub", methods=["POST"])
def test_ec_tec_cpx2__velocity_hub():

    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["ec_tec2_hub"]
    try:
        deviceID = requests.get(url).json()["array_velocity_field"][0]["rotorID"]
        print(deviceID)
        hub_velocity = requests.get(url).json()["array_velocity_field"][0]["hub_velocity"]
        print(hub_velocity)
    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to EC failed")

    return str(hub_velocity), 201

@bp.route("/ec_tec3_hub", methods=["POST"])
def test_ec_tec_cpx3__velocity_hub():

    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["ec_tec3_hub"]
    try:
        deviceID = requests.get(url).json()["array_velocity_field"][0]["rotorID"]
        print(deviceID)
        hub_velocity = requests.get(url).json()["array_velocity_field"][0]["hub_velocity"]
        print(hub_velocity)
    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to EC failed")

    return str(hub_velocity), 201

        
# @bp.route("/ec_tec1", methods=["POST"])        
# def test_ec_tec_main_dim_device():
    
#     request_body = request.get_json()
#     if not request_body:
#         return bad_request("No request body provided")
#     url = request_body["ec_tec1"]
#     try:
#         main_dim_device = requests.get(url).json()["main_dim_device"]
#         print(main_dim_device)
#     except requests.exceptions.RequestException as err:
#         print(err)
#         return bad_request("HTTP request to EC failed")

#     return str(main_dim_device), 201


# @bp.route("/ec_tec2", methods=["POST"])
# def test_ec_tec_cpx2():
    
#     request_body = request.get_json()
#     if not request_body:
#         return bad_request("No request body provided")
#     url = request_body["ec_tec2"]
#     try:
#         rotor_diameter = requests.get(url).json()["rotor_diameter"]
#         print(rotor_diameter)
#     except requests.exceptions.RequestException as err:
#         print(err)
#         return bad_request("HTTP request to EC failed")

#     return str(rotor_diameter), 201


# @bp.route("/ec_tec3", methods=["POST"])
# def test_ec_tec_cpx3():
    
#     request_body = request.get_json()
#     if not request_body:
#         return bad_request("No request body provided")
#     url = request_body["ec_tec3"]
#     try:
#         rotor_diameter = requests.get(url).json()["rotor_diameter"]
#         print(rotor_diameter)
#     except requests.exceptions.RequestException as err:
#         print(err)
#         return bad_request("HTTP request to EC failed")

#     return str(rotor_diameter), 201

# TODO: Add route to API/DREDD (can probably skip this transaction until integration phase)
@bp.route("/mc_id", methods=["POST"])
def mc_id():
    """
    Flask blueprint route for posting a URL to the appropriate path for getting *mc_id* details from MC module.

    To be used in PACT testing example.

    Example request body for MC:

    {
        'farm': '¿¿
    }

    :return: ¿¿ from MC module (for the specified farm URL)
    :rtype: float
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["mc_id"]
    try:
        complexity = requests.get(url).json()["complexity"]
        print(complexity)
        typee = requests.get(url).json()["type"]
        print(typee)
    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to MC failed")

    return str(complexity), 201

@bp.route("/mc_dim", methods=["POST"])
def mc_dim():
    """
    Flask blueprint route for posting a URL to the appropriate path for getting *mc_id* details from MC module.

    To be used in PACT testing example.

    Example request body for MC:

    {
        'farm': '¿¿
    }

    :return: ¿¿ from MC module (for the specified farm URL)
    :rtype: float
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["mc_dim"]
    try:
        dimension = requests.get(url).json()["characteristic_dimension"]
        print(dimension)

    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to MC failed")

    return str(dimension), 201

# TODO: Add route to API/DREDD (can probably skip this transaction until integration phase)
@bp.route("/mc_pto_damping_cpx1", methods=["POST"])
def mc_pto_damping_cpx1():

    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["mc_pto_damping_cpx1"]
    try:
        pto_damping = requests.get(url).json()["pto_damping"]
        print(pto_damping)
    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to MC failed")

    return str(pto_damping), 201

# TODO: Add route to API/DREDD (can probably skip this transaction until integration phase)
@bp.route("/mc_pto_damping_cpx2", methods=["POST"])
def mc_pto_damping_cpx2():

    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["mc_pto_damping_cpx2"]
    try:
        pto_damping = requests.get(url).json()["pto_damping"]
        print(pto_damping)
    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to MC failed")

    return str(pto_damping), 201

# TODO: Add route to API/DREDD (can probably skip this transaction until integration phase)
@bp.route("/mc_pto_damping_cpx3", methods=["POST"])
def mc_pto_damping_cpx3():
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["mc_pto_damping_cpx3"]
    try:
        pto_damping = requests.get(url).json()["pto_damping"]
        print(pto_damping)
    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to MC failed")

    return str(pto_damping), 201

# TODO: Add route to API/DREDD (can probably skip this transaction until integration phase)
@bp.route("/mc_tec_cpx1", methods=["POST"])
def mc_tec_cpx1():
    """
    Flask blueprint route for posting a URL to the appropriate path for getting *mc_id* details from MC module.

    To be used in PACT testing example.

    Example request body for MC:

    {
        'farm': '¿¿
    }

    :return: ¿¿ from EC module (for the specified farm URL)
    :rtype: float
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["mc_tec_cpx1"]
    try:
        cp = requests.get(url).json()["cp"]
        print(cp)
        number_rotor = requests.get(url).json()["number_rotor"]
        print(number_rotor)
    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to MC failed")

    return str(cp), 201

# TODO: Add route to API/DREDD (can probably skip this transaction until integration phase)
@bp.route("/mc_tec_cpx2", methods=["POST"])
def mc_tec_cpx2():
    """
    Flask blueprint route for posting a URL to the appropriate path for getting *mc_id* details from MC module.

    To be used in PACT testing example.

    Example request body for MC:

    {
        'farm': '¿¿
    }

    :return: ¿¿ from EC module (for the specified farm URL)
    :rtype: float
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["mc_tec_cpx2"]
    try:
        cp = requests.get(url).json()["cp"]
        print(cp)
        number_rotor = requests.get(url).json()["number_rotor"]
        print(number_rotor)
        tip_speed_ratio = requests.get(url).json()["tip_speed_ratio"]
        print(tip_speed_ratio)
        ct = requests.get(url).json()["ct"]
        print(ct)
        cut_in_velocity = requests.get(url).json()["cut_in_velocity"]
        print(cut_in_velocity)
        cut_out_velocity = requests.get(url).json()["cut_out_velocity"]
        print(cut_out_velocity)
    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to MC failed")

    return str(cp), 201

# TODO: Add route to API/DREDD (can probably skip this transaction until integration phase)
@bp.route("/mc_tec_cpx3", methods=["POST"])
def mc_tec_cpx3():
    """
    Flask blueprint route for posting a URL to the appropriate path for getting *mc_id* details from MC module.

    To be used in PACT testing example.

    Example request body for MC:

    {
        'farm': '¿¿
    }

    :return: ¿¿ from EC module (for the specified farm URL)
    :rtype: float
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["mc_tec_cpx3"]
    try:
        cp = requests.get(url).json()["cp"]
        print(cp)
        number_rotor = requests.get(url).json()["number_rotor"]
        print(number_rotor)
        tip_speed_ratio = requests.get(url).json()["tip_speed_ratio"]
        print(tip_speed_ratio)
        ct = requests.get(url).json()["ct"]
        print(ct)
        cut_in_velocity = requests.get(url).json()["cut_in_velocity"]
        print(cut_in_velocity)
        cut_out_velocity = requests.get(url).json()["cut_out_velocity"]
        print(cut_out_velocity)
        cp_ct_velocity = requests.get(url).json()["cp_ct_velocity"]
        print(cp_ct_velocity)
    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to MC failed")

    return str(cp), 201


# TODO: Add route to API/DREDD (can probably skip this transaction until integration phase)
@bp.route("/sc_1", methods=["POST"])
def sc_1():
    """
    Flask blueprint route for posting a URL to the appropriate path for getting *sc_id* details from SC module.

    To be used in PACT testing example.

    Example request body for SC:

    {
        '¿¿': '¿¿
    }

    :return: ¿¿ from SC module (for the specified farm URL)
    :rtype: float
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["sc_1"]
    try:
        hs = requests.get(url).json()["hs"]["mean"]
        print(hs)
        tp = requests.get(url).json()["tp"]["mean"]
        print(tp)
    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to SC failed")

    return str(tp), 201


@bp.route("/sc_wec_2_3", methods=["POST"])
def sc_wec_2_3():
   
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["sc_wec_2_3"]
    try:
        bins1 = requests.get(url).json()["bins1"]
        print(bins1)
        bins2 = requests.get(url).json()["bins2"]
        print(bins2)
        id = requests.get(url).json()["id"]
        print(id)
        pdf = requests.get(url).json()["pdf"]
        print(pdf)

    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to SC failed")

    return str(bins1), 201


@bp.route("/sc_tec_cpx3", methods=["POST"])
def sc_tec_cpx3():
   
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["sc_tec_cpx3"]
    try:
        p = requests.get(url).json()["p"]
        print(p)

    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to SC failed")

    return str(p), 201










