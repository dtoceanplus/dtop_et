# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
def join_validation_error_messages(error):
    """
    Converts the marshmallow validation error messages from a dictionary to a single string.

    Required in the case that more than one validation errors are raised. Extracts the actual messages from each
    validation error and concatenates to a single string.

    :param marshmallow.exceptions.ValidationError error: the validation error raised by marshmallow
    :return: the single concatenated error message
    :rtype: str
    """
    msg = " ".join(["".join(str(i)) for i in list(error.messages.values())])

    return msg
