This package contains example of [Blueprint](http://flask.pocoo.org/docs/1.0/blueprints/).

Each Blueprint represents an application component.

Documentation:
- [Modular Applications with Blueprints](http://flask.pocoo.org/docs/1.0/blueprints/)
- [A Better Application Structure](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-xv-a-better-application-structure)
