# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# This package uses business logic functions
# It provides business logic via HTTP API.
# It create a blueprint and define routes.

from flask import Blueprint, render_template, url_for

bp = Blueprint("api_core", __name__)

@bp.route("/api")
def doc():
    """Return the ReDoc documentation for the API.
    
    Returns : ReDoc documentation
    """

    spec_url = url_for("static", filename="openapi.json")

    return render_template("api/index.html", spec_url=spec_url)

# @bp.route("/")
# def index():
#     """
#       @oas [get] /api
#       description: Returns the ReDoc documentation for the API
#     """
#     spec_url = url_for("static", filename="openapi.json")
#     print(spec_url)
#     return render_template("api/index.html", spec_url=spec_url)
