# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import json
import os
from marshmallow.fields import String

import numpy as np
import requests
from dtop_energytransf.utils import get_project_root
from dtop_energytransf.service import db



class ProcessRepresentation(object):
    """
    Process the output data coming from Energy Transformation into the Digital Representation.

    The initialisation process is as follows

    - retrieve the ET data 

    - process the ET data and

    - save the processed data to the local ET database 

    Args:
        study (obj): Energy Transformation study object
        data (dicc): Representation results data
        url (str, optional): the url for the ET output results. Defaults to None.    

    Attributes:
        study (obj): Energy Transformation study object
        _url (str): the url for the ET output results
        _representation_results (dicc): Representation results data
        _processed_representation_data (dicc): Processed representation results data
        validation_error (bool) : Validation error flag. Default to False  
        
    """

    def __init__(self, study, data, url=None):

        self.study = study
        self._url = url
        self._representation_results = {}
        self._processed_representation_data = {}
        self.validation_error = False

        self._representation_results = data
        # self._validate_representation_inputs()

        self._process_representation_data()
        self._save_representation_data()

    # def _validate_representation_inputs(self):
    #     """
    #     General validation to ensure all the needed fields for each case have been provided for the SC inputs request body
               
    #     """
    #     if self.study.machine_characterisation_data.technology== "Wave":
    #         if self.study.energy_capture_data.ec_cmpx == 1 :
    #             if not "hs" in self._sc_results or not "mean" in self._sc_results["hs"]:
    #                 self.validation_error = True
    #         else: #cmpx  2, 3
    #             if not "bins1" in self._sc_results or not "bins2" in self._sc_results or not "id" in self._sc_results or not "pdf" in self._sc_results:
    #                 self.validation_error = True
    #     else:
    #         if not "id" in self._sc_results or not "p" in self._sc_results:
    #             self.validation_error = True
            

    def _process_representation_data(self):
        """Process the ET results data and extract/calculate the parameters required for the representation
        """
        
        array_dict = copy.deepcopy(data_utils.array_data)

        array = [
            {
                "id": "1",
                "properties": {
                    "array_mass": array_dict["Array_mass"],
                    "array_cost": array_dict["Array_cost"]
                }, 
                "assessment": {
                    "array_annual_mean_mechanical_power": array_dict["Array_P_Mech"],
                    "array_annual_mean_electrical_power": array_dict["Array_P_Elect"],
                    "array_annual_mean_grid_conditioning_active_power": array_dict["Array_Pa_Grid"],
                    "array_annual_mean_grid_conditioning_reactive_power": array_dict["Array_Pr_Grid"],
                    "array_annual_transformed_mechanical_energy": array_dict["Array_E_Mech"],
                    "array_annual_transformed_electrical_energy": array_dict["Array_E_Elect"],
                    "array_annual_grid_conditioning_energy": array_dict["Array_E_Grid"],
                }             
            }
        ]

        array[0]["id"] = "1"

        for k in array[0]["properties"]:
            array[0]["properties"][k]["origin"] = "ET"
            array[0]["properties"][k].pop("label")
            
        for k in array[0]["assessment"]:
            array[0]["assessment"][k]["origin"] = "ET"
            array[0]["assessment"][k].pop("label")
     
        device_list = copy.deepcopy(data_utils.device_data)
        number_device = len(device_list)
        pto_list = copy.deepcopy(data_utils.pto_data)
        number_of_ptos_per_device = len(pto_list)

        device = []
        pto = []
        mechanical_conversion = []
        electrical_conversion = []
        grid_conditionining = []

        for i in range(number_device):
            device.append({})
            device[i]["id"] = "1_" + str(i + 1)
            device[i]["properties"] = {}
            device[i]["properties"]["number_of_ptos_per_device"]  = copy.deepcopy(device_list[i]["Dev_par_PTOs"])
            device[i]["properties"]["cut_in_out"] = copy.deepcopy(device_list[i]["Dev_cut_in_out"])
            device[i]["assessment"] = {}
            device[i]["assessment"]["device_annual_transformed_mechanical_power"] = copy.deepcopy(device_list[i]["Dev_P_Mech"])
            device[i]["assessment"]["device_annual_transformed_electrical_power"] = copy.deepcopy(device_list[i]["Dev_P_Elect"])
            device[i]["assessment"]["device_annual_grid_conditioning_active_power"] = copy.deepcopy(device_list[i]["Dev_Pa_Grid"])
            device[i]["assessment"]["device_annual_grid_conditioning_reactive_power"] = copy.deepcopy(device_list[i]["Dev_Pr_Grid"])
            device[i]["assessment"]["device_annual_mechanical_transformed_energy"] = copy.deepcopy(device_list[i]["Dev_E_Mech"])
            device[i]["assessment"]["device_annual_electrical_transformed_energy"] = copy.deepcopy(device_list[i]["Dev_E_Elect"])              
            for k in range(number_of_ptos_per_device):
                pto.append({})
                pto[i*number_of_ptos_per_device+k]["id"] = "1_" + str(i + 1) + "_" + str(k + 1)
                pto[i*number_of_ptos_per_device+k]["assessment"] = {}
                pto[i*number_of_ptos_per_device+k]["assessment"]["stress_load"] = copy.deepcopy(pto_list[i][k]["Stress_Loads"])
                pto[i*number_of_ptos_per_device+k]["assessment"]["stress_load_range"] = copy.deepcopy(pto_list[i][k]["Stress_Load_Range"])
                pto[i*number_of_ptos_per_device+k]["assessment"]["s_n"] = copy.deepcopy(pto_list[i][k]["S_N"])
                mechanical_conversion.append({})
                mechanical_conversion[i*number_of_ptos_per_device+k]["id"] = "1_" + str(i + 1) + "_" + str(k + 1) + "1"
                mechanical_conversion[i*number_of_ptos_per_device+k]["properties"] = {}
                mechanical_conversion[i*number_of_ptos_per_device+k]["properties"]["mechanical_conversion_size"] = copy.deepcopy(pto_list[i][k]["Mech_size"]) 
                mechanical_conversion[i*number_of_ptos_per_device+k]["properties"]["mechanical_conversion_cost"] = copy.deepcopy(pto_list[i][k]["Mech_cost"])
                mechanical_conversion[i*number_of_ptos_per_device+k]["properties"]["mechanical_conversion_mass"] = copy.deepcopy(pto_list[i][k]["Mech_mass"])
                mechanical_conversion[i*number_of_ptos_per_device+k]["assessment"] = {}
                mechanical_conversion[i*number_of_ptos_per_device+k]["assessment"]["mechanical_mean_transformed_power"] = copy.deepcopy(pto_list[i][k]["MechT_Power"])
                mechanical_conversion[i*number_of_ptos_per_device+k]["assessment"]["mechanical_transformed_energy"] = copy.deepcopy(pto_list[i][k]["MechT_Energy"])
                mechanical_conversion[i*number_of_ptos_per_device+k]["assessment"]["mechanical_conversion_annual_failure_rate"] = copy.deepcopy(pto_list[i][k]["MechT_Damage"])
                electrical_conversion.append({})
                electrical_conversion[i*number_of_ptos_per_device+k]["id"] = "1_" + str(i + 1) + "_" + str(k + 1) + "1"
                electrical_conversion[i*number_of_ptos_per_device+k]["properties"] = {}
                electrical_conversion[i*number_of_ptos_per_device+k]["properties"]["electrical_conversion_size"] = copy.deepcopy(pto_list[i][k]["Elect_P_rated"]) 
                electrical_conversion[i*number_of_ptos_per_device+k]["properties"]["electrical_conversion_cost"] = copy.deepcopy(pto_list[i][k]["Elect_cost"]) 
                electrical_conversion[i*number_of_ptos_per_device+k]["properties"]["electrical_conversion_mass"] = copy.deepcopy(pto_list[i][k]["Elect_mass"])
                electrical_conversion[i*number_of_ptos_per_device+k]["assessment"] = {}
                electrical_conversion[i*number_of_ptos_per_device+k]["assessment"]["electrical_mean_transformed_power"] = copy.deepcopy(pto_list[i][k]["ElectT_Power"])
                electrical_conversion[i*number_of_ptos_per_device+k]["assessment"]["electrical_transformed_energy"] = copy.deepcopy(pto_list[i][k]["ElectT_Energy"])
                electrical_conversion[i*number_of_ptos_per_device+k]["assessment"]["electrical_conversion_annual_failure_rate"] = copy.deepcopy(pto_list[i][k]["ElectT_Damage"])
                grid_conditionining.append({})
                grid_conditionining[i*number_of_ptos_per_device+k]["id"] = "1_" + str(i + 1) + "_" + str(k + 1) + "1"
                grid_conditionining[i*number_of_ptos_per_device+k]["properties"] = {}
                grid_conditionining[i*number_of_ptos_per_device+k]["properties"]["grid_conditiong_rated_power"] = copy.deepcopy(pto_list[i][k]["Grid_P_rated"])
                grid_conditionining[i*number_of_ptos_per_device+k]["properties"]["grid_conditiong_cost"] = copy.deepcopy(pto_list[i][k]["Grid_cost"])
                grid_conditionining[i*number_of_ptos_per_device+k]["properties"]["grid_conditioning_mass"] = copy.deepcopy(pto_list[i][k]["Grid_mass"]) 
                grid_conditionining[i*number_of_ptos_per_device+k]["assessment"] = {}
                grid_conditionining[i*number_of_ptos_per_device+k]["assessment"]["grid_conditioning_mean_active_power"] = copy.deepcopy(pto_list[i][k]["GridC_Active_Power"]) 
                grid_conditionining[i*number_of_ptos_per_device+k]["assessment"]["grid_conditioning_mean_reactive_power"] = copy.deepcopy(pto_list[i][k]["GridC_reactive_Power"]) 
                grid_conditionining[i*number_of_ptos_per_device+k]["assessment"]["grid_conditioning_energy"] = copy.deepcopy(pto_list[i][k]["GridC_Energy"]) 

        for k in device[0]["properties"]:
            device[0]["properties"][k]["origin"] = "ET"
            device[0]["properties"][k].pop("label")
        
        for k in device[0]["assessment"]:
            device[0]["assessment"][k]["origin"] = "ET"
            device[0]["assessment"][k].pop("label")
        
        for k in pto[0]["assessment"]:
            pto[0]["assessment"][k]["origin"] = "ET"
            pto[0]["assessment"][k].pop("label")

        for k in mechanical_conversion[0]["properties"]:
            mechanical_conversion[0]["properties"][k]["origin"] = "ET"
            mechanical_conversion[0]["properties"][k].pop("label")
        
        for k in mechanical_conversion[0]["assessment"]:
            mechanical_conversion[0]["assessment"][k]["origin"] = "ET"
            mechanical_conversion[0]["assessment"][k].pop("label")

        for k in electrical_conversion[0]["properties"]:
            electrical_conversion[0]["properties"][k]["origin"] = "ET"
            electrical_conversion[0]["properties"][k].pop("label")
        
        for k in electrical_conversion[0]["assessment"]:
            electrical_conversion[0]["assessment"][k]["origin"] = "ET"
            electrical_conversion[0]["assessment"][k].pop("label")

        for k in grid_conditionining[0]["properties"]:
            grid_conditionining[0]["properties"][k]["origin"] = "ET"
            grid_conditionining[0]["properties"][k].pop("label")
        
        for k in grid_conditionining[0]["assessment"]:
            grid_conditionining[0]["assessment"][k]["origin"] = "ET"
            grid_conditionining[0]["assessment"][k].pop("label")          
       
        self._processed_representation_data = {"array" : array, "device" : device, "pto": pto, "mechanical_conversion"}
           
    def _save_representation_data(self):
        """
        Write the processed SC data to the local ET database.

        Store the processed SC inputs in the :class:`~dtop_energytransf.storage.models.models.SiteCharacterisationData`
        model table.
        """
        self.study.representation.update(**self._processed_representation_data)
        # status = self.study.status + 10
        # aux_dict = {"status": status,"site_characterisation_study_id" : self._processed_sc_data["sc_id"]}
        # self.study.update(**aux_dict)

        db.session.commit()        
        
