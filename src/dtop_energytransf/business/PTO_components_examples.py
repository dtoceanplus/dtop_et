# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from dtop_energytransf.business.transf_stages import (
    Elect_Transf as ElectT_cpx2,
    Mech_Transf as MechT_cpx2,
    Grid_Cond as Elect2G_cpx2,
    Control as ctrl_obj,
)

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os


# Take all outputs from the Energy Capture Module --> Data Generated locally in ET to test the module progress
path = os.path.dirname(os.path.abspath(__file__)) + "\\DB\\json_data\\"
EC_file = "EC_out_OES_Sphere"
EC_out = pd.read_json(path + EC_file + ".json", orient="records", lines=True)


# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Mechanical Conversion
# ----------------------------------------------------------------------------------------------------------------------
print("Hydraulic testing")
D_hyd = 1.4e-4
Ap = 0.2
Hyd = MechT_cpx2.Hydraulic(D_hyd, Ap)
Hyd.env_impact()
Hyd.cost()

print("Gearbox testing")
GB_Pnom = 10000
GB_t_ratio = 10
GB = MechT_cpx2.Gearbox(GB_Pnom, GB_t_ratio)
GB.env_impact()
GB.cost()

print("AirTurbine testing")
Type_turbine = "Wells"  # "Impulse" / "Wells"
turb_D = 1
S_owc_c = np.pi * 2.5 ** 2  # OWC
AirT = MechT_cpx2.AirTurbine(Type_turbine, turb_D, S_owc_c)
AirT.env_impact()
AirT.cost()

print("Mechanical Simplified testing")
P_max_simplified = 500e3
t_ratio = 10
efficiency = 0.8
Mech_sim = MechT_cpx2.Simplified(P_max_simplified, t_ratio, efficiency)
Mech_sim.env_impact()
Mech_sim.cost()

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Electrical Conversion
# -----------------------------------------------------------------------------------------------------------------------
# Data from the catalogue------------------------------
print("SCIG testing")
Gen_Nom_P = 100e3  # W
pp = 2
Vnom = 400.0
fnom = 50.0
rel_T_maxnom = 2.0
rel_V_maxnom = 1.725
gen_class = "Class_F"
SCIG = ElectT_cpx2.SCIG(
    Gen_Nom_P, pp, Vnom, fnom, rel_T_maxnom, rel_V_maxnom, gen_class
)
SCIG.env_impact()
SCIG.cost()

print("Simplified generator testing")
Simp_Nom_P = 50000
SimpG = ElectT_cpx2.Simplified(Simp_Nom_P)
SimpG.env_impact()
SimpG.cost()
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Grid Conditioning of the Electrical Power
# --------------------------------------------
# Data from the catalogue------------------------------
Vdc = 1200
fsw = 5000
VnomGen = 690
fnomGen = 50.0
Lgen = 0.0005
Rgen = 0.0001
Vgrid = 690
Rgrid = 0.0001
cosfi_grid = 1
B2B_Nom_P = 100e3  # W
# -----------------------------------------------------
print("B2B cmp2 testing")
B2B = Elect2G_cpx2.B2B2level(
    B2B_Nom_P, Vdc, fsw, VnomGen, fnomGen, Lgen, Rgen, Vgrid, Rgrid, cosfi_grid
)
B2B.env_impact()
B2B.cost()

print("B2B simplified testing")
B2BSimp_Nom_P = 100e3  # W
SimpB2B = Elect2G_cpx2.Simplified(B2BSimp_Nom_P)
SimpB2B.env_impact()
SimpB2B.cost()
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# **********************************************************************************************************************
# Call to control object to get the non-dimensional distributions of Motion/Power/Loads
# **********************************************************************************************************************
adim_vel = np.arange(0.01, 5, 0.01)
vel_mean = 0
PC = ctrl_obj.Control("Passive", adim_vel, vel_mean)
UC = ctrl_obj.Control("User_defined", adim_vel, vel_mean)
prob_P = PC.prob_P
prob_T = PC.prob_T
prob_T_range = PC.prob_T_range
#  ---------------------------------------------------------------------------------------------------------------------

#  ---------------------------------------------------------------------------------------------------------------------
# Parallel verification of a PTO object with the same inputs as those defined above object by object
#  *********************************************************************************************************************
# How to build a PTO object
#  *************************
from dtop_energytransf.business import PTO as PTO_class

PTO_ID = "PTO_1_1"

# Required parameters to initialise an Air Turbine
Type_turbine = "Wells"  # "Impulse" / "Wells"
turb_D = 1
S_owc_c = np.pi * 2.5 ** 2  # OWC
obj_args = []
mechT_params = {
    "Type": "AirTurbine",  # The name of the object
    "init_args": [
        Type_turbine,
        turb_D,
        S_owc_c,
    ],  # Required arguments to initialise the object
    "perf_args": obj_args,  # Required arguments to perform the performance of specific objects
    "mech_t_ratio": 1,
}
# These four arguments are compulsory.
# The t_ratio simulates an intermediate gearbox but it doesn't add any losses, it just accommodates the speed and torque
# of the mechanical transformation to the generator. t_ratio=omega_in/omega_out=omega_mech/omega_elect. This field must
# be 1 when the gearbox object is selected, as it already has its own t_ratio input in the 'args' field.

# Required parameters to initialise a SCIG
Gen_Nom_P = 100e3  # W
pp = 2
Vnom = 400.0
fnom = 50.0
rel_T_maxnom = 2.0
rel_V_maxnom = 1.725
gen_class = "Class_F"
electT_params = {
    "Type": "SCIG",
    "init_args": [Gen_Nom_P, pp, Vnom, fnom, rel_T_maxnom, rel_V_maxnom, gen_class],
}

# Required parameters to initialise a B2B
Vdc = 1200
fsw = 5000
VnomGen = 690
fnomGen = 50.0
Lgen = 0.0005
Rgen = 0.0001
Vgrid = 690
Rgrid = 0.0001
cosfi_grid = 1
B2B_Nom_P = 100e3  # W
gridC_params = {
    "Type": "B2B2level",
    "init_args": [
        B2B_Nom_P,
        Vdc,
        fsw,
        VnomGen,
        fnomGen,
        Lgen,
        Rgen,
        Vgrid,
        Rgrid,
        cosfi_grid,
    ],
}

# Required parameters to initialise a Control object
adim_vel = np.arange(0.01, 5, 0.01)
vel_mean = 0
control_params = {"Type": "Passive", "init_args": [adim_vel, vel_mean]}

print("PTO object initialisation testing")
PTO_1 = PTO_class.PTO(PTO_ID, mechT_params, electT_params, gridC_params, control_params)
#  ---------------------------------------------------------------------------------------------------------------------

# Initialisation of variables for all SS
Occ_ann = np.zeros(len(EC_out["Hs[m]"]))
Mech_P_in = np.zeros(len(EC_out["Hs[m]"]))
Mech_P_shaft = np.zeros(len(EC_out["Hs[m]"]))
Elect_P = np.zeros(len(EC_out["Hs[m]"]))
Grid_P = np.zeros(len(EC_out["Hs[m]"]))
GB_P = np.zeros(len(EC_out["Hs[m]"]))
Mech_E_in = np.zeros(len(EC_out["Hs[m]"]))
Mech_E_shaft = np.zeros(len(EC_out["Hs[m]"]))
Elect_E = np.zeros(len(EC_out["Hs[m]"]))
Grid_E = np.zeros(len(EC_out["Hs[m]"]))
GB_E = np.zeros(len(EC_out["Hs[m]"]))
Mech_eff = np.zeros(len(EC_out["Hs[m]"]))
Elect_eff = np.zeros(len(EC_out["Hs[m]"]))
Grid_eff = np.zeros(len(EC_out["Hs[m]"]))
GB_eff = np.zeros(len(EC_out["Hs[m]"]))
omega_SS = np.zeros(len(EC_out["Hs[m]"]))
Mech_damage = np.zeros(len(EC_out["Hs[m]"]))
Elect_damage = np.zeros(len(EC_out["Hs[m]"]))
Grid_damage = np.zeros(len(EC_out["Hs[m]"]))
GB_damage = np.zeros(len(EC_out["Hs[m]"]))

cont_c_SS = np.zeros(len(EC_out["Hs[m]"]))
for c_SS in range(0, len(EC_out["Hs[m]"])):

    # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # Captured power in a given Sea State
    # -----------------------------------
    ind_Pmax = EC_out["Pmean[W]"][c_SS][:].index(max(EC_out["Pmean[W]"][c_SS][:]))
    Cpto = EC_out["Cpto[Ns/m]"][c_SS][ind_Pmax]
    sigma_v = np.sqrt(EC_out["Pmean[W]"][c_SS][ind_Pmax] / Cpto)
    EC_T = 365.25 * 24 * 3600 * EC_out["Occ"][c_SS]
    # Reference vectors to compute the transformations at all sea states
    vels = sigma_v * adim_vel[0:-1]
    P_levels = Cpto * vels ** 2
    P_mean = prob_P * P_levels
    # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    print(
        "Hs[m]:"
        + str(EC_out["Hs[m]"][c_SS])
        + "  Tp[s]:"
        + str(EC_out["Tp[s]"][c_SS])
        + " Time[h]:"
        + str(int(EC_T / 3600))
    )

    #  **** PTO Mechanical Transformation assessment *******************************************************************
    #  *****************************************************************************************************************
    # -- Performance --
    obj_args = [1]
    print("AirTurbine testing")
    AirT.performance(Cpto, vels, prob_T, prob_P, obj_args)
    AirT.omega = AirT.omega
    AirT.reliability(prob_T_range, EC_T, EC_out["Tp[s]"][c_SS])

    print("Hydraulic testing")
    Hyd.performance(Cpto, vels, prob_T, prob_P, obj_args)
    Hyd.reliability(prob_T_range, EC_T, EC_out["Tp[s]"][c_SS])

    print("Gearbox testing")
    GB.performance(Cpto, vels, prob_T, prob_P, obj_args)
    GB.reliability(prob_T_range, EC_T, EC_out["Tp[s]"][c_SS])

    print("Mechanical Simplified testing")
    Mech_sim.performance(Cpto, vels, prob_T, prob_P, obj_args)
    Mech_sim.reliability(
        prob_T_range, EC_T, EC_out["Tp[s]"][c_SS]
    )  # --> Correct with Tz instead of Tp (Tz=sqrt(m0/m2))

    # * GENERATOR Electrical Transformation assessment *************************************************************
    print("SCIG testing")
    SCIG.performance(AirT.Mech_T * AirT.omega, AirT.omega, AirT.Mech_P_mean, prob_T)
    SCIG.reliability(prob_T_range, EC_T)

    print("Simplified generator testing")
    SimpG.performance(AirT.Mech_T * AirT.omega, AirT.omega, AirT.Mech_P_mean, prob_T)
    SimpG.reliability(prob_T_range, EC_T)

    # * POWER ELECTRONICS Grid Conditioning ****************************************************************************
    print("B2B cmp2 testing")
    B2B.performance(
        AirT.Mech_T * AirT.omega * SCIG.Elect_eff,
        SCIG.Elect_I_est,
        SCIG.Elect_cosphi,
        SCIG.V_est,
        SCIG.f_est,
        SCIG.Elect_P_mean,
    )
    B2B.reliability(
        AirT.Mech_T * AirT.omega * SCIG.Elect_eff,
        sum(SCIG.Elect_P_mean),
        prob_T_range,
        EC_T,
        EC_out["Tp[s]"][c_SS],
    )

    print("B2B simplified testing")
    SimpB2B.performance(
        AirT.Mech_T * AirT.omega * SCIG.Elect_eff,
        SCIG.Elect_I_est,
        SCIG.Elect_cosphi,
        SCIG.V_est,
        SCIG.f_est,
        SCIG.Elect_P_mean,
    )
    SimpB2B.reliability(
        AirT.Mech_T * AirT.omega * SCIG.Elect_eff,
        sum(SCIG.Elect_P_mean),
        prob_T_range,
        EC_T,
        EC_out["Tp[s]"][c_SS],
    )

    # ******************************************************************************************************************
    # A whole PTO object verification
    print("*****PTO perf/reliab testing*****")
    PTO_1.performance(Cpto, sigma_v)
    PTO_1.reliability(EC_T, EC_out["Tp[s]"][c_SS])
    # ******************************************************************************************************************

    # Sea State Results collection to provide at
    Occ_ann[c_SS] = EC_out["Occ"][c_SS]
    Mech_P_in[c_SS] = np.sum(P_mean)
    Mech_P_shaft[c_SS] = np.sum(AirT.Mech_P_mean)
    Elect_P[c_SS] = np.sum(SCIG.Elect_P_mean)
    Grid_P[c_SS] = np.sum(B2B.Grid_P_mean)
    Mech_eff[c_SS] = AirT.Mech_eff_mean
    Elect_eff[c_SS] = SCIG.Elect_eff_mean
    Grid_eff[c_SS] = B2B.Grid_eff_mean

    Mech_E_in[c_SS] = EC_T / 3600 * np.sum(P_mean)
    Mech_E_shaft[c_SS] = EC_T / 3600 * np.sum(AirT.Mech_P_mean)
    Elect_E[c_SS] = EC_T / 3600 * np.sum(SCIG.Elect_P_mean)
    Grid_E[c_SS] = EC_T / 3600 * np.sum(B2B.Grid_P_mean)

    Mech_damage[c_SS] = AirT.Mech_damage
    Elect_damage[c_SS] = SCIG.Elect_damage
    Grid_damage[c_SS] = B2B.Grid_damage
    # omega_SS[c_SS] = AirT.omega
    # ******************************************************************************************************************

# Power Visualization at all Sea States
cont_c_SS = range(0, len(EC_out["Hs[m]"]))
plt.figure(1), plt.plot(
    cont_c_SS, Mech_eff, cont_c_SS, Elect_eff, cont_c_SS, Grid_eff
), plt.legend(["P_mech", "Elect", "grid"]), plt.title("Power"), plt.show()

plt.figure(2), plt.plot(
    cont_c_SS, Mech_P_in, cont_c_SS, Mech_P_shaft, cont_c_SS, Elect_P, cont_c_SS, Grid_P
), plt.legend(
    [
        "P_mech in: " + str(np.sum(Mech_P_in * Occ_ann)),
        "P shaft: " + str(np.sum(Mech_P_shaft * Occ_ann)),
        "Elect: " + str(np.sum(Elect_P * Occ_ann)),
        "grid: " + str(np.sum(Grid_P * Occ_ann)),
    ]
), plt.title(
    "Power"
), plt.show()
