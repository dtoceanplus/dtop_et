# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# from dtop_energytransf.business import EC2Mech as MechT
# from dtop_energytransf.business import Mech2Elect as ElectT
# from dtop_energytransf.business import Elect2Grid as Elect2G

from dtop_energytransf.business.transf_stages import Mech_Transf as MechT_cpx2
from dtop_energytransf.business.transf_stages import Elect_Transf as ElectT_cpx2
from dtop_energytransf.business.transf_stages import Grid_Cond as Elect2G_cpx2

import numpy as np
import pandas as pd
import os

# Path to store the catalogue
path = os.path.dirname(os.path.abspath(__file__))
w_catalogue = (
    path + "/xlsx_data/" + "ET_Catalogue.xlsx"
)  # The excel sheet which represents the catalogue of the whole module
writer = pd.ExcelWriter(w_catalogue)
# ----------------------------------------------------

# Catalogue for the Mechanical Transformation
# Air Turbine
# -------------
turb_D = np.arange(0.2, 5, 0.2)  # Number of turbines to be included in the catalogue
ids_turb = list(np.zeros(len(turb_D)))
Type_turb = list(np.zeros(len(turb_D)))
Diameter = np.zeros(len(turb_D))
phi = list(np.zeros(len(turb_D)))
psi = list(np.zeros(len(turb_D)))
pi = list(np.zeros(len(turb_D)))
m1 = np.zeros(len(turb_D))
log_a1 = np.zeros(len(turb_D))
m2 = np.zeros(len(turb_D))
log_a2 = np.zeros(len(turb_D))
mass = np.zeros(len(turb_D))
cost = np.zeros(len(turb_D))
shaft_D = np.zeros(len(turb_D))
n_blades = np.zeros(len(turb_D))
L_blade = np.zeros(len(turb_D))
A_blade = np.zeros(len(turb_D))
for cD in range(0, len(turb_D)):
    AirT = MechT_cpx2.AirTurbine("Wells", turb_D[cD], np.pi * 2.5 ** 2)
    AirT.env_impact()
    AirT.cost()
    # Catalogue generation
    ids_turb[cD] = "MechT_AirT_" + str(cD)
    Type_turb[cD] = "Wells"
    Diameter[cD] = turb_D[cD]
    phi[cD] = np.arange(0, 1, 0.01)
    psi[cD] = (
        AirT.airT_perf["Kp"] * phi[cD] ** AirT.airT_perf["alpha_p"]
        + AirT.airT_perf["Kp0"]
    )
    pi[cD] = (
        AirT.airT_perf["Kt"] * phi[cD] ** AirT.airT_perf["alpha_t"]
        + AirT.airT_perf["Kt0"]
    )
    pi[cD][phi[cD] > AirT.nd_stall] = 0
    m1[cD] = AirT.airT_reliab["fatigue_life"][0][0]
    log_a1[cD] = AirT.airT_reliab["fatigue_life"][0][1]
    m2[cD] = AirT.airT_reliab["fatigue_life"][1][0]
    log_a2[cD] = AirT.airT_reliab["fatigue_life"][1][1]
    mass[cD] = AirT.Mech_mass
    cost[cD] = AirT.Mech_cost
    shaft_D[cD] = AirT.airT_reliab["shaftD"] * turb_D[cD]
    n_blades[cD] = AirT.mass_props[0]
    L_blade[cD] = 0.5 * (1 - AirT.mass_props[1]) * turb_D[cD]
    A_blade[cD] = AirT.mass_props[2] * AirT.mass_props[3] ** 2 * turb_D[cD] ** 2
Wells_catalogue = pd.DataFrame({"id": ids_turb})
Wells_catalogue["Type"] = Type_turb
Wells_catalogue["Diameter [m]"] = Diameter
Wells_catalogue["phi [-]"] = phi
Wells_catalogue["psi [-]"] = psi
Wells_catalogue["pi [-]"] = pi
Wells_catalogue["m1"] = m1
Wells_catalogue["log(a1)"] = log_a1
Wells_catalogue["m2"] = m2
Wells_catalogue["log(a2)"] = log_a2
Wells_catalogue["Mass [kg]"] = mass
Wells_catalogue["Cost [€]"] = cost
Wells_catalogue["Shaft diam [m]"] = shaft_D
Wells_catalogue["Blade number"] = n_blades
Wells_catalogue["Blade length [m]"] = L_blade
Wells_catalogue["Blade section [m2]"] = A_blade
# Excel sheet generation to define catalogues for OCC
Wells_catalogue.to_excel(writer, sheet_name="MechT_AirTurbine", index=False)
# -------------------------------------------------------------------------------

# Hydraulic System  ****Try to update the catalogue generation with the object itself once up and running*****
# -------------
hydmotor_D = np.arange(50, 2000, 50) / (2 * np.pi * 1e6)
ids_hyd = list(np.zeros(len(hydmotor_D)))
Type_hyd = list(np.zeros(len(hydmotor_D)))
piston_A = np.zeros(len(hydmotor_D))
piping_hydloss_coeff = np.zeros(len(hydmotor_D))
motor_flow_control = np.zeros(len(hydmotor_D))
max_flow = np.zeros(len(hydmotor_D))
oil_visc = np.zeros(len(hydmotor_D))
oil_dens = np.zeros(len(hydmotor_D))
lam_leak_coeff = np.zeros(len(hydmotor_D))
bulk_mod = np.zeros(len(hydmotor_D))
turb_leak_coeff = np.zeros(len(hydmotor_D))
visc_loss_coeff = np.zeros(len(hydmotor_D))
fric_loss_coeff = np.zeros(len(hydmotor_D))
motor_hydloss_coeff = np.zeros(len(hydmotor_D))
shaftD = np.zeros(len(hydmotor_D))
mass = np.zeros(len(hydmotor_D))
m1 = np.zeros(len(hydmotor_D))
log_a1 = np.zeros(len(hydmotor_D))
m2 = np.zeros(len(hydmotor_D))
log_a2 = np.zeros(len(hydmotor_D))
cost = np.zeros(len(hydmotor_D))
for cD in range(0, len(hydmotor_D)):
    # Catalogue generation
    ids_hyd[cD] = "MechT_Hyd_" + str(cD)
    Type_hyd[cD] = "ET_hyd"
    piston_A[cD] = (
        np.pi * ((np.sqrt(0.2 / np.pi) ** 3 / 1.4e-4 * hydmotor_D[cD]) ** (1 / 3)) ** 2
    )  # The piston diam**3 changes linearly with the motor displacement
    piping_hydloss_coeff[
        cD
    ] = 177031.226  # 64/1000 * 0.5 * args_pto['rho'] * 50 / (np.pi*0.05**2)
    motor_flow_control[cD] = 1
    max_flow[cD] = hydmotor_D[cD] * 427.95
    oil_visc[cD] = 0.03476
    oil_dens[cD] = 869
    lam_leak_coeff[cD] = 1.042e-9
    bulk_mod[cD] = 1.66e9
    turb_leak_coeff[cD] = 1.20e-5
    visc_loss_coeff[cD] = 153.407
    fric_loss_coeff[cD] = 0.0048
    motor_hydloss_coeff[cD] = 0
    shaftD[cD] = (4.928 * hydmotor_D[cD]) ** (
        1 / 3
    )  # Shaft diam**3 changes linearly with the motor size
    mass[cD] = 135.2 * 0.785 * hydmotor_D[cD] * (2 * np.pi * 1e6)
    m1[cD] = 3.0
    log_a1[cD] = 10.97
    m2[cD] = 5.0
    log_a2[cD] = 13.617
    cost[cD] = (
        1392.9 * 0.785 * hydmotor_D[cD] * (2 * np.pi * 1e6)
    )  # y = 1589,6·(0.785·Displacement·(2·pi·1e6)) + 24911  Hydraulic PTO cost / Pnom ( kW )
    # OJO! integrar esto en el business logic!!!!!
Hydraulic_catalogue = pd.DataFrame({"id": ids_hyd})
Hydraulic_catalogue["Type"] = Type_hyd
Hydraulic_catalogue["Motor_Vol [m3/rad]"] = hydmotor_D
Hydraulic_catalogue["Piston Area [m]"] = piston_A
Hydraulic_catalogue["Hydr Loss coeff (piping) [-]"] = piping_hydloss_coeff
Hydraulic_catalogue["Motor flow fraction [-]"] = motor_flow_control
Hydraulic_catalogue["Maximum flow [m3/s]"] = max_flow
Hydraulic_catalogue["Oil Viscosity [Pa·s]"] = oil_visc
Hydraulic_catalogue["Oil density [kg/m3]"] = oil_dens
Hydraulic_catalogue["Laminar leakage coeff [-]"] = lam_leak_coeff
Hydraulic_catalogue["Bulk Mod [Pa]"] = bulk_mod
Hydraulic_catalogue["Turbulent leakage coeff [-]"] = turb_leak_coeff
Hydraulic_catalogue["Viscous loss coeff [-]"] = visc_loss_coeff
Hydraulic_catalogue["Friction loss coeff [-]"] = fric_loss_coeff
Hydraulic_catalogue["Hydr Loss coeff (motor) [-]"] = motor_hydloss_coeff
Hydraulic_catalogue["Shaft Diam [m]"] = shaftD
Hydraulic_catalogue["Mass [kg]"] = mass
Hydraulic_catalogue["m1"] = m1
Hydraulic_catalogue["log(a1)"] = log_a1
Hydraulic_catalogue["m2"] = m2
Hydraulic_catalogue["log(a2)"] = log_a2
Hydraulic_catalogue["cost [€]"] = cost
# Excel sheet generation to define catalogues for OCC
Hydraulic_catalogue.to_excel(writer, "MechT_Hydraulic", index=False)
# -------------------------------------------------------------------------------

# # Gearbox System  ****Try to update the catalogue generation with the object itself once up and running*****
# -------------
P_nom_GB = np.arange(50e3, 500e3, 50e3)
ids_GB = list(np.zeros(len(P_nom_GB)))
Type_GB = list(np.zeros(len(P_nom_GB)))
P_norm = list(np.zeros(len(P_nom_GB)))
shaftD = np.zeros(len(P_nom_GB))
P_max_GB = np.zeros(len(P_nom_GB))
m1 = np.zeros(len(P_nom_GB))
log_a1 = np.zeros(len(P_nom_GB))
m2 = np.zeros(len(P_nom_GB))
log_a2 = np.zeros(len(P_nom_GB))
efficiency = list(np.zeros(len(P_nom_GB)))
speed_ref = list(np.zeros(len(P_nom_GB)))
life_ref = list(np.zeros(len(P_nom_GB)))
cost = np.zeros(len(P_nom_GB))
mass = np.zeros(len(P_nom_GB))
for cD in range(0, len(P_nom_GB)):
    # Catalogue generation
    ids_GB[cD] = "MechT_GB_" + str(cD)
    Type_GB[cD] = "ET_GB"
    P_max_GB = 3 * P_nom_GB[cD]
    P_norm[cD] = [0.05, 0.2, 0.3, 0.5, 0.8, 1]
    shaft_D[cD] = 0.0001 * P_nom_GB[cD]
    efficiency[cD] = [0.4, 0.4, 0.5, 0.7, 0.9, 0.95]
    # speed_ref[cD] = [20, 30, 40, 50, 60, 70, 80, 90, 100, 125, 150, 200, 250, 300, 400, 500, 600, 700, 800,
    #                         900, 1000, 1200, 1500, 1800, 2000, 2500, 3000, 3500, 4000, 5000, 6000]
    # life_ref[cD] = [800000, 800000, 700000, 600000, 550000, 500000, 450000, 400000, 385000, 350000, 300000,
    #                        225000, 175000, 150000, 100000, 80000, 70000, 60000, 50000, 45000, 38000, 32000, 24000,
    #                        20000, 18000, 14000, 11000, 10000, 8500, 6700, 5200]
    m_steep = 10 / 3
    log_a_constant = m_steep * np.log10(0.0005 * 4000e6 / 1.25) + 6
    m1[cD] = m_steep
    log_a1[cD] = log_a_constant
    m2[cD] = m_steep
    log_a2[cD] = log_a_constant
    cost[cD] = 55.88 / 1000 * P_nom_GB[cD]
    mass[cD] = 6.9948 / 1000 * P_nom_GB[cD] + 1647.5 / 1000

Gearbox_catalogue = pd.DataFrame({"id": ids_GB})
Gearbox_catalogue["Type"] = Type_GB
Gearbox_catalogue["Rated Power [W]"] = P_nom_GB
Gearbox_catalogue["Maximum Power [W]"] = P_max_GB
Gearbox_catalogue["Shaft Diam [m]"] = shaftD
Gearbox_catalogue["Power Factor [-]"] = P_norm
Gearbox_catalogue["efficiency [-]"] = efficiency
Gearbox_catalogue["Cost [€]"] = cost
Gearbox_catalogue["m1"] = m1
Gearbox_catalogue["log(a1)"] = log_a1
Gearbox_catalogue["m2"] = m2
Gearbox_catalogue["log(a2)"] = log_a2
Gearbox_catalogue["Mass [kg]"] = mass

# Excel sheet generation to define catalogues for OCC
Gearbox_catalogue.to_excel(writer, "MechT_Gearbox", index=False)
# -------------------------------------------------------------------------------

# Catalogue for the Electrical transformation
Gen_Nom_P = np.arange(
    50e3, 500e3, 50e3
)  # Number of SCIGs to be included in the catalogue
ids_SCIG = list(np.zeros(len(Gen_Nom_P)))
SCIG_type = list(np.zeros(len(Gen_Nom_P)))
SCIG_P_rated = np.zeros(len(Gen_Nom_P))
SCIG_V_rated = np.zeros(len(Gen_Nom_P))
SCIG_I_rated = np.zeros(len(Gen_Nom_P))
SCIG_f_rated = np.zeros(len(Gen_Nom_P))
SCIG_T_max = np.zeros(len(Gen_Nom_P))
SCIG_V_max = np.zeros(len(Gen_Nom_P))
SCIG_pp = np.zeros(len(Gen_Nom_P))
SCIG_mass = np.zeros(len(Gen_Nom_P))
SCIG_shaftD = np.zeros(len(Gen_Nom_P))
SCIG_R_stat = np.zeros(len(Gen_Nom_P))
SCIG_sigma_e = np.zeros(len(Gen_Nom_P))
SCIG_sigma_h = np.zeros(len(Gen_Nom_P))
SCIG_B = np.zeros(len(Gen_Nom_P))
SCIG_thick = np.zeros(len(Gen_Nom_P))
SCIG_winding = np.zeros(len(Gen_Nom_P))
SCIG_frict_T = np.zeros(len(Gen_Nom_P))
SCIG_cost = np.zeros(len(Gen_Nom_P))
SCIG_k = np.zeros(len(Gen_Nom_P))
SCIG_k0 = np.zeros(len(Gen_Nom_P))
SCIG_Temp_max = np.zeros(len(Gen_Nom_P))
for cPE in range(0, len(Gen_Nom_P)):
    # Need to introduce the inputs from the user------------
    pp = 2
    Vnom = 400.0
    fnom = 50.0
    rel_T_maxnom = 2.0
    rel_V_maxnom = 1.725
    gen_class = "Class_F"
    # ------------------------------------------------------------------
    Gen_1 = ElectT_cpx2.SCIG(
        Gen_Nom_P[cPE], pp, Vnom, fnom, rel_T_maxnom, rel_V_maxnom, gen_class
    )
    Gen_1.env_impact()
    Gen_1.cost()
    ids_SCIG[cPE] = "ElecT_SCIG_" + str(cPE)
    SCIG_type[cPE] = "SCIG"
    SCIG_P_rated[cPE] = Gen_Nom_P[cPE]
    SCIG_V_rated[cPE] = Gen_1.V_nom
    SCIG_I_rated[cPE] = Gen_1.I_nom
    SCIG_f_rated[cPE] = Gen_1.f_nom
    SCIG_T_max[cPE] = Gen_1.T_max
    SCIG_V_max[cPE] = Gen_1.V_max
    SCIG_pp[cPE] = Gen_1.pp
    SCIG_mass[cPE] = Gen_1.Elect_mass
    SCIG_shaftD[cPE] = Gen_1.shaft_diam
    SCIG_R_stat[cPE] = Gen_1.Res
    SCIG_sigma_e[cPE] = Gen_1.sigma_e
    SCIG_sigma_h[cPE] = Gen_1.sigma_h
    SCIG_B[cPE] = Gen_1.B
    SCIG_thick[cPE] = Gen_1.thick_max
    SCIG_winding[cPE] = Gen_1.wind_mass_fraction * Gen_1.Elect_mass
    if Gen_Nom_P[cPE] < 3e5:
        mech_losses = (
            Gen_1.x_f[0] * Gen_1.shaft_diam ** 3
            + Gen_1.x_f[1] * Gen_1.shaft_diam ** 2
            + Gen_1.x_f[2] * Gen_1.shaft_diam
        )
    else:
        mech_losses = (
            Gen_1.x_f[0] * 237.451 ** 3
            + Gen_1.x_f[1] * 237.451 ** 2
            + Gen_1.x_f[2] * 237.451
        )
    SCIG_frict_T[
        cPE
    ] = mech_losses  # Friction torque [W/rpm] --> beware of the units of this torque!!!
    SCIG_cost[cPE] = Gen_1.Elect_cost
    SCIG_k[cPE] = Gen_1.Life[0]
    SCIG_k0[cPE] = Gen_1.Life[1]
    SCIG_Temp_max[cPE] = Gen_1.Life[2]
SCIG_catalogue = pd.DataFrame({"id": ids_SCIG})
SCIG_catalogue["Type"] = SCIG_type
SCIG_catalogue["P_rated [W]"] = SCIG_P_rated
SCIG_catalogue["V_rated [V]"] = SCIG_V_rated
SCIG_catalogue["I_rated [A]"] = SCIG_I_rated
SCIG_catalogue["f_rated [Hz]"] = SCIG_f_rated
SCIG_catalogue["T_max [N·m]"] = SCIG_T_max
SCIG_catalogue["V_max [V]"] = SCIG_V_max
SCIG_catalogue["pp"] = SCIG_pp
SCIG_catalogue["Mass [Kg]"] = SCIG_mass
SCIG_catalogue["Shaft Diameter [m]"] = SCIG_shaftD
SCIG_catalogue["Stator Resistance [Ohms]"] = SCIG_R_stat
SCIG_catalogue["sigma_e"] = SCIG_sigma_e
SCIG_catalogue["sigma_h"] = SCIG_sigma_h
SCIG_catalogue["Magnetic Flux Density [T]"] = SCIG_B
SCIG_catalogue["Plate thickness [mm]"] = SCIG_thick
SCIG_catalogue["Winding mass [kg]"] = SCIG_winding
SCIG_catalogue.to_excel(writer, "ElectT_SCIG", index=False)
# ----------------------------------------------------------------------------

# Catalogue for the Grid conditioning
B2B_Nom_P = np.arange(
    50e3, 500e3, 50e3
)  # Number of B2Bs to be included in the catalogue
ids_B2B = list(np.zeros(len(B2B_Nom_P)))
B2B_type = list(np.zeros(len(B2B_Nom_P)))
B2B_IGBT_Vceo = np.zeros(len(B2B_Nom_P))
B2B_IGBT_Rce = np.zeros(len(B2B_Nom_P))
B2B_IGBT_Vcc = np.zeros(len(B2B_Nom_P))
B2B_IGBT_a_fit = np.zeros(len(B2B_Nom_P))
B2B_IGBT_b_fit = np.zeros(len(B2B_Nom_P))
B2B_IGBT_c_fit = np.zeros(len(B2B_Nom_P))
B2B_DIODE_Vfo = np.zeros(len(B2B_Nom_P))
B2B_DIODE_Rt = np.zeros(len(B2B_Nom_P))
B2B_DIODE_Vcc = np.zeros(len(B2B_Nom_P))
B2B_DIODE_a_fit = np.zeros(len(B2B_Nom_P))
B2B_DIODE_b_fit = np.zeros(len(B2B_Nom_P))
B2B_DIODE_c_fit = np.zeros(len(B2B_Nom_P))
B2B_L_gen = np.zeros(len(B2B_Nom_P))
B2B_R_gen = np.zeros(len(B2B_Nom_P))
B2B_L_grid = np.zeros(len(B2B_Nom_P))
B2B_V_grid = np.zeros(len(B2B_Nom_P))
B2B_R_grid = np.zeros(len(B2B_Nom_P))
B2B_V_dc = np.zeros(len(B2B_Nom_P))
B2B_f_sw = np.zeros(len(B2B_Nom_P))
B2B_k0 = np.zeros(len(B2B_Nom_P))
B2B_k = np.zeros(len(B2B_Nom_P))
B2B_T_0 = np.zeros(len(B2B_Nom_P))
B2B_T_rated = np.zeros(len(B2B_Nom_P))
B2B_T_max = np.zeros(len(B2B_Nom_P))
B2B_cost = np.zeros(len(B2B_Nom_P))
B2B_mass = np.zeros(len(B2B_Nom_P))
for cPG in range(0, len(B2B_Nom_P)):
    # Need to introduce the inputs from the user-----------------------
    Vdc = 1200
    fsw = 5000
    VnomGen = 690
    fnomGen = 50.0
    Lgen = 0.0005
    Rgen = 0.0001
    Vgrid = 690  # Input from the user
    Rgrid = 0.0001
    cosfi_grid = 1
    # ------------------------------------------------------------------
    PE_1 = Elect2G_cpx2.B2B2level(
        B2B_Nom_P[cPG], Vdc, fsw, VnomGen, fnomGen, Lgen, Rgen, Vgrid, Rgrid, cosfi_grid
    )
    PE_1.env_impact()
    PE_1.cost()
    ids_B2B[cPG] = "GridC_B2B_" + str(cPG)
    B2B_type[cPG] = "B2B2Level"
    B2B_IGBT_Vceo[cPG] = PE_1.Igbt_Vceo
    B2B_IGBT_Rce[cPG] = PE_1.Igbt_Rce
    B2B_IGBT_Vcc[cPG] = PE_1.Igbt_Vnom  # ???????? Eider Revisar please
    B2B_IGBT_a_fit[cPG] = PE_1.Igbt_a
    B2B_IGBT_b_fit[cPG] = PE_1.Igbt_b
    B2B_IGBT_c_fit[cPG] = PE_1.Igbt_c
    B2B_DIODE_Vfo[cPG] = PE_1.Diod_Vfo
    B2B_DIODE_Rt[cPG] = PE_1.Diod_Rt
    B2B_DIODE_Vcc[cPG] = PE_1.Diod_Vnom  # ????'Eider revisar please
    B2B_DIODE_a_fit[cPG] = PE_1.Diod_a
    B2B_DIODE_b_fit[cPG] = PE_1.Diod_b
    B2B_DIODE_c_fit[cPG] = PE_1.Diod_c
    B2B_L_gen[cPG] = PE_1.Lgen
    B2B_R_gen[cPG] = PE_1.Rgen
    B2B_L_grid[cPG] = 0  # ?????? Eider revisar please
    B2B_V_grid[cPG] = PE_1.V_grid
    B2B_R_grid[cPG] = PE_1.R_grid
    B2B_V_dc[cPG] = PE_1.Vdc
    B2B_f_sw[cPG] = PE_1.fsw
    B2B_k0[cPG] = PE_1.PowerConverter_life[0]
    B2B_k[cPG] = PE_1.PowerConverter_life[1]
    B2B_T_0[cPG] = PE_1.PowerConverter_temp[0]
    B2B_T_rated[cPG] = PE_1.PowerConverter_temp[1]
    B2B_T_max[cPG] = PE_1.PowerConverter_temp[2]
    B2B_cost[cPG] = PE_1.Grid_cost
    B2B_mass[cPG] = PE_1.Grid_mass
B2B_catalogue = pd.DataFrame({"id": ids_B2B})
B2B_catalogue["Type"] = B2B_type
B2B_catalogue["P_rated [W]"] = B2B_Nom_P
B2B_catalogue["IGBT_Vceo [V]"] = B2B_IGBT_Vceo
B2B_catalogue["IGBT_Rce [Ohms]"] = B2B_IGBT_Rce
B2B_catalogue["IGBT_Vcc [V]"] = B2B_IGBT_Vcc
B2B_catalogue["IGBT_a_fit"] = B2B_IGBT_a_fit
B2B_catalogue["IGBT_b_fit"] = B2B_IGBT_b_fit
B2B_catalogue["IGBT_c_fit"] = B2B_IGBT_c_fit
B2B_catalogue["DIODE_Vfo [V]"] = B2B_DIODE_Vfo
B2B_catalogue["DIODE_Rt [Ohms]"] = B2B_DIODE_Rt
B2B_catalogue["DIODE_Vcc [V]"] = B2B_DIODE_Vcc
B2B_catalogue["DIODE_a_fit"] = B2B_DIODE_a_fit
B2B_catalogue["DIODE_b_fit"] = B2B_DIODE_b_fit
B2B_catalogue["DIODE_c_fit"] = B2B_DIODE_c_fit
B2B_catalogue["L_gen [H]"] = B2B_L_gen
B2B_catalogue["R_gen [Ohms]"] = B2B_R_gen
B2B_catalogue["L_grid [H]"] = B2B_L_grid
B2B_catalogue["V_grid [V]"] = B2B_V_grid
B2B_catalogue["R_grid [Ohms]"] = B2B_R_grid
B2B_catalogue["V_dc [V]"] = B2B_V_dc
B2B_catalogue["f_sw [Hz]"] = B2B_f_sw
B2B_catalogue["k0"] = B2B_k0
B2B_catalogue["k"] = B2B_k
B2B_catalogue["T_0"] = B2B_T_0
B2B_catalogue["T_rated"] = B2B_T_rated
B2B_catalogue["T_max"] = B2B_T_max
B2B_catalogue["Cost [€]"] = B2B_cost
B2B_catalogue["Mass [kg]"] = B2B_mass
B2B_catalogue.to_excel(writer, "GridC_PowerConverter", index=False)
# ------------------------------------------------------------------
# Save the ET_Catalogue excel sheet
writer.save()
