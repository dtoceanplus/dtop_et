# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.stats import chi2, foldnorm, rayleigh
import os


def input_json_gen(
    Elect_CMPX, Grid_CMPX, Mech_CMPX, Type_mech_transf, Type_turbine, type_control
):
    # Tenemos que tener en cuenta el tipo de mechanical transformation para coger unos datos u otros
    # USER INPUT == > (AirTurbine - Hydraulic - Gearbox / CMPX2 - 3) - Mech_Simplified CMPX3
    """Elect_CMPX = 2
    Grid_CMPX = 2
    Mech_CMPX = 2
    Type_turbine = "Impulse"            # USER INPUT ==> Impulse - Wells CMPX2-CMPX3
    """
    # ********************************************************
    # Comandos 'varios' para preparar datos
    # ----------------------------------------

    # path to load reference data from other files useful for json generation and storing generated json
    path = os.path.dirname(os.path.abspath(__file__))
    # path to store reference data in the test folder. When runing tests the current_working_directory is taken as that of
    # the test file instead of the object origin file
    # path_test = os.path.split(os.path.split(os.path.split(os.path.dirname(os.path.abspath(__file__)))[0])[0])[0] + '\\test\\business/DB/'

    # Tentative outputs from EC module -- Geometry: OWC test
    EC_dummy = pd.ExcelFile(path + "/xlsx_data/EC_data_template_OWC_test.xlsx")
    # Columns to be read from the excel sheet
    Cptos = list(range(1, 106, 3))
    Pmeans = list(range(3, 106, 3))
    SS_vals = list(range(2, 106, 3))
    EC_Pmean = pd.read_excel(
        EC_dummy, "Estados", skiprows=7, usecols=Cptos[2:] + Pmeans[2:]
    )
    SS = pd.read_excel(EC_dummy, "Estados", skiprows=1, usecols=SS_vals[2:], nrows=4)
    SS = SS.drop(2)
    SS = SS.transpose()
    SS.columns = ["Hs[m]", "Tp[s]", "Occ[percentage]"]
    SS.index = list(range(0, len(SS["Hs[m]"]), 1))
    EC_out = pd.DataFrame({"Hs[m]": SS["Hs[m]"]})
    EC_out["Tp[s]"] = SS["Tp[s]"]
    EC_out.insert(2, "Cpto[Ns/m]", "empty_cell")
    EC_out.insert(3, "Pmean[W]", "empty_cell")
    for c_SS in range(0, len(SS["Hs[m]"]), 1):
        EC_out["Cpto[Ns/m]"][c_SS] = EC_Pmean["Cpto [N s/m]." + str(c_SS + 2)][:]
        EC_out["Pmean[W]"][c_SS] = EC_Pmean["Pot pneum [kW]." + str(c_SS + 2)][:]
    EC_out.to_json(
        path + "\\json_data\\EC_out_OWC_test.json", orient="records", lines=True
    )
    # EC_out.to_json(path_test + 'EC_out_OWC_test.json', orient='records', lines=True)
    # -----------------------------------------------------------------------------------------------------------------------

    # Tentative outputs from EC module -- Geometry: OES Sphere
    EC_dummy = pd.ExcelFile(path + "/xlsx_data/EC_data_template_OES_Sphere_prob.xlsx")
    # Columns to be read from the excel sheet
    Cptos = list(range(1, 472, 3))
    Pmeans = list(range(3, 472, 3))
    SS_vals = list(range(2, 472, 3))
    EC_Pmean = pd.read_excel(
        EC_dummy, "Estados", skiprows=7, usecols=Cptos[2:] + Pmeans[2:]
    )
    SS = pd.read_excel(EC_dummy, "Estados", skiprows=1, usecols=SS_vals[2:], nrows=4)
    SS = SS.drop(2)
    SS = SS.transpose()
    SS.columns = ["Hs[m]", "Tp[s]", "Occ[percentage]"]
    SS.index = list(range(0, len(SS["Hs[m]"]), 1))
    EC_out = pd.DataFrame({"Hs[m]": SS["Hs[m]"]})
    EC_out["Tp[s]"] = SS["Tp[s]"]
    EC_out["Occ"] = SS["Occ[percentage]"] / 100
    EC_out.insert(2, "Cpto[Ns/m]", "empty_cell")
    EC_out.insert(3, "Pmean[W]", "empty_cell")
    for c_SS in range(0, len(SS["Hs[m]"]), 1):
        EC_out["Cpto[Ns/m]"][c_SS] = EC_Pmean["Cpto [N s/m]." + str(c_SS + 2)][:]
        EC_out["Pmean[W]"][c_SS] = EC_Pmean["Pot pneum [kW]." + str(c_SS + 2)][:]
    EC_out.to_json(
        path + "\\json_data\\EC_out_OES_Sphere.json", orient="records", lines=True
    )
    # EC_out.to_json(path_test + 'EC_out_OES_Sphere.json', orient='records', lines=True)
    # -------------------------------------------------------------------------------------------------------------------

    # MECHANICAL TRANSFORMATION INPUT GENERATION
    # -------------------------------------------------------------------------------------------------------------------
    # Wells turbine json generation
    if Type_mech_transf == "AirTurbine":
        if Mech_CMPX == 2:  # CMPX 3 DATA FROM OUR DATA
            if Type_turbine == "Wells":
                Wells = pd.ExcelFile(path + "/xlsx_data/Wells.xlsx")
                Wells_mech = pd.read_excel(
                    Wells, "PI_Wells", skiprows=0, usecols=[1, 2]
                )
                Wells_hyd = pd.read_excel(
                    Wells, "P_Q_Wells", skiprows=0, usecols=[1, 2]
                )
                Wells_eff = pd.read_excel(
                    Wells, "Rend_Wells", skiprows=0, usecols=[1, 2]
                )
                # Linear fitting of the PQ Wells curve to store in the same format as the biradial turb phi-psi, phi-pi and phi-eta
                # phi:flow // psi:pressure head // pi:torque
                # a_c, b_c = np.polyfit(Wells_hyd['Ψ'], Wells_hyd['Φ'], 1) # This fitting yielded unrealistically high mech power
                a_c = 0.375  # --> From Falçao paper about Wells turbine
                Wells_perf = pd.DataFrame({"phi_mech": [Wells_mech["Ψ"] * a_c]})
                Wells_perf["pi_mech"] = [Wells_mech["Π"]]
                Wells_perf["phi_hyd"] = [Wells_hyd["Φ"]]
                Wells_perf["psi_hyd"] = [Wells_hyd["Ψ"]]
                Wells_perf["phi_eff"] = [Wells_eff["Ψ"] * a_c]
                Wells_perf["eta_eff"] = [Wells_eff["η"]]
                Wells_perf["shaftD"] = [0.1]
                Wells_perf["fatigue_life"] = [
                    [[3, 10.97], [5, 13.617]]
                ]  # W3 curve from DNV-RP-C203, two sections, limit 10**7
                Wells_perf["mass"] = [
                    170
                ]  # kg/m^3 [[8, 0.678, 0.1027, 0.212, 7850]] # nº bldes, Rinout, Aunit, chord_to_D, rho
                Wells_perf["cost"] = [[330000, 2.3, 2 / 3]]
                Wells_perf.to_json(
                    path + "\\json_data\\Wells.json", orient="records", lines=True
                )
                # Wells_perf.to_json(path_test + 'Wells.json', orient='records', lines=True)
                # -----------------------------------------------------------------------------------------------------------------------
            else:
                if Type_turbine == "Impulse":
                    # Biradial turbine json generation
                    Impulse_turb = pd.ExcelFile(
                        path + "/xlsx_data/Impulsos_Bi_radial.xlsx"
                    )
                    Impulse_turb_mech = pd.read_excel(
                        Impulse_turb, "T_Q_Impulsos", skiprows=0, usecols=[1, 2]
                    )
                    Impulse_turb_hyd = pd.read_excel(
                        Impulse_turb, "P_Q_Impulsos", skiprows=0, usecols=[1, 2]
                    )
                    Impulse_turb_eff = pd.read_excel(
                        Impulse_turb, "Impulsos_Rend_Flow", skiprows=0, usecols=[1, 2]
                    )
                    Impulse_turb_perf = pd.DataFrame(
                        {"phi_mech": [Impulse_turb_mech["Φ"]]}
                    )
                    Impulse_turb_perf["pi_mech"] = [Impulse_turb_mech["Π"]]
                    Impulse_turb_perf["phi_hyd"] = [Impulse_turb_hyd["Φ"]]
                    Impulse_turb_perf["psi_hyd"] = [Impulse_turb_hyd["Ψ"]]
                    Impulse_turb_perf["phi_eff"] = [Impulse_turb_eff["Φ"]]
                    Impulse_turb_perf["eta_eff"] = [Impulse_turb_eff["η"]]
                    Impulse_turb_perf["shaftD"] = [0.1]
                    Impulse_turb_perf["fatigue_life"] = [
                        [[3, 10.97], [5, 13.617]]
                    ]  # W3 curve from DNV-RP-C203, two sections, limit 10**7
                    Impulse_turb_perf["mass"] = [
                        155
                    ]  # kg/m^3 [[7, 0.015, 0.45, 0.22, 7850]] # nºblades, thick to R, s to R, b to R, rho --> paper: 'novel...
                    Impulse_turb_perf["cost"] = [
                        [330000, 2.3, 2 / 3]
                    ]  # These values are for a Wells turbine, look for the biradial values!!!!
                    Impulse_turb_perf.to_json(
                        path + "\\json_data\\Impulse.json", orient="records", lines=True
                    )
                    # Impulse_turb_perf.to_json(path_test + 'Impulse.json',orient='records',lines=True)
        else:
            if Mech_CMPX == 3:  # CMPX 3-----DATA FROM USER INPUT/GENERAL DATABASE
                if Type_turbine == "Wells":
                    Wells = pd.ExcelFile(path + "/xlsx_data/Wells.xlsx")
                    Wells_mech = pd.read_excel(
                        Wells, "PI_Wells", skiprows=0, usecols=[1, 2]
                    )
                    Wells_hyd = pd.read_excel(
                        Wells, "P_Q_Wells", skiprows=0, usecols=[1, 2]
                    )
                    Wells_eff = pd.read_excel(
                        Wells, "Rend_Wells", skiprows=0, usecols=[1, 2]
                    )
                    # Linear fitting of the PQ Wells curve to store in the same format as the biradial turb phi-psi, phi-pi and phi-eta
                    # phi:flow // psi:pressure head // pi:torque
                    # a_c, b_c = np.polyfit(Wells_hyd['Ψ'], Wells_hyd['Φ'], 1) # This fitting yielded unrealistically high mech power
                    a_c = 0.375  # --> From Falçao paper about Wells turbine
                    Wells_perf = pd.DataFrame({"phi_mech": [Wells_mech["Ψ"] * a_c]})
                    Wells_perf["pi_mech"] = [Wells_mech["Π"]]
                    Wells_perf["phi_hyd"] = [Wells_hyd["Φ"]]
                    Wells_perf["psi_hyd"] = [Wells_hyd["Ψ"]]
                    Wells_perf["phi_eff"] = [Wells_eff["Ψ"] * a_c]
                    Wells_perf["eta_eff"] = [Wells_eff["η"]]
                    Wells_perf["shaftD"] = [0.1]
                    Wells_perf["fatigue_life"] = [
                        [[3, 10.97], [5, 13.617]]
                    ]  # W3 curve from DNV-RP-C203, two sections, limit 10**7
                    Wells_perf["mass"] = [
                        170
                    ]  # kg/m^3 [[8, 0.678, 0.1027, 0.212, 7850]] # nº bldes, Rinout, Aunit, chord_to_D, rho
                    Wells_perf["cost"] = [[330000, 2.3, 2 / 3]]
                    Wells_perf.to_json(
                        path + "\\json_data\\Wells.json", orient="records", lines=True
                    )
                    # Wells_perf.to_json(path_test + 'Wells.json', orient='records', lines=True)
                    # -----------------------------------------------------------------------------------------------------------------------
                else:
                    if Type_turbine == "Impulse":
                        # Biradial turbine json generation
                        Impulse_turb = pd.ExcelFile(
                            path + "/xlsx_data/Impulsos_Bi_radial.xlsx"
                        )
                        Impulse_turb_mech = pd.read_excel(
                            Impulse_turb, "T_Q_Impulsos", skiprows=0, usecols=[1, 2]
                        )
                        Impulse_turb_hyd = pd.read_excel(
                            Impulse_turb, "P_Q_Impulsos", skiprows=0, usecols=[1, 2]
                        )
                        Impulse_turb_eff = pd.read_excel(
                            Impulse_turb,
                            "Impulsos_Rend_Flow",
                            skiprows=0,
                            usecols=[1, 2],
                        )
                        Impulse_turb_perf = pd.DataFrame(
                            {"phi_mech": [Impulse_turb_mech["Φ"]]}
                        )
                        Impulse_turb_perf["pi_mech"] = [Impulse_turb_mech["Π"]]
                        Impulse_turb_perf["phi_hyd"] = [Impulse_turb_hyd["Φ"]]
                        Impulse_turb_perf["psi_hyd"] = [Impulse_turb_hyd["Ψ"]]
                        Impulse_turb_perf["phi_eff"] = [Impulse_turb_eff["Φ"]]
                        Impulse_turb_perf["eta_eff"] = [Impulse_turb_eff["η"]]
                        Impulse_turb_perf["shaftD"] = [0.1]
                        Impulse_turb_perf["fatigue_life"] = [
                            [[3, 10.97], [5, 13.617]]
                        ]  # W3 curve from DNV-RP-C203, two sections, limit 10**7
                        Impulse_turb_perf["mass"] = [
                            155
                        ]  # kg/m^3 [[7, 0.015, 0.45, 0.22, 7850]] # nºblades, thick to R, s to R, b to R, rho --> paper: 'novel...
                        Impulse_turb_perf["cost"] = [
                            [330000, 2.3, 2 / 3]
                        ]  # These values are for a Wells turbine, look for the biradial values!!!!
                        Impulse_turb_perf.to_json(
                            path + "\\json_data\\Impulse.json",
                            orient="records",
                            lines=True,
                        )
                        # Impulse_turb_perf.to_json(path_test + 'Impulse.json',orient='records',lines=True)
    else:  # AirTurbine - Hydraulic - Gearbox
        if Type_mech_transf == "Gearbox":
            if Mech_CMPX == 2:  # Gearbox CMPX 2  GEARBOX
                # Gearbox json data generation
                # Data por simplified model cpx=1. Power load vs. efficiency curve
                Gearbox = pd.DataFrame(
                    {"power_loads_norm": [[0.05, 0.2, 0.3, 0.5, 0.8, 1]]}
                )
                Gearbox["eff_levels"] = [[0.1, 0.4, 0.5, 0.7, 0.9, 0.95]]
                Gearbox["maxP_rel"] = [3]  # P_max/P_nom=3
                Gearbox["Cost"] = [
                    55.88 / 1000
                ]  # cost relation per W ( Gamesa consult )
                Gearbox["Mass"] = [
                    [6.9948 / 1000, 1647.5 / 1000]
                ]  # Mass relation per W
                m_steep = 10 / 3
                log_a_constant = m_steep * np.log10(0.0005 * 4000e6 / 1.25) + 6
                Gearbox["fatigue_life"] = [
                    [[m_steep, log_a_constant], [m_steep, log_a_constant]]
                ]  # values adapted, eq in the TN
                Gearbox["shaftD"] = [0.0001]  # (0.0001*P_nom[W])
                # Gearbox['failure_rate_cpx1'] = [0.2]  # TO BE DEFINED
                Gearbox.to_json(
                    path + "\\json_data\\Gearbox.json",
                    orient="records",
                    lines=True,
                    double_precision=15,
                )
            else:
                if Mech_CMPX == 3:  #  CMPX 3  GEARBOX user input
                    # Gearbox json data generation
                    # Data por simplified model cpx=1. Power load vs. efficiency curve
                    Gearbox = pd.DataFrame(
                        {"power_loads_norm": [[0.05, 0.2, 0.3, 0.5, 0.8, 1]]}
                    )
                    Gearbox["eff_levels"] = [[0.1, 0.4, 0.5, 0.7, 0.9, 0.95]]
                    Gearbox["maxP_rel"] = [3]  # P_max/P_nom=3
                    Gearbox["Cost"] = [
                        55.88 / 1000
                    ]  # cost relation per W ( Gamesa consult )
                    Gearbox["Mass"] = [
                        [6.9948 / 1000, 1647.5 / 1000]
                    ]  # Mass relation per W
                    m_steep = 10 / 3
                    log_a_constant = m_steep * np.log10(0.0005 * 4000e6 / 1.25) + 6
                    Gearbox["fatigue_life"] = [
                        [[m_steep, log_a_constant], [m_steep, log_a_constant]]
                    ]  # values adapted, eq in the TN
                    Gearbox["shaftD"] = [0.0001]  # (0.0001*P_nom[W])
                    # Gearbox['failure_rate_cpx1'] = [0.2]  # TO BE DEFINED
                    Gearbox.to_json(
                        path + "\\json_data\\Gearbox.json",
                        orient="records",
                        lines=True,
                        double_precision=15,
                    )
        else:
            if Type_mech_transf == "Hydraulic":
                if Mech_CMPX == 2:  # HIDRAULIC CMPX2
                    # Hydraulic json data generation
                    # Data por simplified model cpx=1. Power load vs. efficiency curve
                    # ******--> REVISAR con JLM!?!?!?!
                    Hydraulic = pd.DataFrame(
                        {"hyd_mot_eff": [[0.0019, 0.745]]}
                    )  # y= 0.0019*% capfactor +0,745
                    # ******
                    Hydraulic["Oil"] = [[0.03476, 869]]  # [[oil_visc, oil_dens]]
                    Hydraulic["Loss_coeffs"] = [
                        [
                            1.042e-9,
                            1.20e-5,
                            153.407,
                            0.0048,
                            0,
                            64
                            / 1000
                            * 0.5
                            * Hydraulic["Oil"][0][1]
                            * 50
                            / (np.pi * 0.05 ** 2),
                        ]
                    ]  # [[lam_leak_coeff, turb_leak_coeff,
                    # visc_loss_coeff, fric_loss_coeff, motor_hydloss_coeff, pipes_hydloss_coeff]]
                    Hydraulic["Bulk_Mod"] = [1.66e9]  # [bulk_mod]
                    Hydraulic["maxflow_rel"] = [427.95]  # q_max/D=427.95
                    Hydraulic["shaftD"] = [4.928]  # (4.928*hydmotor_D[cD])**(1/3)
                    Hydraulic["cost"] = [
                        [1589.6 * 0.785 * (2 * np.pi * 1e6), 24911]
                    ]  # y = 1589,6x + 24911  Hydraulic PTO cost / Pnom ( kW )
                    Hydraulic["mass"] = [
                        132.2 * 0.785 * (2 * np.pi * 1e6)
                    ]  # y = 132,2x - 1,6056  Hydraulic PTO weight / Pnom (kW)
                    Hydraulic["fatigue_life"] = [
                        [[3, 10.97], [5, 13.617]]
                    ]  # W3 curve from DNV-RP-C203, two sections, limit 10**7
                    # --------------------------------------------------------------------------
                    #  Efficiency Curve for cmpx1 -- Static curve (needs fitting in the _init_)
                    Hydraulic["cmpx1_cf"] = [
                        [
                            0.024390244,
                            0.097560976,
                            0.195121951,
                            0.292682927,
                            0.393292683,
                            0.490853659,
                            0.588414634,
                            0.68597561,
                            0.783536585,
                            0.87804878,
                            0.975609756,
                            1.076219512,
                            1.173780488,
                            1.268292683,
                            1.365853659,
                        ]
                    ]
                    Hydraulic["cmpx1_eff"] = [
                        [
                            0.23,
                            0.51,
                            0.605,
                            0.675,
                            0.7025,
                            0.705,
                            0.7025,
                            0.7075,
                            0.72,
                            0.7325,
                            0.735,
                            0.74,
                            0.7475,
                            0.7425,
                            0.745,
                        ]
                    ]
                    Hydraulic.to_json(
                        path + "\\json_data\\Hydraulic.json",
                        orient="records",
                        lines=True,
                        double_precision=15,
                    )
                    # ----------------------------------------------------------------------------------------------------------------------
                else:
                    if Mech_CMPX == 2:  # HIDRAULIC CMPX3
                        # Hydraulic json data generation
                        # Data por simplified model cpx=1. Power load vs. efficiency curve
                        # ******--> REVISAR con JLM!?!?!?!
                        Hydraulic = pd.DataFrame(
                            {"hyd_mot_eff": [[0.0019, 0.745]]}
                        )  # y= 0.0019*% capfactor +0,745
                        # ******
                        Hydraulic["Oil"] = [[0.03476, 869]]  # [[oil_visc, oil_dens]]
                        Hydraulic["Loss_coeffs"] = [
                            [
                                1.042e-9,
                                1.20e-5,
                                153.407,
                                0.0048,
                                0,
                                64
                                / 1000
                                * 0.5
                                * Hydraulic["Oil"][0][1]
                                * 50
                                / (np.pi * 0.05 ** 2),
                            ]
                        ]  # [[lam_leak_coeff, turb_leak_coeff,
                        # visc_loss_coeff, fric_loss_coeff, motor_hydloss_coeff, pipes_hydloss_coeff]]
                        Hydraulic["Bulk_Mod"] = [1.66e9]  # [bulk_mod]
                        Hydraulic["maxflow_rel"] = [427.95]  # q_max/D=427.95
                        Hydraulic["shaftD"] = [4.928]  # (4.928*hydmotor_D[cD])**(1/3)
                        Hydraulic["cost"] = [
                            [1589.6 * 0.785 * (2 * np.pi * 1e6), 24911]
                        ]  # y = 1589,6x + 24911  Hydraulic PTO cost / Pnom ( kW )
                        Hydraulic["mass"] = [
                            132.2 * 0.785 * (2 * np.pi * 1e6)
                        ]  # y = 132,2x - 1,6056  Hydraulic PTO weight / Pnom (kW)
                        Hydraulic["fatigue_life"] = [
                            [[3, 10.97], [5, 13.617]]
                        ]  # W3 curve from DNV-RP-C203, two sections, limit 10**7
                        # --------------------------------------------------------------------------
                        #  Efficiency Curve for cmpx1 -- Static curve (needs fitting in the _init_)
                        Hydraulic["cmpx1_cf"] = [
                            [
                                0.024390244,
                                0.097560976,
                                0.195121951,
                                0.292682927,
                                0.393292683,
                                0.490853659,
                                0.588414634,
                                0.68597561,
                                0.783536585,
                                0.87804878,
                                0.975609756,
                                1.076219512,
                                1.173780488,
                                1.268292683,
                                1.365853659,
                            ]
                        ]
                        Hydraulic["cmpx1_eff"] = [
                            [
                                0.23,
                                0.51,
                                0.605,
                                0.675,
                                0.7025,
                                0.705,
                                0.7025,
                                0.7075,
                                0.72,
                                0.7325,
                                0.735,
                                0.74,
                                0.7475,
                                0.7425,
                                0.745,
                            ]
                        ]
                        Hydraulic.to_json(
                            path + "\\json_data\\Hydraulic.json",
                            orient="records",
                            lines=True,
                            double_precision=15,
                        )
    if Elect_CMPX == 2:  # ELECTRIC CMPX2
        # -------------------------------------------------------------------------------------------------------------------
        # SCIG json data generation
        # ******************************************************************************************************************
        # x1, x2 and x3 linear functions coeffs of the nominal rot_speed [rpm] of the generator
        SCIG = pd.DataFrame({"x_f": [[2e-7, -1e-5, 6e-4]]})
        SCIG["shaft_diam"] = [
            [0.00000000000002, -0.000000007, 0.001, 27.451]
        ]  # USER INPUT CMPX3
        # Cubic function coefficients with nominal power [W]
        # Assumed phi_cos relation with the mechanical power [W]
        SCIG["phi_cos"] = [[8e-15, -0.000000001, 0.00005, 0.0827]]  # USER INPUT CMPX3
        # cosphi[cP_act] = self.phi_cos[0] * P_act[cP_act] ** 3 + self.phi_cos[1] * P_act[cP_act] ** 2 + \
        # self.phi_cos[2] * P_act[cP_act] + self.phi_cos[3]
        # Histeresis and eddy current loss coefficients
        SCIG["sigma_h"] = [5.01]  # USER INPUT CMPX3
        SCIG["sigma_e"] = [44.8]  # USER INPUT CMPX3
        SCIG["B"] = [0.8]  # USER INPUT CMPX3
        SCIG["Gen_mass"] = [[0.0056, 55.041]]  # USER INPUT CMPX3
        SCIG["wind_mass_fraction"] = [0.6]  # USER INPUT CMPX3
        SCIG["I_nom"] = [[0.0018, 0.934]]  # USER INPUT CMPX3
        SCIG["Res"] = [[72584, -1.236]]  # USER INPUT CMPX3
        Life = {}
        Life["Class_A"] = [-0.069, 17.1288228, 105]  # USER INPUT CMPX3 k, k0, Temp_max
        Life["Class_B"] = [
            -0.069,
            18.8974785,
            130,
        ]  # USER INPUT CMPX3 user should select the class of generator
        Life["Class_F"] = [-0.069, 20.7619675, 155]  # USER INPUT CMPX3
        Life["Class_H"] = [-0.069, 22.1243926, 180]  # USER INPUT CMPX3
        SCIG["Life"] = [Life]
        SCIG["cost"] = [[2.5, 0.7]]  # USER INPUT CMPX3
        SCIG["om_shaft_norm"] = [[0, 0.03, 0.06, 0.1, 0.185, 0.4, 0.6, 0.8, 1]]
        SCIG["eff_levels"] = [[0, 0.4, 0.5, 0.6, 0.7, 0.8, 0.85, 0.88, 0.9]]
        SCIG["thick_max"] = [0.64]
        SCIG["Manufacturer"] = "none"
        SCIG["Date"] = 20052020
        SCIG.to_json(
            path + "\\json_data\\SCIG.json",
            orient="records",
            lines=True,
            double_precision=15,
        )
        # SCIG.to_json(path_test +'SCIG.json',orient='records',lines=True,double_precision=15)
    else:
        if Elect_CMPX == 3:
            # -------------------------------------------------------------------------------------------------------------------
            # SCIG json data generation
            # ******************************************************************************************************************
            # x1, x2 and x3 linear functions coeffs of the nominal rot_speed [rpm] of the generator
            SCIG = pd.DataFrame({"x_f": [[2e-7, -1e-5, 6e-4]]})
            SCIG["shaft_diam"] = [
                [0.00000000000002, -0.000000007, 0.001, 27.451]
            ]  # USER INPUT CMPX3
            # Cubic function coefficients with nominal power [W]
            # Assumed phi_cos relation with the mechanical power [W]
            SCIG["phi_cos"] = [
                [8e-15, -0.000000001, 0.00005, 0.0827]
            ]  # USER INPUT CMPX3
            # cosphi[cP_act] = self.phi_cos[0] * P_act[cP_act] ** 3 + self.phi_cos[1] * P_act[cP_act] ** 2 + \
            # self.phi_cos[2] * P_act[cP_act] + self.phi_cos[3]
            # Histeresis and eddy current loss coefficients
            SCIG["sigma_h"] = [5.01]  # USER INPUT CMPX3
            SCIG["sigma_e"] = [44.8]  # USER INPUT CMPX3
            SCIG["B"] = [0.8]  # USER INPUT CMPX3
            SCIG["Gen_mass"] = [[0.0056, 55.041]]  # USER INPUT CMPX3
            SCIG["wind_mass_fraction"] = [0.6]  # USER INPUT CMPX3
            SCIG["I_nom"] = [[0.0018, 0.934]]  # USER INPUT CMPX3
            SCIG["Res"] = [[72584, -1.236]]  # USER INPUT CMPX3
            Life = {}
            Life["Class_A"] = [
                -0.069,
                17.1288228,
                105,
            ]  # USER INPUT CMPX3 k, k0, Temp_max
            Life["Class_B"] = [
                -0.069,
                18.8974785,
                130,
            ]  # USER INPUT CMPX3 user should select the class of generator
            Life["Class_F"] = [-0.069, 20.7619675, 155]  # USER INPUT CMPX3
            Life["Class_H"] = [-0.069, 22.1243926, 180]  # USER INPUT CMPX3
            SCIG["Life"] = [Life]
            SCIG["cost"] = [[2.5, 0.7]]  # USER INPUT CMPX3
            SCIG["om_shaft_norm"] = [[0, 0.03, 0.06, 0.1, 0.185, 0.4, 0.6, 0.8, 1]]
            SCIG["eff_levels"] = [[0, 0.4, 0.5, 0.6, 0.7, 0.8, 0.85, 0.88, 0.9]]
            SCIG["thick_max"] = [0.64]
            SCIG["Manufacturer"] = "none"
            SCIG["Date"] = 20052020
            SCIG.to_json(
                path + "\\json_data\\SCIG.json",
                orient="records",
                lines=True,
                double_precision=15,
            )
            # SCIG.to_json(path_test +'SCIG.json',orient='records',lines=True,double_precision=15)
    # ----------------------------------------------------------------------------------------------------------------------
    # Power Converter Losses data json generation
    if Grid_CMPX == 2:  # GRID CMPX2
        # Data from module Infineon 1700V 150A IGBT and Diode Characterization values
        PowerConverter = pd.DataFrame(
            {"IGBT_Char": [["Vceo", "Rce", "a", "b", "c", "Vnom"]]}
        )
        PowerConverter["IGBT150"] = [
            [0.85, 0.011497, 0.008490, 0.0006578, -0.0000002294, 900]
        ]  # USER INPUT CMPX3
        PowerConverter["Diode_Char"] = [["Vfo", "Rt", "a", "b", "c", "Vnom"]]
        PowerConverter["Diode150"] = [
            [0.81, 0.006625, 0.0110730, 0.0002368, -0.0000003392, 900]
        ]  # USER INPUT CMPX3
        PowerConverter["IGBT450"] = [
            [0.85, 0.00358333, 0.06026, 0.0003002, 0.0000006978, 900]
        ]  # USER INPUT CMPX3
        PowerConverter["Diode450"] = [
            [0.8, 0.00271605, 0.02424, 0.0002509, 0.0000001585, 900]
        ]  # USER INPUT CMPX3
        PowerConverter["IGBT800"] = [
            [1.15, 0.00178125, 0.0977, 0.00028, 0.000000347, 900]
        ]  # USER INPUT CMPX3
        PowerConverter["Diode800"] = [
            [0.875, 0.0011, 0.049, 0.000352, -0.000000105, 900]
        ]  # USER INPUT CMPX3
        PowerConverter["IGBT1600"] = [
            [1, 0.001, 0.258, 0.000275, 0.000000163, 900]
        ]  # USER INPUT CMPX3
        PowerConverter["Diode1600"] = [
            [0.85, 0.000589, 0.095, 0.000314, -0.00000004, 900]
        ]  # USER INPUT CMPX3
        PowerConverter["power_loads_norm"] = [[0.1, 0.2, 0.3, 0.5, 0.8, 1]]
        PowerConverter["eff_levels"] = [[0.97, 0.975, 0.98, 0.982, 0.981, 0.978]]
        PowerConverter["failure_rate_cpx1"] = [0.27]
        mass = {}
        mass["Copper"] = 0.000137  # kg/W
        mass["Plastic"] = 0.00006  # kg/W
        mass["Steel"] = 0.000326  # kg/W
        mass["Iron"] = 0.00005  # kg/W
        mass["Zink"] = 0.00002  # kg/W
        PowerConverter["mass"] = [mass]
        PowerConverter["cost"] = [0.1]  # €/W #list
        PowerConverter["life"] = [[2e19, -7.885]]  # creo que k k0
        PowerConverter["temp"] = [[0, 40, 120]]  # 0 temp rated, temp max
        PowerConverter["Manufacturer"] = "none"
        PowerConverter["Date"] = 20052020
        PowerConverter.to_json(
            path + "\\json_data\\PowerConverter.json",
            orient="records",
            lines=True,
            double_precision=15,
        )
        # PowerConverter.to_json(path_test +'PowerConverter.json',orient='records',lines=True,double_precision=15)
        # --------------------------------------------------------------------------------------------------------------
    else:
        if Grid_CMPX == 3:  # GRID CMPX2
            # Data from module Infineon 1700V 150A IGBT and Diode Characterization values
            PowerConverter = pd.DataFrame(
                {"IGBT_Char": [["Vceo", "Rce", "a", "b", "c", "Vnom"]]}
            )
            PowerConverter["IGBT150"] = [
                [0.85, 0.011497, 0.008490, 0.0006578, -0.0000002294, 900]
            ]  # USER INPUT CMPX3
            PowerConverter["Diode_Char"] = [["Vfo", "Rt", "a", "b", "c", "Vnom"]]
            PowerConverter["Diode150"] = [
                [0.81, 0.006625, 0.0110730, 0.0002368, -0.0000003392, 900]
            ]  # USER INPUT CMPX3
            PowerConverter["IGBT450"] = [
                [0.85, 0.00358333, 0.06026, 0.0003002, 0.0000006978, 900]
            ]  # USER INPUT CMPX3
            PowerConverter["Diode450"] = [
                [0.8, 0.00271605, 0.02424, 0.0002509, 0.0000001585, 900]
            ]  # USER INPUT CMPX3
            PowerConverter["IGBT800"] = [
                [1.15, 0.00178125, 0.0977, 0.00028, 0.000000347, 900]
            ]  # USER INPUT CMPX3
            PowerConverter["Diode800"] = [
                [0.875, 0.0011, 0.049, 0.000352, -0.000000105, 900]
            ]  # USER INPUT CMPX3
            PowerConverter["IGBT1600"] = [
                [1, 0.001, 0.258, 0.000275, 0.000000163, 900]
            ]  # USER INPUT CMPX3
            PowerConverter["Diode1600"] = [
                [0.85, 0.000589, 0.095, 0.000314, -0.00000004, 900]
            ]  # USER INPUT CMPX3
            PowerConverter["power_loads_norm"] = [[0.1, 0.2, 0.3, 0.5, 0.8, 1]]
            PowerConverter["eff_levels"] = [[0.97, 0.975, 0.98, 0.982, 0.981, 0.978]]
            PowerConverter["failure_rate_cpx1"] = [0.27]
            mass = {}
            mass["Copper"] = 0.000137  # kg/W
            mass["Plastic"] = 0.00006  # kg/W
            mass["Steel"] = 0.000326  # kg/W
            mass["Iron"] = 0.00005  # kg/W
            mass["Zink"] = 0.00002  # kg/W
            PowerConverter["mass"] = [mass]
            PowerConverter["cost"] = [0.1]  # €/W #list
            PowerConverter["life"] = [[2e19, -7.885]]  # creo que k k0
            PowerConverter["temp"] = [[0, 40, 120]]  # 0 temp rated, temp max
            PowerConverter["Manufacturer"] = "none"
            PowerConverter["Date"] = 20052020
            PowerConverter.to_json(
                path + "\\json_data\\PowerConverter.json",
                orient="records",
                lines=True,
                double_precision=15,
            )
            # PowerConverter.to_json(path_test +'PowerConverter.json',orient='records',lines=True,double_precision=15)
        # ----------------------------------------------------------------------------------------------------------------------

    # Control - a json file with user provided PDFs. It is initialised with the passive PDFs for the code testing
    if type_control == "Passive":
        adim_vel = np.arange(0.001, 10, 0.001)
        vel_mean = 0
        # Power levels
        pdf_P = chi2.pdf(
            adim_vel ** 2, 1
        )  # One degree of freedom Chi Square distribution
        # Load levels
        pdf_T = foldnorm.pdf(adim_vel, vel_mean)  # Folded normal distribution
        # Load ranges
        pdf_T_range = rayleigh.pdf(adim_vel)  # Rayleigh distribution

        Control = pd.DataFrame({"adim_vel": [adim_vel]})
        Control["Power_levels"] = [pdf_P]
        Control["Load_levels"] = [pdf_T]
        Control["Load_ranges"] = [pdf_T_range]
        Control.to_json(
            path + "\\json_data\\Control.json",
            orient="records",
            lines=True,
            double_precision=15,
        )
    else:  # USER INPUT CONTROL
        adim_vel = np.arange(0.001, 10, 0.001)
        vel_mean = 0
        # Power levels
        pdf_P = chi2.pdf(
            adim_vel ** 2, 1
        )  # One degree of freedom Chi Square distribution
        # Load levels
        pdf_T = foldnorm.pdf(adim_vel, vel_mean)  # Folded normal distribution
        # Load ranges
        pdf_T_range = rayleigh.pdf(adim_vel)  # Rayleigh distribution

        Control = pd.DataFrame({"adim_vel": [adim_vel]})
        Control["Power_levels"] = [pdf_P]
        Control["Load_levels"] = [pdf_T]
        Control["Load_ranges"] = [pdf_T_range]
        Control.to_json(
            path + "\\json_data\\Control.json",
            orient="records",
            lines=True,
            double_precision=15,
        )
