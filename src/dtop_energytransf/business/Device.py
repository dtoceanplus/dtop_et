# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import numpy as np
import re
from scipy.optimize import curve_fit

from dtop_energytransf.business import PTO as PTO_class
import matplotlib.pyplot as plt
import os
from pathlib import Path
from dtop_energytransf.utils import get_project_root

root = get_project_root()
PATH_STORAGE = os.path.join(root, Path('storage/'))

class Device:
    """
    This class initialises a device with the corresponding PTOs in specific configuration

    All the outputs are given in diccionaries with the following structure:

        variable name = {
            "label": "Displayed name",

            "unit": "-",
                
            "description": "Description of the variable",
                
            "value": value of the variable,

        }


    Args:
        dev_id (int): Device identifier
        technology (str): Device Tecnology. Will be "Wave" or "Tidal"
        dev_inputs (dict): All inputs about the device arquitecture
        PTO_inputs (dict): All inputs about PTOs, four dictionaries one for each object within the PTO   

    Attributes:
        id (dict): structure with the Device ID. Identifier of the device
        Dev_tech (dict): structure with the Technology. Wave or Tidal technology   
        Dev_dof_PTO (dict): structure with the Active degrees of freedom for each PTO (Wave) or Number of rotors of each device (tidal) 
        Dev_par_PTOs (dict): structure with the Number of Parallel PTOs per device
        Dev_Mech_type (dict) : structure with the Type of Mechanical Transformation
        Dev_Elect_type (dict): structure with the Type of Electrical Transformation
        Dev_Grid_type (dict): structure with the Type of Grid Conditioning 
        Dev_Control_strat (dict): structure with the Control Strategy
        Dev_shutdown_flag (dict): structure with the Shutdown Flag
        Dev_cut_in_out (dict): structure with the Cut-in / Cut-off speeds
        Dev_PTO_mass (dict) : structure with the Mass of the PTO
        Dev_elect_mass (dict) : structure with the Mass of the electrical subsystem
        Dev_mech_mass (dict) : structure with the Mass of the mechanical subsystem
        Dev_grid_mass (dict) : structure with the Mass of the grid conditioning subsystem
        Dev_PTO_cost (dict) : structure with the Cost of the PTO
        Dev_rated_power (dict) : structure with the Rated power of the device
        Dev_Env_Conditions (dict) : structure with the Environmental Conditions
        Dev_Captured_Power (dict) : structure with the Characteristics of the capture system Cpto / sigma_v
        Dev_P_Capt (dict) : structure with the Captured power per sea state in kW
        Dev_P_Mech (dict) : structure with the Mechanical power per sea state in kW
        Dev_P_Elect (dict) : structure with the Electrical power per sea state in kW
        Dev_Pr_Grid (dict) : structure with the Reactive power per sea state in kW
        Dev_Pa_Grid (dict) : structure with the Active power per sea state in kW
        Dev_E_Capt (dict) : structure with the Annual Captured Energy in kWh
        Dev_E_Mech (dict) : structure with the Annual Mechanical Energy in kWh
        Dev_E_Elect (dict) : structure with the Annual Electrical Energy in kWh
        Dev_E_Grid (dict) : structure with the Annual Energy delivered at grid in kWh
        PTOs (dict): Dictionary of PTOs

    Returns: 
        none    

    """    
    def __init__(self, dev_id, technology, dev_inputs, PTO_inputs):
             
        #  Device variables
        self.id = {
            "label": "Device ID",
            "description": "Identifier of the device",
            "unit": "-",
            "value": dev_id,
        }
        self.Dev_tech = {
            "label": "Technology",
            "description": "Wave or Tidal technology",
            "unit": "-",
            "value": technology,
        }
        if technology == "Wave":
            self.Dev_dof_PTO = {
                "label": "PTO dofs",
                "description": "Active degrees of freedom for each PTO",
                "unit": "-",
                "value": dev_inputs["dof"],
            }  # Detection of how many dof_PTOs has the dev.
        else: # in case of tidal the device can have different number of rotors
            self.Dev_dof_PTO = {
                "label": "Number of rotors",
                "description": "Number of rotors of each device",
                "unit": "-",
                "value": dev_inputs["dof"],
            }              

        self.Dev_par_PTOs = {
            "label": "Parallel PTOs",
            "description": "Number of Parallel PTOs per device",
            "unit": "-",
            "value": dev_inputs["Parallel_PTOs"],
        }  
        self.Dev_Mech_type = {
            "label": "Mechanical Transformation",
            "description": "Type of Mechanical Transformation",
            "unit": "-",
            "value": PTO_inputs[0]["Type"],
        }
        self.Dev_Elect_type = {
            "label": "Electrical Transformation",
            "description": "Type of Electrical Transformation",
            "unit": "-",
            "value": PTO_inputs[1]["Type"],
        }
        self.Dev_Grid_type = {
            "label": "Grid Conditioning Transformation",
            "description": "Type of Grid Conditioning Transformation",
            "unit": "-",
            "value": PTO_inputs[2]["Type"],
        }
        self.Dev_Control_strat = {
            "label": "Control Strategy",
            "description": "Control Strategy",
            "unit": "-",
            "value": PTO_inputs[3]["Type"],
        }
        self.Dev_shutdown_flag = {
            "label": "Shutdown Flag",
            "description": "Shutdown Flag",
            "unit": "-",
            "value": dev_inputs["Shutdown_flag"],
        }
        self.Dev_cut_in_out = {
            "label": "Cut-in / Cut-off",
            "description": "Cut-in / Cut-off",
            "unit": "rad/s",
            "value": dev_inputs["cut_in_out"],
        }

        # Device design output
        self.Dev_PTO_mass = {
            "label": "PTO Mass",
            "description": "Mass of the PTO",
            "unit": "kg",
            "value": 0.0,
        }
        self.Dev_elect_mass = {
            "label": "Electrical subsystem Mass",
            "description": "Mass of the electrical subsystem",
            "unit": "kg",
            "value": 0.0,
        }
        self.Dev_mech_mass = {
            "label": "Mechanical subsystem Mass",
            "description": "Mass of the mechanical subsystem",
            "unit": "kg",
            "value": 0.0,
        }
        self.Dev_grid_mass = {
            "label": "Grid conditioning Mass",
            "description": "Mass of the grid conditioning subsystem",
            "unit": "kg",
            "value": 0.0,
        }
        self.Dev_PTO_cost = {
            "label": "PTO Cost",
            "description": "Cost of the PTO",
            "unit": "Euros",
            "value": 0.0,
        }
        self.Dev_rated_power = {
            "label": "Rated Power",
            "description": "Rated power of the device",
            "unit": "kW",
            "value": 0.0,
        }  # kW

        # Variables per environmental state
        self.Dev_Env_Conditions = {
            "label": "Environmental Conditions",
            "description": "Environmental Conditions",
            "unit": "-",
            "value": [],
        }
        self.Dev_Captured_Power = {
            "label": "Cpto / sigma_v",
            "description": "Characteristics of the capture system",
            "unit": "-",
            "value": [],
        }
        self.Dev_P_Capt = {
            "label": "Captured Power per sea state",
            "description": "Captured power per sea state",
            "unit": "kW",
            "value": [],
        }
        self.Dev_P_Mech = {
            "label": "Mechanical Power per sea state",
            "description": "Mechanical power per sea state",
            "unit": "kW",
            "value": [],
        }
        self.Dev_P_Elect = {
            "label": "Electrical Power per sea state",
            "description": "Electrical power per sea state",
            "unit": "kW",
            "value": [],
        }
        self.Dev_Pr_Grid = {
            "label": "Reactive power per sea state",
            "description": "Reactive power per sea state",
            "unit": "kW",
            "value": [],
        }
        self.Dev_Pa_Grid = {
            "label": "Active Power per sea state",
            "description": "Active power per sea state",
            "unit": "kW",
            "value": [],
        }
        self.Dev_E_Capt = {
            "label": "Annual Captured Energy",
            "description": "Annual Captured Energy",
            "unit": "kWh",
            "value": [],
        }
        self.Dev_E_Mech = {
            "label": "Annual Mechanical Energy",
            "description": "Annual Captured Energy",
            "unit": "kWh",
            "value": [],
        }
        self.Dev_E_Elect = {
            "label": "Annual Electrical Energy",
            "description": "Annual Electrical Energy",
            "unit": "kWh",
            "value": [],
        }
        self.Dev_E_Grid = {
            "label": "Annual Grid Energy",
            "description": "Annual Energy delivered at grid",
            "unit": "kWh",
            "value": [],
        }

        # PTO objects and outputs
        self.PTOs = {}  # Dictionary of PTOs (dofs), the keys are the IDs
        
        # PTOs are created following the specified inputs
        PTO_number = 0
        for c_PTO_dofs in np.arange(0, self.Dev_dof_PTO["value"], 1):
            for c_par_PTO in np.arange(0, self.Dev_par_PTOs["value"], 1):
                # PTO output dictionary, with all fields defined
                PTO_ID = "PTO_" + str(c_PTO_dofs) + "_" + str(c_par_PTO)
                
                PTO_number = PTO_number+1
                # PTO_name = "PTO_"+str(int(dev_id)+1)+"_"+str(PTO_number)
                self.PTOs[PTO_ID] = {
                    "PTO": PTO_class.PTO(PTO_ID, self.Dev_tech["value"], *PTO_inputs)
                }
                # self.PTOs[PTO_ID]["id"] = {
                #     "label": "PTO ID",
                #     "unit": "-",
                #     "description": "Identifier of the PTO",
                #     "value": self.PTOs[PTO_ID]["PTO"].id,
                # }
                self.PTOs[PTO_ID]["id"] = {
                    "label": "PTO ID",
                    "unit": "-",
                    "description": "Identifier of the PTO",
                    "value": PTO_ID,
                }
                if PTO_inputs[0]["Type"] == "Mech_Simplified": 
                    self.PTOs[PTO_ID]["Mech_size"] = {
                        "label": "Mechanical System Maximum Power (Pmax)",
                        "unit": "kW",
                        "description": "Maximum Power of the mechanical system is 5 times nominal power",
                        "value": self.PTOs[PTO_ID]["PTO"].MechT_obj.size / 1000,
                    }
                elif PTO_inputs[0]["Type"] == "Gearbox" : 
                    self.PTOs[PTO_ID]["Mech_size"] = {
                        "label": "Mechanical System Nominal Power (Pnom)",
                        "unit": "kW",
                        "description": "Nominal Power of the mechanical system",
                        "value": self.PTOs[PTO_ID]["PTO"].MechT_obj.size / 1000,
                    }                    
                elif PTO_inputs[0]["Type"] == "Hydraulic": 
                    self.PTOs[PTO_ID]["Mech_size"] = {
                        "label": "Mechanical System Motor Size",
                        "unit": "m3/rad",
                        "description": "Hydraulic: motor size",
                        "value": self.PTOs[PTO_ID]["PTO"].MechT_obj.size ,
                    }    
                elif PTO_inputs[0]["Type"] == "Direct_Tidal": 
                    self.PTOs[PTO_ID]["Mech_size"] = {
                        "label": "Mechanical System - None",
                        "unit": "-",
                        "description": "Direct drive tidal",
                        "value": "-" ,
                    }       
                else:
                    self.PTOs[PTO_ID]["Mech_size"] = {
                    "label": "Mechanical System - Main dimension",
                    "unit": "m",
                    "description": "Air Turbine diameter",
                    "value": self.PTOs[PTO_ID]["PTO"].MechT_obj.size,
                    }
                self.PTOs[PTO_ID]["MechT_omega"] = {
                    "label": "Mechanical System - output rotational speed",
                    "unit": "rad/s",
                    "description": "Mean rotational speed in the output of the mechanical system. For a better design should be close to the electrical system nominal speed ",
                    "value": [],
                }                    
                self.PTOs[PTO_ID]["Mech_mass"] = {
                    "label": "Mechanical System - mass",
                    "unit": "kg",
                    "description": "Mass of the mechanical system",
                    "value": self.PTOs[PTO_ID]["PTO"].MechT_obj.Mech_mass,
                }
                self.PTOs[PTO_ID]["Mech_cost"] = {
                    "label": "Mechanical System - Cost",
                    "unit": "Euro",
                    "description": "Cost of the mechanical system",
                    "value": self.PTOs[PTO_ID]["PTO"].MechT_obj.Mech_cost,
                }
                self.PTOs[PTO_ID]["Elect_P_rated"] = {
                    "label": "Electrical System - Rated Power",
                    "unit": "kW",
                    "description": "Rated Power of the electrical system",
                    "value": self.PTOs[PTO_ID]["PTO"].ElectT_obj.P_nom / 1000,
                }
                self.PTOs[PTO_ID]["Elect_mass"] = {
                    "label": "Electrical System - Main Dimension",
                    "unit": "kg",
                    "description": "Mass of the electrical system",
                    "value": self.PTOs[PTO_ID]["PTO"].ElectT_obj.Elect_mass,
                }
                self.PTOs[PTO_ID]["Elect_cost"] = {
                    "label": "Electrical System - Cost",
                    "unit": "Euro",
                    "description": "Cost of the electrical system",
                    "value": self.PTOs[PTO_ID]["PTO"].ElectT_obj.Elect_cost,
                }
                self.PTOs[PTO_ID]["ElecT_omega"] = {
                    "label": "Electrical System - rated rotational speed",
                    "unit": "rad/s",
                    "description": "Nominal Rotational speed of the electrical system",
                    "value": [],
                }                 
                self.PTOs[PTO_ID]["Grid_V"] = {
                    "label": "Grid Voltage",
                    "unit": "V",
                    "description": "Voltage of the Grid",
                    "value": self.PTOs[PTO_ID]["PTO"].GridC_obj.V_grid,
                }
                self.PTOs[PTO_ID]["Grid_P_rated"] = {
                    "label": "Grid Conditioning System - Rated Power",
                    "unit": "kW",
                    "description": "Rated Power of the grid conditioning system",
                    "value": self.PTOs[PTO_ID]["PTO"].GridC_obj.P_nom / 1000,
                }
                self.PTOs[PTO_ID]["Grid_mass"] = {
                    "label": "Grid Conditioning System - Mass",
                    "unit": "kg",
                    "description": "Mass of the grid conditioning system",
                    "value": self.PTOs[PTO_ID]["PTO"].GridC_obj.Grid_mass,
                }
                self.PTOs[PTO_ID]["Grid_cost"] = {
                    "label": "Grid Conditioning System - Cost",
                    "unit": "Euro",
                    "description": "Cost of the grid conditioning system",
                    "value": self.PTOs[PTO_ID]["PTO"].GridC_obj.Grid_cost,
                }
                self.PTOs[PTO_ID]["MechT_Damage"] = {
                    "label": "Mechanical System - Damage",
                    "unit": "-",
                    "description": "Damage of the mechanical system",
                    "value": [],
                }
                self.PTOs[PTO_ID]["ElectT_Damage"] = {
                    "label": "Electrical System - Damage",
                    "unit": "-",
                    "description": "Damage of the electrical system",
                    "value": [],
                }
                self.PTOs[PTO_ID]["GridC_Damage"] = {
                    "label": "Grid Conditioning System - Damage",
                    "unit": "-",
                    "description": "Damage of the grid conditioning system",
                    "value": [],
                }
                self.PTOs[PTO_ID]["Captured_Power"] = {
                    "label": "Captured Power",
                    "unit": "kW",
                    "description": "Captured Power",
                    "value": [],
                }
                self.PTOs[PTO_ID]["MechT_Power"] = {
                    "label": "Mechanical Power",
                    "unit": "kW",
                    "description": "Mechanical Power",
                    "value": [],
                }
                self.PTOs[PTO_ID]["ElectT_Power"] = {
                    "label": "Electrical  Power",
                    "unit": "kW",
                    "description": "Electrical Power",
                    "value": [],
                }
                self.PTOs[PTO_ID]["GridC_Active_Power"] = {
                    "label": "Grid Conditioning Active Power",
                    "unit": "kW",
                    "description": "Grid Conditioning Active Power",
                    "value": [],
                }
                self.PTOs[PTO_ID]["GridC_reactive_Power"] = {
                    "label": "Grid Conditioning Reactive Power",
                    "unit": "kVAr",
                    "description": "Grid Conditioning Reactive Power",
                    "value": [],
                }
                self.PTOs[PTO_ID]["MechT_eff"] = {
                    "label": "Mechanical Efficiency",
                    "unit": "%",
                    "description": "Mechanical Transformation efficiency",
                    "value": [],
                }
                self.PTOs[PTO_ID]["ElecT_eff"] = {
                    "label": "Electrical Efficiency",
                    "unit": "%",
                    "description": "Electrical Transformation efficiency",
                    "value": [],
                }
                self.PTOs[PTO_ID]["GridC_eff"] = {
                    "label": "Grid Conditioning Efficiency",
                    "unit": "%",
                    "description": "Grid Conditioning efficiency",
                    "value": [],
                }                                
                self.PTOs[PTO_ID]["Captured_Energy"] = {
                    "label": "Captured energy",
                    "unit": "kWh",
                    "description": "Captured Energy",
                    "value": [],
                }
                self.PTOs[PTO_ID]["MechT_Energy"] = {
                    "label": "Mechanical energy",
                    "unit": "kWh",
                    "description": "Mechanical Energy",
                    "value": [],
                }
                self.PTOs[PTO_ID]["ElectT_Energy"] = {
                    "label": "Electrical energy",
                    "unit": "kWh",
                    "description": "Electrical Energy",
                    "value": [],
                }
                self.PTOs[PTO_ID]["GridC_Energy"] = {
                    "label": "Grid conditioned energy",
                    "unit": "kWh",
                    "description": "Grid Conditioned Energy",
                    "value": [],
                }
                self.PTOs[PTO_ID]["S_N"] = {
                    "label": "SN curve energy",
                    "unit": "-",
                    "description": "SN curve",
                    "value": self.PTOs[PTO_ID]["PTO"].MechT_obj.S_N,
                }
                # print(
                #     "PTO with ID "
                #     + self.id["value"]
                #     + "_"
                #     + PTO_ID
                #     + " has been created"
                # )

                # Accumulated cost and mass for the whole device
                self.Dev_PTO_mass["value"] = round((
                    self.Dev_PTO_mass["value"]
                    + self.PTOs[PTO_ID]["Mech_mass"]["value"]
                    + self.PTOs[PTO_ID]["Elect_mass"]["value"]
                    + self.PTOs[PTO_ID]["Grid_mass"]["value"]
                ), 2)
                self.Dev_elect_mass["value"] = round((
                    self.Dev_elect_mass["value"]
                    + self.PTOs[PTO_ID]["Mech_mass"]["value"]
                ), 2)
                self.Dev_mech_mass["value"] = round((
                    self.Dev_mech_mass["value"]
                    + self.PTOs[PTO_ID]["Elect_mass"]["value"]
                ), 2)
                self.Dev_grid_mass["value"] = round((
                    self.Dev_grid_mass["value"]
                    + self.PTOs[PTO_ID]["Grid_mass"]["value"]
                ),2)
                self.Dev_PTO_cost["value"] = (
                    self.Dev_PTO_cost["value"]
                    + self.PTOs[PTO_ID]["Mech_cost"]["value"]
                    + self.PTOs[PTO_ID]["Elect_cost"]["value"]
                    + self.PTOs[PTO_ID]["Grid_cost"]["value"]
                )
                self.Dev_rated_power["value"] = (
                    self.Dev_rated_power["value"]
                    + self.PTOs[PTO_ID]["Grid_P_rated"]["value"] 
                )  # 1/1000 W to kW

    def performance(self, perf_inputs, env_conds, et_id):
        """
        Method to assess the performance of the device in all environmental conditions. The sigma_v is computed with
        mean captured power and pto damping. However, if there are parallel ptos then the Cpto is equally divided.
        
        Args:
            perf_inputs (list): List of performance inputs of a Device. Includes sigma_v, Cpto and inverse of omega (only in tidal)
            env_conds (list): Corresponding environmental conds: Hs (wave), Vc (tidal), Occ, Tz and site id 
        
        Returns: 
            none

        """

        # -------------------------------------------------------------------------------------------
        # The performance results are only applicable for the set inputted to the function, NOT
        # accumulated with previous performance function runs. Then all perf results of the device are reset here.
        self.Dev_Env_Conditions["value"] = env_conds
        self.Dev_Captured_Power["value"] = perf_inputs
        self.Dev_P_Capt["value"] = {"Power": np.zeros(
            len(self.Dev_Env_Conditions["value"]["Occ"])
        )}
        self.Dev_P_Mech["value"] = {"Power":np.zeros(
            len(self.Dev_Env_Conditions["value"]["Occ"])
        )}
        self.Dev_P_Elect["value"] = {"Power":np.zeros(
           len(self.Dev_Env_Conditions["value"]["Occ"])
        )}
        self.Dev_Pr_Grid["value"] = {"Power":np.zeros(
            len(self.Dev_Env_Conditions["value"]["Occ"])
        )}
        self.Dev_Pa_Grid["value"] = {"Power":np.zeros(
           len(self.Dev_Env_Conditions["value"]["Occ"])
        )}
        self.Dev_P_Capt["value"]["id"]= self.Dev_Env_Conditions["value"]["id"]
        self.Dev_P_Mech["value"]["id"]= self.Dev_Env_Conditions["value"]["id"]
        self.Dev_P_Elect["value"]["id"]= self.Dev_Env_Conditions["value"]["id"]
        self.Dev_Pr_Grid["value"]["id"]= self.Dev_Env_Conditions["value"]["id"]
        self.Dev_Pa_Grid["value"]["id"]= self.Dev_Env_Conditions["value"]["id"]
        self.Dev_E_Capt["value"] = 0
        self.Dev_E_Mech["value"] = 0
        self.Dev_E_Elect["value"] = 0
        self.Dev_E_Grid["value"] = 0
        # --------------------------------------------------

        # Device assessment per Environmental Condition
        for c_PTOs in self.PTOs.keys():
            # print(c_PTOs)
            # -------------------------------------------------------------------------------------------
            # Size reset of damage, power and energy of each PTO
            self.PTOs[c_PTOs]["MechT_Damage"]["value"] = 0.0
            self.PTOs[c_PTOs]["ElectT_Damage"]["value"] = 0.0
            self.PTOs[c_PTOs]["GridC_Damage"]["value"] = 0.0
            self.PTOs[c_PTOs]["Captured_Power"]["value"] = np.zeros(
                len(self.Dev_Env_Conditions["value"]["Occ"])
            )
            self.PTOs[c_PTOs]["MechT_Power"]["value"] = np.zeros(
                len(self.Dev_Env_Conditions["value"]["Occ"])
            )
            self.PTOs[c_PTOs]["MechT_omega"]["value"] = np.zeros(
                len(self.Dev_Env_Conditions["value"]["Occ"])
            )
            self.PTOs[c_PTOs]["ElectT_Power"]["value"] = np.zeros(
                len(self.Dev_Env_Conditions["value"]["Occ"])
            )
            self.PTOs[c_PTOs]["ElecT_omega"]["value"] = np.zeros(
                len(self.Dev_Env_Conditions["value"]["Occ"])
            )            
            self.PTOs[c_PTOs]["GridC_Active_Power"]["value"] = np.zeros(
                len(self.Dev_Env_Conditions["value"]["Occ"])
            )
            self.PTOs[c_PTOs]["GridC_reactive_Power"]["value"] = np.zeros(
                len(self.Dev_Env_Conditions["value"]["Occ"])
            )
            self.PTOs[c_PTOs]["MechT_eff"]["value"] = np.zeros(
                len(self.Dev_Env_Conditions["value"]["Occ"])
            )
            self.PTOs[c_PTOs]["ElecT_eff"]["value"] = np.zeros(
                len(self.Dev_Env_Conditions["value"]["Occ"])
            )
            self.PTOs[c_PTOs]["GridC_eff"]["value"] = np.zeros(
                len(self.Dev_Env_Conditions["value"]["Occ"])
            )                                    
            self.PTOs[c_PTOs]["Captured_Energy"]["value"] = 0
            self.PTOs[c_PTOs]["MechT_Energy"]["value"] = 0
            self.PTOs[c_PTOs]["ElectT_Energy"]["value"] = 0
            self.PTOs[c_PTOs]["GridC_Energy"]["value"] = 0
            # self.PTOs[c_PTOs]["Stress_Loads"] = {
            #     "label": "Stress Loads",
            #     "description": "Stresss Loads",
            #     "unit": "-",
            #     "value": [],
            # }  # np.zeros(len(self.Dev_Env_Conditions['Occ']))
            # self.PTOs[c_PTOs]["Stress_Load_Range"] = {
            #     "label": "Stress Load Range",
            #     "description": "Stress Load Range",
            #     "unit": "-",
            #     "value": [],
            # }  # np.zeros(len(self.Dev_Env_Conditions['Occ']))

            self.PTOs[c_PTOs]["ultimate_stress"] = {
                "label": "Ultimate stress",
                "description": "Ultimate Stress",
                "unit": "-",
                "value": 0,
            }

            self.PTOs[c_PTOs]["maximum_stress_probability"] = {
                "label": "Maximum stress Probability",
                "description": "Stress Probability",
                "unit": "-",
                "value": {"stress" : [], "probability" : []},
            }

            self.PTOs[c_PTOs]["fatigue_stress_probability"] = {
                "label": "Fatigue Stress Probability",
                "description": "Fatigue Stress Probability",
                "unit": "-",
                "value": {"stress" : [], "probability" : []},
            }

            self.PTOs[c_PTOs]["number_cycles"] = {
                "label": "NUme",
                "description": "Fatigue Stress Probability",
                "unit": "-",
                "value": 0,
            }

            fit = False
            loads_flag = False
            # -------------------------------------------------------------------------------------------

            for c_EC in range(0, len(self.Dev_Env_Conditions["value"]["Occ"])):

                # Required parameters for PTO reliability
                EC_time = (
                    365.25 * 24 * 3600 * self.Dev_Env_Conditions["value"]["Occ"][c_EC]
                )
                EC_Tz = self.Dev_Env_Conditions["value"]["Tz"][
                    c_EC
                ]  # Mean period of the spectrum

                
                pto_index = re.findall(r"\d+", c_PTOs)  
                # extract all numbers -- pto indexes -- from c_PTOs. The
                # first number refers to the dof_pto and the second to the par_pto.
                
                # Check if the PTO is working within the cut-in-out range
               
                if self.Dev_tech["value"] == "Tidal":
                    if (
                        self.Dev_Env_Conditions["value"]["Vc"][int(pto_index[0])][c_EC]
                        >= self.Dev_cut_in_out["value"][0]
                        and self.Dev_Env_Conditions["value"]["Vc"][int(pto_index[0])][c_EC]
                        <= self.Dev_cut_in_out["value"][1]
                    ):
                        EC_in = True
                    else:
                        EC_in = False
                elif self.Dev_tech["value"] == "Wave":
                    # if (
                    #     self.Dev_Env_Conditions["value"]["Hs"][c_EC]
                    #     >= self.Dev_cut_in_out["value"][0]
                    #     and self.Dev_Env_Conditions["value"]["Hs"][c_EC]
                    #     <= self.Dev_cut_in_out["value"][1]
                    # ):
                    #     EC_in = True
                    # else:
                    #     EC_in = False
                    EC_in = True

                if EC_in:

                    # Performance input data

                    # sigma_v: Wave:buoy speed; Tidal: Current speed
                    sigma_v = self.Dev_Captured_Power["value"]["sigma_v"][
                        int(pto_index[0])
                    ][c_EC]

                    Cpto = self.Dev_Captured_Power["value"]["C_pto"][int(pto_index[0])][c_EC] 
                    if self.Dev_Env_Conditions["value"]["Occ"][c_EC] == 0:
                        self.Dev_Captured_Power["value"]["C_pto"][int(pto_index[0])][c_EC] = 0


                    # Control_params needs to be updated in the tidal case to account for the mean current velocity
                    # while the Wave case does not need to be updated
                    if self.Dev_tech["value"] == "Tidal":
                        
                        Cpto = self.Dev_Captured_Power["value"]["C_pto"][int(pto_index[0])][c_EC]    
                        # The control object is updated with the mean current speed
                        self.PTOs[c_PTOs]["PTO"].Control_obj.adim_vel_mean = (
                            self.Dev_Env_Conditions["value"]["Vc"][int(pto_index[0])][c_EC] / sigma_v
                        )
                        getattr(
                            self.PTOs[c_PTOs]["PTO"].Control_obj,
                            self.PTOs[c_PTOs]["PTO"].Control_obj.type,
                        )()
                       
                        # The PTO.MechT_perf_args is updated to compute the torque on the shaft
                       

                        self.PTOs[c_PTOs]["PTO"].MechT_perf_args["Ct_tidal"] = self.Dev_Captured_Power["value"]["Ct_tidal"][
                            int(pto_index[0])
                        ][c_EC]
                     
                        # The captured power is updated with the corresponding equation4
                        # self.PTOs[c_PTOs]["Captured_Power"]["value"][c_EC] = self.PTOs[c_PTOs]["PTO"].MechT_obj.P_capt
                    
                        self.PTOs[c_PTOs]["Captured_Power"]["value"][c_EC] = round(((
                            Cpto/ self.Dev_par_PTOs["value"])* self.Dev_Env_Conditions["value"]["Vc"][int(pto_index[0])][c_EC] ** 2 )/1000, 2) # kW
                        
                    elif self.Dev_tech["value"] == "Wave":
                        # The captured power is updated with the corresponding equation
                        self.PTOs[c_PTOs]["Captured_Power"]["value"][c_EC] = round(((
                            Cpto / self.Dev_par_PTOs["value"]) * sigma_v ** 2)/1000, 2) # kW


                    # Performance assessment of the PTO
                    self.PTOs[c_PTOs]["PTO"].performance(
                        Cpto / self.Dev_par_PTOs["value"], sigma_v
                    )

                    if self.Dev_tech["value"] == "Tidal":
                        self.PTOs[c_PTOs]["Captured_Power"]["value"][c_EC] = round(self.Dev_Captured_Power["value"]["Captured_Power_EC"][((int(self.id["value"]) ) * self.Dev_dof_PTO["value"])+ int(pto_index[0])][c_EC]/1000,3)
                    elif self.Dev_tech["value"] == "Wave":
                        self.PTOs[c_PTOs]["Captured_Power"]["value"][c_EC] =  round(np.sum(self.PTOs[c_PTOs]["PTO"].MechT_obj.Pcapt)/1000,2) # OLD EQUATION

                    # self.PTOs[c_PTOs]["Captured_Power"]["value"][c_EC] =  round(np.sum(self.PTOs[c_PTOs]["PTO"].MechT_obj.Pcapt)/1000,2) # OLD EQUATION
               
                    # Save plots of each PTO performance
                    # Velocity vector defining velocity ranges occurring within the sea state.
                    vels = sigma_v * self.PTOs[c_PTOs]["PTO"].Control_obj.adim_vel[0:-1]
                    P_levels = (Cpto /self.Dev_par_PTOs["value"]) * vels**2 
                    P_mean = self.PTOs[c_PTOs]["PTO"].Control_obj.prob_P * P_levels
                    plt.rcParams.update({'figure.max_open_warning': 0})
                    plt.figure()
                    plt.plot(P_levels, P_mean, P_levels, self.PTOs[c_PTOs]["PTO"].MechT_obj.Mech_P_mean, P_levels, self.PTOs[c_PTOs]["PTO"].ElectT_obj.Elect_P_mean, P_levels, self.PTOs[c_PTOs]["PTO"].GridC_obj.Grid_P_mean),\
                    plt.legend(['P_capt', 'P_mech', 'P_elect', 'P_grid']) #,\
                    idplot = self.Dev_Env_Conditions["value"]["id"][c_EC]
                    plt.title('Mean Power of each transformation stage: Dev ' + self.id["value"] + '_' + c_PTOs +"_SS_" + str(idplot))
                    plt.xlabel('Power W')
                    plt.ylabel('Mean Power W')
                    root = get_project_root()
                    
                    path_save = os.path.join(PATH_STORAGE, "figures", et_id)
                    os.makedirs(path_save, exist_ok=True)                    
                    name_figure = "ET_" + et_id + "_Dev_" + self.id["value"] + "_" + c_PTOs + "_Site_" + str(idplot) + "_Performance.png"
                    path_figure = os.path.join(path_save, name_figure)
                    # folder = './src/dtop_energytransf/storage/figures/'
                    # figure_name = os.path.join(folder + 'ET_' + et_id + '_Dev_' + self.id["value"] + '_' + c_PTOs + '_Performance.png')
                    plt.savefig (path_figure)
                    plt.close('all')
                    # plt.show()

                    # ---------------------Mechanical Loads for RAMS---------------------------
                    # Note: The differences between wave and tidal for loads pdf are due to being proportional to the
                    # speed (buoy) in the wave case and to the square of the speed (current) in the tidal case
                    # -------------------------------------------------------------------------
                    # if not loads_flag:
                    # if self.Dev_tech == 'Tidal':
                    #    t_prob_act = Cpto * self.Dev_Captured_Power['Ct_tidal'][int(pto_index[0])][
                    #        c_EC] / self.Dev_par_PTOs * self.PTOs[c_PTOs]['PTO'].Control_obj.adim_vel[
                    #                                    0:-1] * sigma_v**2
                    # elif self.Dev_tech == 'Wave':

                    #   t_prob_act = Cpto / self.Dev_par_PTOs * self.PTOs[c_PTOs]['PTO'].Control_obj.adim_vel[
                    #                                            0:-1] * sigma_v
                    # v_max = (self.PTOs[c_PTOs]['PTO'].Control_obj.adim_vel[-1]) * sigma_v
                    # estreses por sea states
                    # self.PTOs[c_PTOs]['MechT_Loads'].append( [self.PTOs[c_PTOs]['PTO'].Control_obj.adim_vel[0:-1]*sigma_v,
                    #                                         self.PTOs[c_PTOs]['PTO'].MechT_obj.tao,
                    #                                         self.PTOs[c_PTOs]['PTO'].Control_obj.prob_T *
                    #                                         self.Dev_Env_Conditions['Occ'][c_EC]])
                    #
                    # self.PTOs[c_PTOs]['MechT_Load_Range'] = [
                    #         self.PTOs[c_PTOs]['PTO'].Control_obj.adim_vel[0:-1] * sigma_v,
                    #         self.PTOs[c_PTOs]['PTO'].MechT_obj.tao,
                    #         self.PTOs[c_PTOs]['PTO'].Control_obj.prob_T_range *
                    #         self.Dev_Env_Conditions['Occ'][c_EC]]
                    # loads_flag = True
                    # else:
                    #
                    #     #Reference velocity vector
                    #     v_max = np.max([v_max, (self.PTOs[c_PTOs]['PTO'].Control_obj.adim_vel[-1]) * sigma_v])
                    #     vel_prob_act = np.arange(0.01, v_max, (v_max-0.01) / self.PTOs[c_PTOs]['PTO'].Control_obj.bins)
                    #
                    #     # Actual load distribution
                    #     vel_pdf_act = (self.PTOs[c_PTOs]['PTO'].Control_obj.adim_vel) * sigma_v
                    #     pdf_v_act = self.PTOs[c_PTOs]['PTO'].Control_obj.pdf_T
                    #     if self.Dev_tech == 'Tidal':
                    #         prob_v_act = np.interp(vel_prob_act[:-1], vel_pdf_act, pdf_v_act) *\
                    #                      np.diff((vel_prob_act / sigma_v)**2)
                    #     elif self.Dev_tech == 'Wave':
                    #         prob_v_act = np.interp(vel_prob_act[:-1], vel_pdf_act, pdf_v_act) * \
                    #                      np.diff(vel_prob_act) / sigma_v
                    #
                    #     # Actual load-range distribution
                    #     pdf_vr_act = self.PTOs[c_PTOs]['PTO'].Control_obj.pdf_T_range
                    #     prob_vr_act = np.interp(vel_prob_act[:-1], vel_pdf_act, pdf_vr_act) * \
                    #                   np.diff(vel_prob_act) / sigma_v
                    #
                    #     # Already existing load distribution
                    #     if self.Dev_tech == 'Tidal':
                    #         vel_pdf_ex = self.PTOs[c_PTOs]['MechT_Loads'][0]
                    #         pdf_t_ex = self.PTOs[c_PTOs]['MechT_Loads'][2][:-1] / np.diff(vel_pdf_ex**2)
                    #         prob_t_ex = np.interp(vel_prob_act[:-1], vel_pdf_ex[:-1], pdf_t_ex) * np.diff(
                    #             vel_prob_act ** 2)
                    #     elif self.Dev_tech == 'Wave':
                    #         vel_pdf_ex = self.PTOs[c_PTOs]['MechT_Loads'][0]
                    #         pdf_t_ex = self.PTOs[c_PTOs]['MechT_Loads'][2][:-1] / np.diff(vel_pdf_ex)
                    #         prob_t_ex = np.interp(vel_prob_act[:-1], vel_pdf_ex[:-1], pdf_t_ex) * np.diff(vel_prob_act)
                    #
                    #     # Already existing load-range distribution
                    #     velr_pdf_ex = self.PTOs[c_PTOs]['MechT_Load_Range'][0]
                    #     pdf_tr_ex = self.PTOs[c_PTOs]['MechT_Load_Range'][2][:-1] / np.diff(vel_pdf_ex)
                    #     prob_tr_ex = np.interp(vel_prob_act[:-1], velr_pdf_ex[:-1], pdf_tr_ex) * np.diff(
                    #         vel_prob_act)
                    #
                    #     # Fitting of the velocity-torque relation, in the first production EC
                    #     # (considered representative, it is a reasonable approximation)
                    #     if not fit:
                    #         if self.Dev_tech == 'Tidal':
                    #             C_mean = np.mean(self.Dev_Captured_Power['C_pto'][int(pto_index[0])]) * np.mean(
                    #                 self.Dev_Captured_Power['Ct_tidal'][int(pto_index[0])])
                    #         elif self.Dev_tech == 'Wave':
                    #             C_mean = np.mean(self.Dev_Captured_Power['C_pto'][int(pto_index[0])])
                    #         fit = True
                    #
                    #     if self.Dev_tech == 'Tidal':
                    #         t_prob_act = C_mean / self.Dev_par_PTOs * vel_prob_act**2
                    #     elif self.Dev_tech == 'Wave':
                    #         t_prob_act = C_mean / self.Dev_par_PTOs * vel_prob_act
                    #
                    #     # Accumulated MechT_loads over the Environmental Conditions
                    #     self.PTOs[c_PTOs]['MechT_Loads'] = [vel_prob_act[0:-1],
                    #                                     t_prob_act[0:-1],
                    #                                     prob_v_act * self.Dev_Env_Conditions['Occ'][c_EC] +
                    #                                     prob_t_ex]
                    #
                    #     # Accumulated MechT_load-range over the Environmental Conditions
                    #     self.PTOs[c_PTOs]['MechT_Load_Range'] = [vel_prob_act[0:-1],
                    #                                          t_prob_act[0:-1],
                    #                                          prob_vr_act * self.Dev_Env_Conditions['Occ'][c_EC] +
                    #                                          prob_tr_ex]
                    # -------------------------------------------------------------------------------
                    # ----------------END of Mechanical loads computation-----------------
                    # -------------------------------------------------------------------------------

                    # Mean power per sea state of the PTO
                    self.PTOs[c_PTOs]["MechT_Power"]["value"][c_EC] = round((
                        np.sum(self.PTOs[c_PTOs]["PTO"].MechT_obj.Mech_P_mean) / 1e3
                    ),2)
                    self.PTOs[c_PTOs]["ElectT_Power"]["value"][c_EC] = round((
                        np.sum(self.PTOs[c_PTOs]["PTO"].ElectT_obj.Elect_P_mean) / 1e3
                    ),2)
                    self.PTOs[c_PTOs]["GridC_Active_Power"]["value"][c_EC] = round((
                        np.sum(self.PTOs[c_PTOs]["PTO"].GridC_obj.Grid_P_mean) / 1e3
                    ), 2)
                    self.PTOs[c_PTOs]["GridC_reactive_Power"]["value"][c_EC] = round((
                        np.sum(self.PTOs[c_PTOs]["PTO"].GridC_obj.Grid_P_mean_r) / 1e3
                    ), 2)
                    self.PTOs[c_PTOs]["MechT_eff"]["value"][c_EC] = round((
                        np.sum(self.PTOs[c_PTOs]["PTO"].MechT_obj.Mech_eff_mean) * 100
                    ), 2)   
                    self.PTOs[c_PTOs]["ElecT_eff"]["value"][c_EC] = round((
                        np.sum(self.PTOs[c_PTOs]["PTO"].ElectT_obj.Elect_eff_mean) * 100
                    ), 2)
                    self.PTOs[c_PTOs]["GridC_eff"]["value"][c_EC] = round((
                        np.sum(self.PTOs[c_PTOs]["PTO"].GridC_obj.Grid_eff_mean) * 100
                    ), 2)                                                         

                    # mechanical and electrical system rotational speed for outputs
                    omega_mean = 0
                    cont_nozero = 0
                    
                    for contw in range(0, len(self.PTOs[c_PTOs]["PTO"].ElectT_obj.omega_elect)):
                        if self.PTOs[c_PTOs]["PTO"].ElectT_obj.omega_elect[contw] != 0 :
                            omega_mean = omega_mean + self.PTOs[c_PTOs]["PTO"].ElectT_obj.omega_elect[contw]
                            cont_nozero = cont_nozero +1
                    if (self.Dev_Env_Conditions["value"]["Occ"][c_EC]!=0) & (cont_nozero!=0):
                        omega_mean = round((omega_mean / cont_nozero), 2)
                    else:
                        omega_mean = 0    
                    
                    self.PTOs[c_PTOs]["MechT_omega"]["value"][c_EC] = round(omega_mean, 2)
                    # self.PTOs[c_PTOs]["ElecT_omega"]["value"][c_EC] = round(self.PTOs[c_PTOs]["PTO"].ElectT_obj.om_nom, 2)
                    # Mean power per Sea state of the device
                    self.Dev_P_Capt["value"]["Power"][c_EC] = round((
                        self.Dev_P_Capt["value"]["Power"][c_EC]
                        + self.PTOs[c_PTOs]["Captured_Power"]["value"][c_EC] 
                    ),2)  
                    self.Dev_P_Mech["value"]["Power"][c_EC] = round((
                        self.Dev_P_Mech["value"]["Power"][c_EC]
                        + self.PTOs[c_PTOs]["MechT_Power"]["value"][c_EC]
                    ),2)
                    self.Dev_P_Elect["value"]["Power"][c_EC] = round((
                        self.Dev_P_Elect["value"]["Power"][c_EC]
                        + self.PTOs[c_PTOs]["ElectT_Power"]["value"][c_EC]
                    ),2)
                    self.Dev_Pa_Grid["value"]["Power"][c_EC] = round((
                        self.Dev_Pa_Grid["value"]["Power"][c_EC]
                        + self.PTOs[c_PTOs]["GridC_Active_Power"]["value"][c_EC]
                    ),2)
                    self.Dev_Pr_Grid["value"]["Power"][c_EC] = round((
                        self.Dev_Pr_Grid["value"]["Power"][c_EC]
                        + self.PTOs[c_PTOs]["GridC_reactive_Power"]["value"][c_EC]
                    ),2)

                    # Reliability assessment of each PTO
                    self.PTOs[c_PTOs]["PTO"].reliability(EC_time, EC_Tz)

                    # Damage over all environmental states
                    self.PTOs[c_PTOs]["MechT_Damage"]["value"] = (
                        self.PTOs[c_PTOs]["MechT_Damage"]["value"]
                        + self.PTOs[c_PTOs]["PTO"].MechT_obj.Mech_damage
                    )                    
                    self.PTOs[c_PTOs]["ElectT_Damage"]["value"] = (
                        self.PTOs[c_PTOs]["ElectT_Damage"]["value"]
                        + self.PTOs[c_PTOs]["PTO"].ElectT_obj.Elect_damage
                    )
                    self.PTOs[c_PTOs]["GridC_Damage"]["value"] = (
                        self.PTOs[c_PTOs]["GridC_Damage"]["value"]
                        + self.PTOs[c_PTOs]["PTO"].GridC_obj.Grid_damage
                    )

                    self.PTOs[c_PTOs]["ultimate_stress"]["value"] = self.PTOs[c_PTOs]["PTO"].MechT_obj.ultimate_stress

                    self.PTOs[c_PTOs]["maximum_stress_probability"]["value"]["stress"] = self.PTOs[c_PTOs]["PTO"].MechT_obj.tao

                    self.PTOs[c_PTOs]["maximum_stress_probability"]["value"]["probability"] = self.PTOs[c_PTOs]["PTO"].Control_obj.prob_T* self.Dev_Env_Conditions["value"]["Occ"][c_EC]

                    self.PTOs[c_PTOs]["fatigue_stress_probability"]["value"]["stress"] = self.PTOs[c_PTOs]["PTO"].MechT_obj.tao

                    self.PTOs[c_PTOs]["fatigue_stress_probability"]["value"]["probability"] = self.PTOs[c_PTOs]["PTO"].Control_obj.prob_T_range* self.Dev_Env_Conditions["value"]["Occ"][c_EC]

                    self.PTOs[c_PTOs]["number_cycles"]["value"] = self.PTOs[c_PTOs]["PTO"].MechT_obj.N
                    # self.PTOs[c_PTOs]["Stress_Loads"]["value"].append(
                    #     [
                    #         self.PTOs[c_PTOs]["PTO"].Control_obj.adim_vel[0:-1]
                    #         * sigma_v,
                    #         self.PTOs[c_PTOs]["PTO"].MechT_obj.tao,
                    #         self.PTOs[c_PTOs]["PTO"].Control_obj.prob_T
                    #         * self.Dev_Env_Conditions["value"]["Occ"][c_EC],
                    #     ]
                    # )

                                      
                    # self.PTOs[c_PTOs]["Stress_Load_Range"]["value"].append(
                    #     [
                    #         self.PTOs[c_PTOs]["PTO"].Control_obj.adim_vel[0:-1]
                    #         * sigma_v,
                    #         self.PTOs[c_PTOs]["PTO"].MechT_obj.tao,
                    #         self.PTOs[c_PTOs]["PTO"].MechT_obj.N,
                    #         self.PTOs[c_PTOs]["PTO"].Control_obj.prob_T_range
                    #         * self.Dev_Env_Conditions["value"]["Occ"][c_EC],
                    #         self.PTOs[c_PTOs]["PTO"].MechT_obj.ultimate_stress,
                    #     ]
                    # )

                    
            self.PTOs[c_PTOs]["ElecT_omega"]["value"] = round(self.PTOs[c_PTOs]["PTO"].ElectT_obj.om_nom, 2)
            # Annual produced Energy of each PTO
            self.PTOs[c_PTOs]["Captured_Energy"]["value"] = round((
                np.sum(
                    np.array(self.PTOs[c_PTOs]["Captured_Power"]["value"])
                    * np.array(self.Dev_Env_Conditions["value"]["Occ"])
                )
                * 24
                * 365.25 
            ),2)  # kWh
            self.PTOs[c_PTOs]["MechT_Energy"]["value"] = round((
                np.sum(
                    np.array(self.PTOs[c_PTOs]["MechT_Power"]["value"])
                    * np.array(self.Dev_Env_Conditions["value"]["Occ"])
                )
                * 24
                * 365.25 
            ),2)  # kWh
            self.PTOs[c_PTOs]["ElectT_Energy"]["value"] = round((
                np.sum(
                    np.array(self.PTOs[c_PTOs]["ElectT_Power"]["value"])
                    * np.array(self.Dev_Env_Conditions["value"]["Occ"])
                )
                * 24
                * 365.25 
            ), 2)  # kWh
            self.PTOs[c_PTOs]["GridC_Energy"]["value"] = round((
                np.sum(
                    np.array(self.PTOs[c_PTOs]["GridC_Active_Power"]["value"])
                    * np.array(self.Dev_Env_Conditions["value"]["Occ"])
                )
                * 24
                * 365.25 
            ), 2)  # kWh

            # print(
            #     "PTO performance assessment with ID "
            #     + self.id["value"]
            #     + "_"
            #     + c_PTOs
            #     + " has been done"
            # )

        # After the assessment of all PTOs over all Sea States the Energy of the device is computed  #  Wh
        self.Dev_E_Capt["value"] = round((
            np.sum(
                np.array(self.Dev_P_Capt["value"]["Power"])
                * np.array(self.Dev_Env_Conditions["value"]["Occ"])
            )
            * 24
            * 365.25
        ), 2) # kWh
        self.Dev_E_Mech["value"] = round((
            np.sum(
                np.array(self.Dev_P_Mech["value"]["Power"])
                * np.array(self.Dev_Env_Conditions["value"]["Occ"])
            )
            * 24
            * 365.25
        ), 2)
        self.Dev_E_Elect["value"] = round((
            np.sum(
                np.array(self.Dev_P_Elect["value"]["Power"])
                * np.array(self.Dev_Env_Conditions["value"]["Occ"])
            )
            * 24
            * 365.25
        ),2)
        self.Dev_E_Grid["value"] = round((
            np.sum(
                np.array(self.Dev_Pa_Grid["value"]["Power"])
                * np.array(self.Dev_Env_Conditions["value"]["Occ"])
            )
            * 24
            * 365.25
        ),2)
