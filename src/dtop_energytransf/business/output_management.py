# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import copy
import json
import types

import pandas as pd
import numpy as np

from dtop_energytransf.storage.models.models import ArrayResults, DeviceResults, PtoResults, Representation
from ..service import db


class AnalysisResults:
    """
    Manage the outputs of analysis mode.

    Functions for saving results and deleting results from the local database.
    In the case where an analysis is "re-run" (i.e. the user changes inputs and presses the "Run" button a second time),
    it is necessary to first delete all the rows corresponding to the first run, and then add the lates results.
    This is necessary because if certain inputs change, for example the number of devices or PTOs, then the number of
    rows in the database corresponding to the same study will be affected. For instance if the number of PTOs changes
    from 6 to 4, there will be two "hanging" rows in the database that should not be there. For this reason, the
    workflow is to first delete all corresponding rows, and then add the new results.
    
    Checks to see if there is an existing row for the ArrayResults in the database.

    If there is then deletes this row (and the associated device and pto entries).

    The class then adds the new results to the database.

    :param dtop_energytransf.business.analysis.RunAnalysis analysis: instance of the RunAnalysis class
    
    Attributes:
        _analysis (inst): instance of the RunAnalysis class (dtop_energytransf.business.analysis.RunAnalysis)
        _study (inst): Instance of the Energy Transformation Class
        _array_results_db (inst) : instance of the ArrayResults model
    """

    def __init__(self, analysis):

        self._analysis = analysis
        self._study = analysis.study

        self._array_results_db = ArrayResults.query.filter(ArrayResults.et_study_id == self._study.id)
        if self._array_results_db.all():
            # Delete all Array, Device and PTO results if study is re-run
            # Required in case number of parallel PTOs input is changed and there are 'hanging' PTOs in the results
            self._delete_analysis_results()
        self._add_analysis_results()

    def _add_analysis_results(self):
        """Loop through the results and add array, device and pto data to the local database"""
        array_json = json.loads(json.dumps(self._analysis.array_results.__dict__, sort_keys=True, cls=NpEncoder))
        device_json = json.loads(json.dumps(self._analysis.device_results, sort_keys=True, cls=NpEncoder))
        pto_json = json.loads(json.dumps(self._analysis.pto_results, sort_keys=True, cls=NpEncoder))
        # Add array results
        array_db = ArrayResults(**array_json)
        array_db.et_study = self._study
        db.session.add(array_db)
        # Add device results
        for i, device in enumerate(device_json):
            device_db = DeviceResults(**device)
            device_db.et_study = self._study
            device_db.array_results = array_db
            db.session.add(device_db)
            # Add PTO results
            for pto_k in pto_json[i]:
                pto_db = PtoResults(**pto_k)
                pto_db.et_study = self._study
                pto_db.array_results = array_db
                pto_db.device_results = device_db
                db.session.add(pto_db)
        
        # Add Digital Represention
        representation = Representation(**self._analysis._representation_data)
        representation.et_study = self._study
        db.session.add(representation)

        self._study.status = 100
        db.session.commit()
    
    def _delete_analysis_results(self):
        """Delete the array, device and pto results from the database for the specified study"""
        db.session.delete(self._array_results_db.first())
        db.session.commit()


class NpEncoder(json.JSONEncoder):
    """Class for converting array, device and PTO instances to JSON data"""

    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        elif isinstance(obj, pd.Series):
            return obj.to_json()
        elif isinstance(obj, types.FunctionType):
            pass
        else:
            return super(NpEncoder, self).__dict__
