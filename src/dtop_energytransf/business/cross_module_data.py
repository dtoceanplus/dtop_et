# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import json
import os
from marshmallow.fields import String
from statistics import mean

from flask import make_response, jsonify

import numpy as np
import requests
from dtop_energytransf.utils import get_project_root
from dtop_energytransf.service import db
from dtop_energytransf.storage.catalogue_data import *  # TODO: remove after catalogue testing

def reshape_pto_damping_matrix(mat, site_conditions, tp_machine, hs_machine, dir_machine):
    """ZOH interpolation of numpy matrixes between the isolated scatter diagram and the lease area scatter diagram
    
    Args:
        mat (list): numpy array to be used
        site_conditions (list): metocean conditions at the lease area site, stored as dictionary with keys Hs, Tp and Dir. The data is a UNIQUE SET (no duplicates) provided as lists.
        tp_machine (list): list of wave periods (Tp) in s from the MC module, UNIQUE SET (no duplicates)
        hs_machine (list): list of wave heights (Hs) in m from the MC module, UNIQUE SET (no duplicates)
        dir_machine (list): list of wave directions (Dir) in deg from the MC module, UNIQUE SET (no duplicates)
    
    Returns: 
        out_mat (list): zoh copy of the input matrix mat
    """

    # ensure all the variable are unique
    # site
    tp_site = np.array(np.unique(site_conditions['Tp']))
    hs_site = np.array(np.unique(site_conditions['Hs']))
    dir_site = np.array(np.unique(site_conditions['Dir']))
    # machine
    tp_machine = np.unique(tp_machine)
    hs_machine = np.unique(hs_machine)
    dir_machine = np.unique(dir_machine)

    n_site_tp = len(tp_site)
    n_site_hs = len(hs_site)
    n_site_dir = len(dir_site)
    mat_numpy = np.array(mat)
    n_dof = mat_numpy.shape[-1]
    i_per = np.argmin(np.abs(np.subtract.outer(np.unique(tp_machine), tp_site)), 0)
    i_hei = np.argmin(np.abs(np.subtract.outer(np.unique(hs_machine), hs_site)), 0)
    i_dir = np.argmin(np.abs(np.subtract.outer(np.unique(dir_machine), dir_site)), 0)
    out_mat = np.zeros((n_site_tp, n_site_hs, n_site_dir, n_dof, n_dof))
    for iper in range(n_site_tp):
        for ihei in range(n_site_hs):
            for idir in range(n_site_dir):
                out_mat[iper, ihei, idir, :, :] = mat_numpy[
                    i_per[iper], i_hei[ihei], i_dir[idir], :, :]
    
    return out_mat.tolist()


def get_url_and_headers(module):
    """
    Common function provided by OCC to get DTOP basic module URL and headers

    :param str module: the DTOP module nickname (e.g. 'mm')

    :return: the module API url and the headers instance
    :rtype: tuple
    """
    protocol = os.getenv("DTOP_PROTOCOL", "http")
    domain = os.environ["DTOP_DOMAIN"]
    auth = os.getenv("DTOP_AUTH", "")

    # print()
    # print("Authorization header value :")
    header_auth = f"{auth}"
    # print(header_auth)

    # print()
    # print("Headers - auth :")
    headers = {"Authorization": header_auth}
    # print(headers)

    # print()
    # print("Host header value :")
    header_host = f'{module + "." + domain}'
    # print(header_host)

    server_url = f'{protocol + "://" + header_host}'

    # print()
    # print("Check if DTOP is deployed on DNS resolved server or on workstation :")

    try:
        response = requests.get(server_url, headers=headers)
        # print("DTOP appears to be deployed on DNS resolved server")
        module_api_url = f"{server_url}"
    except requests.ConnectionError as exception:
        # print("DTOP appears to be deployed on workstation")
        docker_ws_ip = "http://172.17.0.1:80"
        module_api_url = f"{docker_ws_ip}"
        # print()
        # print("Headers - auth and localhost :")
        headers["Host"] = header_host
        # print(headers)

    # print()
    # print("Basic Module Open API URL :")
    # print(module_api_url)
    # print()

    return module_api_url, headers


def call_intermodule_service(module, uri):
    """
    Generic function for communicating directly with the BE of other modules.

    Uses the *get_url_and_headers* function.
    Leads to a 400 error if the module server is not available.
    If the request is unsuccessful, a HTTP error is returned.

    :param str module: the nickname for the module that is being communicated with
    :param str uri: the URI specifying the appropriate route to call for the metric
    :return: the request response if successful, tuple of error and status code otherwise
    :rtype: requests.Response or tuple
    """
    # TODO: update print statements
    # print()
    # print("Test example - request from Catalog BE to Main Module BE to get users' list")

    # print()
    api_url, headers = get_url_and_headers(module)
    # print("MM Open API URL to Get users :")
    users_url = f"{api_url + uri}"
    # print(users_url)

    # print()
    # print("CM BE to MM BE request and response :")
    # print()

    try:
        resp = requests.get(users_url, headers=headers)
        resp.raise_for_status()
        # print(resp.json())
        return resp
    except requests.exceptions.ConnectionError as errc:
        # print("Error Connecting:", errc)
        # print("The module server is not available")

        resp = make_response(jsonify({"message": str(errc)}), 400)
        return resp
    except requests.exceptions.HTTPError as errh:
        # print("HTTP Error:", errh)
        resp = make_response(jsonify({"message": str(errh)}), 400)
        return resp
        # return errh, errh.response.status_code

class ProcessSiteCharacterisation(object):
    """
    Process the output data coming from the Site Characterisation (SC) module.

    Requires the appropriate URL from the SC module to be initialised.

    The **sc_data** property contains the processed SC data required as inputs to the Energy Transformation module.
    
    The initialisation process is as follows

        - retrieve the SC data using the provided URL

        - process the SC data and

        - save the process data to the local ET database 

    Args:
        study (inst): instance of the Energy Transformation study 
        data (dict): all the inputs from Site Characterisation module for the study (or from the json in standalone)
        url (str, optional): The url for the SC output results. Defaults to None.
    
    Attributes:
        study (inst): Instance of the Energy Transformation study 
        _url (str): The url for the SC output results
        _sc_results (dict): Raw inputs from SC module or from the json in standalone
        _processed_sc_data (dict): Inputs from SC processed as needed for Energy Transformation
        validation_error (bool): Validation error flag. Default to False    
        _processed_mc_data (dict): Inputs from MC processed as needed for Energy Transformation

    Returns:
        none    
    """

    def __init__(self, study, data, url=None):

        #http://localhost:{port}/sc/{ProjectId}/farm/scenarii_matrices/currents

        self.study = study
        self._url = url
        self._sc_results = {}
        self._processed_sc_data = {}
        self.validation_error = False
        self._processed_mc_data = {}

        if (self.study.site_characterisation_study_id>0): 
            self._get_sc_results(self.study.site_characterisation_study_id, self.study.machine_characterisation_study_id) # TODO: insert url
            self._validate_sc_inputs()
        else:
            self._sc_results = data
            self._validate_sc_inputs()

        # if self.study.standalone: #standalone mode
        #     self._sc_results = data
        #     self._validate_sc_inputs()
        # else: #integrated mode
        #     self._get_sc_results() # TODO: insert url
        #     self._validate_sc_inputs()
        
        if not self.validation_error:
            self._process_sc_data()
            self._save_sc_data()

    
    def _get_sc_results(self, scId,mcId):
        """Get Site Characterisation results for the requested DTO+ project.

        Use the provided URL to make a request to SC module and return the results.

        Args:
            scId (int): SC study id
            mcId (int): MC study id
        
        Returns:
            none            
        """        

        if self.study.machine_characterisation_data.technology== "Wave": # WAVE cpx1

            if self.study.energy_capture_data.ec_cmpx == 1: 

                # url1 =  http://localhost:{port}/sc/{ProjectId}/point/statistics/waves/Basic
                url1 = '/sc/'+str(scId)+'/point/statistics/waves/Basic'

                data = call_intermodule_service("sc", url1 ).json()
                # data = requests.get(url1).json()
                self._sc_results = {"hs" : {"mean" : data["hs"]["mean"]}, "tp" : {"mean" : data["tp"]["mean"]}}
  
            else: # WAVE cpx2/3
                
                url1 = '/sc/'+str(scId)+'/point/statistics/waves/EJPD3v/hs_tp_dp_flattened'
                data_url1 = call_intermodule_service("sc", url1 ).json()
                url2 = '/sc/'+str(scId)+'/point/statistics/waves/Basic'
                data_url2 = call_intermodule_service("sc", url2 ).json()

                self._sc_results = {}
                self._sc_results["pdf"] = data_url1["pdf"]
                self._sc_results["bins1"] = data_url1["bins1"]
                self._sc_results["bins2"] = data_url1["bins2"]
                self._sc_results["bins3"] = data_url1["bins3"]
                self._sc_results["id"] = data_url1["id"]
                self._sc_results["tp"] = {"mean" : data_url2["tp"]["mean"]}
                url3 ='/mc/'+str(mcId)+'/model/wec/complexity3'               
                data_url3 = call_intermodule_service('mc', url3).json()

                self._sc_results["hs_mc"] = data_url3["hs_capture_width"]
                self._sc_results["tp_mc"] = data_url3["tp_capture_width"]
                self._sc_results["dir_mc"] = data_url3["wave_angle_capture_width"]
                
                # hs, tp, dir = np.meshgrid(np.arange(0.25,8.75,0.5),np.arange(4.5,18,1), [0])
                # self._sc_results["hs_mc"] = hs.ravel().tolist()
                # self._sc_results["tp_mc"] = tp.ravel().tolist()
                # self._sc_results["dir_mc"] = dir.ravel().tolist()
        else: # Tidal cpx 1/2

            url1 =  '/sc/'+str(scId)+'/point/statistics/waves/Basic'

            if self.study.energy_capture_data.ec_cmpx == 1 or self.study.energy_capture_data.ec_cmpx == 2 :
                
                data_url1 = call_intermodule_service("sc", url1 ).json()
                self._sc_results = {}
                self._sc_results = {"hs" : {"mean" : data_url1["hs"]["mean"]}, "tp" : {"mean" : data_url1["tp"]["mean"]}}

            else: #Tidal cmpx 3 

                url1 = '/sc/'+str(scId)+'/farm/scenarii_matrices/currents'

                data_url1 = call_intermodule_service("sc", url1 ).json()

                self._sc_results = {}
                self._sc_results["id"] = data_url1["id"]
                self._sc_results["p"] = data_url1["p"]

    def _validate_sc_inputs(self):
        """
        General validation to ensure all the needed fields for each case have been provided for the SC inputs request body
               
        """
        if self.study.machine_characterisation_data.technology== "Wave":
            if self.study.energy_capture_data.ec_cmpx == 1 :
                if not "hs" in self._sc_results or not "mean" in self._sc_results["hs"]:
                    self.validation_error = True
            else: #cmpx  2, 3
                if not "bins1" in self._sc_results or not "bins2" in self._sc_results or not "id" in self._sc_results or not "pdf" in self._sc_results:
                    self.validation_error = True
        else:  
            if self.study.energy_capture_data.ec_cmpx == 3 : # tidal cmpx 3
                if not "id" in self._sc_results or not "p" in self._sc_results:
                    self.validation_error = True
            

    def _process_sc_data(self):
        """
        Process the SC results data and extract/calculate the parameters required for ET module
        
        """
        Hs_aux = []
        Tp_aux = []
        Hs = []
        Tp = []
        Dir = []
        Occ = []
        scid = []
        sum_occ = 0

        if self.study.machine_characterisation_data.technology== "Wave":
            if self.study.energy_capture_data.ec_cmpx == 1: # http://localhost:{port}/sc/{ProjectId}/point/statistics/waves/Basic
                self._processed_sc_data = {
                    "Hs": self._sc_results["hs"]["mean"],
                    "sc_id": [1],
                    "Occ" : [1]
                }    
            else: #cmpx 2 and 3 // http://localhost:{port}/sc/{ProjectId}/point/statistics/waves/EJPD3v/hs_tp_dp_flattened
                # Hs.append(mean([float(el) for el in self._sc_results["bins1"].split('-')]))
                # Tp.append(mean([float(el) for el in self._sc_results["bins2"].split('-')]))
                # Dir.append(mean([float(el) for el in self._sc_results["bins3"].split('-')]))
                scid.extend([el for el in self._sc_results["id"]])
                sum_occ = sum( self._sc_results["pdf"]) 
                for cont in range (0, len(self._sc_results["pdf"])):
                    Hs.append(mean([float(el) for el in self._sc_results["bins1"][cont].split('-')]))
                    Tp.append(mean([float(el) for el in self._sc_results["bins2"][cont].split('-')]))
                    Dir.append(mean([float(el) for el in self._sc_results["bins3"][cont].split('-')]))
                #     sum_occ = sum_occ + self._sc_results["pdf"][cont] 
                #     Hs_aux = self._sc_results["bins1"][cont]
                #     x=Hs_aux.split("-")
                #     Hs.append((float(x[0])+float(x[1])) / 2)
                #     Tp_aux = self._sc_results["bins2"][cont]
                #     x=Tp_aux.split("-")
                #     Tp.append((float(x[0])+float(x[1])) / 2)
                #     Dir_aux = self._sc_results["bins3"][cont]
                #     x=Dir_aux.split("-")
                #     Dir.append((float(x[0])+float(x[1])) / 2)
                #     scid.append(self._sc_results["id"][cont])
                Occ[:] = [x /sum_occ for x in self._sc_results["pdf"]]    
                self._processed_sc_data = {
                    "Hs": Hs,
                    "Tz": Tp,
                    "sc_id": scid,
                    "Occ" : Occ
                } 
                if self.study.machine_characterisation_data.mc_cmpx == 3 and self.study.machine_characterisation_study_id>0: #only at cmpx 3 in the integrated mode
                #TODO Above read from SC direction "Dir" --> DONE
                    cpto_mc = self.study.machine_characterisation_data.Cpto
                    site = {
                        "Hs": Hs,
                        "Tp": Tp,
                        "Dir": Dir, # to read from SC
                    }
                    # TODO read from MC the site data used to obtain Cpto
                    # note that these variables below must be read from MC but not stored in the DB
                    tp_mc = self._sc_results["tp_mc"]
                    hs_mc = self._sc_results["hs_mc"]
                    dir_mc = self._sc_results["dir_mc"]
                    
                    # TODO copy somewhere (I think out of SC) francesco's function?
                    # Call to reshape function
                    self._processed_mc_data["Cpto"] = reshape_pto_damping_matrix(cpto_mc, site, tp_mc, hs_mc, dir_mc) 

        else: # Tidal   
            if self.study.energy_capture_data.ec_cmpx == 1 or self.study.energy_capture_data.ec_cmpx == 2 : # http://localhost:{port}/sc/{ProjectId}/point/statistics/waves/Basic
                self._processed_sc_data = {
                    "Tz": [1],
                    "sc_id": [1],
                    "Occ" : [1]
                }    
            else: #cmpx 3 // http://localhost:{port}/sc/{ProjectId}/point/statistics/waves/EJPD3v/hs_tp_dp_flattened
                for cont in range (0, len(self._sc_results["p"])): # p from: http://localhost:{port}/sc/{ProjectId}/farm/scenarii_matrices/currents
                    Occ.append(self._sc_results["p"][cont])
                    scid.append(self._sc_results["id"][cont]) 
                    Tp.append(1)
                self._processed_sc_data = {
                    "Tz": Tp,
                    "sc_id": scid,
                    "Occ" : Occ
                }             
           
    def _save_sc_data(self):
        """
        Write the processed SC data to the local ET database.

        Store the processed SC inputs in the :class:`~dtop_energytransf.storage.models.models.SiteCharacterisationData`
        model table.
        
        """
        self.study.site_characterisation_data.update(**self._processed_sc_data)
        if (self.study.machine_characterisation_data.technology =="Wave" and self.study.machine_characterisation_data.mc_cmpx == 3 and self.study.machine_characterisation_study_id>0): #only at cmpx 3 in the integrated mode
            # also need to update MC data since Cpto has been updated interpolating SC info
            self.study.machine_characterisation_data.update(**self._processed_mc_data) 
        # status = self.study.status + 10
        # aux_dict = {"status": status,"site_characterisation_study_id" : self._processed_sc_data["sc_id"]}
        # self.study.update(**aux_dict)

        db.session.commit()        

 
class ProcessMachineCharacterisation(object):
    """
     Process the output data coming from the Machine Characterization (MC) module.

    Requires the appropriate URL from the MC module to be initialised.

    The **mc_data** property contains the processed MC data required as inputs to the Energy Transformation module.
    
    The initialisation process is as follows

        - retrieve the MC data using the provided URL

        - process the MC data and

        - save the process data to the local ET database 
    
    Args:
        study (obj): Energy Transformation study object
        data (dict): all the inputs from Machine Characterisation module for the study (or from the json in standalone)
        url (str, optional): The url for the MC output results. Defaults to None.
    
    Attributes:
        study (inst): Instance of Energy Transformation study
        _url (str) : The url for the SM output results
        _mc_results (dict) : Raw inputs from MC module or from the json in standalone
        _processed_mc_data (dict) : Inputs from MC processed as needed for Energy Transformation
        validation_error (bool) : Validation error flag. Default to False    
        pto_matrix (list) : Pto_damping list from MC
        dof (int) : Number of Degrees of Freedom 


    Returns:
        none  

    """

    def __init__(self, study, data, url=None):

        self.study = study
        self._url = url
        self._mc_results = {}
        self._processed_mc_data = {}
        self.validation_error = False
        if (self.study.machine_characterisation_study_id>0): 
            self._get_mc_results(self.study.machine_characterisation_study_id) # TODO: insert url
            self._validate_mc_inputs()
        else:
            self._mc_results = data
            self._validate_mc_inputs()
        # if self.study.standalone:
        #     self._mc_results = data
        #     self._validate_mc_inputs()            
        # else:
        #     self._get_mc_results()
       
        if not self.validation_error:        
            self._process_mc_data()
            self._save_mc_data()
        self._type = {}
       

    def _get_mc_results(self, mcId):
        """
        Get Machine Characterisation results for the requested DTO+ project.

        Use the provided URL to make a request to MC module and return the results.

        Args:
            mcId (int): Machine Characterization id
            
        Returns:
            none
        """        

        # TODO: For now it returns dummy JSON data currently stored in MOCK_DATA folder, eventually connect to SC API
        # Next step is to use Mountebank to mock module rather thant MOCK_DATA folder
        
        # For example, use:
        # self._mc_results = requests.get(self._url)
        # if "url" in self.data:
        #     self._url = data ["url"]
        #     self._mc_results = requests.get(self._url).json()
        # else:
        #     self._mc_results = {}
        #     self._mc_results ["complexity"]  = data["complexity"]
        # o desde la GUI o desde la ruta corrispondiente
        # input management en el caso de la GUI por el standalone
        # self._type["type"] = self._inputMC["type"]
        # self._type["complexity"] = self._inputMC["complexity"]
        


        url1 = '/mc/'+str(mcId)
        data_url1 = call_intermodule_service('mc', url1).json()
        
        self._mc_results = {}
        self._mc_results["technology"] = data_url1["type"]
        self._mc_results["complexity"] = data_url1["complexity"]
        
        # if self._mc_results["technology"] == "WEC":
        #     self._mc_results["technology"] = "Wave"
        # else:
        #     self._mc_results["technology"] = "Tidal"


        if self._mc_results["technology"] == "WEC":  # WAVE MC =========================================
            # cpx_MC_wave = ProcessMachineCharacterisation()._type["complexity"]
            
            if self._mc_results["complexity"] == 1:
                
                url2 = '/mc/'+str(mcId)+'/model/wec/complexity1'
                data_url2 = call_intermodule_service('mc', url2).json()
                self._mc_results["id"] = data_url1["id"]
                self._mc_results["pto_damping"] = data_url2["pto_damping"]      
            
            elif self._mc_results["complexity"] == 2:  # WAVE cpx2 cpx_MC_wave = 2 --------------------------------

                url2 ='/mc/'+str(mcId)+'/model/wec/complexity2'               
                data_url2 = call_intermodule_service('mc', url2).json()
                self._mc_results["id"] = data_url1["id"]
                self._mc_results["pto_damping"] = data_url2["pto_damping"]    

            else:  # cpx_MC_wave = 3 -------------------------------------------------------------
                
                url2 ='/mc/'+str(mcId)+'/model/wec/complexity3'               
                data_url2 = call_intermodule_service('mc', url2).json()
                self._mc_results["id"] = data_url1["id"]
                self._mc_results["pto_damping"] = data_url2["pto_damping"]
                self._mc_results["hs_mc"] = data_url2["hs_capture_width"]
                self._mc_results["tp_mc"] = data_url2["tp_capture_width"]
                self._mc_results["dir_mc"] = data_url2["wave_angle_capture_width"]


        else:  # TIDAL =========================================================================
            url3 ='/mc/'+str(mcId)+'/dimensions'
            data_url3 = call_intermodule_service('mc', url3).json()
            self._mc_results["characteristic_dimension"] = data_url3["characteristic_dimension"]

            if self._mc_results["complexity"] == 1:
                
                url2 ='/mc/'+str(mcId)+'/model/tec/complexity1'
                data_url2 = call_intermodule_service('mc', url2).json()
                self._mc_results["id"] = data_url1["id"]
                self._mc_results["cp"] = data_url2["cp"]
                self._mc_results["number_rotor"] = data_url2["number_rotor"]

            elif self._mc_results["complexity"] == 2:  # cpx MC tidal=2 ------------------------------

                url2 ='/mc/'+str(mcId)+'/model/tec/complexity2'
                data_url2 = call_intermodule_service('mc', url2).json()
                self._mc_results["id"] = data_url1["id"]
                self._mc_results["cp"] = data_url2["cp"]
                self._mc_results["number_rotor"] = data_url2["number_rotor"]
                self._mc_results["tip_speed_ratio"] = data_url2["tip_speed_ratio"]
                self._mc_results["ct"] = data_url2["ct"]
                self._mc_results["cut_in_velocity"] = data_url2["cut_in_velocity"]
                self._mc_results["cut_out_velocity"] = data_url2["cut_out_velocity"]

            else:  # cpx_MC_tidal = 3-----------------------------------------------------------
                
                url2 ='/mc/'+str(mcId)+'/model/tec/complexity3'
                data_url2 = call_intermodule_service('mc', url2).json()
                self._mc_results["id"] = data_url1["id"]
                self._mc_results["cp"] = data_url2["cp"]
                self._mc_results["number_rotor"] = data_url2["number_rotor"]
                self._mc_results["tip_speed_ratio"] = data_url2["tip_speed_ratio"]
                self._mc_results["ct"] = data_url2["ct"]
                self._mc_results["cut_in_velocity"] = data_url2["cut_in_velocity"]
                self._mc_results["cut_out_velocity"] = data_url2["cut_out_velocity"]
                self._mc_results["cp_ct_velocity"] = data_url2["cp_ct_velocity"]
        # print(self._mc_results)
    def _validate_mc_inputs(self):
        """
        General validation to ensure all the needed fields for each case have been provided for the EC inputs request body
               
        """

        if not "technology" in self._mc_results or not "complexity" in self._mc_results :
            self.validation_error = True
        elif self._mc_results["complexity"] < 1 or self._mc_results["complexity"] > 3 :
            self.validation_error = True 
        elif self._mc_results["technology"]== "WEC": 
            if not "pto_damping" in self._mc_results:
                self.validation_error = True
        elif self._mc_results["technology"]== "TEC":
            if not "cp" in self._mc_results or not "number_rotor" in self._mc_results or not "characteristic_dimension" in self._mc_results:
                self.validation_error = True          
            elif self._mc_results["complexity"] == 2 or self._mc_results["complexity"] == 3: 
                if not "tip_speed_ratio" in self._mc_results or not "ct" in self._mc_results or not "cut_in_velocity" in self._mc_results or not "cut_out_velocity" in self._mc_results:
                    self.validation_error = True 
                elif self._mc_results["complexity"] == 3 and not "cp_ct_velocity" in self._mc_results:
                    self.validation_error = True

        elif (self._mc_results["technology"] != "WEC" and self._mc_results["technology"] != "TEC"):
            self.validation_error = True 

        # elif not "id" in self._mc_results: # id not required in standalone, if not provided set to 1 for checking there are no errors
        #     self._m_results["id"]=1 
        
     
    def _process_mc_data(self):
        """Process the MC results data and extract/calculate the parameters required for ET module"""
        if self._mc_results["technology"] == "WEC":
            self._mc_results["technology"] = "Wave"
        else:
            self._mc_results["technology"] = "Tidal"

        if self._mc_results["technology"] == "Wave":  # WAVE MC =========================================
            if self._mc_results["complexity"] == 1:  # Wave cmpx1 MC------------------------------
                self._processed_mc_data = {
                    "technology": self._mc_results["technology"],
                    "mc_cmpx": self._mc_results["complexity"],
                    "mc_id": self._mc_results["id"],
                    "dof": 1, # Only one degree of freedom cmpx1 wave
                    "Cpto": self._mc_results["pto_damping"],
                    "cut_in_out": []  # TODO check if MC provides this for Wave
                }
            elif self._mc_results["complexity"] == 2:  # Wave cmpx3 MC------------------------------
                #self._calculate_dof() # we obtain dof               
                self._processed_mc_data = {
                    "technology": self._mc_results["technology"],
                    "mc_cmpx": self._mc_results["complexity"],
                    "mc_id": self._mc_results["id"],
                    "dof": 1,  # Only one degree of freedom cmpx2 wave
                    "Cpto": self._mc_results["pto_damping"],
                    "cut_in_out": []  # TODO check if MC provides this for Wave
                }
            else:  # Wave cmpx3 MC------------------------------------------------------------
                self._calculate_dof() # we obtain dof
                self._processed_mc_data = {
                    "technology": self._mc_results["technology"],
                    "mc_cmpx": self._mc_results["complexity"],
                    "mc_id": self._mc_results["id"],
                    "dof": self.dof,
                    "Cpto": self._mc_results["pto_damping"],
                    "cut_in_out": [],  # TODO check if MC provides this for Wave
                }
        else:  # FOR TIDAL
            vel_sigma_v_aux = []
            Cpto_aux = []
            Ct_tidal_aux = []
            radious = self._mc_results["characteristic_dimension"]/2
            if self._mc_results["complexity"] == 1:  # Tidal cmpx1 MC------------------------------
                #sigma_v = self.study.energy_capture_data.sigma_v # to be obtained from EC
                #radious = self.study.energy_capture_data.rotor_diameter # main_dim_device to be obtained from EC
                #Cpto_aux = sigma_v[0] * self._mc_results["cp"] * 0.5 * 1024 * np.pi * radious ** 2
                # We need to obtain Ct; Ct=1/mean_omega; TSR= 5.8; TSR=mean_omega*R/current_vel;
                #
                #Ct_tidal_aux = radious/(sigma_v[0]*5.8)
                for num_rot in range (0,self.study.energy_capture_data.num_devices*self._mc_results["number_rotor"]):
                    vel_sigma_v_aux =self.study.energy_capture_data.sigma_v[num_rot]
                    vel_sigma_v_aux = [10*x for x in self.study.energy_capture_data.sigma_v[num_rot]]
                    Cpto_aux.append( [vel_sigma_v_aux[0] * self._mc_results["cp"] * 0.5 * 1024 * np.pi * radious ** 2])  
                    Ct_tidal_aux.append([radious/(vel_sigma_v_aux[0]*5.8)])
   
                self._processed_mc_data = {
                    "technology": self._mc_results["technology"],
                    "mc_cmpx": self._mc_results["complexity"],
                    "mc_id": self._mc_results["id"],
                    "cut_in_out": [0.05,5],  # at cmpx1 we assume this values
                    "Cp": [self._mc_results["cp"]],
                    "number_rotor": self._mc_results["number_rotor"], 
                    "TSR": 5.8,  # at cmpx1 we assume this values
                    "Ct_tidal": Ct_tidal_aux,
                    "Cpto": Cpto_aux,
                    "dof": 1, # Only one degree of freedom for tidal
                    "rotor_diameter": self._mc_results["characteristic_dimension"]  # http://localhost:{port}/mc/{mcId}/dimensions   "characteristic_dimension"
                }
            elif self._mc_results["complexity"] == 2:  # Tidal cmpx2 MC----------------------------
                for num_rot in range (0,self.study.energy_capture_data.num_devices*self._mc_results["number_rotor"]):
                    vel_sigma_v_aux = [10*x for x in self.study.energy_capture_data.sigma_v[num_rot]]
                    Cpto_aux.append([vel_sigma_v_aux[0] * self._mc_results["cp"] * 0.5 * 1024 * np.pi * radious ** 2])
                    # Ct_tidal_aux.append([self._mc_results["ct"]])
                    inv_omega= [radious/(vel_sigma_v_aux[0]*self._mc_results["tip_speed_ratio"])]
                    Ct_tidal_aux.append(inv_omega)

                self._processed_mc_data = {
                    "technology": self._mc_results["technology"],
                    "mc_cmpx": self._mc_results["complexity"],
                    "mc_id": self._mc_results["id"],
                    "cut_in_out": [self._mc_results["cut_in_velocity"], self._mc_results["cut_out_velocity"]] ,
                    "Cp": [self._mc_results["cp"]],
                    "number_rotor": self._mc_results["number_rotor"],
                    "TSR": self._mc_results["tip_speed_ratio"],
                    "Ct_tidal": Ct_tidal_aux,
                    "Cpto": Cpto_aux,
                    "dof": 1, # Only one degree of freedom for tidal
                    "rotor_diameter": self._mc_results["characteristic_dimension"]  # http://localhost:{port}/mc/{mcId}/dimensions   "characteristic_dimension"
                }
            else:  # Tidal cmpx3 MC-----------------------------------------------
                # for num_dev in range (0,self.study.energy_capture_data.num_devices):
                #     Cpto_dev = []
                #     for ss in range (0, len(self._mc_results["cp"])):
                #         sigma_v_aux = self.study.energy_capture_data.sigma_v[num_dev][ss]
                #         Cpto_dev.append(sigma_v_aux * self._mc_results["cp"][ss] * 0.5 * 1024 * np.pi * radious ** 2)      
                #     Cpto_aux.append(Cpto_dev)       
                # ct_aux = self._mc_results["ct"]
                # for cont_ct in range(0, len(ct_aux)):
                #     if ct_aux[cont_ct] < 0.1 :
                #         ct_aux[cont_ct] = 0.1 
                #   

                for num_rot in range (0,self.study.energy_capture_data.num_devices*self._mc_results["number_rotor"]):
                    vel_sigma_v_aux = [10*x for x in self.study.energy_capture_data.sigma_v[num_rot]]
                    cp_aux= np.interp(vel_sigma_v_aux, self._mc_results["cp_ct_velocity"] , self._mc_results["cp"], left=0, right=0)
                    cp_list = cp_aux.tolist()
                    product = [x*y for x,y in zip(vel_sigma_v_aux,cp_list)]
                    aux = 0.5 * 1024 * np.pi * radious ** 2
                    cpto_rot= [ n * aux for n in product ] # calculates Cpto per rotor and per SS
                    Cpto_aux.append(cpto_rot)
                    ct_aux= np.interp(vel_sigma_v_aux, self._mc_results["cp_ct_velocity"] , self._mc_results["ct"], left=0, right=0)
                    ct_list = ct_aux.tolist()
                    # Ct_tidal_aux.append(ct_list)
                    ct_value = []
                    # Using Equations in Unsaker and Phillips  
                    for i, ccp in enumerate(cp_list):
                        # alpha=max(-1,27*ccp/8-1)
                        # alpha = min(27*ccp/8-1,1)  # Limit alpha to 1. Assumed equal to 1 if ccp greater than beltz limit
                        # phi = np.arccos(alpha) 
                        
                        # a = 2/3*np.cos(phi/3+2/3*np.pi)+2/3 # Impelmenting equation 71. i disregarded the second solution for ccp>0.5 
                        a = 1/3 # ideal conditions
                        aux_fact_1 = ct_list[i]/(4*a*(1-a))
                        aux_fact_2 = max( 1/36 , (aux_fact_1-1)/(2*a*(1-a))) # threshold value fixed to TSR = 6
                        lambda_tsr =np.sqrt(1/aux_fact_2) # Implementing eq. 55
                        inv_omega = radious/(vel_sigma_v_aux[i]*lambda_tsr)
                        ct_value.append(inv_omega)
                    
                    Ct_tidal_aux.append(ct_value) # In reality this is not a thrust coefficient, it is just the inverse of the rotational speed

                self._processed_mc_data = {
                    "technology": self._mc_results["technology"],
                    "mc_cmpx": self._mc_results["complexity"],
                    "mc_id": self._mc_results["id"],
                    "cut_in_out": [self._mc_results["cut_in_velocity"], self._mc_results["cut_out_velocity"]] ,
                    "Cp": self._mc_results["cp"],
                    "number_rotor": self._mc_results["number_rotor"],
                    "TSR": self._mc_results["tip_speed_ratio"],
                    "Ct_tidal": Ct_tidal_aux, #self._mc_results["ct"],
                    "cp_ct_velocity": self._mc_results["cp_ct_velocity"],  # only at cpx 3
                    "Cpto": Cpto_aux,
                    "dof": 1, # Only one degree of freedom for tidal
                    "rotor_diameter": self._mc_results["characteristic_dimension"]  # http://localhost:{port}/mc/{mcId}/dimensions   "characteristic_dimension"
                }

    def _calculate_dof(self):
        """Calculate the DOF (degrees of freedom)"""

        pto_matrix =[]

        pto_matrix = self._mc_results["pto_damping"]  # data obtained at cmpx 3
        dof = 0
        n_rows = len(pto_matrix[0][0][0][0]) # max dof n_rows^2
        # for cont1 in range(0, 5):
        #     for cont2 in range(0, 5):
        #         if pto_matrix[cont1][cont2] > 0:
        #             dof = dof + 1

        for cont1 in range(0, n_rows):
            for cont2 in range(0, n_rows):
                if pto_matrix[0][0][0][cont1][cont2] > 0:
                    dof = dof + 1                    

        self.pto_matrix = pto_matrix
        self.dof = dof

    def _save_mc_data(self):
        """
        Write the processed MC data to the local ET database.

        Store the processed MC inputs in the
        :class:`~dtop_energytransf.storage.models.models.MachineCharacterisationData` model table.
        """
        
        self.study.machine_characterisation_data.update(**self._processed_mc_data)
        # status = self.study.status + 10
        # aux_dict = {"status": status,"machine_characterisation_study_id" : self._processed_mc_data["mc_id"]}
        # self.study.update(**aux_dict)

        db.session.commit()


class ProcessEnergyCapture(object):
    """
    Process the output data coming from the Energy Capture (EC) module.

    Requires the appropriate URL from the EC module to be initialised.

    The **ec_data** property contains the processed EC data required as inputs to the Energy Transformation module.
    
    The initialisation process is as follows

        - retrieve the EC data using the provided URL

        - process the EC data and

        - save the process data to the local ET database 

    Args:
        study (inst): Instance of the Energy Transformation study 
        data (dict): all the inputs from Energy Capture module for the study (or from the json in standalone)
        url (str, optional): The url for the EC output results. Defaults to None.
    
    Attributes:
        study (inst): Instance of the Energy Transformation study 
        _url (str) : The url for the SM output results
        _ec_results (dict) : Raw inputs from EC module or from the json in standalone
        _processed_ec_data (dict) : Inputs from EC processed as needed for Energy Transformation
        validation_error (bool) : Validation error flag. Default to False    

    Returns:
        none    
    
    """

    def __init__(self, study, data, url=None):
        
        self.study = study
        self._url = url
        self._ec_results = {}
        self._processed_ec_data = {}
        self.validation_error = False

        if (self.study.energy_capture_study_id>0): 
            self._get_ec_results(self.study.energy_capture_study_id) # TODO: insert url
            self._validate_ec_inputs()
        else:
            self._ec_results = data
            self._validate_ec_inputs()

        # if self.study.standalone:
        #     self._ec_results = data
        #     self._validate_ec_inputs()
        # else:
        #     self._get_ec_results()

        if not self.validation_error:
            self._process_ec_data()
            self._save_ec_data()

        
    def _get_ec_results(self,ecId):
        """
        Get Energy Capture results for the requested DTO+ project.

        Use the provided URL to make a request to EC module and return the results.

        Args:
            ecId (int): Energy Capture id

        Returns:
            none    

        """
        # TODO: For now it returns dummy JSON data currently stored in MOCK_DATA folder, eventually connect to SC API
        # Next step is to use Mountebank to mock module rather thant MOCK_DATA folder
        # For example, use:
        # self._ec_results = requests.get(self._url)
        
        # if self._url: 
        #     self._ec_results = requests.get(self._url)
        #  "rotor_diameter": 5  # / ec / {ecId} / inputs / machine / tec / complexity3
        #  "rotor_diameter": 5  # / ec / {ecId} / inputs / machine / tec / complexity2 ==>  rotor_diameter
        # NB: "type", "complexity", "number_devices", "capturedPower"
        
        
        #  at this direction ==> /ec/{ecId}/devices we have an array of dictionaries and
        #  here /ec/{ecId}/devices/{deviceId} we will acces with the idevice id to de diccionary with the properties
        #  of each device

        contp = 0
        P_capt = []

        url1 ='/ec/'+str(ecId)
        data_url1 = call_intermodule_service('ec', url1).json()
        
        self._ec_results = {}
        self._ec_results["technology"] = data_url1["type"]
        self._ec_results["complexity"] = data_url1["complexity"]


        if self._ec_results["technology"] == "WEC" :  
            
            if self._ec_results["complexity"] == 1: # Wave cmpx1 EC ------------------
                url2 ='/ec/'+str(ecId)+'/farm'
                data_url2 = call_intermodule_service('ec', url2).json()

                url3 ='/ec/'+str(ecId)+'/devices'
                data_url3 = call_intermodule_service('ec', url3).json()
                # url2 = http://localhost:{port}/ec/{ecId}/farm
                # url3 = http://localhost:{port}/ec/{ecId}/devices
                # data_url2 = requests.get(url2).json()
                # data_url3 = requests.get(url3).json()
                self._ec_results["id"] = data_url1["id"]
                self._ec_results["number_devices"] = data_url2["number_devices"]
                self._ec_results["captured_power_per_condition"] =[]
                for i in range (self._ec_results["number_devices"]):
                    aux_dict = {"deviceID" : data_url3[i]["id"], "capturedPower": data_url3[i]["captured_power_per_condition"]["capturedPower"], "siteConditionID": data_url3[i]["captured_power_per_condition"]["siteConditionID"]}
                    self._ec_results["captured_power_per_condition"].append(aux_dict) 

            if self._ec_results["complexity"] == 2:

                url2 ='/ec/'+str(ecId)+'/farm'
                data_url2 = call_intermodule_service('ec', url2).json()

                url3 ='/ec/'+str(ecId)+'/devices'
                data_url3 = call_intermodule_service('ec', url3).json()

                # url2 = http://localhost:{port}/ec/{ecId}/farm
                # url3 = http://localhost:{port}/ec/{ecId}/devices
                # data_url2 = requests.get(url2).json()
                # data_url3 = requests.get(url3).json()
                self._ec_results["id"] = data_url1["id"]
                self._ec_results["number_devices"] = data_url2["number_devices"]
                self._ec_results["captured_power_per_condition"] =[]
                for i in range (self._ec_results["number_devices"]):
                    aux_dict = {"deviceID" : data_url3[i]["id"], "capturedPower": data_url3[i]["captured_power_per_condition"]["capturedPower"], "siteConditionID": data_url3[i]["captured_power_per_condition"]["siteConditionID"]}
                    self._ec_results["captured_power_per_condition"].append(aux_dict) 


            else:  # Wave cmpx3 EC ------------------
                url2 ='/ec/'+str(ecId)+'/farm'
                data_url2 = call_intermodule_service('ec', url2).json()

                url3 ='/ec/'+str(ecId)+'/devices'
                data_url3 = call_intermodule_service('ec', url3).json()                
                # url2 = http://localhost:{port}/ec/{ecId}/farm
                # url3 = http://localhost:{port}/ec/{ecId}/devices
                # data_url2 = requests.get(url2).json()
                # data_url3 = requests.get(url3).json()               
                self._ec_results["id"] = data_url1["id"]
                self._ec_results["number_devices"] = data_url2["number_devices"]
                self._ec_results["captured_power_per_condition"] =[]
                for i in range (self._ec_results["number_devices"]):
                    aux_dict = {"deviceID" : data_url3[i]["id"], "capturedPower": data_url3[i]["captured_power_per_condition"]["capturedPower"], "siteConditionID": data_url3[i]["captured_power_per_condition"]["siteConditionID"]}
                    self._ec_results["captured_power_per_condition"].append(aux_dict) 


        else:  # TIDAL ==========================================================

            # TODO VINCENZO we dont need main_dimension_device nor rotor_diameter from EC

            if self._ec_results["complexity"] == 1:  # Tidal cmpx 1 MC-----------------

                url2 ='/ec/'+str(ecId)+'/farm'
                data_url2 = call_intermodule_service('ec', url2).json()

                url3 ='/ec/'+str(ecId)+'/rotors'
                data_url3 = call_intermodule_service('ec', url3).json()

                url4 ='/ec/'+str(ecId)+'/results/tec/complexity1'
                data_url4 = call_intermodule_service('ec', url4).json()

                # url5 ='/ec/'+str(ecId)+'/inputs/machine/tec/complexity1'
                # data_url5 = call_intermodule_service('ec', url5).json()                
                # url2 = http://localhost:{port}/ec/{ecId}/farm
                # url3 = http://localhost:{port}/ec/{ecId}/devices
                # url4 = http://localhost:{port}/ec/{ecId}/results/tec/complexity1
                # url5 = http://localhost:{port}/ec/{ecId}/inputs/machine/tec/complexity1
                # data_url2 = requests.get(url2).json()
                # data_url3 = requests.get(url3).json()
                # data_url4 = requests.get(url4).json()
                # data_url5 = requests.get(url5).json()
                self._ec_results["id"] = data_url1["id"]
                self._ec_results["number_devices"] = data_url2["number_devices"]
                self._ec_results["number_rotor"] = int(len(data_url3)/data_url2["number_devices"]) #this is not an output of EC!!!!!
                self._ec_results["captured_power_per_condition"] = []
                for i in range (len(data_url3)):
                    aux_dict = {"rotorID": data_url3[i]["id"],"deviceID" : data_url3[i]["deviceID"], "capturedPower": data_url3[i]["captured_power_per_condition"]["capturedPower"], "siteConditionID": data_url3[i]["captured_power_per_condition"]["siteConditionID"]}
                    self._ec_results["captured_power_per_condition"].append(aux_dict) 

                # self._ec_results["capturedPower"] = data_url3["captured_power_per_condition"]["capturedPower"]
                # self._ec_results["siteConditionID"] = data_url3["captured_power_per_condition"]["siteConditionID"]
                self._ec_results["array_velocity_field"] =  data_url4["array_velocity_field"]
                # self._ec_results["rotorID"] =  { "rotorID" : data_url4["array_velocity_field"]["rotorID"]}
                # self._ec_results["main_dim_device"] = data_url5["main_dim_device"]

            elif self._ec_results["complexity"] == 2:  # Tidal cmpx 2 MC---------------
                url2 ='/ec/'+str(ecId)+'/farm'
                data_url2 = call_intermodule_service('ec', url2).json()

                url3 ='/ec/'+str(ecId)+'/rotors'
                data_url3 = call_intermodule_service('ec', url3).json()

                url4 ='/ec/'+str(ecId)+'/results/tec/complexity2'
                data_url4 = call_intermodule_service('ec', url4).json()
                # url2 = http://localhost:{port}/ec/{ecId}/farm
                # url3 = http://localhost:{port}/ec/{ecId}/devices
                # url4 = http://localhost:{port}/ec/{ecId}/results/tec/complexity2
                # url5 = http://localhost:{port}/ec/{ecId}/inputs/machine/tec/complexity2
                # data_url2 = requests.get(url2).json()
                # data_url3 = requests.get(url3).json()
                # data_url4 = requests.get(url4).json()
                # data_url5 = requests.get(url5).json()
                self._ec_results["id"] = data_url1["id"]
                self._ec_results["number_devices"] = data_url2["number_devices"]
                self._ec_results["number_rotor"] = int(len(data_url3)/data_url2["number_devices"]) #this is not an output of EC!!!!!
                self._ec_results["captured_power_per_condition"] = []
                for i in range (len(data_url3)):
                    aux_dict = {"rotorID": data_url3[i]["id"],"deviceID" : data_url3[i]["deviceID"], "capturedPower": data_url3[i]["captured_power_per_condition"]["capturedPower"], "siteConditionID": data_url3[i]["captured_power_per_condition"]["siteConditionID"]}
                    self._ec_results["captured_power_per_condition"].append(aux_dict) 

                # self._ec_results["capturedPower"] = data_url3["captured_power_per_condition"]["capturedPower"]
                # self._ec_results["siteConditionID"] = data_url3["captured_power_per_condition"]["siteConditionID"]
                self._ec_results["array_velocity_field"] =  data_url4["array_velocity_field"]
                #self._ec_results["rotor_diameter"] = data_url5["rotor_diameter"]


            else:  # Tidal cmpx 3 MC----------------------------------------------

                url2 ='/ec/'+str(ecId)+'/farm'
                data_url2 = call_intermodule_service('ec', url2).json()

                url3 ='/ec/'+str(ecId)+'/rotors'
                data_url3 = call_intermodule_service('ec', url3).json()

                url4 ='/ec/'+str(ecId)+'/results/tec/complexity3'
                data_url4 = call_intermodule_service('ec', url4).json()
                # url2 = http://localhost:{port}/ec/{ecId}/farm
                # url3 = http://localhost:{port}/ec/{ecId}/devices
                # url4 = http://localhost:{port}/ec/{ecId}/results/tec/complexity3
                # url5 = http://localhost:{port}/ec/{ecId}/inputs/machine/tec/complexity3

                # data_url2 = requests.get(url2).json()
                # data_url3 = requests.get(url3).json()
                # data_url4 = requests.get(url4).json()
                # data_url5 = requests.get(url5).json()
                self._ec_results["id"] = data_url1["id"]
                self._ec_results["number_devices"] = data_url2["number_devices"]
                self._ec_results["number_rotor"] = int(len(data_url3)/data_url2["number_devices"]) #this is not an output of EC!!!!!
                self._ec_results["captured_power_per_condition"] = []
                for i in range (len(data_url3)):
                    aux_dict = {"rotorID": data_url3[i]["id"],"deviceID" : data_url3[i]["deviceID"], "capturedPower": data_url3[i]["captured_power_per_condition"]["capturedPower"], "siteConditionID": data_url3[i]["captured_power_per_condition"]["siteConditionID"]}
                    self._ec_results["captured_power_per_condition"].append(aux_dict) 

                # self._ec_results["capturedPower"] = data_url3["captured_power_per_condition"]["capturedPower"]
                # self._ec_results["siteConditionID"] = data_url3["captured_power_per_condition"]["siteConditionID"]
                self._ec_results["array_velocity_field"] =  data_url4["array_velocity_field"]
                #self._ec_results["rotor_diameter"] = data_url5["rotor_diameter"]


    def _validate_ec_inputs(self):
        """
        General validation to ensure all the neaded fields for each case have been provided for the EC inputs request body
               
        """
        if not "technology" in self._ec_results or not "complexity" in self._ec_results or not "number_devices" in self._ec_results or not "captured_power_per_condition" in self._ec_results:
            self.validation_error = True
        elif self._ec_results["complexity"] != 1 and self._ec_results["complexity"] != 2 and self._ec_results["complexity"] != 3:
            self.validation_error = True            
        elif not "capturedPower" in self._ec_results["captured_power_per_condition"][0] or not "siteConditionID" in self._ec_results["captured_power_per_condition"][0] or not "deviceID" in self._ec_results["captured_power_per_condition"][0]: 
            self.validation_error = True             
        elif self._ec_results["technology"] == "TEC":
            if not "array_velocity_field" in self._ec_results or not "number_rotor" in self._ec_results:
                self.validation_error = True                
            elif not "rotorID" in self._ec_results["captured_power_per_condition"][0]: 
                self.validation_error = True   
            elif not "hub_velocity" in self._ec_results["array_velocity_field"][0] or not "rotorID" in self._ec_results["array_velocity_field"][0]: 
                self.validation_error = True                   
            elif self._ec_results["number_devices"] * self._ec_results["number_rotor"] != len(self._ec_results["captured_power_per_condition"]):
                self.validation_error = True  
        elif self._ec_results["technology"] == "WEC": 
            if self._ec_results["number_devices"] != len(self._ec_results["captured_power_per_condition"]):                                    
                self.validation_error = True           
        elif (self._ec_results["technology"] != "WEC" and self._ec_results["technology"] != "TEC"):
                self.validation_error = True                 
        # elif not "id" in self._ec_results: # id not required in standalone, if not provided set to 1 for checking there are no errors
        #     self._ec_results["id"] = 1          

    def _process_ec_data(self):
        """
        Process the EC results data here to extract/calculate the necessary parameters
                
        """
        # HERE IS THE POST PROCESSING SECTION. MAKE SURE THAT THE VARIABLES MATCH between modules
        # self._processed_ec_data = self._ec_results
        capt_power_aux = [] 
        site_cond_aux =[]  
        #TODO, revisar si esto es dentro de wec. revisar sitecond
        for num_dev in range (0,(self._ec_results["number_devices"])):
            capt_power = [1000*i for i in self._ec_results["captured_power_per_condition"][num_dev]["capturedPower"]] #convert incoming kw in w
            capt_power_aux.append(capt_power) 
            site_cond_aux.append(self._ec_results["captured_power_per_condition"][num_dev]["siteConditionID"])  
                             
        if self._ec_results["technology"] == "WEC":  # WAVE ===============================
            if self._ec_results["complexity"] == 1:  # # CMPX 1 WAVE EC---------------
                self._processed_ec_data = {
                    "ec_cmpx": self._ec_results["complexity"],
                    "num_devices": self._ec_results["number_devices"],
                    "Captured_Power": capt_power_aux,
                    "siteConditionID": site_cond_aux,
                    "ec_id": self._ec_results["id"]
                }
            elif self._ec_results["complexity"] == 2:  # CMPX 2 WAVE EC---------------
                self._processed_ec_data = {
                    "ec_cmpx": self._ec_results["complexity"],
                    "num_devices": self._ec_results["number_devices"],
                    "Captured_Power": capt_power_aux,
                    "siteConditionID": site_cond_aux,
                    "ec_id": self._ec_results["id"]
                }
            else:  # CMPX 3 WAVE EC----------------------------------------------
                self._processed_ec_data = {
                    "ec_cmpx": self._ec_results["complexity"],
                    "num_devices": self._ec_results["number_devices"],
                    "Captured_Power": capt_power_aux,
                    "siteConditionID": site_cond_aux,
                    "ec_id": self._ec_results["id"]
                }
        else:  # TIDAL ==========================================================
            sigma_v_aux = []
            capt_power_aux = []
            sorted_list_capt_power = sorted(self._ec_results["captured_power_per_condition"], key = lambda i: (i['deviceID'], i['rotorID']))
            sorted_list_velocity = sorted(self._ec_results["array_velocity_field"], key = lambda i: (i['rotorID']))
            for num_rot in range (0,self._ec_results["number_rotor"]*self._ec_results["number_devices"]):
                sigma_v = [max(0.1*x, 1.e-6) for x in sorted_list_velocity[num_rot]["hub_velocity"]] #sigma_v is 10% hub velocity
                sigma_v_aux.append(sigma_v)
                # sigma_v_aux.append(sorted_list_velocity[num_rot]["hub_velocity"]) 
                capt_power = [1000*i for i in sorted_list_capt_power[num_rot]["capturedPower"]] #convert incoming kw in w
                capt_power_aux.append(capt_power) 
            
            if self._ec_results["complexity"] == 1:  # # CMPX 1 TIDAL EC---------------
                self._processed_ec_data = {
                    "ec_cmpx": self._ec_results["complexity"],
                    "num_devices": self._ec_results["number_devices"],
                    "Captured_Power": capt_power_aux,
                    "siteConditionID": site_cond_aux,
                    "sigma_v": sigma_v_aux,                    
                    "ec_id": self._ec_results["id"]
                }
            elif self._ec_results["complexity"] == 2:  # CMPX 2 TIDAL EC---------------
                self._processed_ec_data = {
                    "ec_cmpx": self._ec_results["complexity"],
                    "num_devices": self._ec_results["number_devices"],
                    "Captured_Power": capt_power_aux,
                    "sigma_v": sigma_v_aux,
                    "siteConditionID": site_cond_aux,
                    "ec_id": self._ec_results["id"]                    
                }
            else:  # CMPX 3 TIDAL EC----------------------------------------------
                self._processed_ec_data = {
                    "ec_cmpx": self._ec_results["complexity"],
                    "num_devices": self._ec_results["number_devices"],
                    "Captured_Power": capt_power_aux,
                    "sigma_v": sigma_v_aux,
                    "siteConditionID": site_cond_aux,
                    "ec_id": self._ec_results["id"]
                }

    def _save_ec_data(self):
        """
        Write the processed EC data to the local ET database.

        Store the processed EC inputs in the :class:`~dtop_energytransf.storage.models.models.EnergyCaptureData`
        model table.

        """
        self.study.energy_capture_data.update(**self._processed_ec_data)
        # status = self.study.status + 10
        # aux_dict = {"status": status, "energy_capture_study_id" : self._processed_ec_data["ec_id"]}
        # self.study.update(**aux_dict)

        db.session.commit()

class ProcessCatalogue(object):
    """
    Process the output data coming from the Catalogue module.

    Requires the appropriate URL from the Catalogue module to be initialised.

    The **Catalogue** property contains the processed Catalogue data required as inputs to the Energy Transformation module.
    The initialisation process is as follows:

    - retrieve the Catalogue data using the provided URL

    - save the processed data to the local ET database 

    Args:
        study (inst): Instance of the Energy Transformation study 
        names (int): Flag to know if only the names of the catalogue objects must be retrieved, or after selecting an object, all the values must be retrieved.
        url (str, optional): the url for reading the Catalogue output results. Defaults to None.

    Attributes:
        study (inst): Instance of the Energy Transformation study  
        _url (str) = the url for reading the Catalogue output results
        _mec_results (dict): diccionary with the mechanical parameters from the catalogue
        _elec_results (dict): diccionary with the electrical parameters from the catalogue     
        _grid_results (dict): diccionary with the grid parameters from the catalogue       
        _control_results (dict): diccionary with the control parameters from the catalogue            

    Returns:
        none     

    """
    def __init__(self, study, names, url=None):

        self.study = study
        self._url = url
        self._mec_results = {}
        self._elec_results = {}     
        self._grid_results = {}       
        self._control_results = {}
        root = get_project_root()
        self.folder = str(root) + "/storage/catalogue_data"        
            
        if  names == 1: # first round only to show the object names to the users
            self._get_catalogue_names()
        else: # user has selected the objects. second round to import the data from the catalogue
            self._get_catalogue_results()
        self._process_catalogue_data()
        self._save_catalogue_data()               

    def _get_catalogue_names(self):
      
        """
        Get Catalogue object data for the requested DTO+ project.

        Use the provided URL to make a request to Catalogue module and return the results.
        
        """
      
        # Mechanical catalogue
        # url_air = file:///api/mecht_air_turbine  
        # air_data1 = requests.get(url_air).json()
        
        url_air = '/api/mecht_air_turbine?fields[MechtAirTurbine]=id,name'
        air_data_resp = call_intermodule_service('cm', url_air)
        if air_data_resp.status_code == 200:
            air_data1 = air_data_resp.json()
            # with open(f"{self.folder}/mecht_air_turbine_name.json", "r") as f:
            #     air_data1 = json.load(f)
            self._air_data = [conns["attributes"] for conns in air_data1["data"]]
            air_obj = []
            for num_air in range (0, len(self._air_data)):
                air_obj.append(self._air_data[num_air]["name"])         
            self._mec_results["catalogue_turb"] = air_obj 


        # # url_hyd = file:///api/mecht_hydraulic
        # hyd_data1 = requests.get(url_hyd).json()
        url_hyd = '/api/mecht_hydraulic?fields[MechtHydraulic]=id,name'
        # hyd_data1 = call_intermodule_service('cm', url_hyd).json()

        hyd_data_resp = call_intermodule_service('cm', url_hyd)
        if hyd_data_resp.status_code == 200:
            hyd_data1 = hyd_data_resp.json()     
            self._hyd_data = [conns["attributes"] for conns in hyd_data1["data"]]
            hyd_obj = []
            for num_hyd in range (0, len(self._hyd_data)):
                hyd_obj.append(self._hyd_data[num_hyd]["name"])         
            self._mec_results["catalogue_hyd"] = hyd_obj 

        # # url_gear = file:///api/mecht_gearbox
        # gearbox_data1 = requests.get(url_gear).json()
        url_gear = '/api/mecht_gearbox?fields[MechtGearbox]=id,name'
        # gearbox_data1 = call_intermodule_service('cm', url_gear).json()
        gearbox_resp = call_intermodule_service('cm', url_gear)
        if gearbox_resp.status_code == 200:
        # if "data" in gearbox_resp:   
            gearbox_data1 = gearbox_resp.json()     
        # with open(f"{self.folder}/mecht_gearbox_name.json", "r") as f:
        #     gearbox_data1 = json.load(f)         
            self._gearbox_data = [conns["attributes"] for conns in gearbox_data1["data"]]
            gb_obj = []
            for num_gb in range (0, len(self._gearbox_data)):
                gb_obj.append(self._gearbox_data[num_gb]["name"])         
            self._mec_results["catalogue_gb"] = gb_obj      
        
        # # Electrical catalogue
        # # url_el = file:///api/electt_scig
        # elec_data1 = requests.get(url_el).json()
        url_el = '/api/electt_scig?fields[ElecttScig]=id,name'
        # elec_data1 = call_intermodule_service('cm', url_el).json()
        elec_resp = call_intermodule_service('cm', url_el)
        if elec_resp.status_code == 200:
        # if "data" in elec_resp:   
            elec_data1 = elec_resp.json()   
        # with open(f"{self.folder}/electt_scig_name.json", "r") as f:
        #     elec_data1 = json.load(f)         
            self._elec_data = [conns["attributes"] for conns in elec_data1["data"]]
            elec_obj = []
            for num_elec in range (0, len(self._elec_data)):
                elec_obj.append(self._elec_data[num_elec]["name"])         
            self._elec_results["catalogue_gen"] = elec_obj                 

        # # Grid Conditioning catalogue
        # # url_gr = file:///api/gridc_power_converter
        # grid_data1 = requests.get(url_gr).json()
        url_gr = '/api/gridc_power_converter?fields[GridcPowerConverter]=id,name'
        # grid_data1 = call_intermodule_service('cm', url_gr).json()
        grid_resp = call_intermodule_service('cm', url_gr)
        if grid_resp.status_code == 200:
        # if "data" in grid_resp:   
            grid_data1 = grid_resp.json()   
        # with open(f"{self.folder}/gridc_power_converter_name.json", "r") as f:
        #     grid_data1 = json.load(f)          
            self._grid_data = [conns["attributes"] for conns in grid_data1["data"]]
            grid_obj = []
            for num_grid in range (0, len(self._grid_data)):
                grid_obj.append(self._grid_data[num_grid]["name"])         
            self._grid_results["catalogue_grid"] = grid_obj        

        # # Control catalogue
        # # url_co = not defined yet
        # control_data1 = requests.get(url_co).json() 
        url_co = '/api/control?fields[Control]=id,name'
        # control_data1 = call_intermodule_service('cm', url_co).json()
        control_resp = call_intermodule_service('cm', url_co)
        if control_resp.status_code == 200:
            control_data1 = control_resp.json()   
        # with open(f"{self.folder}/control_name.json", "r") as f:
        #     control_data1 = json.load(f)          
            self._control_data = [conns["attributes"] for conns in control_data1["data"]]
            control_obj = []
            for num_con in range (0, len(self._control_data)):
                control_obj.append(self._control_data[num_con]["name"])         
            self._control_results["catalogue_cont"] = control_obj          

    def _get_catalogue_results(self):
        """
        Get Catalogue data for the requested DTO+ project.

        Use the provided URL to make a request to Catalogue module and return the results.
        
        """
        
        # Mechanical catalogue
        if self.study.mechanical_inputs.complexity_level == 3:
            url_air = '/api/mecht_air_turbine'
            air_data_resp = call_intermodule_service('cm', url_air)
            if air_data_resp.status_code == 200:
                air_data = air_data_resp.json()
            # with open(f"{self.folder}/mecht_air_turbine.json", "r") as f:
            #     air_data = json.load(f)
                self._air_data = [conns["attributes"] for conns in air_data["data"]]
                mec_name = self.study.mechanical_inputs.name
                mec_id = -1
                for num in range (0, len(self._air_data)):
                    if self._air_data[num]["name"] == mec_name:  
                        mec_id = self._air_data[num]["id"]
                        type = self._air_data[num]["turb_type"]
                        
                if mec_id != -1: #the selected object is an airturbine
                    # file_name = "mecht_air_turbine_" + str(mec_id) + ".json"
                    # with open(f"{self.folder}/{file_name}", "r") as f:
                    #     air_data = json.load(f)      
                    url_air = '/api/mecht_air_turbine/' + str(mec_id)
                    air_data_resp = call_intermodule_service('cm', url_air)
                    if air_data_resp.status_code == 200: 
                        air_data = air_data_resp.json()       
                        self._air_data = air_data["data"]["attributes"]
                        del self._air_data["id"]  # remove "id" key from dictionary
                        for key, value in self._air_data.items():
                            if isinstance(value, str):
                                if value[0] == "[":
                                    aux=json.loads(self._air_data[key])
                                    self._air_data[key] = aux             
                        if type == 'Wells':
                            self._mec_results["wells_data"] = self._air_data
                        else:
                            self._mec_results["impulse_data"] = self._air_data
                else: #the selected object is not an airturbine, keep searching
                    url_hyd = '/api/mecht_hydraulic'
                    hyd_data_resp = call_intermodule_service('cm', url_hyd)
                    if hyd_data_resp.status_code == 200:
                        hyd_data = hyd_data_resp.json()
                    # with open(f"{self.folder}/mecht_hydraulic.json", "r") as f:
                    #     hyd_data = json.load(f)            
                        self._hyd_data = [conns["attributes"] for conns in hyd_data["data"]]
                        for num in range (0, len(self._hyd_data)):
                            if self._hyd_data[num]["name"] == mec_name:  
                                mec_id = self._hyd_data[num]["id"]
                        if mec_id != -1: #the selected object is an hydraulic                     
                            # file_name = "mecht_hydraulic_" + str(mec_id) + ".json"
                            url_hyd = '/api/mecht_hydraulic/' + str(mec_id)
                            hyd_data_resp = call_intermodule_service('cm', url_hyd)
                            if hyd_data_resp.status_code == 200:
                                hyd_data = hyd_data_resp.json()       
                            # with open(f"{self.folder}/{file_name}", "r") as f:
                            #     hyd_data = json.load(f)                 
                                self._hyd_data = hyd_data["data"]["attributes"]
                                del self._hyd_data["id"]  # remove "id" key from dictionary
                                for key, value in self._hyd_data.items():
                                    if isinstance(value, str):
                                        if value[0] == "[":
                                            aux=json.loads(self._hyd_data[key])
                                            self._hyd_data[key] = aux                 
                                self._mec_results["hydraulic_data"] = self._hyd_data
                        else: #the selected object is not an hydraulic, keep searching
                            url_gear = '/api/mecht_gearbox'
                            gearbox_data_resp = call_intermodule_service('cm', url_gear)
                            if gearbox_data_resp.status_code == 200:
                                gearbox_data = gearbox_data_resp.json()
                            # with open(f"{self.folder}/mecht_gearbox_name.json", "r") as f:
                            #     gearbox_data = json.load(f)                 
                                self._gearbox_data = [conns["attributes"] for conns in gearbox_data["data"]]
                                for num in range (0, len(self._gearbox_data)):
                                    if self._gearbox_data[num]["name"] == mec_name:  
                                        mec_id = self._gearbox_data[num]["id"]
                                if mec_id != -1: #the selected object is a gearbox                     
                                    # url_gear = file:///api/mecht_gearbox/{mec_id}
                                    # gearbox_data = requests.get(url_gear).json()
                                    url_gear = '/api/mecht_gearbox/' + str(mec_id)
                                    gearbox_data_resp = call_intermodule_service('cm', url_gear)
                                    if gearbox_data_resp.status_code == 200:
                                        gearbox_data = gearbox_data_resp.json()  
                                    # file_name = "mecht_gearbox_" + str(mec_id) + ".json"
                                    # with open(f"{self.folder}/{file_name}", "r") as f:
                                    #     gearbox_data = json.load(f)                     
                                        self._gearbox_data = gearbox_data["data"]["attributes"]
                                        del self._gearbox_data["id"]  # remove "id" key from dictionary
                                        for key, value in self._gearbox_data.items():
                                            if isinstance(value, str):
                                                if value[0] == "[":
                                                    aux=json.loads(self._gearbox_data[key])
                                                    self._gearbox_data[key] = aux                    
                                        self._mec_results["gearbox_data"] = self._gearbox_data 
                                else:
                                    print("Wrong mechanical object name")  
                
        # Electrical catalogue
        if self.study.electrical_inputs.complexity_level == 3: 
            url_el = '/api/electt_scig'
            elec_data_resp = call_intermodule_service('cm', url_el)
            if elec_data_resp.status_code == 200:
                elec_data = elec_data_resp.json()
            # with open(f"{self.folder}/electt_scig.json", "r") as f:
            #     elec_data = json.load(f)         
                self._elec_data = [conns["attributes"] for conns in elec_data["data"]]
                elec_name = self.study.electrical_inputs.name
                elec_id = -1
                for num in range (0, len(self._elec_data)):
                    if self._elec_data[num]["name"] == elec_name:  
                        elec_id = self._elec_data[num]["id"]        
                if elec_id != -1: #electrical object found                     
                    url_el = '/api/electt_scig/' + str(elec_id)
                    elec_data_resp = call_intermodule_service('cm', url_el)
                    if elec_data_resp.status_code == 200:
                        elec_data = elec_data_resp.json()  
                    # file_name = "electt_scig_" + str(elec_id) + ".json"
                    # with open(f"{self.folder}/{file_name}", "r") as f:
                    #     elec_data = json.load(f)                      
                        self._elec_data = elec_data["data"]["attributes"]
                        del self._elec_data["id"]  # remove "id" key from dictionary   
                        for key, value in self._elec_data.items():
                            if isinstance(value, str):
                                if value[0] == "[" or value[0] == "{":
                                    aux=json.loads(self._elec_data[key])
                                    self._elec_data[key] = aux
                        self._elec_results = self._elec_data 
                else:
                    print("Wrong electrical object name")     
            
        
        # Grid Conditioning catalogue
        if self.study.grid_inputs.complexity_level == 3:      
            url_gr = '/api/gridc_power_converter'
            grid_data_resp = call_intermodule_service('cm', url_gr)
            if grid_data_resp.status_code == 200:
                grid_data = grid_data_resp.json()
            # with open(f"{self.folder}/gridc_power_converter.json", "r") as f:
            #     grid_data = json.load(f)         
                self._grid_data = [conns["attributes"] for conns in grid_data["data"]]
                grid_name = self.study.grid_inputs.name
                grid_id = -1
                for num in range (0, len(self._grid_data)):
                    if self._grid_data[num]["name"] == grid_name:  
                        grid_id = self._grid_data[num]["id"]        
                if grid_id != -1: #grid object found                     
                    url_gr = '/api/gridc_power_converter/' + str(grid_id)
                    grid_data_resp = call_intermodule_service('cm', url_gr)
                    if grid_data_resp.status_code == 200:
                        grid_data = grid_data_resp.json()
                    # file_name = "gridc_power_converter_" + str(grid_id) + ".json"
                    # with open(f"{self.folder}/{file_name}", "r") as f:
                    #     grid_data = json.load(f)
                        self._grid_data = grid_data["data"]["attributes"] 
                        del self._grid_data["id"]  # remove "id" key from dictionary     
                        for key, value in self._grid_data.items():
                            if isinstance(value, str):
                                if value[0] == "[" or value[0] == "{":
                                    aux=json.loads(self._grid_data[key])
                                    self._grid_data[key] = aux         
                        self._grid_results = self._grid_data 
                else:
                    print("Wrong grid object name")             

        # Control catalogue
        if self.study.control_inputs.complexity_level == 3 and (self.study.control_inputs.control_type == "User defined" or self.study.control_inputs.control_type == "User_defined"):        
            url_co = '/api/control'
            control_data_resp = call_intermodule_service('cm', url_co)
            if control_data_resp.status_code == 200:
                control_data = control_data_resp.json()  
                # with open(f"{self.folder}/control.json", "r") as f:
                #     control_data = json.load(f)         
                self._control_data = [conns["attributes"] for conns in control_data["data"]]
                con_name = self.study.control_inputs.name
                con_id = -1
                for num in range (0, len(self._control_data)):
                    if self._control_data[num]["name"] == con_name:  
                        con_id = self._control_data[num]["id"]        
                if con_id != -1: #control object found                     
                    url_co = '/api/control/' + str(con_id)
                    control_data_resp = call_intermodule_service('cm', url_co)
                    if "data" in control_data_resp:
                        control_data = control_data_resp.json()
                    # file_name = "control_" + str(con_id) + ".json"
                    # with open(f"{self.folder}/{file_name}", "r") as f:
                    #     control_data = json.load(f) 
                        self._control_data = control_data["data"]["attributes"]
                        for key, value in self._control_data.items():
                            if isinstance(value, str):
                                if value[0] == "[":
                                    aux=json.loads(self._control_data[key])
                                    self._control_data[key] = aux                     
                        self._control_results["adim_vel"] = self._control_data["adim_vel"]
                        self._control_results["power_levels"] = self._control_data["power_pdf"]
                        self._control_results["load_levels"] = self._control_data["load_pdf"]
                        self._control_results["load_ranges"] = self._control_data["load_range_pdf"] 
                        self._control_results["date"] = self._control_data["date"]
                else:
                    print("Wrong control object name")    

       
    def _process_catalogue_data(self):
        """
        Process the Catalogue results data here to extract/calculate the necessary parameters
        
        """
        # not needed for catalogue, there is no process to make


    def _save_catalogue_data(self):
        """
        Write the processed Catalogue data to the local ET database.

        Store the processed Catalogue inputs in the :class:`~dtop_energytransf.storage.models.models.EnergyCaptureData`
        model table.
        
        """
        self.study.mechanical_inputs.update(**self._mec_results)
        self.study.electrical_inputs.update(**self._elec_results)
        self.study.grid_inputs.update(**self._grid_results)
        self.study.control_inputs.update(**self._control_results)
        db.session.commit()
