This package is for business logic.

There are three main object:
- Array
- Device
- PTO

Array objects will be made up of devices and devices made up of PTOs.

All data input and output are provided in a dictionary at each level (Array/Device/PTO)

The lowest level objects are the three transformation stages and the control. These four classes
are included in the 'transf_stages' folder.

The file 'main_thread.py' represents an example of the inputs gathered from GUI/Modules and reformatted
in order to build up an array object and run its performance assessment.
