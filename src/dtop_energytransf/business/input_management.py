# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from marshmallow import ValidationError

from dtop_energytransf.service import db
from dtop_energytransf.storage.schemas.schemas import InputsSchema


class InputManagement:
    """
    Manage the inputs required for the Energy Transformation module.

    For a single ET study, accepts and validates JSON request body data and saves this to the local ET database.

    **Public methods**

    - :meth:`validate_complexity_levels_and_inputs()` - perform the validation of input data, dependent on \
    complexity levels and technology

    - :meth:`update_db()` - update the input data in the local ET database
    
    By default the validation error field is set to False.

    This is set to True if any of the validation checks fail.

    When initialised, the class automatically performs the basic validation of the input JSON data against the
    :class:`~dtop_energytransf.storage.schemas.schemas.InputsSchema`

    :param dtop_energytransf.storage.models.models.EnergyTransformationStudy study: instance of an ET study
    :param dict input_data: the de-serialised JSON data extracted from the request body
    
    Attributes:
       _input_data (dict): the de-serialised JSON data extracted from the request body
       study (inst): instance of an ET study (dtop_energytransf.storage.models.models.EnergyTransformationStudy)
       validation_error (bool): Validation error flag

    Returns:
        none   

    """

    def __init__(self, study, input_data):

        self._input_data = input_data
        self.study = study
        self.validation_error = False

        self._validate_schema()

    def _validate_schema(self):
        """
        General validation to ensure no erroneous fields have been provided and that the general structure of the
        request body is correct

        Uses :class:`~dtop_energytransf.storage.schemas.schemas.InputsSchema` schema to perform validation of inputs.
        """
        try:
            inputs_schema = InputsSchema(
                only=("general_inputs", "mechanical_inputs", "electrical_inputs", "grid_inputs","control_inputs")
            )
            inputs_schema.load(self._input_data)
        except ValidationError as err:
            self.validation_error = err

    def validate_complexity_levels_and_inputs(self):
        """
        Perform validation of the inputs to the ET module. This is not coded. Can be done as a further improvement

        Validation is dependent on the technology being analysed and the complexity levels of each phase.

        :return: indication of whether validation has passed or failed; True if validation has passed, False otherwise.
        :rtype: bool
        """
        # TODO: validation to go here
        # if validation fails:
        #     return False
        # else
        return True

    def validate_cross_module_data(self):
        """
        Validate cross module data
        
        This is not coded. Can be done as a further improvement
        """
        # TODO: this might be a good place to execute the cross module data processing, which needs CPX levels
        pass

    def update_db(self):
        """
        Update the input tables in the local ET database

        Updates the general inputs, mechanical inputs, electrical inputs, grid inputs and control inputs tables.

        Returns:
            none
            
        """
        self._input_data['control_inputs']['complexity_level'] = self._input_data['mechanical_inputs']['complexity_level']
        self.study.general_inputs.update(**self._input_data['general_inputs'])
        self.study.mechanical_inputs.update(**self._input_data['mechanical_inputs'])
        self.study.electrical_inputs.update(**self._input_data['electrical_inputs'])
        self.study.grid_inputs.update(**self._input_data['grid_inputs'])
        self.study.control_inputs.update(**self._input_data['control_inputs'])
        self.study.status = 70

        db.session.commit()
