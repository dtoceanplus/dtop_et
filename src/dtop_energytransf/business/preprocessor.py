# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import numpy as np
import math

class Preprocessor(object):
    """
    Pre-process the user input data, cross-module data and other default data to be used by the Array class.

    The :class:`~dtop_energytransf.business.Array.Array` class is the main entry-point to the business logic.
    It requires an array ID and a set of inputs for the array, device and PTO to be initialised.
    This class uses the relevant data from the database table and converts it to the format required by the Array class.
    
    The array ID, array inputs and device inputs are formatted when the class is initialised.
    The final input argument required by the Array class is the set of PTO inputs.
    This set of inputs is a list containing the mechanical, electrical, grid and control initialisation parameters.
    Individual processors are required for each of these stages and for each complexity level.
    When the class is initialised, the appropriate processor for each stage is identified.
    
    Args:
        ec_data (dict): processed data from EC module
        mc_data (dict): processed data from MC module
        sc_data (dict): processed data from SC module
        general_inputs (inst): instance of GeneralInputs model (dtop_energytransf.storage.models.models.GeneralInputs)
        mech_inputs (inst): instance of MechanicalInputs model (dtop_energytransf.storage.models.models.MechanicalInputs mech_inputs)
        elec_inputs (inst): instance of ElectricalInputs model (dtop_energytransf.storage.models.models.ElectricalInputs)
        grid_inputs (inst): instance of GridInputs model (dtop_energytransf.storage.models.models.GridInputs)
        control_inputs (inst): instance of ControlInputs model dtop_energytransf.storage.models.models.ControlInputs)

    Attributes:
        _ec_data (dict): processed data from EC module
        _mc_data (dict): processed data from MC module
        _sc_data (dict): processed data from SC module
        _mechanical_inputs (inst): instance of MechanicalInputs model (dtop_energytransf.storage.models.models.MechanicalInputs mech_inputs)
        _electrical_inputs (inst): instance of ElectricalInputs model (dtop_energytransf.storage.models.models.ElectricalInputs)
        _grid_inputs (inst): instance of GridInputs model (dtop_energytransf.storage.models.models.GridInputs)
        _control_inputs (inst): instance of ControlInputs model dtop_energytransf.storage.models.models.ControlInputs)
        _mechanical_params (dict): Required initialisation parameters for the mechanical transformation coming from the GUI
        _electrical_params (dict): Required initialisation parameters for the electrical transformation coming from the GUI
        _grid_params (dict): Required initialisation parameters for the grid conditioning coming from the GUI
        _control_params(dict): Required initialisation parameters for the control coming from the GUI   
        _technology (str): Technology of the Device (wave or tidal)
        _dof (int): Number of Degrees of Freedom
        _n_devices (int): Number of devices
        _c_pto (list): PTO damping applied by the PTO to get the mean power and velocity std
        _p_capt (list): Captured Energy read from Energy Capture module
        _sigma_v (list): 0.1 of hub velocity (Tidal) or sqrt (Captured Power / Cpto) (Wave)
        ct_tidal (list): Inverse of omega
        _environmental (List): The environmental conditions, coherent with each perf_inputs item to assess all devices: Hs (wave), Vc (tidal), Occ, Tz and site id 
        _array_perf_inputs (List): List of performance inputs required to evaluate the performance of the Array class for all devices. Includes sigma_v, Cpto and inverse of omega
        _mech_processor (func) : mechanical inputs process function
        _elec_processor (func) : electrical inputs process function
        _grid_processor (func) : grid inputs process function
        _control_processor (func) : control inputs process function
        _mech_cat (dict): dictionary with all the mechanical parameters from the catalogue
        _elec_cat (dict): dictionary with all the electrical parameters from the catalogue
        _grid_cat (dict): dictionary with all the grid parameters from the catalogue
        _control_cat (dict): dictionary with all the control parameters from the catalogue
        _array_id (str): Array identifier
        _array_inputs (dict) : Required inputs to define all required info at array level        
        device_inputs (dict): Required inputs at device level
        pto_inputs (list): Required inputs at pto level
    
    Returns:
        none

    """

    # Default grid voltage for CPX 1
    GRID_V = 690
    # Default array id
    DEFAULT_ARRAY_ID = "Array_01"

    def __init__(
        self,
        ec_data,
        mc_data,
        sc_data,
        general_inputs,
        mech_inputs,
        elec_inputs,
        grid_inputs,
        control_inputs,
    ):
         
        self._ec_data = ec_data
        self._mc_data = mc_data
        self._sc_data = sc_data
        self._mechanical_inputs = mech_inputs
        self._electrical_inputs = elec_inputs
        self._grid_inputs = grid_inputs
        self._control_inputs = control_inputs
        self._mechanical_params = None
        self._electrical_params = None
        self._grid_params = None
        self._control_params = None
        self._technology = mc_data.technology
        self._dof = mc_data.dof

        self._n_devices = ec_data.num_devices
        self._c_pto = mc_data.Cpto
        self._p_capt = ec_data.Captured_Power
        self._sigma_v = ec_data.sigma_v
        self.ct_tidal = mc_data.Ct_tidal
        self._environmental = []
        self._array_perf_inputs = []
        self._mech_processor = None
        self._elec_processor = None
        self._grid_processor = None
        self._control_processor = None
        self._pto_inputs = []
        self._mech_cat = {}
        self._elec_cat = {}
        self._grid_cat = {}
        self._control_cat = {}
        self._process_catalogue()
        self._initialise_individual_processors()

        self._array_id = self.DEFAULT_ARRAY_ID  # TODO: Come back to this when implementing DESIGN mode
        self._array_inputs = {
            "Technology": self._technology, 
            "Number of devices": ec_data.num_devices,
        }
        if self._technology == "Wave":
            self._device_inputs = {             
                "dof": mc_data.dof,
                "Parallel_PTOs": general_inputs.parallel_ptos,
                "Shutdown_flag": general_inputs.dev_shut_flag,
                "cut_in_out": mc_data.cut_in_out
            }
        else:
            self._device_inputs = {             
                "dof": mc_data.number_rotor,
                "Parallel_PTOs": general_inputs.parallel_ptos,
                "Shutdown_flag": general_inputs.dev_shut_flag,
                "cut_in_out": mc_data.cut_in_out
            }
              
    def _initialise_individual_processors(self):
        """Initialise the individual processor for each of the transformation stages"""
        self._mech_processor = self._format_method_name('mech', self._mechanical_inputs)
        self._elec_processor = self._format_method_name('elec', self._electrical_inputs)
        self._grid_processor = self._format_method_name('grid', self._grid_inputs)
        self._control_processor = self._format_method_name('control', self._control_inputs)

    def _format_method_name(self, stage_name, input_data):
        """Return the appropriate processing function for the request stage and complexity level

        Args:
            stage_name (str): Transformation stage name (mech, elec, grid or control)
            input_data (inst): Instance of the model of the input data corresponding to the transformation stage

        Returns:
            func: Return the appropriate processing function for the request stage and complexity level
        """        
        
        return getattr(self, f"_process_{stage_name}_inputs_cpx{input_data.complexity_level}")

    def preprocess(self):
        """
        Perform the pre-processing of the input data.

        Executes the four processors for each of the stages to process the PTO inputs.
        Then creates the required list of input data for the PTO input set.

        Returns:
            none

        """
        self._mech_processor()
        self._elec_processor()
        self._grid_processor()
        self._control_processor()
        self._pto_inputs = [
            self._mechanical_params,
            self._electrical_params,
            self._grid_params,
            self._control_params
        ]

    def _process_mech_inputs_cpx1(self):
        """Process mechanical inputs for mechanical complexity level 1"""
        self._mechanical_params = {
            "Type": "Mech_Simplified", 
            "init_args": [
                self._mechanical_inputs.rated_power,
                self._mechanical_inputs.t_ratio,  
                self._technology,
            ]}
        if self._technology == "Wave":
            self._mechanical_params["perf_args"] = [] # not needed in simplified
        else: # in Tidal Mechanical Performance Args will be updated later with Ct_tidal EC input
            self._mechanical_params["perf_args"] = {}
        self._mechanical_params["mech_t_ratio"]= 1  # only needed for Airturbine and Hydraulic to simulate a gearbox speed ratio
        

    def _process_mech_inputs_cpx2(self):
        """Process mechanical inputs for mechanical complexity level 2"""
        self._mechanical_params = {
            "Type": self._mechanical_inputs.Type}
        if self._mechanical_inputs.Type == "AirTurbine":
            self._mechanical_params["init_args"] = [self._mechanical_inputs.turb_type, self._mechanical_inputs.diameter,
                                                    self._mechanical_inputs.Sowc, self._mech_cat]
            self._mechanical_params["perf_args"] = [] # not needed in Airturbine
            self._mechanical_params["mech_t_ratio"] = self._mechanical_inputs.mech_t_ratio
        elif self._mechanical_inputs.Type == "Hydraulic":
            self._mechanical_params["init_args"] = [self._mechanical_inputs.size, self._mechanical_inputs.Ap, self._mech_cat]
            self._mechanical_params["perf_args"] = [self._mechanical_inputs.flow]
            self._mechanical_params["mech_t_ratio"] = self._mechanical_inputs.mech_t_ratio            
        elif self._mechanical_inputs.Type == "Gearbox":
            self._mechanical_params["init_args"] = [self._mechanical_inputs.rated_power,
                                                    self._mechanical_inputs.t_ratio, self._technology, self._mech_cat]
            if self._technology == "Wave":
                self._mechanical_params["perf_args"] = [7.62]
            else: # in Tidal Mechanical Performance Args will be updated later with Ct_tidal EC input
                self._mechanical_params["perf_args"] = {}
            self._mechanical_params["mech_t_ratio"] = 1 # only needed for Airturbine and Hydraulic to simulate a gearbox speed ratio
        elif self._mechanical_inputs.Type == "Direct_Tidal":
            self._mechanical_params["init_args"] = []
            self._mechanical_params["perf_args"] = {}
            self._mechanical_params["mech_t_ratio"] = 1                        



    def _process_mech_inputs_cpx3(self):
        """Process mechanical inputs for mechanical complexity level 3"""
        self._mechanical_params = {
            "Type": self._mechanical_inputs.Type}
        if self._mechanical_inputs.Type == "AirTurbine":
            self._mechanical_params["init_args"] = [self._mechanical_inputs.turb_type, self._mechanical_inputs.diameter,
                                                    self._mechanical_inputs.Sowc, self._mech_cat]
            self._mechanical_params["perf_args"] = []  # not needed in Airturbine
            self._mechanical_params["mech_t_ratio"] = self._mechanical_inputs.mech_t_ratio
        elif self._mechanical_inputs.Type == "Hydraulic":
            self._mechanical_params["init_args"] = [self._mechanical_inputs.size, self._mechanical_inputs.Ap, self._mech_cat]
            self._mechanical_params["perf_args"] = [self._mechanical_inputs.flow]
            self._mechanical_params["mech_t_ratio"] = self._mechanical_inputs.mech_t_ratio
        elif self._mechanical_inputs.Type == "Gearbox":
            self._mechanical_params["init_args"] = [self._mechanical_inputs.rated_power,
                                                    self._mechanical_inputs.t_ratio, self._technology, self._mech_cat]
            if self._technology == "Wave":
                self._mechanical_params["perf_args"] = [7.62]
            else: # in Tidal Mechanical Performance Args will be updated later with Ct_tidal EC input
                self._mechanical_params["perf_args"] = {}
            self._mechanical_params["mech_t_ratio"] = 1  # only needed for Airturbine and Hydraulic to simulate a gearbox speed ratio
        elif self._mechanical_inputs.Type == "Direct_Tidal":
            self._mechanical_params["init_args"] = []
            self._mechanical_params["perf_args"] = {}
            self._mechanical_params["mech_t_ratio"] = 1   


    def _process_elec_inputs_cpx1(self):
        """Process electrical inputs for electrical complexity level 1"""
        self._electrical_params = {
            "Type": "Elect_Simplified",
            "init_args": [self._electrical_inputs.rated_power]
        }

    def _process_elec_inputs_cpx2(self):
        """Process electrical inputs for electrical complexity level 2"""
        self._electrical_params = {
            "Type": self._electrical_inputs.Type,
            "init_args": [
                self._electrical_inputs.rated_power,
                self._electrical_inputs.pp,
                self._electrical_inputs.Vnom,
                self._electrical_inputs.fnom,
                self._electrical_inputs.rel_T_maxnom,
                self._electrical_inputs.rel_V_maxnom,
                self._electrical_inputs.gen_class,
                self._elec_cat
            ]
        }

    def _process_elec_inputs_cpx3(self):
        """Process electrical inputs for electrical complexity level 3"""
        self._electrical_params = {
            "Type": self._electrical_inputs.Type,
            "init_args": [
                self._electrical_inputs.rated_power,
                self._electrical_inputs.pp,
                self._electrical_inputs.Vnom,
                self._electrical_inputs.fnom,
                self._electrical_inputs.rel_T_maxnom,
                self._electrical_inputs.rel_V_maxnom,
                self._electrical_inputs.gen_class,
                self._elec_cat
            ]
        }

    def _process_grid_inputs_cpx1(self):
        """Process grid inputs for grid transformation complexity level 1"""
        self._grid_params = {
            "Type": "Grid_Simplified",
            "init_args": [
                self._grid_inputs.rated_power,
                self.GRID_V  # TODO: Assumed for CPX 1, will be an input for CPX 2 and 3
            ],
        }

    def _process_grid_inputs_cpx2(self):
        """Process grid inputs for grid transformation complexity level 2"""
        self._grid_params = {
            "Type": self._grid_inputs.Type,
            "init_args": [
                self._grid_inputs.rated_power,
                self._grid_inputs.Vdc,
                self._grid_inputs.fsw,
                self._electrical_inputs.Vnom,
                self._electrical_inputs.fnom,
                self._electrical_inputs.Lgen,
                self._electrical_inputs.Rgen,
                self._grid_inputs.Vgrid,
                self._grid_inputs.Rgrid,
                self._grid_inputs.cosfi,
                self._grid_inputs.Lgrid,
                self._grid_inputs.Fgrid,
                self._grid_cat
            ],
        }

    def _process_grid_inputs_cpx3(self):
        """Process grid inputs for grid transformation complexity level 3"""
        self._grid_params = {
            "Type": self._grid_inputs.Type,
            "init_args": [
                self._grid_inputs.rated_power,
                self._grid_inputs.Vdc,
                self._grid_inputs.fsw,
                self._electrical_inputs.Vnom,
                self._electrical_inputs.fnom,
                self._electrical_inputs.Lgen,
                self._electrical_inputs.Rgen,
                self._grid_inputs.Vgrid,
                self._grid_inputs.Rgrid,
                self._grid_inputs.cosfi,
                self._grid_inputs.Lgrid,
                self._grid_inputs.Fgrid,
                self._grid_cat
            ],
        }

    def _process_control_inputs_cpx1(self):
        """Process control inputs for control complexity level 1 (uses DEFAULT values from DB)"""
        self._control_params = {
            "Type": self._control_inputs.control_type,
            "init_args": [
                self._control_inputs.n_sigma,
                self._control_inputs.bins,
                self._control_inputs.vel_mean,
                self._control_cat
            ]
        }

    def _process_control_inputs_cpx2(self):
        """Process control inputs for control complexity level 2 (passive, values from the GUI)"""
        self._control_params = {
            "Type": self._control_inputs.control_type,
            "init_args": [
                self._control_inputs.n_sigma,
                self._control_inputs.bins,
                self._control_inputs.vel_mean,
                self._control_cat
            ]
        }

    def _process_control_inputs_cpx3(self):
        """Process control inputs for control complexity level 3 (user defined, values from the GUI)"""
        self._control_params = {
            "Type": self._control_inputs.control_type,
            "init_args": [
                self._control_inputs.n_sigma,
                self._control_inputs.bins,
                self._control_inputs.vel_mean,
                self._control_cat
            ]
        }

    def _process_catalogue(self):
        """Process catalogue inputs"""
        self._elec_cat = {
            "x_f": self._electrical_inputs.x_f,
            "shaft_diam": self._electrical_inputs.shaft_diam,
            "phi_cos":self._electrical_inputs.phi_cos,
            "cos_phi":self._electrical_inputs.cos_phi,
            "sigma_h": self._electrical_inputs.sigma_h,
            "sigma_e": self._electrical_inputs.sigma_e,
            "B": self._electrical_inputs.B,
            "Gen_mass": self._electrical_inputs.Gen_mass,
            "wind_mass_fraction": self._electrical_inputs.wind_mass_fraction,
            "I_nom": self._electrical_inputs.I_nom,
            "Res": self._electrical_inputs.Res,
            "Life": self._electrical_inputs.Life,
            "cost": self._electrical_inputs.cost,
            "thick_max": self._electrical_inputs.thick_max
        }
        self._grid_cat = {
            "temp": self._grid_inputs.temp,
            "cost": self._grid_inputs.cost,
            "life": self._grid_inputs.life,
            "mass": self._grid_inputs.mass,
            "IGBT150": self._grid_inputs.IGBT150,
            "IGBT450": self._grid_inputs.IGBT450,
            "IGBT800": self._grid_inputs.IGBT800,
            "IGBT1600": self._grid_inputs.IGBT1600,
            "Diode150": self._grid_inputs.Diode150,
            "Diode450": self._grid_inputs.Diode450,
            "Diode800": self._grid_inputs.Diode800,
            "Diode1600": self._grid_inputs.Diode1600,

        }
        self._control_cat = {
            "adim_vel": self._control_inputs.adim_vel,
            "Power_levels": self._control_inputs.power_levels,
            "Load_levels": self._control_inputs.load_levels,
            "Load_ranges": self._control_inputs.load_ranges
        }
        if self._mechanical_inputs.Type == "AirTurbine":
            if self._mechanical_inputs.turb_type == "Wells":
                self._mech_cat = {
                    "at_cat": self._mechanical_inputs.wells_data
                }
            elif self._mechanical_inputs.turb_type == "Impulse":
                self._mech_cat = {
                    "at_cat": self._mechanical_inputs.impulse_data
                }
        elif self._mechanical_inputs.Type == "Hydraulic":
            self._mech_cat = {
                "hy_cat": self._mechanical_inputs.hydraulic_data
            }
        elif self._mechanical_inputs.Type == "Gearbox":
            self._mech_cat = {
                "gb_cat": self._mechanical_inputs.gearbox_data
            }



    # def generate_performance_inputs(self):
    #     """
    #     Generate the performance data required to evaluate the performance of the array.
    #     """
    #     lst_sigma2 = []
    #     lst_cpto2 = []
    #     for cont in range(0, self._dof):
    #         lst_sigma2.append([float(p) / c for p, c in zip(self._p_capt, self._c_pto)])
    #         #lst_sigma2.append([np.sqrt(float(p) / c) for p, c in zip(self._p_capt, self._c_pto)])
    #         lst_cpto2.append(self._c_pto)
    #         perf_inputs = {"sigma_v": lst_sigma2, "C_pto": lst_cpto2}
    #         self._array_perf_inputs.append(perf_inputs)


    def generate_performance_inputs(self):
        """
        Generate the performance data required to evaluate the performance of the array.

        Returns:
            none
        """
        cont_dof = 0
        cpto_aux = []
        Device_perf_inputs = {}
        Device_environmental = {}
        if self._technology == "Wave":            
            for cont20 in range(0, self._n_devices): 
                lst_sigma2 = []
                lst_cpto2 = []
                if self._ec_data.ec_cmpx == 1: # complexity 1
                    sigma_aux = np.sqrt(self._p_capt[cont20][0] / self._c_pto[0])
                    lst_sigma2.append([sigma_aux])
                    lst_cpto2.append([self._c_pto[0]])
                else:
                    if self._ec_data.ec_cmpx == 2: 
                        sea_states = len(self._p_capt[0]) 
                        cpto_dof = []
                        sigma2_dof = []
                        for cont50 in range(0, sea_states):                
                            cpto_aux = self._c_pto[0]
                            cpto_dof.append(cpto_aux)
                            aux = (self._p_capt[cont20][cont50] / self._dof)/cpto_aux
                            sigma2_dof.append(np.sqrt(aux))    
                        lst_cpto2.append(cpto_dof)                       
                        lst_sigma2.append(sigma2_dof)
                    else: #complexity  3
                        num_hs = len(self._c_pto)
                        num_tp = len(self._c_pto[0])
                        num_dir = len(self._c_pto[0][0])
                        n_rows = len(self._c_pto[0][0][0][0]) # max dof n_rows^2
                        for cont1 in range(0, n_rows):
                            for cont2 in range(0, n_rows):
                                cpto_dof = []
                                sigma2_dof = []
                                counter_sea_state = 0
                                for cont50 in range(0, num_hs):
                                    for cont60 in range(0, num_tp):
                                        for cont70 in range(0, num_dir): 
                                            if self._c_pto[cont50][cont60][cont70][cont1][cont2] > 0:                      
                                                cpto_aux = self._c_pto[cont50][cont60][cont70][cont1][cont2]
                                                cpto_dof.append(cpto_aux)
                                                aux = (self._p_capt[cont20][counter_sea_state] / self._dof)/cpto_aux
                                                sigma2_dof.append(np.sqrt(aux))
                                            counter_sea_state = counter_sea_state +1
                                #print("-*-*-*cpto_dof", cpto_dof)
                                if cpto_dof != []:
                                    lst_cpto2.append(cpto_dof)
                                #print("-*-*-*lst_cpto2", lst_cpto2)
                                    lst_sigma2.append(sigma2_dof)
                Device_perf_inputs = {"sigma_v": lst_sigma2, "C_pto": lst_cpto2}
                # print("-*-*-*lst_cpto2-device", lst_cpto2) # cpto per device
                # print("-*-*-*lst_sigma2-device", lst_sigma2) # sigmav per device
                Device_environmental = {"Hs": self._sc_data.Hs, "Occ": self._sc_data.Occ, "Tz" : self._sc_data.Tz, "id" : self._sc_data.sc_id}
                self._array_perf_inputs.append(Device_perf_inputs)
                self._environmental.append(Device_environmental)
              
        else:  # Tidal
            sea_states = len(self._p_capt[0])
            cont_r = 0
            for cont3 in range(0, self._n_devices):
                lst_sigma2 = []
                lst_cpto2 = []
                lst_ct = []
                Vc_dev = []
                for cont5 in range(0, self._mc_data.number_rotor):  # dof = number of ptos "number_rotor" vigilar
                    if self._ec_data.ec_cmpx < 4:
                        # lst_sigma2.append(self._sigma_v[cont_r]) #old
                        lst_cpto2.append(self._c_pto[cont_r])
                        lst_ct.append(self.ct_tidal[cont_r]) 
                        Vc_real = []
                        for ii in range(len(self._p_capt[cont_r])):
                            if self._p_capt[cont_r][ii]> 1e-9:
                                Vc_real.append(math.sqrt(self._p_capt[cont_r][ii]/(self._device_inputs["Parallel_PTOs"]*self._c_pto[cont_r][ii]))) #to be canceled if it doesnt work  
                            else:
                                Vc_real.append(0.)

                        # Vc_aux = [10*x for x in self._sigma_v[cont_r]] #old                   
                        Vc_aux = [min(Vc_real[i],10*self._sigma_v[cont_r][i]) for i in range(len(self._sigma_v[cont_r]))] #to be cancelled if it doesn't work
                        lst_sigma2.append([x/10 for x in Vc_aux]) #to be cancelled  if it doesn't work
                        Vc_dev.append(Vc_aux) 
                        cont_r = cont_r+1
                        
                Device_perf_inputs = {"sigma_v": lst_sigma2, "C_pto": lst_cpto2, "Ct_tidal": lst_ct, "Captured_Power_EC" : self._p_capt}
                Device_environmental = {"Vc": Vc_dev, "Occ": self._sc_data.Occ, "Tz" : self._sc_data.Tz, "id" : self._sc_data.sc_id}
                self._array_perf_inputs.append(Device_perf_inputs)
                self._environmental.append(Device_environmental)

    @property
    def array_class_inputs(self):
        """
        The tuple of inputs required to initialise the main Array class in the business logic.

        Returns:
            array_class_inputs (tuple): The tuple of inputs required to initialise the main Array class in the business logic.
        """
        array_class_inputs = (
            self._array_id,
            self._array_inputs,
            self._device_inputs,
            self._pto_inputs,
        )

        return array_class_inputs

    @property
    def array_perf_inputs(self):
        """
        The tuple of performance inputs required to evaluate the performance of the Array class

        Returns:
            array_perf_inputs (tuple): The tuple of performance inputs required to evaluate the performance of the Array class
 
        """
        array_perf_inputs = (self._array_perf_inputs, self._environmental)

        return array_perf_inputs
