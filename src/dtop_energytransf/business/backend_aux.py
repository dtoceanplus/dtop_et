# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# from dtop_energytransf.business import PTO as PTO_class
# from dtop_energytransf.business import Device as Dev_class
from dtop_energytransf.business import Array as Array_class
from copy import deepcopy

import types
import numpy as np


def et_array_to_jsondict(array_obj: Array_class):
    # Array object to json serializable dictionary
    arr_copy = deepcopy(array_obj)
    # -----------------------------------------------------------------------------------------
    # pto_obj = arr_copy.Devices['DEV_0']['Device'].PTOs['PTO_0_0']['PTO']
    # dic_pto = pto_to_dict(pto_obj)
    arr_excluded = ["Devices"]
    arr_ndarrays = [
        "Array_P_Capt",
        "Array_P_Elect",
        "Array_P_Mech",
        "Array_Pa_Grid",
        "Array_Pr_Grid",
    ]
    dic_results = object_to_dict_array(
        arr_copy, excluded=arr_excluded, ndarrays=arr_ndarrays
    )
    # Extra transformation
    # transform unit32 to unit
    dic_results["BoM"]["value"]["qnt"] = [
        int(x) for x in dic_results["BoM"]["value"]["qnt"]
    ]
    # transform Series to json
    # for k, v in dic_results["Array_Env_Conditions"]["value"].items():
    #     dic_results["Array_Env_Conditions"]["value"].update({k: v.tolist().to_json(orient="split")})
    # -----------------------------------------------------------------------------------------
    # DEVICES
    dev_dict_ndarrays = [
        "Captured_Power",
        "MechT_Power",
        "ElectT_Power",
        "GridC_Active_Power",
        "GridC_reactive_Power",
    ]
    dev_dict_excluded = ["Device"]
    dev_ndarrays = [
        "Dev_P_Capt",
        "Dev_P_Elect",
        "Dev_P_Mech",
        "Dev_Pa_Grid",
        "Dev_Pr_Grid",
    ]
    dev_excluded = ["PTOs"]
    dic_key = {}
    for key in arr_copy.Devices.keys():
        dic_dev = object_to_dict_array(
            arr_copy.Devices[key]["Device"],
            excluded=dev_excluded,
            ndarrays=dev_ndarrays,
        )

        pto_dict_ndarrays = [
            "Captured_Power",
            "MechT_Power",
            "ElectT_Power",
            "GridC_Active_Power",
            "GridC_reactive_Power",
        ]
        pto_dict_excluded = ["PTO"]
        pto_dict_lst_ndarrays = ["Stress_Loads", "Stress_Load_Range"]
        dic_aux = {}
        # ------------------------------------------------------------------------------------
        # PTO
        for k_pto in arr_copy.Devices[key]["Device"].PTOs.keys():
            dic_aux_pto = {}
            for k, v in arr_copy.Devices[key]["Device"].PTOs[k_pto].items():
                if k in pto_dict_ndarrays:
                    v = v.tolist()
                if k not in pto_dict_excluded:
                    dic_aux_pto.update({k: v})
            dic_aux_pto["Mech_mass"] = int(dic_aux_pto["Mech_mass"])
            # transform list with ndarrays in lists
            for vble in pto_dict_lst_ndarrays:
                lst_ss = []
                for ss in dic_aux_pto[vble]:
                    lst_dof = []
                    for dof in ss:
                        if isinstance(dof, np.ndarray):
                            lst_dof.append(dof.tolist())
                        else:
                            lst_dof.append(dof)
                    lst_ss.append(lst_dof)
                dic_aux_pto.update({vble: lst_ss})
            # dic_pto_obj = pto_to_dict(
            #     arr_copy.Devices[key]["Device"].PTOs[k_pto]["PTO"]
            # )
            # dic_aux_pto.update({"PTO": dic_pto_obj})
            dic_aux.update({k_pto: dic_aux_pto})
        dic_dev.update({"PTOs": dic_aux})
        # ------------------------------------------------------------------------------------
        dic_aux = {}
        for k_dev, v_dev in arr_copy.Devices[key].items():
            if k_dev in dev_dict_ndarrays:
                v_dev = v_dev.tolist()
            # if k_dev not in dev_dict_excluded:
            #     dic_aux.update({k_dev: v_dev})
        for k, v in dic_aux.items():
            dic_dev.update({k: v})
        del dic_aux
        # Extra transformation in device
        # for k, v in dic_dev["Dev_Env_Conditions"]["value"].items():
        #     dic_dev["Dev_Env_Conditions"]["value"].update({k: v.to_json(orient="split")})
        for k in dic_dev["Dev_Captured_Power"]["value"].keys():
            lst_aux = []
            for x in dic_dev["Dev_Captured_Power"]["value"][k]:
                lst_aux.append(x.tolist())
            dic_dev["Dev_Captured_Power"]["value"].update({k: lst_aux})
        dic_key.update({key: dic_dev})
    dic_results.update({"Devices": dic_key})
    return dic_results


def object_to_dict(et_obj, excluded: list, ndarrays: list):
    obj_copy = deepcopy(et_obj)
    dic_obj = {}
    for att in [x for x in obj_copy.__dir__() if not x.startswith("_")]:
        att_value = getattr(obj_copy, att)
        if not isinstance(att_value, types.MethodType):
            if att in ndarrays:
                # transform numpy arrays to list
                att_value = att_value.tolist()
            if att not in excluded:
                dic_obj.update({att: att_value})
    return dic_obj


def object_to_dict_array(et_obj, excluded: list, ndarrays: list):
    obj_copy = deepcopy(et_obj)
    dic_obj = {}
    for att in [x for x in obj_copy.__dir__() if not x.startswith("_")]:
        att_value = getattr(obj_copy, att)
        if not isinstance(att_value, types.MethodType):
            if att in ndarrays:
                # transform numpy arrays to list
                att_value["value"] = att_value["value"].tolist()
            if att not in excluded:
                dic_obj.update({att: att_value})
    return dic_obj


def pto_to_dict(pto_obj):
    pto_objects = ["Control_obj", "ElectT_obj", "GridC_obj", "MechT_obj"]
    dic_pto = object_to_dict(pto_obj, excluded=pto_objects, ndarrays=[])
    # Keys in this dictionary must match with elements in pto_objects
    pto_ndarrays = {
        "Control_obj": [
            "adim_vel",
            "pdf_P",
            "pdf_T",
            "pdf_T_range",
            "prob_P",
            "prob_T",
            "prob_T_range",
        ],
        "ElectT_obj": [
            "Elect_I_est",
            "Elect_I_mean",
            "Elect_P_mean",
            "Elect_cosphi",
            "Elect_eff",
            "I_est",
            "N",
            "V_est",
            "f_est",
        ],
        "GridC_obj": [
            "Grid_P_mean",
            "Grid_P_mean_r",
            "I_grid",
            "N",
            "V1",
            "V2",
            "cosphi_gen",
            "pe_eff",
            "power_gen",
        ],
        "MechT_obj": ["Mech_P_mean", "Mech_T", "Mech_T_mean", "N", "omega", "tao"],
    }
    for att in pto_objects:
        att_ndarrays = pto_ndarrays[att]
        my_dic = object_to_dict(
            getattr(pto_obj, att), excluded=[], ndarrays=att_ndarrays
        )
        if att == "MechT_obj":
            # change_int = ['Mech_mass', 'mass_props', 'mass_unit']
            change_int = ["Mech_mass", "mass_unit"]
            for x in change_int:
                my_dic.update({x: int(my_dic[x])})
            # pandas
            if "AirT_adim" in my_dic:
                my_dic.update(
                    {"AirT_adim": my_dic["AirT_adim"].to_json(orient="split")}
                )
        dic_pto.update({att: my_dic})
    return dic_pto
