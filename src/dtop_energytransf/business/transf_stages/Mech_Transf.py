# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import numpy as np
import pathlib
import pandas as pd
from scipy.optimize import curve_fit, minimize
import os
from scipy.stats import chi2
import matplotlib.pyplot as plt
from dtop_energytransf.utils import get_project_root


def eff_fit(x, k1, k2, k3, k4):
    y = 1 - k1 * np.exp(k2 * x) - k3 * np.exp(k4 * x)
    return y


class EC2Mech:
    """Parent class to force all classes compute the required metrics.

    Parent class for the sub-class to compute the Captured Energy to Mechanical Energy. It provides all sub-classes
    with functions to compute what the four assessments are expecting at each environmental
    state. The specified functions are:

    - performance()
    
    - reliability()
    
    - env_impact()
    
    - cost()
    
    The initialisation is to be done at sub-class level (child), and all the required properties are also defined at
    sub-class level so that the definitive parameters/metrics can be evaluated with the functions provided
    by this class (parent).

    """

    def performance(self, Cpto, vel_v, torque_prob, pot_prob, obj_args):
        """Method to compute the performance subject to specific environmental conditions

        Function to compute the performance of a child class subject to specific environmental conditions,
        represented in the torque and power probabilities

        :param float Cpto: PTO damping represented through the operational conditions of the child object
        :param list vel_v: Velocity vector within the environmental condition from which the power is generated
        :param list torque_prob: Probability distribution of the torque levels, corresponding to the vel_v
        :param list pot_prob: Probability distribution of the power levels, corresponding to the vel_v
        :param dict obj_args: other inputs that may be needed within the object
        :return: The method doesn't return values, it stores the results in the corresponding variables

        Required to be DEFINED in the child:
         - self.Mech_T - 1D array
         - self.omega - 1D array

        Attributes:
            Mech_T_mean (list): Output mechanical torque multiplied by probability distribution of the torque levels
            Mech_P_mean (list): Output mechanical power multiplied by probability distribution of the power levels
            Mech_eff_mean (float): Mean mechanical efficiency value

        """

        #  Linearization of the Mechanical Transformation object in case it is defined in the child
        if hasattr(self, "perf_props"):
            self.perf_props(Cpto, vel_v, pot_prob, obj_args)
        
        Pcapt = Cpto * vel_v ** 2 * pot_prob

        self.Pcapt = Pcapt

        self.Mech_T_mean = self.Mech_T * torque_prob
        self.Mech_P_mean = self.omega * self.Mech_T * pot_prob

        # Negative torque/power/efficiencies are made zero to avoid weird results
        self.Mech_T_mean[self.Mech_T_mean < 0] = 0
        self.Mech_P_mean[self.Mech_P_mean < 0] = 0

        if np.sum(Pcapt) == 0:
            self.Mech_eff_mean = 0 * Pcapt
        else:
            self.Mech_eff_mean = np.sum(self.Mech_P_mean) / np.sum(Pcapt)

        # print('Mech_eff: ' + str(self.Mech_eff_mean))

    def reliability(self, load_prob, EC_T, Tz):
        """Method to compute the damage subject to a specific environmental condition

        Function to compute the damage of the load ranges on the child object during all working hours of the year
        of the corresponding environmental condition

        :param list load_prob: Probability distribution of load ranges
        :param int EC_T: Annual equivalent time of the corresponding environmental condition [s]
        :param float Tz: Average period of the force ranges
        :return: The method doesn't return values, it stores the results in the corresponding variables

        Required to be DEFINED in the child:
         - N - 1D array

        Attributes:
            Mech_damage (float): Damage of the mechanical object during the total time 'EC_T'
            direct_flag (int): Flag for internal purposes. Will be 1 in case of direct drive option in tidal (no mechanical object)
        
        """

        #  In case there is method in the child to compute the life, it is here executed
        if hasattr(self, "reliability_props"):
            self.reliability_props()

        cycles = EC_T / Tz  # Total number of cycles in the Environmental Condition
        T_cycles = load_prob * cycles  # Actual number of cycles of each range
        self.Mech_damage = np.sum(
            T_cycles / self.N
        )  # Damage during the EC total time 'EC_T'
        if self.direct_flag == 1 :
            self.Mech_damage = 0

        #aux=0
        #time=0        
        #for conter in range(0, len(T_cycles)): 
        #    aux=aux+T_cycles[conter] 
        #    if aux>1:
        #        for conter2 in range(0,conter):
        #            time=time+T_cycles[conter2]*7
        #        break
        #time=(time/(365*24*60*60))     
        #print("failure rate mechanical",time)
        #print('Mech_damage: ' + str(self.Mech_damage))

    def env_impact(self):
        """Method to compute the environmental impact (mass) of the sub-class

        Function to calculate the total mass of the child object. It is based on a non-dimensional mass precomputed
        in the child where: total_mass=non_dim_mass*size^3

        Required to be DEFINED in the child:
         - mass_unit - float
         
        Attributes:
            Mech_mass (float): Mass of the mechanical object in [kg]

        :return: The method doesn't return values, it stores the results in the corresponding variables
    
        """

        # Mass properties initialization based on env_props function in the child
        if hasattr(self, "env_props"):
            self.env_props()

        self.Mech_mass = round(self.mass_unit * self.size ** 3, 2)

        # print('Mech_mass: ' + str(self.Mech_mass))

    def cost(self):
        """Method to compute the cost of the subclass

        Function to calculate the total cost of the child object. It is based on a cost function pre-loaded
        in the child.

        Required to be DEFINED in the child:
         - Mech_cost_adim - a cost factor so that the total cost is proportional to it and the cubic power of the size

        Attributes:
            Mech_cost (float): Cost of the mechanical object in €

        :return: The method doesn't return values, it stores the results in the corresponding variables

        """

        # Mass properties initialization based on env_props function in the child
        if hasattr(self, "cost_props"):
            self.cost_props()

        self.Mech_cost = round(self.Mech_cost_adim * self.size ** 3, 2)

        # print('Mech_cost: ' + str(self.Mech_cost))


class AirTurbine(EC2Mech):
    """Class representing Air Turbines

    The AirTurbine object computes the turbine properties representing an Air Turbine of types Wells or
    Impulse (biradial), so that the performance, reliability, environmental impact and cost can be computed with the
    functions inherited from the parent object. The functions available in this class are:

    - __init__: The basic properties about performance/reliability/cost/mass are read from the catalogue and GUI
    
    Initialization function for the air turbines defined in the catalogue. Based on the turbine type its
    non-dimensional properties are loaded. Al the variables to be stored in the object in the different methods are
    here described at the beginning.
    The flow-pressure and flow-torque properties are fitted with analytic functions (turb_app method) and its
    coefficients stored in the object variable airT_perf to be used afterwards in the methods of the class.

    - perf_props: The rotational speed is defined so that the Air Turbine provides the PTO damping provided as input
    
    - reliability_props: The admissible number of cycles is computed for each torque/stress range based on the SN curve
    
    - env_props: A non-dimensional mass of te turbine is computed
    
    :param str turb_type: Two admisible types of turbines are accepted Wells or Impulse (biradial).
    :param float diam: turbine's diameter [m]
    :param float s_owc: internal surface water level area [m2]
    :param dict catalogue: dictionary with all the mechanical parameters from the catalogue

    Attributes:
        direct_flag (int): Internal flag to inform the parent if there is no mechanical object (1 means no mechanical object)
        Mech_T (list): dimensional torque vector of the linearized turbine [N·m]
        omega (list): rotational speed of the linearized turbine [rad/s]
        tao (list): Torsional shear stress ranges on the shaft [MPa]
        ultimate_stress (list): ultimate stress MPa
        N (list): Number of cycles that the turbine can work at each stress range
        Mech_damage (list): Sum of the fractions of actual cycles over the cycle capacity 'N' (>1 failure occurs)
        mass_unit (float): Turbine equivalent density  --> mass=mass_unit*D**3
        Mech_mass (list): Total mass of the turbine [kg]
        Mech_cost_adim (float): a cost factor so that the total cost is proportional to it and the cubic power of the size
        Mech_cost (void): Total cost of the turbine [€] is computed in the parent class
        type (str): Turbine type --> Turbine type Wells / Impulse(biradial)
        rho_air (float): Air Density 1.29 [kg/m3]
        airT_perf (dict): Non-dimensional performance parameters of the fitting of pressure and torque in AirT_adim
        airT_reliab (dict): Data needed to perform the reliability
        mass_props (list): Turbine geometry related parameters used for total mass computation
        cost_f (list): Parameters to compute the cost of a turbine of a given diameter
        nd_stall (list): non-dimensional flow at which the stall starts (only applicable to Wells)
        size (float): Turbine diameter [m]
        s_owc (float): Internal surface water level area [m2]
        S_N (list): S_N curve from the catalogue
        fatigue_life (list): S_N curve from the catalogue used for fatigue

    """

    def __init__(self, turb_type, diam, s_owc, catalogue):

        #  -------------------------------------------------------------------------------------------------------------
        # Read non-dimensional turbine data from the DataBase
        # root = get_project_root()
        # folder = str(root) + "/business/DB/json_data/"
        # path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))+'\\DB\\json_data\\'
        # AirT_adim = pd.read_json(
        #     folder + turb_type + ".json", orient="records", lines=True
        # )
        air_cat=catalogue
        self.direct_flag = 0
        #  -------------------------------------------------------------------------------------------------------------

        # --------------------------------------------------------------------------------------------------------------
        # Data initialization for all variables of the class stored in 'self'
        # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        # Rules:
        #     - Variables stored/computed in the parent functions must always start with Mech_
        #     - At least the required variables in all methods inherited from the parent must be here defined
        # +++++++++++++++++++++++++++++++++++++++++++++++++++++++
        #  Data initialisation
        # -----------------------
        #  Required by the parent functions
        # --------------Performance
        self.Mech_T = []  # dimensional torque vector of the linearized turbine [N·m]
        self.omega = []  # rotational speed of the linearized turbine [rad/s]
        self.Mech_P_mean = []  # Mean Power at each power level [W]
        self.Mech_T_mean = []  # Mean torque at each torque level [N·m]
        self.Pcapt = []
        self.Mech_eff_mean = (
            []
        )  # Total mean efficiency of the mechanical transformation [-]
        # ---------------Reliability
        self.tao = []  # Torsional shear stress ranges on the shaft [MPa]
        self.ultimate_stress = []  # ultimate stress MPa
        self.N = []  # Number of cycles that the turbine can work at each stress range
        self.Mech_damage = (
            []
        )  # Sum of the fractions of actual cycles over the cycle capacity 'N' (>1 failure occurs)
        # ---------------Environmental
        self.mass_unit = []  # Turbine equivalent density  --> mass=mass_unit*D**3
        self.Mech_mass = []  # Total mass of the turbine [kg]
        # ---------------Cost
        self.Mech_cost_adim = []
        self.Mech_cost = []  # Total cost of the turbine [€]
        # Other variables
        self.type = turb_type  # type --> Turbine type Wells / Impulse(biradial)
        self.rho_air = 1.29  # Air Density [kg/m3]
        # self.AirT_adim = AirT_adim  # Non-dimensional properties (flow/pressure/torque/eff) of the turbine 'DataFrame'

        self.airT_perf = (
            {}
        )  # Non-dimensional performance parameters of the fitting of pressure and torque in AirT_adim
        self.airT_reliab = {}  # Data needed to perform the reliability
        self.mass_props = (
            []
        )  # Turbine geometry related parameters used for total mass computation
        self.cost_f = (
            []
        )  # Parameters to compute the cost of a turbine of a given diameter
        self.nd_stall = (
            []
        )  # non-dimensional flow at which the stall starts (only applicable to Wells)
        self.size = diam  # Turbine diameter [m]
        self.s_owc = s_owc  # Internal surface water level area [m2]

        # --------------------------------------------------------------------------------------------------------------
        # Discard 'NaN' values
        inds_hyd = np.isnan(air_cat["at_cat"]["phi_hyd"]) == False
        inds_mech = np.isnan(air_cat["at_cat"]["phi_mech"]) == False

        # Non-dimensional performance fitting
        def turb_app(x, K, alpha, K0):
            # Potential type function
            return K * x ** alpha + K0


        # Hydrodynamic side
        a_hyd, b_hyd = curve_fit(
            turb_app,
            np.array(air_cat["at_cat"]["phi_hyd"])[inds_hyd],
            np.array(air_cat["at_cat"]["psi_hyd"])[inds_hyd],
            p0=[3, 1, 0],
        )

        # Mechanical side curve, up to the maximum 'pi' value to account for the stall in the Wells turbine
        ind_T = np.where(np.array(air_cat["at_cat"]["pi_mech"])[inds_mech] == max(np.array(air_cat["at_cat"]["pi_mech"])[inds_mech]))[0][0]


        a_mech, b_mech = curve_fit(
            turb_app,
            air_cat["at_cat"]["phi_mech"][0:ind_T],
            air_cat["at_cat"]["pi_mech"][0:ind_T],
            p0=[3, 1, 0],
        )
        # Non-dimensional flow of a Wells turbine at which stall starts
        if self.type == "Wells":
            self.nd_stall = air_cat["at_cat"]["phi_mech"][ind_T]
        # Non-dimensional parameter storing
        self.airT_perf["Kp"] = a_hyd[0]
        self.airT_perf["alpha_p"] = a_hyd[1]
        self.airT_perf["Kp0"] = a_hyd[2]
        self.airT_perf["Kt"] = a_mech[0]
        self.airT_perf["alpha_t"] = a_mech[1]
        self.airT_perf["Kt0"] = a_mech[2]
        self.airT_reliab["shaftD"] = air_cat["at_cat"]["shaftD"]
        self.airT_reliab["fatigue_life"] = air_cat["at_cat"]["fatigue_life"]
        self.S_N = self.airT_reliab["fatigue_life"]
        self.mass_props = air_cat["at_cat"]["mass"]
        self.cost_f = air_cat["at_cat"]["cost"]
        self.fatigue_life = air_cat["at_cat"]["fatigue_life"]


    def perf_props(self, Cpto, vel_v, pow_prob, obj_args):
        """Summary: Performance of te linearized turbine to represent the PTO damping (Cpto)

        Function that defines the turbine performance properties based on the equivalent linear turbine. The rotational
        speed of the non-linear turbine is set so that it produces the same mean power as a linear turbine, whose
        flow-pressure relation is completely linear, with a steep equal to the Cpto.

        :param float Cpto: The linear power take off damping
        :param list vel_v: Velocity vector representing all velocity levels
        :param list pow_prob: Probability distribution of the power levels
        :param list obj_args: Performance specific inputs at sea state level 
        :return: The method doesn't return values, it stores the results in the corresponding variables
        """

        def turb_lin(steep_i, Pcapt_i, vel_v_i, pow_prob_i):
            """Linearization of the turbine

            Turbine linearization. It finds the steepness in the non-dimensional flow-pressure curve that provides a
            rotational speed of the turbine that produces the same mean power as the captured power.

            :param steep_i: Steepness in the non-dimensional flow-pressure curve
            :param (float) Pcapt_i: Mean captured power with the Cpto input
            :param (list) vel_v_i: Velocity vector representing all velocity levels of the OWC surface of the chamber
            :param (list) pow_prob_i:  Probability distribution of the power levels
            :return: The method returns the error between the mean captured power and the mean captured power of the
            non-linear turbine with the rotational speed defined by 'steep'
            """

            # Rotational speed of the equivalent (steepness of the phi-psi) linear turbine
            om_T = Cpto * self.size / (self.s_owc ** 2 * self.rho_air * steep_i)
            # Vector of non-dimensional flow and pressure for a given omega
            phi_iter = self.s_owc * vel_v_i / (om_T * self.size ** 3)
            psi_iter = (
                self.airT_perf["Kp"] * phi_iter ** self.airT_perf["alpha_p"]
                + self.airT_perf["Kp0"]
            )

            F_pto_nolin = (
                psi_iter * self.s_owc * self.rho_air * om_T ** 2 * self.size ** 2
            )
            Pot_iter = np.sum(F_pto_nolin * vel_v_i * pow_prob_i)
            # Error computed between the captured energy and the computed captured energy by the non-linear air turbine
            err_Plin = abs(Pcapt_i - Pot_iter)
            return err_Plin

        # Hydrodynamic captured power of the linear mechanical PTO
        Pcapt = sum(Cpto * vel_v ** 2 * pow_prob)

        # The 'turb_lin' function computes steep, the equivalent linear relation between phi and psi to produce the
        # same hydrodynamic power as the linear system
        steep = minimize(
            turb_lin,
            [1.0],
            args=(Pcapt, vel_v, pow_prob),
            method="BFGS",
            options={"gtol": 1e-6, "disp": False},
        )
        # Resulting rotational velocity
        self.omega = (
            np.ones(len(vel_v))
            * Cpto
            * self.size
            / (self.s_owc ** 2 * self.rho_air * steep.x)
        )
       
        # Corresponding mechanical torque on the turbine rotating at 'omega' [rad/s]
        phi = self.s_owc * vel_v / (self.omega * self.size ** 3)
        pi = (
            self.airT_perf["Kt"] * phi ** self.airT_perf["alpha_t"]
            + self.airT_perf["Kt0"]
        )
        if self.type == "Wells":
            # Consider creating another function to better represent the stall zone
            pi[phi > self.nd_stall] = 0

        # Computed variables stored in object's 'self' variable
        self.Mech_T = pi * self.rho_air * self.omega ** 2 * self.size ** 5
        self.Mech_T[self.Mech_T < 0] = 0

    def reliability_props(self):
        """This method computes the admissible number of cycles at each
        Based on the SN curves defined in the catalogue the admissible life is computed, based on the shear stress
        range in the shaft of the air turbine, whose diameter is also specified in the catalogue.
        
        :return: The method doesn't return values, it stores the results in the corresponding variables
        """

        # Given the torque on the turbine compute the corresponding torsional stress 'tao'
        T = (
            self.Mech_T
        )  # --> revisar cual será el vector equivalente a los rangos de par
        r_shaft = 0.5 * self.airT_reliab["shaftD"] * self.size
        J = np.pi / 2 * r_shaft ** 4
        self.tao = T * r_shaft / J / 1e6  # [MPa]
        self.ultimate_stress = (
            10 ** ((self.fatigue_life[0][1] - 3) / self.fatigue_life[0][0])
        ) / 0.9  # (MPa))
        # **Manual** correction for null torques, set to 0.1[MPa], which almost doesn't consume any life
        self.tao[self.tao == 0] = 0.1

        # Obtain the corresponding fatigue life curves from the DB for the two sections
        first_sect = self.airT_reliab["fatigue_life"][0]
        second_sect = self.airT_reliab["fatigue_life"][1]
        sigma_lim = 10 ** ((7 - first_sect[1]) * (-1 / first_sect[0]))
        N_life = np.zeros([len(self.tao)])
        # Life of the tension ranges within the first section of the fatigue curves
        inds_first = np.abs(self.tao) > sigma_lim
        N_life[inds_first] = 10 ** (
            first_sect[1]
            - first_sect[0] * np.log10(np.abs(np.array(self.tao)[inds_first]))
        )

        # Life of the tension ranges within the second section of the fatigue curves
        inds_second = np.abs(self.tao) < sigma_lim
        N_life[inds_second] = 10 ** (
            second_sect[1]
            - second_sect[0] * np.log10(np.abs(np.array(self.tao)[inds_second]))
        )

        # **Manual** Assigment of ONE cycle life to those tension ranges higher than the tension range corresponding
        # to 1000cycles (low cycle region, out of the scope of this work)
        sigma_max = 10 ** ((3 - first_sect[1]) * (-1 / first_sect[0]))
        N_life[self.tao > sigma_max] = 1

        # Admissible number of cycles for the stress ranges at the shaft
        self.N = N_life

    def env_props(self):
        """Method to compute the non-dimensional mass of the air turbine.

        This method is based on the geometric properties of the blades as well as the shaft. The resulting value must be
        a non-dimensional mass function of the cube of the turbine diameter
        
        :return: The method doesn't return values, it stores the results in the corresponding variables
        
        """
        # Unitary turbine mass, proportional to the cubic of the diameter
        if self.type == "Wells":
            # mass_props=[nºbldes, Rinout, Aunit, chord_to_D, rho]
            # Z = self.mass_props[0]
            # L_blade_toD = 0.5*(1-self.mass_props[1])
            # A_blade_toD2 = self.mass_props[2]*self.mass_props[3]**2
            # V_unit_shaft = np.pi*self.airT_reliab['shaftD']**2/2
            # self.mass_unit = (Z*L_blade_toD*A_blade_toD2+V_unit_shaft)*self.mass_props[4]
            self.mass_unit = self.mass_props
        elif self.type == "Impulse":
            # mass_props=[nºblades, thick to R, s to R, b to R, rho]
            # Z = self.mass_props[0]
            # t_toD = self.mass_props[1]*0.5
            # s_toD = np.pi*self.mass_props[2]*0.5
            # b_toD = self.mass_props[3]*0.5
            # V_unit_shaft = np.pi * self.airT_reliab['shaftD'] ** 2 / 2
            self.mass_unit = (
                self.mass_props
            )  # (Z*t_toD*s_toD*b_toD+V_unit_shaft)*self.mass_props[4]
        else:
            raise Warning("No turbine has been defined")

    def cost_props(self):
        """
        Function to obtain the non-dimensional cost so that it is used in the parent cost function

        The cost function for the turbine is defined as follows:
         - cost_f = [C0/D0**(3X)]*D**(3X) where C0, D0 and X are the factors of cost_f

        :return: none
        """

        self.Mech_cost_adim = (
            self.cost_f[0]
            / self.cost_f[1] ** (3 * self.cost_f[2])
            * self.size ** (self.cost_f[2])
        )


class Hydraulic(EC2Mech):
    """Summary: Class representing Hydraulic System

    - __init__: The basic properties about performance/reliability/cost/mass are read from the catalogue
    
    - perf_props: The rotational speed is defined so that the Air Turbine provides the PTO damping provided as input
    
    - reliability_props: The admissible number of cycles is computed for each torque/stress range based on the SN curve
    
    - env_props: A non-dimensional mass of te turbine is computed
    
    :param float size: Hydraulic motor size [m3/rad]
    :param float Ap: Cross section of the piston [m2]
    :param dict catalogue: dictionary with all the mechanical parameters from the catalogue

    Attributes:
        direct_flag (int): Internal flag to inform the parent if there is no mechanical object (1 means no mechanical object)
        Mech_T (list): dimensional torque vector of the hydraulic [N·m]
        omega (list): rotational speed of the hydraulic [rad/s]
        tao (list): Torsional shear stress ranges on the shaft [MPa]
        ultimate_stress (list): ultimate stress MPa
        N (list): Number of cycles that the turbine can work at each stress range
        Mech_damage (list): Sum of the fractions of actual cycles over the cycle capacity 'N' (>1 failure occurs)
        mass_unit (float): equivalent density  --> mass=mass_unit*D**3
        Mech_mass (list): Total mass of the hydraulic object [kg]
        Mech_cost_adim (float): a cost factor so that the total cost is proportional to it and the cubic power of the size
        Mech_cost (void): Total cost of the hydraulic [€] is computed in the parent class        
        size (float): Hydraulic: motor size [m3/rad]
        Ap (float): Hydraulic: Cross section of the piston [m2]
        x (float): Flow portion allowed to pass through the hydraulic motor       
        cost_data (list): Cost factors to obtain the total cost : Cost=cost[0]*size+cos[1]
        weight_data (float): mass per charasteristic size (m^3/rad)
        oil_visc (float): Oil viscosity [Pa·s] 
        oil_dens (float): Oil density [kg/m3] 
        leak_lam (float): Loss coefficient parameters: laminar leakage coeff [-]
        leak_turb (float): Loss coefficient parameters: turbulent leakage coeff [-]
        loss_visc(float): Loss coefficient parameters: viscous loss coeff [-]
        loss_fric (float): Loss coefficient parameters: friction losses coeff [-]
        loss_mot (float): Loss coefficient parameters: hydraulic motor coeff [-]
        loss_piping (float): Loss coefficient parameters:
        bulk (float): Material property characterizing the compresibility of a fluid [Pa]
        shaftD_f (float): non-dimensional shaft ( dimension is given proportional to  diameter)
        reliab (list): S_N curve from the catalogue used for fatigue 
        S_N (list): S_N curve from the catalogue
        maxflow (float): Maximun flow 
        fatigue_life (list): S_N curve from the catalogue used for fatigue  

    Returns: 
        none                    

    """


    def __init__(self, size, Ap, catalogue):
        """
        Summary: Initialisatin of a Hydraulic based on json data and the sizing provided in the inputs
        """

        #  Load data for the hydraulic object
        # path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))+'\\DB\\json_data\\'
        # root = get_project_root()
        # folder = str(root) + "/business/DB/json_data/"
        # Hydraulic_data = pd.read_json(
        #     folder + "Hydraulic" + ".json", orient="records", lines=True
        # )
        hyd_cat = catalogue
        self.direct_flag = 0
        #  --------------------------------------------------------------------------------------

        # --------------------------------------------------------------------------------------------------------------
        # Data initialization for all variables of the class stored in 'self'
        # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        # Rules:
        #     - Variables stored/computed in the parent functions must always start with Mech_
        #     - At least the required variables in all methods inherited from the parent must be here defined
        # +++++++++++++++++++++++++++++++++++++++++++++++++++++++
        #  Data initialisation
        # -----------------------
        #  Required by the parent functions
        # --------------Performance
        self.Mech_T = []
        self.omega = []
        self.Mech_P_mean = []
        self.Mech_T_mean = []
        self.Pcapt = []
        self.Mech_eff_mean = []
        # ---------------Reliability
        self.tao = []  # Torsional shear stress ranges on the shaft [MPa]
        self.ultimate_stress = []  # ultimate stress MPa
        self.N = []
        self.Mech_damage = []
        # ---------------Environmental
        self.mass_unit = []
        self.Mech_mass = []
        # ---------------Cost
        self.Mech_cost_adim = []
        self.Mech_cost = []
        # ---------------Other variables
        self.size = size
        self.Ap = Ap
        self.x = []

        self.cost_data = hyd_cat["hy_cat"]["cost"]
        self.weight_data = hyd_cat["hy_cat"]["mass"]
        self.oil_visc = hyd_cat["hy_cat"]["Oil"][0]
        self.oil_dens = hyd_cat["hy_cat"]["Oil"][1]
        self.leak_lam = hyd_cat["hy_cat"]["Loss_coeffs"][0]
        self.leak_turb = hyd_cat["hy_cat"]["Loss_coeffs"][1]
        self.loss_visc = hyd_cat["hy_cat"]["Loss_coeffs"][2]
        self.loss_fric = hyd_cat["hy_cat"]["Loss_coeffs"][3]
        self.loss_mot = hyd_cat["hy_cat"]["Loss_coeffs"][4]
        self.loss_piping = hyd_cat["hy_cat"]["Loss_coeffs"][5]
        self.bulk = hyd_cat["hy_cat"]["Bulk_Mod"]
        self.shaftD_f = hyd_cat["hy_cat"]["shaftD"]

        self.reliab = hyd_cat["hy_cat"]["fatigue_life"]
        self.S_N = self.reliab
        self.maxflow = hyd_cat["hy_cat"]["maxflow_rel"] * self.size

        # self.reliab = Hydraulyc_data['cmpx1_reliab'][0]
        self.fatigue_life = hyd_cat["hy_cat"]["fatigue_life"]

    def perf_props(self, Cpto, vel_v, pow_prob, obj_args):
        """Function to define the mean working point of the hydrulic system. 

        Calculation of the efficiency.

        :param float Cpto: Applied PTO damping by the hydraulic system (piston force)
        :param list vel_v: velocity levels within the sea state
        :param list pow_prob: probability distr. of power for the corresponding velocity level
        :param list obj_args: Performance specific inputs at sea state level - 'x' control variable in the hydraulic object
        :return: It does not return variables. It stores the performance data.
        """

        # Hydrodynamic captured power of the linear mechanical PTO
        Pcapt = sum(Cpto * vel_v ** 2 * pow_prob)
        sigma_v = np.sqrt(Pcapt / Cpto)

        # Operational variables
        self.x = obj_args[0]  # Flow portion allowed to pass through the hydraulic motor
        p_p = abs(Cpto) * vel_v / self.Ap  # Pressure in the piston
        pms = (
            p_p - self.loss_piping * (self.Ap * vel_v) ** 2
        )  # Pressure difference in the motor

        pms[pms < 0] = 0  # Make zero all values <0
        om_mean = (
            0.637 * self.Ap * sigma_v / (self.x * self.size)
        )  # Mean rotational speed for efficiencies

        # Efficiency computation
        S = self.oil_visc * om_mean / pms
        sigma_eff = om_mean * self.size ** (1 / 3) / np.sqrt(2 * pms / self.oil_dens)
        vol_eff = 1 / (
            1
            + self.leak_lam / (abs(self.x) * S)
            + pms / self.bulk
            + self.leak_turb / (abs(self.x) * sigma_eff)
        )  # Volumetric efficiency
        vol_eff[vol_eff < 0] = 0  # Make zero all values <0
        torque_eff = (
            1
            - self.loss_visc * S / abs(self.x)
            - self.loss_fric / abs(self.x)
            - self.loss_mot * abs(self.x) ** 2 * sigma_eff ** 2
        )  # Torque efficiency
        torque_eff[torque_eff < 0] = 0  # Make zero all values <0
        q_p = (
            vel_v * self.Ap * vol_eff
        )  # Flow in the piston and the motor in steady state
        q_p[q_p > self.maxflow] = 0  # Make zero all values <0
        oms = q_p / (self.x * self.size * vol_eff)

        # Required variables from within the parent
        self.Mech_T = self.x * self.size * pms * torque_eff

        self.Mech_T[oms == 0] = 0  # Make zero all values <0
        self.omega = oms

        # #  cmpx1: Summary: Performance of te linearized Hydraulic to represent the PTO damping (Cpto)
        # self.eff = np.interp(P/P_nom, self.eff_cf, self.eff_eff) # Interpolation depending on the CF of the PTO

    def reliability_props(self):
        """
        This method computes the admissible number of cycles at each environmental state
        
        :return: The method doesn't return values, it stores the results in the corresponding variables
        """

        r_shaft = 0.5 * (self.shaftD_f * self.size) ** (1 / 3)
        J = np.pi / 2 * r_shaft ** 4
        self.tao = self.Mech_T * r_shaft / J / 1e6  # [MPa]
        self.ultimate_stress = (
            10 ** ((self.fatigue_life[0][1] - 3) / self.fatigue_life[0][0])
        ) / 0.9  # (MPa)

        # **Manual** correction for null torques, set to 0.1[MPa], which almost doesn't consume any life
        self.tao[self.tao == 0] = 0.1

        # Obtain the corresponding fatigue life curves from the DB for the two sections
        first_sect = self.reliab[0]
        second_sect = self.reliab[1]
        sigma_lim = 10 ** ((7 - first_sect[1]) * (-1 / first_sect[0]))
        N_life = np.zeros([len(self.tao)])

        # Life of the tension ranges within the first section of the fatigue curves
        inds_first = np.abs(self.tao) > sigma_lim
        N_life[inds_first] = 10 ** (
            first_sect[1]
            - first_sect[0] * np.log10(np.abs(np.array(self.tao)[inds_first]))
        )

        # Life of the tension ranges within the second section of the fatigue curves
        inds_second = np.abs(self.tao) < sigma_lim
        N_life[inds_second] = 10 ** (
            second_sect[1]
            - second_sect[0] * np.log10(np.abs(np.array(self.tao)[inds_second]))
        )

        # **Manual** Assigment of ONE cycle life to those tension ranges higher than the tension range corresponding
        # to 1000cycles (low cycle region, out of the scope of this work)
        sigma_max = 10 ** ((3 - first_sect[1]) * (-1 / first_sect[0]))
        N_life[self.tao > sigma_max] = 1

        # Admissible number of cycles for the stress ranges at the shaft
        self.N = N_life

    def env_props(self):
        """Method to compute the non-dimensional mass of the hydraulic.

        The resulting value must be a non-dimensional mass function of the cube of the hydraulic motor size [m3/rad]
        
        :return: The method doesn't return values, it stores the results in the corresponding variables"""

        self.mass_unit = self.size * self.weight_data / (self.size ** 3)

        # y = 132.2 * 0.785 * (2 * np.pi * 1e6) * x - 1,6056  Hydraulic PTO mass
        # it is divided by the cubic power of the size to be coherent with the parent environmental function

    def cost_props(self):
        """Function to obtain the non-dimensional cost so that it is used in the parent cost function    

        :return: The method doesn't return values, it stores the results in the corresponding variables"""

        self.Mech_cost_adim = (self.size * self.cost_data[0] + self.cost_data[1]) / (
            self.size ** 3
        )
        # y = 1589,6 * 0.785 * (2 * np.pi * 1e6)x + 24911  Hydraulic PTO cost/Pnom(kW)
        # it is divided by the cubic power of the size to be coherent with the parent cost function


class Gearbox(EC2Mech):
    """Class representing a Gearbox

    The Gearbox object computes the gearbox properties representing an Spur gear, so that the performance, reliability, environmental impact and cost can be computed with the functions inherited from the parent object.
    
    The functions available in this class are:

    - __init__: The basic properties about performance/reliability/cost/mass are read from the catalogue
    
    Initialization function for the gearbox defined in the catalogue. All the variables to be stored in the object in the different methods are here described at the beginning.
    
    - perf_props: The efficiency is computed depending on Power Load and number of gears
    
    - reliability_props: The admissible number of cycles is computed 
    
    - env_props: A non-dimensional mass of te gearbox is computed
    
    - cost_props: The non-dimensional cost of the gearbox
    
    Args:
        P_nom (int): Nominal power of the gearbox from the GUI [kW]
        t_ratio (float): Gearbox transmission ratio. Relationship between the speed of the device and rotating speed of the generator shaft [dimensionless]
        Technology (str): Technology of the device (wave or tidal)
        catalogue (dict): Dictionary with all the mechanical parameters from the catalogue

    Attributes:
        direct_flag (int): Internal flag to inform the parent if there is no mechanical object (1 means no mechanical object)
        Mech_T (list): dimensional torque vector of the gearbox [N·m]
        omega (list): rotational speed of the gearbox [rad/s]
        tao (list): Torsional shear stress ranges on the shaft [MPa]
        ultimate_stress (list): ultimate stress MPa
        N (list): Number of cycles that the turbine can work at each stress range
        Mech_damage (list): Sum of the fractions of actual cycles over the cycle capacity 'N' (>1 failure occurs)
        reliab (list): Data to build the fatigue strength curves
        mass_unit (float): equivalent density  --> mass=mass_unit*D**3
        Mech_mass (list): Total mass of the gearbox [kg]
        Mech_cost_adim (float): a cost factor so that the total cost is proportional to it and the cubic power of the size
        Mech_cost (void): Total cost of the gearbox [€] is computed in the parent class 
        size (int): Nominal power of the gearbox in [W]. It is received in kW from the GUI and converted to W
        P_max (int): Maximum peak power of the gearbox in [W]
        t_ratio (list): Gearbox transmission ratio. Relationship between the speed of the device and rotating speed of the generator shaft [dimensionless]
        power_loads_norm (list): Normalized power loads. X-axis of the efficiency curve.
        eff_levels (list): Normalized efficiency levels. Y-axis of the efficiency curve.
        speed_levels (list): not used in the current version
        GBcost (float): Parametric cost factor [€/W]
        GBmass (list): Mass factors [X0,X1], where mass is obtained proportional to this parameters and nominal power mass=GB_mass[0]*Pnom+GB_mass[1]
        w (float): Not used in the current version
        perf_f (list): array with efficiency values corresponding to each power
        shaftD (float): Shaft diameter 
        fatigue_life (list): Gearbox fatigue life: format =[[m1,log(a)],[m2,log(a2)]]; m = negative inverse slope of the S - N curve and log_a is intercept of log N axis
        S_N (list): S_N curve from the catalogue 

    Returns: 
        none    

    """

    
    def __init__(self, P_nom, t_ratio, Technology, catalogue):

        # Read data from DB
        # ********************************
        #  os.path.dirname(os.path.dirname(os.path.realpath(__file__))) +
        #root = get_project_root()
        #file_gear = str(root) + "/business/DB/json_data/Gearbox.json"
        #Gear_DB = pd.read_json(file_gear, orient="records", lines=True)
        Gear_cat = catalogue
        # --------------------------------------------------------------------------------------------------------------
        # Data initialization for all variables of the class stored in 'self'
        # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        # Rules:
        #     - Variables stored/computed in the parent functions must always start with Mech_
        #     - At least the required variables in all methods inherited from the parent must be here defined
        # +++++++++++++++++++++++++++++++++++++++++++++++++++++++
        #  Data initialisation
        # -----------------------
        #  Required by the parent functions
        self.tech = Technology
        self.direct_flag = 0
        # --------------Performance
        self.Mech_T = []  # dimensional torque vector on the output shaft [N·m]
        self.omega = []  # rotational speed of the output shaft [rad/s]
        self.Mech_P_mean = []  # Mean Power at each power level [W]
        self.Mech_T_mean = []  # Mean torque at each torque level [N·m]
        self.Pcapt = []
        self.Mech_eff_mean = (
            []
        )  # Total mean efficiency of the mechanical transformation [-]
        # ---------------Reliability
        self.tao = []  # Torsional shear stress ranges on the shaft [MPa]
        self.ultimate_stress = []  # ultimate stress MPa
        self.N = []  # Number of cycles that the gearbox can work at each stress range
        self.Mech_damage = (
            []
        )  # Sum of the fractions of actual cycles over the cycle capacity 'N' (>1 failure occurs)
        self.reliab = []  # Data to build the fatigue strength curves
        # ---------------Environmental
        self.mass_unit = []  # Equivalent density  --> mass=mass_unit*D**3
        self.Mech_mass = []  # Total mass of the gearbox [kg]
        # ---------------Cost
        self.Mech_cost_adim = []
        self.Mech_cost = []  # Total cost of the gearbox [€]
        # Other variables
        self.size = P_nom * 1000 #Pnom is received in kW from the GUI
        self.P_max = Gear_cat["gb_cat"]["maxP_rel"] * P_nom *1000
        self.t_ratio = t_ratio  # Transmission ratio
        self.power_loads_norm = []
        self.eff_levels = []
        self.speed_levels = []
        self.GBcost = []
        self.GBmass = []
        self.w = []
        self.perf_f = []
        # self.eff_fit = []
        self.shaftD = []
        self.fatigue_life = Gear_cat["gb_cat"]["fatigue_life"]        
        # --------------------------------------------------------------------------------------------------------------

        # -------------------------------
        # Data init for performance assessment
        # def eff_fit(x, k1, k2, k3, k4):
        #     y = 1 - k1 * np.exp(k2 * x) - k3 * np.exp(k4 * x)
        #     return y

        self.power_loads_norm = Gear_cat["gb_cat"]["power_loads_norm"]
        # array with normalized power loads [0..1] [pu]
        self.eff_levels = Gear_cat["gb_cat"]["eff_levels"]
        # array with efficiency values corresponding to each power
        a_gear, b_gear = curve_fit(
            eff_fit,
            np.array(self.power_loads_norm),
            np.array(self.eff_levels),
            bounds=([-10.0, -10.0, -10.0, -10.0], [10.0, 0.0, 10.0, 0.0]),
        )
        self.perf_f = a_gear  # k1 == a[0] ; k2 == a[1] in the eff_fit function
        # self.eff_fit = (
        #     eff_fit  # The function is stored to be used in the perf_props function
        # )

        # -------------------------------
        # Data init for reliability assessment
        self.GBcost = Gear_cat["gb_cat"]["Cost"]
        self.GBmass = Gear_cat["gb_cat"]["Mass"]
        self.shaftD = Gear_cat["gb_cat"]["shaftD"] * self.size
        self.reliab = Gear_cat["gb_cat"]["fatigue_life"]
        self.S_N = self.reliab
        # self.speed_levels = Gear_DB['speed_levels'][0]
        # self.life_levels = Gear_DB['life_levels'][0]
        # self.w = 9 / 8  # For life calculation of Roller bearings
        # self.wheel_bearings = 5  # number of bearings considered for each wheel
        # self.n_speeds = int(self.n_engagements - 1)  # number of intermediate speeds apart from input and output speeds.
        # self.n_engagements = self.i // 8.01 + 1  # each pair of wheels can have a transmission relation of 8.
        # Will be 0 if only 2 wheels

        # --------------------------------------------------------------------------------------------------------------
        # Data initialization for all variables of the class stored in 'self'
        # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        # Rules:
        #     - Any variable stored in self must be initialised here under the corresponding function
        #     - Variables stored/computed in the parent functions must always start with Gear
        #     - At least the required variables in all methods inherited from the parent must be here defined
        # +++++++++++++++++++++++++++++++++++++++++++++++++++++++
        # Initialisation data
        # performance() data
        # self.speed_in = speed_in
        # self.speed_out = speed_out
        # self.P_nom = P_nom
        # self.i = speed_out / speed_in
        # For a higher
        # relation more wheels need to be added. One engagement means one pair of wheels in contact. For example
        # if there are 3 wheels, there will be 2 engagements and one intermediate speed (8.01 is used so if i=8,
        # it computes only 1 engagement)
        # efficiency curve for efficiency calculation
        # load value [0..1] [pu]
        # self.gear_eff = 0  # array with the computed efficiency of the  B2B simplified converter based on input power
        # # [0..1] [pu]
        # self.power_tur = 0  # array with input power, turbine output power levels without considering probability [W]
        # For reliability calculation, in the simplified model 5 bearings per wheel will be considered.
        # If only two wheels, first wheel rotates at speed_in, and second wheel rotates at speed_out. If more than
        # 2 wheels are required (n_engagements>1) intermediate speeds will be considered.

        # --------------------------------------------------------------------------------------------------------------

    def perf_props(self, Cpto, vel_v, pow_prob, obj_args):
        """Performance of the gearbox.

        Function that calculates the efficiency from an efficiency curve. The inputs are power LEVELs at which the gearbox is expected to be working, for the mean Power computation the parent method 'performance' is used
        
        Args:
            Cpto (float): power take off damping
            vel_v (list): Velocity vector representing all velocity levels
            pow_prob (list): Probability distribution of the power levels. Not used in the Gearbox
            obj_args (list): Performance specific inouts at sea state level

        Returns: 
            The method doesn't return values, it stores the results in the corresponding variables
       
        """

        P_in = Cpto * vel_v ** 2
        eff = eff_fit(P_in / self.size, *self.perf_f)
        eff[eff < 0] = 0
        eff[
            eff > 1
        ] = 1  # Careful with the analytical fitting that may provide higher values
        # np.interp(P_in/self.P_nom, self.power_loads_norm, self.eff_levels, left=0, right=0)
        # ***************************************************************************************************************
        # If more than one pair of wheels are used, each pair will add 10% of losses
        eff = (
            eff  # - (self.n_engagements - 1) * 0.1 ESTO COMO RESOLVEMOS?!?!?!?!?!?!?!?
        )
        # ***************************************************************************************************************
        # ->WEC: In speed is the speed of the dof, either linear or rotational  (careful with this!)
        # ->TEC: In speed is the rotational speed of the rotor
        # The gearbox must distinguish between Tidal and Wave
        if self.tech == "Tidal":
            om_rotor = 1 / obj_args["Ct_tidal"]
            self.omega = (
                om_rotor * self.t_ratio * np.ones(len(vel_v))
            )  # Transformation ratio = IN_speed/OUT_speed            
            Ct_tidal = obj_args["Ct_tidal"]
            self.Mech_T = (eff * Ct_tidal * Cpto * vel_v ** 2) /self.t_ratio 
        elif self.tech == "Wave":
            self.omega = (
                vel_v * self.t_ratio
            )  # Transformation ratio = IN_speed/OUT_speed
            self.Mech_T = eff * Cpto * vel_v / self.t_ratio
        self.omega[P_in > self.P_max] = 0
        self.Mech_T[P_in > self.P_max] = 0

    def reliability_props(self):
        """
        This method computes the admissible number of cycles at each environmental state. It is modified with respect to other components. Attention should be paid to how fatigue strength is introduced in the database
       
        Returns: 
            The method doesn't return values, it stores the results in the corresponding variables
        """

        r_shaft = 0.5 * self.shaftD
        J = np.pi / 2 * r_shaft ** 4
        self.tao = (
            2000 * self.Mech_T / (2 * r_shaft * 1000) * np.sqrt(2)
        )  # [N] --> equivalent load assuming tan(alpha)=1

        # Obtain the corresponding fatigue life curves from the DB for the two sections
        first_sect = self.reliab[0]
        second_sect = self.reliab[1]

        # **Manual** correction for null torques, set to 0.1[MPa], which almost doesn't consume any life
        self.tao[self.tao<10**((second_sect[1]-9)/second_sect[0])]=10**((second_sect[1]-9)/second_sect[0])
        #self.tao[self.tao < 0.1] = 10
        self.ultimate_stress = (
            10 ** ((self.fatigue_life[0][1] - 3) / self.fatigue_life[0][0])
        ) / 0.9  # (MPa)

        
        # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        # Correction required to work in loads instead of stress
        #first_sect[1] = first_sect[1] + first_sect[0] * np.log10(self.shaftD ** 2)
        #second_sect[1] = second_sect[1] + second_sect[0] * np.log10(self.shaftD ** 2)
        # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        sigma_lim = 10 ** ((7 - first_sect[1]) * (-1 / first_sect[0]))
        N_life = np.zeros([len(self.tao)])

        # Life of the tension ranges within the first section of the fatigue curves
        inds_first = np.abs(self.tao) > sigma_lim
        N_life[inds_first] = 10 ** (
            first_sect[1]
            - first_sect[0] * np.log10(np.abs(np.array(self.tao)[inds_first]))
        )

        # Life of the tension ranges within the second section of the fatigue curves
        inds_second = np.abs(self.tao) < sigma_lim
        N_life[inds_second] = 10 ** (
            second_sect[1]
            - second_sect[0] * np.log10(np.abs(np.array(self.tao)[inds_second]))
        )

        # **Manual** Assigment of ONE cycle life to those tension ranges higher than the tension range corresponding
        # to 1000cycles (low cycle region, out of the scope of this work)
        sigma_max = 10 ** ((3 - first_sect[1]) * (-1 / first_sect[0]))
        N_life[self.tao > sigma_max] = 1

        # Admissible number of cycles for the stress ranges at the shaft
        self.N = N_life

        # """Summary: This method obtains the life of the Gearbox in hours based on the speed of the bearings
        # It is a simplified method without considering the load and the gears reliability. Only the number of
        # bearings and the speed of each is required.
        # :type om_act: object
        # :return: The method doesn't return values, it stores the results in the corresponding variables
        # """
        # GB_Life = np.zeros([len(om_act)])
        #
        # for cont_om in range(0, len(om_act), 1):
        #     if om_act[cont_om] == 0:
        #         om_act[cont_om] = 20 # minimum value considered in the Life tables
        # # L10 for each first wheel bearing
        # L10_in = np.interp(om_act, self.speed_levels, self.life_levels, left=0, right=0)
        # # L10 for each last wheel bearing
        # L10_out = np.interp(om_act*self.i, self.speed_levels, self.life_levels, left=0, right=0)
        # # If there are more than 2 wheels intermediate speeds must be computed
        # # Calculation of L10 for each intermediate wheel bearings if any
        # if self.n_speeds >= 1:
        #     ai = len(om_act)
        #     L10_int = np.zeros([len(om_act)])  # array for each intermediate speed bearings Life L10
        #     om_step = np.divide((om_act * self.i - om_act), (self.n_speeds + 1))  # step between speeds
        #     L10_tot_int = np.zeros([len(om_act)])  # auxiliary variable for computing the life of all the intermediate wheels bearings
        #     #Life in hours for a bearing in each wheel (n_speeds wheels)
        #     for cont_n  in range (1, self.n_speeds+1, 1):
        #         intermediate_speed = om_act+om_step*cont_n
        #         L10_int = np.interp(intermediate_speed, self.speed_levels, self.life_levels, left=0, right=0)
        #         # Each intermediate speed implies a new wheel with bearings. L10 for those intermediate bearings
        #         L10_tot_int = L10_tot_int + (L10_int[cont_n - 1] ** (-self.w)) * self.wheel_bearings
        #
        #     # Computation of the full Gearbox Life must consider individual bearings life
        #     GB_Life = ((L10_in**(-self.w))*self.wheel_bearings+(L10_out**(-self.w))*self.wheel_bearings+ L10_tot_int)**(-1/self.w)
        # else:
        #     GB_Life = ((L10_in ** (-self.w)) * self.wheel_bearings + (
        #                 L10_out ** (-self.w)) * self.wheel_bearings) ** (-1 / self.w)
        # self.N = GB_Life
        # self.om_out = self.i*om_act  # velocidad de salida

    def env_props(self):
        """Method to compute the non-dimensional mass of the gearbox proportional to the cubic of the size in order to be coherent with the parent function.
        
        Returns: 
            The method doesn't return values, it stores the results in the corresponding variables
        """
        # Unitary mass, proportional to the cubic of the size
        self.mass_unit = (self.GBmass[0] * self.size + self.GBmass[1]) / (
            self.size ** 3
        )

    def cost_props(self):
        """This method computes the non-dimensional cost
        
        Returns: 
            none 
        """

        self.Mech_cost_adim = (self.GBcost * self.size) / (self.size ** 3)
        # it is divided by the cubic power of the size to be coherent with the parent cost function

class Direct_Tidal(EC2Mech):
    """Class representing the tidal turbine when no Gearbox is neaded. It is only used to transform the tidal inputs to 
    the mechanical output (speed, power) needed by the generator.
    
    - __init__: The basic properties about performance/reliability/cost/mass are read from the catalogue

    - perf_props: No losses will be considered

    - reliability_props: no failure as there is not device

    - env_props: no mass

    - cost_props: no cost

    Attributes:
        tech (str): Will be 'Tidal', this object will be only used in tidal
        direct_flag (int): Will be 1 that indicates tidal direct drive, no mechanical transformation object
        size (int): 1
        S_N (list): Will be [[1]]. no failure rate is computed
        Mech_T (list): dimensional torque vector in the shaft [N·m]
        omega (list): rotational speed of the output shaft [rad/s] 
        N (float): Will be 1 as there is no object. Number of cycles that the turbine can work at each stress range
        mass_unit (float): Will be zero as there is no object 
        Mech_cost_adim (float): Will be zero as there is no object 

    Returns: 
        none    
    """

    # def __init__(self, tech, P_nom, t_ratio, Technology):
    def __init__(self):
       
        self.tech = 'Tidal'
        self.direct_flag = 1
        # --------------Performance
        self.Mech_T = []  # dimensional torque vector on the output shaft [N·m]
        self.omega = []  # rotational speed of the output shaft [rad/s]
        self.Mech_P_mean = []  # Mean Power at each power level [W]
        self.Mech_T_mean = []  # Mean torque at each torque level [N·m]
        self.Pcapt = []
        self.Mech_eff_mean = (
            []
        )  # Total mean efficiency of the mechanical transformation [-]
        # ---------------Reliability
        self.tao = []  # Torsional shear stress ranges on the shaft [MPa]
        self.ultimate_stress = []  # ultimate stress MPa
        self.N = []  # Number of cycles that the gearbox can work at each stress range
        self.Mech_damage = (
            []
        )  # Sum of the fractions of actual cycles over the cycle capacity 'N' (>1 failure occurs)
        self.reliab = []  # Data to build the fatigue strength curves
        # ---------------Environmental
        self.mass_unit = []  # Equivalent density  --> mass=mass_unit*D**3
        self.Mech_mass = []  # Total mass of the gearbox [kg]
        # ---------------Cost
        self.Mech_cost_adim = []
        self.Mech_cost = []  # Total cost of the gearbox [€]
        self.size = 1
        self.S_N = [[1]]
        
        # --------------------------------------------------------------------------------------------------------------

    def perf_props(self, Cpto, vel_v, pow_prob, obj_args):
        """Performance calculation method. As there is no mechanical object, efficiency will be 1. Omega and Torque in the shaft are calculated.

        Args:
            Cpto (float): power take off damping
            vel_v (list): Velocity vector representing all velocity levels
            pow_prob (list): Probability distribution of the power levels. Not used in the Direct object
            obj_args (list): Performance specific inputs at sea state level

        Returns: 
            none
        
        """
        
        P_in = Cpto * vel_v ** 2
        eff = 1

        # ***************************************************************************************************************
        # ->WEC: In speed is the speed of the dof, either linear or rotational  (careful with this!)
        # ->TEC: In speed is the rotational speed of the rotor
        # The gearbox must distinguish between Tidal and Wave
        om_rotor = 1 / obj_args["Ct_tidal"]
        self.omega = (
            om_rotor * 1 * np.ones(len(vel_v))
        )         
        Ct_tidal = obj_args["Ct_tidal"]
        self.Mech_T = (eff * Ct_tidal * Cpto * vel_v ** 2) /1
        
    def reliability_props(self):

        """
        number of cycles at each environmental state will be set to 1 as there is no object

        :return: none
        """

        self.N = 1

    def env_props(self):
        """Non-dimensional mass will be set to zero
        
        :return: none
        """
        # Unitary mass, proportional to the cubic of the size
        self.mass_unit = 0

    def cost_props(self):
        """Non-dimensional cost will be set to zero"""

        self.Mech_cost_adim = 0


class Mech_Simplified(EC2Mech):
    """Class representing the Simplified Mechanical conversion. 

    The Simplified object computes the performance, reliability, environmental impact and cost with the functions inherited from the parent object.
    The functions available in this class are:

    - __init__: The basic properties about performance/reliability/cost/mass are read from the catalogue
    
    - perf_props: The efficiency is constant in the defined range
    
    - reliability_props: The admissible number of cycles is computed
    
    - env_props: A non-dimensional mass of the simplified mechanical object is computed
    
    - cost_props: The non-dimensional cost of the Simplified mechanical object

    Args:
        P_nom (int): Rated power (mechanical conversion size) in kW
        t_ratio (int): Relationship between the speed of the device and rotating speed for a simplified mechanical transformation assumption [dimensionless]
        Technology (str): Technology (wave or tidal)
        mean_eff (float, optional): Defaults to 0.6.

    Attributes:    
        direct_flag (int): Internal flag to inform the parent if there is no mechanical object (1 means no mechanical object)
        Mech_T (list): Dimensional torque vector on the output shaft [N·m]
        omega (list): rotational speed of the output shaft [rad/s]
        tao (list): Torsional shear stress ranges on the shaft [MPa]
        ultimate_stress (list): ultimate stress MPa
        N (list): Number of cycles that the turbine can work at each stress range
        Mech_damage (list): Sum of the fractions of actual cycles over the cycle capacity 'N' (>1 failure occurs)
        reliab (list): Data to build the fatigue strength curves
        mass_unit (float): equivalent density  --> mass=mass_unit*D^3
        Mech_mass (list): Total mass of the simplified mechanical object [kg]
        Mech_cost_adim (float): a cost factor so that the total cost is proportional to it and the cubic power of the size
        Mech_cost (void): Total cost of the mechanical object [€] is computed in the parent class 
        size (int): Maximum power will be 5 times nominal power in [W]. Pnom is received in kW from the GUI and converted to W
        t_efficiency (float): Mean efficiency of the simplified mechanical object. Defaults to 0.6.
        t_ratio (list): Relationship between the speed of the device and rotating speed for a simplified mechanical transformation assumption [dimensionless]
        S_N (list): S_N curve will be [[0]] as it is not computed in the mechanical simplified

    """

    def __init__(self, P_nom, t_ratio, Technology, mean_eff=0.6):
                       
        # Data initialization for all variables of the class stored in 'self'
        # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        # Rules:
        #     - Variables stored/computed in the parent functions must always start with Mech_
        #     - At least the required variables in all methods inherited from the parent must be here defined
        # +++++++++++++++++++++++++++++++++++++++++++++++++++++++
        #  Data initialisation
        # -----------------------
        self.tech = Technology
        self.direct_flag = 0
        #  Required by the parent functions
        # --------------Performance
        self.Mech_T = []  # dimensional torque vector on the output shaft [N·m]
        self.omega = []  # rotational speed of the output shaft [rad/s]
        self.Mech_P_mean = []  # Mean Power at each power level [W]
        self.Mech_T_mean = []  # Mean torque at each torque level [N·m]
        self.Pcapt = []
        self.Mech_eff_mean = (
            []
        )  # Total mean efficiency of the mechanical transformation [-]
        # ---------------Reliability
        self.tao = []  # Torsional shear stress ranges on the shaft [MPa]
        self.ultimate_stress = []  # ultimate stress MPa
        self.N = []  # Number of cycles that the gearbox can work at each stress range
        self.Mech_damage = (
            []
        )  # Sum of the fractions of actual cycles over the cycle capacity 'N' (>1 failure occurs)
        self.reliab = []  # Data to build the fatigue strength curves
        # ---------------Environmental
        self.mass_unit = []  # Equivalent density  --> mass=mass_unit*D**3
        self.Mech_mass = []  # Total mass of the gearbox [kg]
        # ---------------Cost
        self.Mech_cost_adim = []
        self.Mech_cost = []  # Total cost of the gearbox [€]
        # Other variables
        self.size = P_nom*1000*5 # maximum power will be 5 times nominal power/ Pnom is received in kW from the GUI
        self.t_efficiency = mean_eff
        self.t_ratio = t_ratio  # Transmission ratio
        self.S_N = [[0]]
        # --------------------------------------------------------------------------------------------------------------

    def perf_props(self, Cpto, vel_v, pow_prob, obj_args):
        """Performance of the simplified object

        Function that calculates the efficiency from an efficiency value within the range specified by the user, up to self.size --> the maximum absorbed power. The inputs are power LEVELs at which the object is expected to be working, for the mean Power computation the parent method 'performance' is used

        Args:
            Cpto (float): PTO damping for the corresponding environmental state
            vel_v (list): velocity vector based on the standard deviation
            pow_prob (list): Probability distribution of the power levels. Not used in the simplified mechanical object
            obj_args (list): ditional arguments needed for each specific object

        Returns: 
            none
         """

        P_in = Cpto * vel_v ** 2
        eff = self.t_efficiency * np.ones(len(vel_v))
        # ***************************************************************************************************************
        # ->WEC: In speed is the speed of the dof, either linear or rotational  (careful with this!)
        # ->TEC: In speed is the current speed
        if self.tech == "Tidal":
            om_rotor = 1 / obj_args["Ct_tidal"]
            self.omega = (
                om_rotor * self.t_ratio * np.ones(len(vel_v))
            )  # Transformation ratio = IN_speed/OUT_speed
            Ct_tidal = obj_args["Ct_tidal"]
            self.Mech_T = (eff * Ct_tidal * Cpto * vel_v ** 2)/ self.t_ratio 
        elif self.tech == "Wave":
            self.omega = (
                vel_v * self.t_ratio
            )  # Transformation ratio = IN_speed/OUT_speed
            self.Mech_T = eff * Cpto * vel_v / self.t_ratio
        self.omega[P_in > self.size] = 0
        self.Mech_T[P_in > self.size] = 0

    def reliability_props(self):

        """
        This method computes the admissible number of cycles at each environmental state. It assumes that there
        is a failure rate of 0.1 at the input power level and is linearly increased up to 1 for 10·input_power
        
        :return: none
        """

        self.tao = self.Mech_T * self.omega  # [W] --> equivalent power level
        self.ultimate_stress = 503  # Mpa
        # self.ultimate_stress = ((10 ** (
        #    (self.airT_reliab["fatigue_life"][0][1] - 3) / self.airT_reliab["fatigue_life"][0][0])) / 0.9)  # (MPa)

        # **Manual** correction for null torques, set to 0.1[MPa], which almost doesn't consume any life
        self.tao[self.tao < 1] = 1

        # Obtain the corresponding fatigue life curves from the DB for the two sections
        annual_cycles = 365.25 * 24 * 3600 / 7  # Assuming a mean period of 7s
        design_working_level = 1*self.size #0.5 * self.size  # -> It lasts the annual cycles
        extreme_working_level = 10* self.size # 10 * self.size  # -> It lasts 0.1% the annual cycles
        m = (np.log10(annual_cycles) - np.log10(0.001 * annual_cycles)) / (
            np.log10(extreme_working_level) - np.log10(design_working_level)
        )  # steepness
        log_a = np.log10(annual_cycles) + m * np.log10(
            design_working_level
        )  # Initial value
        first_sect = [m, log_a]
        # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        # Life of the tension ranges within the first section of the fatigue curves
        N_life = 10 ** (
            first_sect[1] - first_sect[0] * np.log10(np.abs(np.array(self.tao)))
        )

        # **Manual** Assigment of ONE cycle life to those tension ranges higher than the tension range corresponding
        # to 1000cycles (low cycle region, out of the scope of this work)
        sigma_max = 10 ** ((3 - first_sect[1]) * (-1 / first_sect[0]))
        N_life[self.tao > sigma_max] = 1

        # Admissible number of cycles for the stress ranges at the shaft
        self.N = N_life
   
    def env_props(self):
        """Method to compute the non-dimensional mass of the simplified mechanical object
        
        :return: The method doesn't return values, it stores the results in the corresponding variables
        """
        # Unitary mass, proportional to the cubic of the size
        self.mass_unit = 120 / 1000 * self.size / self.size ** 3  # 100kg/kW

    def cost_props(self):
        """This method computes the non-dimensional cost
        
        :return: The method doesn't return values, it stores the results in the corresponding variables
        """

        self.Mech_cost_adim = 500 / 1000 * self.size / self.size ** 3  # 100€/kW
        # it is divided by the cubic power of the size to be coherent with the parent cost function
