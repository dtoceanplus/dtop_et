# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.stats import chi2
import os
from scipy.optimize import curve_fit
from dtop_energytransf.utils import get_project_root



class Mech2Elect:
    """Parent class to force all classes compute the required metrics.

    Parent class for the sub-class to compute the Mechanical Energy to Electrical Energy object. It provides all sub-classes
    with functions to compute what the four assessments are expecting at each environmental state. The specified functions are:

    - performance()
    
    - reliability()
    
    - env_impact()
    
    - cost()
    
    The initialisation is to be done at sub-class level (child), and all the required properties are also defined at
    sub-class level so that the definitive parameters/metrics can be evaluated with the functions provided
    by this class (parent).

    """
    
    # Generic methods that all kinds of Mech2Elect objects should be able to compute to
    # feed the assessments
    def performance(self, P_act, om_act, Mech_P_mean, torque_prob):
        """Method to compute the performance.

        Function to compute the performance of a child class subject taking into account the mechanical input. 

        Necessary to be DEFINED in the child:
        
          - self.Elect_eff - 1D array
        
          - self.Elect_I_est - 1D array
        
          - self.Elect_cosphi - 1D array
        
          - self.V
        
        Args:
           P_act (list): Mechanical power in the shaft in [W] as input to the generator
           om_act (list): Rotational speed in the shaft of the generator [rad/s]
           Mech_P_mean (list): Output mechanical power multiplied by probability distribution of the power levels
           torque_prob (list): Probability distribution of the torque levels

        Attributes:
           Elect_P_mean (list): Output electrical power multiplied by probability distribution of the power levels
           Elect_I_mean (list): Generator estator current multiplied by its probability distribution (torque levels)
           Elect_eff_mean (float): Mean electrical efficiency value
           omega_elect (list): Rotational speed in the shaft of the generator [rad/s]

        Returns:
            none   

        """       
       
        # ********************************************************************************************************
        # Necessary to be DEFINED in the child:
        #  - self.Elect_eff - 1D array
        #  - self.Elect_I_est - 1D array
        #  - self.Elect_cosphi - 1D array
        #  - self.V
        # Necessary to be computed here:
        #  - self.Elect_P_mean - 1D array
        #  - self.Elect_I_mean - 1D array
        #  - self.Elect_eff_mean - float
        # ********************************************************************************************************

        #  If efficiency needs to be updated per environmental condition the method in the child is here executed
        if hasattr(self, "eff"):
            self.eff(P_act, om_act)

        mean_ElectP = np.zeros([len(Mech_P_mean)])
        for cP in range(0, len(Mech_P_mean)):
            mean_ElectP[cP] = self.Elect_eff[cP] * Mech_P_mean[cP]

        # Negative current/power/efficiencies are made zero to avoid weird results
        mean_ElectP[mean_ElectP < 0] = 0
        self.Elect_I_est[mean_ElectP == 0] = 0

        self.Elect_P_mean = mean_ElectP
        self.Elect_I_mean = self.Elect_I_est * torque_prob
        if np.sum(mean_ElectP) == 0:
            self.Elect_eff_mean = 0 * np.sum(mean_ElectP)
        else:
            self.Elect_eff_mean = np.sum(mean_ElectP) / np.sum(Mech_P_mean)
        self.omega_elect = om_act

    def reliability(self, load_prob, EC_T):
        """Method to compute the damage subject to a specific environmental condition

        Function to compute the damage of the load ranges on the child object during all working hours of the year
        of the corresponding environmental condition

        Necessary to be DEFINED in the child:
        
          - N - 1D array

        Args:
            load_prob (list): Probability distribution of load ranges
            EC_T (int): Annual equivalent time of the corresponding environmental condition [s]

        Attributes:
            Elect_damage (float): Damage during the EC total time 'EC_T'

        Returns:
            none

        """        
        # ********************************************************************************************************
        # Necessary to be DEFINED in the child:
        #  - self.N - 1D array
        # Necessary to be computed here:
        #  - self.Elect_damage - float
        # ********************************************************************************************************

        #  If efficiency needs to be updated per environmental condition the method in the child is here executed
        if hasattr(self, "reliability_props"):
            self.reliability_props()

        T_hours = (
            load_prob * EC_T / 3600
        )  # Actual number of hours working of each range
        self.Elect_damage = np.sum(
            T_hours / self.N
        )  # Damage during the EC total time 'EC_T'
        #aux=0
        #time=0
        #for conter in range(0, len(T_hours)): 
        #    aux=aux+T_hours[conter] / self.N[conter]

        #    if aux>1:
        #        break
        #if aux>1:
        #    time=7*conter/(365*24*60*60)                  
        #print("failure rate generator",time)
        # print('Elect_damage: ' + str(self.Elect_damage))

    def env_impact(self):
        """Method to compute the environmental impact (mass) of the sub-class

        Function to calculate the total mass of the child object. It is based on a non-dimensional mass precomputed
        in the child where: total_mass=non_dim_mass*Pnom^2
        
        Necessary to be DEFINED in the child:
        
           - P_nom - float
        
           - Gen_mass_f - a list where Gen_mass_f factors are a and b in a*P_nom+b        

        Attributes:
            Elect_mass (float): Mass of the electrical object in [kg]

        :return: The method doesn't return values, it stores the results in the corresponding variables
        """        

        # ********************************************************************************************************
        # Necessary to be DEFINED in the child:
        #  - self.P_nom - float
        #  - self.Gen_mass_f - a list where Gen_mass_f factors are a and b in a*P_nom+b
        # Necessary to be computed here:
        #  - self.Elect_mass - float
        # ********************************************************************************************************

        # If the child needs the Elect_mass_unit to be updated per EC it is done here
        if hasattr(self, "env_props"):
            self.env_props()

        self.Elect_mass = round(self.Elect_mass_unit * self.P_nom, 2)

        # print('Elect_mass: ' + str(self.Elect_mass))

    def cost(self):
        """Method to compute the cost of the subclass

        Function to calculate the total cost of the child object. It is based on a cost function pre-loaded
        in the child.

        Necessary to be DEFINED in the child:
        
          - P_nom - float
        
          - cost_f - B*P_nom**(X) where B and X are the factors of cost_f    

        Attributes:
           Elect_cost (float): Cost of the electrical object in €

        :return: The method doesn't return values, it stores the results in the corresponding variables           
        """
        
        # ********************************************************************************************************
        # Necessary to be DEFINED in the child:
        #  - self.P_nom - float
        #  - self.cost_f - B*P_nom**(X) where B and X are the factors of cost_f
        # Necessary to be computed here:
        #  - self.Elect_cost - float
        # ********************************************************************************************************

        # If the child needs the Elect_cost_unit to be updated per EC it is done here
        if hasattr(self, "cost_props"):
            self.cost_props()

        self.Elect_cost = round(self.Elect_cost_unit * self.P_nom, 2)

        # print('Elect_cost: ' + str(self.Elect_cost))


class SCIG(Mech2Elect):
    """Class representing the Squirrel Cage Induction Generator (SCIG) object.

    Args:
        Pnom (int): Nominal power of the SCIG
        pp (int): Number of pole pairs
        Vnom (int): SCIG rated voltage
        fnom (int): SCIG nominal frequency
        rel_T_maxnom (float): Ratio of peak to nominal Torque
        rel_V_maxnom (float): Ratio of peak to nominal Voltage. The same ratio applies to obtain the maximum frequency and speed
        gen_class (str): Generator class for life calculation
        catalogue (dict): dictionary with all the electrical parameters from the catalogue

    Attributes:
        Elect_eff (list): List of the computed efficiencies for all the power levels 
        Elect_I_est (list): Stator current of the generator
        Elect_I_mean (list): Stator current with probability distribution
        f_est (list): Working frequency list
        pp (int): Pole pair number
        Elect_cosphi (list): Cos phi list
        N (list): Number of cycles for fatigue calculation
        Elect_damage (list): Sum of the fractions of actual cycles over the cycle capacity 'N' (>1 failure occurs)
        Elect_mass_unit (float): Unitary mass of the generator (to be multiplied by the Pnom^2 for obtaining the mass)
        Elect_mass (list): Total mass of the generator [kg]
        Elect_cost_unit (float): Cost factor
        Elect_cost (float): Total cost of the generator [€]
        I_est (list): Stator current of the generator
        V_est (list): Generator voltge
        P_nom (int): Nominal power of the generator in [W].
        V_nom (int): SCIG rated voltage
        f_nom (int): SCIG nominal frequency
        rel_T_maxnom (float): Ratio of peak to nominal Torque
        rel_V_maxnom (float): Ratio of peak to nominal Voltage. The same ratio applies to obtain the maximum frequency and speed
        om_nom (float): Nominal speed of the generator
        T_nom (float): Nominal torque of the generator
        V_max (float): Admisible maximum voltage of the generator
        T_max (float): Admisible maximum torque of the generator
        f_max (float): Admisible maximum frequency of the generator
        omega_maxV_f (float): Admisible maximum speed of the generator
        omega_lim (float): limit speed
        P_max (float): Admisible maximum power of the generator
        Gen_mass_f (list): Generator mass factors to obtain the mass. a and b factors in  Mass = (a*P_nom+b)
        cos_phi (list): SCIG cos_phi depending on the load factor and pole pairs in SCIG generators
        shaft_diam_f (list): Shaft diameter factors proportional to the power 
        Res_f (list): Resistance factors. Equivalent generator R0 and R1 stator resistance factors in Rst=R0*PnomR1.  for winding losses calculation (Ploss, wind=I2st⋅Rst )
        sigma_e (float): Eddy current loss coefficient
        sigma_h (float): Hysteresis current loss coefficient
        thick_max (float): Maximum thickness of the iron core steel plate
        wind_mass_fraction (float): mass of the stator and rotor ratio from the total mass of the generator (60% by default)
        x_f (list): x1, x2 and x3 linear functions coeffs of the nominal rot_speed [rpm] of the generator
        I_nom_f (list): Nominal current  factors to obtain nominal current depending on the nominal power: I0 and I1 factors in I0*Pnom+I1. For reliability calculations
        B (float): Average magnetic flux density (B) in the air gap
        cost_f (list): Generator cost factors to obtain the cost: B and X factors in Cost unit = (B*(P_nom)^X*1000)/P_nom where P_nom is in kW
        A_eq (float):  Vnom / (fnom * self.B)
        Life (dict): Different insulation parameters depending on the insulation class. K, k0: life asessment parameters . Temp_max: max temperature pr insulation class 𝑁=𝑒𝐾𝑇+𝐾0. Depending on the class of the generator A, B, F, H"
        I_nom (float): Generator nominal current obtained from the parameters in the catalogue
        shaft_diam (float): Generator shaft diameter obtained from the parameters in the catalogue
        Res (float): Generator stator resistance obtained from the parameters in the catalogue

    Returns:
        none

    """    
    # Initialization of the generator based on parameterized resistance, mass, mechanical losses etc
    def __init__(self, Pnom, pp, Vnom, fnom, rel_T_maxnom, rel_V_maxnom, gen_class,catalogue):
        # Variable initialization for the assessments
        #  Required by the parent functions
        # --------------Performance
        self.Elect_eff = []
        self.Elect_I_est = []
        self.Elect_P_mean = []
        self.Elect_I_mean = []
        self.Elect_eff_mean = []
        self.f_est = []
        self.pp = pp
        self.Elect_cosphi = []
        # ---------------Reliability
        self.N = []  # Number of cycles
        self.Elect_damage = []
        # ---------------Environmental
        self.Elect_mass_unit = []
        self.Elect_mass = []
        # ---------------Cost
        self.Elect_cost_unit = []
        self.Elect_cost = []  # Total cost of the gearbox [€]
        # ---------------Other variables
        self.I_est = []
        self.V_est = []
        self.P_nom = Pnom * 1000 #Pnom is received in kW from the GUI
        self.V_nom = Vnom
        self.f_nom = fnom
        self.rel_T_maxnom = rel_T_maxnom
        self.rel_V_maxnom = rel_V_maxnom
        self.om_nom = 2.0 * np.pi * fnom / pp
        self.T_nom = self.P_nom / self.om_nom
        self.V_max = Vnom * rel_V_maxnom
        self.T_max = self.T_nom * rel_T_maxnom
        self.f_max = rel_V_maxnom * fnom
        self.omega_maxV_f = 2 * np.pi * self.f_max / pp
        self.omega_lim = rel_T_maxnom * self.omega_maxV_f
        self.P_max = self.omega_maxV_f * self.T_max
        SCIG_DB = catalogue
        # Read data from the DB useful to compute lossess in the SCIG
        # root = get_project_root()
        # folder = str(root) + "/business/DB/json_data/"
        # path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))+'\\DB\\json_data\\'
        #SCIG_DB = pd.read_json(folder + "SCIG.json", orient="records", lines=True)
        self.Gen_mass_f = SCIG_DB["Gen_mass"]#[0]
        self.cos_phi = SCIG_DB["cos_phi"]
        self.shaft_diam_f = SCIG_DB["shaft_diam"]#[0]
        self.Res_f = SCIG_DB["Res"]#[0]
        self.sigma_e = SCIG_DB["sigma_e"]#[0]
        self.sigma_h = SCIG_DB["sigma_h"]#[0]
        self.thick_max = SCIG_DB["thick_max"]#[0]
        self.wind_mass_fraction = SCIG_DB["wind_mass_fraction"]#[0]
        self.x_f = SCIG_DB["x_f"]#[0]
        self.I_nom_f = SCIG_DB["I_nom"]#[0]
        self.B = SCIG_DB["B"]#[0]  # Magnetic flux density
        self.cost_f = SCIG_DB["cost"]#[0]
        self.A_eq = Vnom / (fnom * self.B)
        # Reliability Data
        self.Life = SCIG_DB["Life"][gen_class]
        # Generator properties derived from the data read
        self.I_nom = self.I_nom_f[0] * self.P_nom + self.I_nom_f[1]
        self.shaft_diam = (
            self.shaft_diam_f[0] * self.P_nom ** 3
            + self.shaft_diam_f[1] * self.P_nom ** 2
            + self.shaft_diam_f[2] * self.P_nom
            + self.shaft_diam_f[3]
        )
        self.Res = self.Res_f[0] * self.P_nom ** self.Res_f[1]

    def eff(self, P_act, om_act):
        """
        Efficiency of the generator computed for each operational condition. The efficiency is based
        on mechanical, iron, winding, brushes, rotor and capacity lossess

        Calculation of lossess in the generator -- The inputs are power LEVELs at which the Generator is expected to
        be working, for the mean Power computation the parent method 'performance' is used
        
        :param list P_act: Power levels
        :param list om_act: Corresponding rotational speed
        
        Returns:
            none
        """

        # --------------------------------------------
        eff = np.zeros([len(P_act)])
        self.I_est = np.zeros([len(P_act)])
        self.I_rot = np.zeros([len(P_act)])
        cosphi = np.zeros([len(P_act)])    
        self.V_est = np.zeros([len(P_act)])
        self.f_est = np.zeros([len(P_act)])

        inds_zero = om_act <= 0
        inds_nonzero = om_act > 0

        for cP_act in range(0, len(P_act), 1):
            if inds_nonzero[cP_act] == True:
                # phi cosine calculated from curve fitting of real generator
                # cosphi[cP_act] = (
                #     self.phi_cos[0] * P_act[cP_act] ** 3
                #     + self.phi_cos[1] * P_act[cP_act] ** 2
                #     + self.phi_cos[2] * P_act[cP_act]
                #     + self.phi_cos[3]
                # )
                # if cosphi[cP_act] > 1:
                #     cosphi[cP_act] = 1.0

                load_factor = P_act[cP_act] / self.P_nom              
                if load_factor > 1:
                    load_factor = 1
                cosphi[cP_act] = (
                    self.cos_phi[self.pp-1][0] * load_factor ** 3
                    + self.cos_phi[self.pp-1][1] * load_factor ** 2
                    + self.cos_phi[self.pp-1][2] * load_factor
                    + self.cos_phi[self.pp-1][3]
                ) 
                if cosphi[cP_act] < 0.05:
                    cosphi[cP_act] = 0.05                    
     
                # Working Torque and frequency
                T_act = P_act[cP_act] / om_act[cP_act]
                self.f_est[cP_act] = om_act[cP_act] * self.pp / (2 * np.pi)

                # Mechanical losses
                rpm_act = om_act[cP_act] * 60 / (2 * np.pi)
                x3 = self.x_f[0] * rpm_act
                x2 = self.x_f[1] * rpm_act
                x1 = self.x_f[2] * rpm_act
                # Mechanical losses are kept constant for Pnom>3e5[W] since no data is available. this limit should be changeable
                # in case late stage with user defined generator is ran. Think about enabling it!!
                if self.P_nom < 3e5:
                    mech_losses = (
                        x3 * self.shaft_diam ** 3
                        + x2 * self.shaft_diam ** 2
                        + x1 * self.shaft_diam
                    )
                else:
                    mech_losses = x3 * 237.451 ** 3 + x2 * 237.451 ** 2 + x1 * 237.451

                # Capacity Losses
                if om_act[cP_act] < self.omega_maxV_f:
                    # Working range up to the 'new' nominal speed "omega_maxV_f"
                    self.V_est[cP_act] = (
                        self.V_nom / self.f_nom * self.f_est[cP_act]
                    )  # Linear variation of the voltage
                    self.I_est[cP_act] = P_act[cP_act] / (
                        self.V_est[cP_act] * cosphi[cP_act] * np.sqrt(3)
                    )
                    if T_act > self.T_max:
                        cap_losses = (T_act - self.T_max) * om_act[cP_act]
                    else:
                        cap_losses = 0
                elif om_act[cP_act] < self.omega_lim:
                    # Working range up to the maximum allowed omega "omega_lim"
                    T_adm = (
                        self.T_max * 1 / (om_act[cP_act] / self.omega_maxV_f) ** 2
                    )  # The maximum torque is lower in this range
                    self.V_est[
                        cP_act
                    ] = self.V_max  # The voltage is constant in this region
                    self.I_est[cP_act] = P_act[cP_act] / (
                        self.V_est[cP_act] * cosphi[cP_act] * np.sqrt(3)
                    )  # Irms
                    if T_act > T_adm:
                        cap_losses = (T_act - T_adm) * om_act[cP_act]
                    else:
                        cap_losses = 0
                #Rotor losess
                self.I_rot[cP_act]=self.I_est[cP_act]*cosphi[cP_act]
                Rotor_losses = self.I_rot[cP_act] ** 2 * self.Res # Design of rotating electrical machins Juha Pyrhonen, page 350
                #Brushes losess
                U_contact=1
                P_brushes=self.Res*cosphi[cP_act]*U_contact         
                # Only compute efficiency if om is within the ranges, otherwise eff=0
                if P_act[cP_act] > 0 and om_act[cP_act] < self.omega_lim:
                    winding_losses = self.I_est[cP_act] ** 2 * self.Res
                    if self.f_est[cP_act] == 0:
                        B_act = 0
                    else:
                        B_act = self.V_est[cP_act] / (
                            self.f_est[cP_act] * self.A_eq
                        )  # in the region 2 the flux density is lower than B
                    Iron_losses = (
                        B_act ** 2
                        * (
                            self.sigma_h * self.f_est[cP_act] / 100
                            + self.sigma_e
                            * (self.thick_max * self.f_est[cP_act] / 100) ** 2
                        )
                        * self.wind_mass_fraction
                        * self.Elect_mass
                    )
                    P_loss = mech_losses + cap_losses + winding_losses + Iron_losses+P_brushes +Rotor_losses


                    eff[cP_act] = (P_act[cP_act] - P_loss) / P_act[cP_act]
                    
                else:
                    eff[cP_act] = 0
                    self.I_est[cP_act] = 0
                    cosphi[cP_act] = 0
              
                # Generator shutdown in case the power is too high
                if P_act[cP_act] > self.P_max:
                    eff[cP_act] = 0
                    self.I_est[cP_act] = 0
                    cosphi[cP_act] = 0
        # The negative efficiencies are converted to 0. It is reasonable since it is passing by zero power
        # values and already turned on.
        eff[eff < 0] = 0
        self.Elect_eff = eff
        
        self.Elect_I_est = self.I_est
        self.Elect_cosphi = cosphi


    # The life of the generator working at the corresponding currents to the operational conditions
    def reliability_props(self):
        """
        This method computes the admissible number of cycles at each environmental state
        
        :return: The method doesn't return values, it stores the results in the corresponding variables
        """       
        # At each point of AT call life equations
        SCIG_life = np.zeros([len(self.Elect_I_est)])
        I_mean=np.sum(self.Elect_I_mean) 
        # AP_vector
        for cont_AP in range(0, len(self.Elect_I_est), 1):
            if self.Elect_I_est[cont_AP] == 0:
                self.Elect_I_est[cont_AP] = 0.1
            Temp=min(150,(150/self.I_nom**2)*(I_mean+self.Elect_I_est[cont_AP])**2)
            #Temp = (self.Life[2] / self.I_nom ** 2) * self.Elect_I_est[cont_AP] ** 2
            SCIG_life[cont_AP] = np.exp(self.Life[0] * Temp + self.Life[1])
        SCIG_life[SCIG_life == 0] = 1
        self.N = SCIG_life

    def env_props(self):
        """
        The non-dimensional mass is obtained here from the Gen_mass_f in the database
        
        :return: The method doesn't return values, it stores the results in the corresponding variables
        """

        self.Elect_mass_unit = (
            self.Gen_mass_f[0] * self.P_nom + self.Gen_mass_f[1]
        ) / self.P_nom

    def cost_props(self):
        """
        Update of the Elect_cost_unit per environmental condition
        
        :return: The method doesn't return values, it stores the results in the corresponding variables
        """

        self.Elect_cost_unit = (
            self.cost_f[0] * (self.P_nom / 1000) ** self.cost_f[1] * 1000
        ) / self.P_nom

class PMSG(Mech2Elect):
    """Class representing the Permanent Magnet Synchronous Generator (PMSG) object.

    Args:
        Pnom (int): Nominal power of the PMSG
        pp (int): Number of pole pairs
        Vnom (int): PMSG rated voltage
        fnom (int): PMSG nominal frequency
        rel_T_maxnom (float): Ratio of peak to nominal Torque
        rel_V_maxnom (float): Ratio of peak to nominal Voltage. The same ratio applies to obtain the maximum frequency and speed
        gen_class (str): Generator class for life calculation
        catalogue (dict): dictionary with all the electrical parameters from the catalogue

    Attributes:
        Elect_eff (list): List of the computed efficiencies for all the power levels 
        Elect_I_est (list): Stator current of the generator
        Elect_I_mean (list): Stator current with probability distribution
        f_est (list): Working frequency list
        pp (int): Pole pair number
        Elect_cosphi (list): Cos phi list
        N (list): Number of cycles for fatigue calculation
        Elect_damage (list): Sum of the fractions of actual cycles over the cycle capacity 'N' (>1 failure occurs)
        Elect_mass_unit (float): Unitary mass of the generator (to be multiplied by the Pnom^2 for obtaining the mass)
        Elect_mass (list): Total mass of the generator [kg]
        Elect_cost_unit (float): Cost factor
        Elect_cost (float): Total cost of the generator [€]
        I_est (list): Stator current of the generator
        V_est (list): Generator voltge
        P_nom (int): Nominal power of the generator in [W].
        V_nom (int): PMSG rated voltage
        f_nom (int): PMSG nominal frequency
        rel_T_maxnom (float): Ratio of peak to nominal Torque
        rel_V_maxnom (float): Ratio of peak to nominal Voltage. The same ratio applies to obtain the maximum frequency and speed
        om_nom (float): Nominal speed of the generator
        T_nom (float): Nominal torque of the generator
        V_max (float): Admisible maximum voltage of the generator
        T_max (float): Admisible maximum torque of the generator
        f_max (float): Admisible maximum frequency of the generator
        omega_maxV_f (float): Admisible maximum speed of the generator
        omega_lim (float): limit speed
        P_max (float): Admisible maximum power of the generator
        Gen_mass_f (list): Generator mass factors to obtain the mass. a and b factors in  Mass = (a*P_nom+b)
        phi_cos (list): PMSG Cos phi factors Assumed phi_cos relation with the mechanical power in cosphi=  cosphi0 * P_act3 + cosphi1 * P_act2 +     cosphi2 * P_act+ cosphi3 
        shaft_diam_f (list): Shaft diameter factors proportional to the power 
        Res_f (list): Resistance factors. Equivalent generator R0 and R1 stator resistance factors in Rst=R0*PnomR1.  for winding losses calculation (Ploss, wind=I2st⋅Rst )
        sigma_e (float): Eddy current loss coefficient
        sigma_h (float): Hysteresis current loss coefficient
        thick_max (float): Maximum thickness of the iron core steel plate
        wind_mass_fraction (float): mass of the stator and rotor ratio from the total mass of the generator (60% by default)
        x_f (list): x1, x2 and x3 linear functions coeffs of the nominal rot_speed [rpm] of the generator
        I_nom_f (list): Nominal current  factors to obtain nominal current depending on the nominal power: I0 and I1 factors in I0*Pnom+I1. For reliability calculations
        B (float): Average magnetic flux density (B) in the air gap
        cost_f (list): Generator cost factors to obtain the cost: B and X factors in Cost unit = (B*(P_nom)^X*1000)/P_nom where P_nom is in kW
        A_eq (float):  Vnom / (fnom * self.B)
        Life (dict): Different insulation parameters depending on the insulation class. K, k0: life asessment parameters . Temp_max: max temperature pr insulation class 𝑁=𝑒𝐾𝑇+𝐾0. Depending on the class of the generator A, B, F, H"
        I_nom (float): Generator nominal current obtained from the parameters in the catalogue
        shaft_diam (float): Generator shaft diameter obtained from the parameters in the catalogue
        Res (float): Generator stator resistance obtained from the parameters in the catalogue

    Returns:
        none

    """ 
    
    # Initialization of the generator based on parameterized resistance, mass, mechanical losses etc
    def __init__(self, Pnom, pp, Vnom, fnom, rel_T_maxnom, rel_V_maxnom, gen_class,catalogue):

        # Variable initialization for the assessments
        #  Required by the parent functions
        # --------------Performance
        self.Elect_eff = []
        self.Elect_I_est = []
        self.Elect_P_mean = []
        self.Elect_I_mean = []
        self.Elect_eff_mean = []
        self.f_est = []
        self.pp = pp
        self.Elect_cosphi = []
        # ---------------Reliability
        self.N = []  # Number of cycles
        self.Elect_damage = []
        # ---------------Environmental
        self.Elect_mass_unit = []
        self.Elect_mass = []
        # ---------------Cost
        self.Elect_cost_unit = []
        self.Elect_cost = []  # Total cost of the gearbox [€]
        # ---------------Other variables
        self.I_est = []
        self.V_est = []
        self.P_nom = Pnom * 1000 #Pnom is received in kW from the GUI
        self.V_nom = Vnom
        self.f_nom = fnom
        self.rel_T_maxnom = rel_T_maxnom
        self.rel_V_maxnom = rel_V_maxnom
        self.om_nom = 2.0 * np.pi * fnom / pp
        self.T_nom = self.P_nom / self.om_nom
        self.V_max = Vnom * rel_V_maxnom
        self.T_max = self.T_nom * rel_T_maxnom
        self.f_max = rel_V_maxnom * fnom
        self.omega_maxV_f = 2 * np.pi * self.f_max / pp
        self.omega_lim = rel_T_maxnom * self.omega_maxV_f
        self.P_max = self.omega_maxV_f * self.T_max
        SCIG_DB = catalogue
        # Read data from the DB useful to compute lossess in the SCIG
        # root = get_project_root()
        # folder = str(root) + "/business/DB/json_data/"
        # path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))+'\\DB\\json_data\\'
        #SCIG_DB = pd.read_json(folder + "SCIG.json", orient="records", lines=True)
        self.Gen_mass_f = SCIG_DB["Gen_mass"]#[0]
        self.phi_cos = SCIG_DB["phi_cos"]#[0]
        self.shaft_diam_f = SCIG_DB["shaft_diam"]#[0]
        self.Res_f = SCIG_DB["Res"]#[0]
        self.sigma_e = SCIG_DB["sigma_e"]#[0]
        self.sigma_h = SCIG_DB["sigma_h"]#[0]
        self.thick_max = SCIG_DB["thick_max"]#[0]
        self.wind_mass_fraction = SCIG_DB["wind_mass_fraction"]#[0]
        self.x_f = SCIG_DB["x_f"]#[0]
        self.I_nom_f = SCIG_DB["I_nom"]#[0]
        self.B = SCIG_DB["B"]#[0]  # Magnetic flux density
        self.cost_f = SCIG_DB["cost"]#[0]
        self.A_eq = Vnom / (fnom * self.B)
        # Reliability Data
        self.Life = SCIG_DB["Life"][gen_class]
        # Generator properties derived from the data read
        self.I_nom = self.I_nom_f[0] * self.P_nom + self.I_nom_f[1]
        self.shaft_diam = (
            self.shaft_diam_f[0] * self.P_nom ** 3
            + self.shaft_diam_f[1] * self.P_nom ** 2
            + self.shaft_diam_f[2] * self.P_nom
            + self.shaft_diam_f[3]
        )
        self.Res = self.Res_f[0] * self.P_nom ** self.Res_f[1]

    def eff(self, P_act, om_act):
        """
        Efficiency of the generator computed for each operational condition. The efficiency is based
        on mechanical, iron, winding and capacity lossess
        Calculation of lossess in the generator -- The inputs are power LEVELs at which the Generator is expected to
        be working, for the mean Power computation the parent method 'performance' is used
        
        :param list P_act: Power levels
        :param list om_act: Corresponding rotational speed
        
        :return: The method doesn't return values, it stores the results in the corresponding variables
        """

        # --------------------------------------------
        eff = np.zeros([len(P_act)])
        self.I_est = np.zeros([len(P_act)])
        cosphi = np.zeros([len(P_act)])
        self.V_est = np.zeros([len(P_act)])
        self.f_est = np.zeros([len(P_act)])

        inds_zero = om_act <= 0
        inds_nonzero = om_act > 0

        for cP_act in range(0, len(P_act), 1):
            if inds_nonzero[cP_act] == True:
                # phi cosine calculated from curve fitting of real generator
                cosphi[cP_act] = (
                    self.phi_cos[0] * P_act[cP_act] ** 3
                    + self.phi_cos[1] * P_act[cP_act] ** 2
                    + self.phi_cos[2] * P_act[cP_act]
                    + self.phi_cos[3]
                )
                if cosphi[cP_act] > 1:
                    cosphi[cP_act] = 1.0

                # Working Torque and frequency
                T_act = P_act[cP_act] / om_act[cP_act]
                self.f_est[cP_act] = om_act[cP_act] * self.pp / (2 * np.pi)

                # Mechanical losses
                rpm_act = om_act[cP_act] * 60 / (2 * np.pi)
                x3 = self.x_f[0] * rpm_act
                x2 = self.x_f[1] * rpm_act
                x1 = self.x_f[2] * rpm_act
                # Mechanical losses are kept constant for Pnom>3e5[W] since no data is available. this limit should be changeable
                # in case late stage with user defined generator is ran. Think about enabling it!!
                if self.P_nom < 3e5:
                    mech_losses = (
                        x3 * self.shaft_diam ** 3
                        + x2 * self.shaft_diam ** 2
                        + x1 * self.shaft_diam
                    )
                else:
                    mech_losses = x3 * 237.451 ** 3 + x2 * 237.451 ** 2 + x1 * 237.451

                # Capacity Losses
                if om_act[cP_act] < self.omega_maxV_f:
                    # Working range up to the 'new' nominal speed "omega_maxV_f"
                    self.V_est[cP_act] = (
                        self.V_nom / self.f_nom * self.f_est[cP_act]
                    )  # Linear variation of the voltage
                    self.I_est[cP_act] = P_act[cP_act] / (
                        self.V_est[cP_act] * cosphi[cP_act] * np.sqrt(3)
                    )
                    if T_act > self.T_max:
                        cap_losses = (T_act - self.T_max) * om_act[cP_act]
                    else:
                        cap_losses = 0
                elif om_act[cP_act] < self.omega_lim:
                    # Working range up to the maximum allowed omega "omega_lim"
                    T_adm = (
                        self.T_max * 1 / (om_act[cP_act] / self.omega_maxV_f) ** 2
                    )  # The maximum torque is lower in this range
                    self.V_est[
                        cP_act
                    ] = self.V_max  # The voltage is constant in this region
                    self.I_est[cP_act] = P_act[cP_act] / (
                        self.V_est[cP_act] * cosphi[cP_act] * np.sqrt(3)
                    )  # Irms
                    if T_act > T_adm:
                        cap_losses = (T_act - T_adm) * om_act[cP_act]
                    else:
                        cap_losses = 0
                # Magnet losess /Imanes / NREL  GeneratorSE: A Sizing Tool for Variable-Speed Wind Turbine Generators
                pFtm = 300 #NREL
                taup = 70/1000 #(m) NREL  GeneratorSE
                b_m = 0.7 * taup 
                r_s = -0.00000003 * self.P_nom ** 2 + 0.0007 * self.P_nom + 0.8874 # NREL
                l_s = 0.0307 * self.P_nom ** 0.4611 # NREL
                Magnet_losess = pFtm * 2 *self.pp * b_m * l_s
                # Only compute efficiency if om is within the ranges, otherwise eff=0
                if P_act[cP_act] > 0 and om_act[cP_act] < self.omega_lim:
                    winding_losses = self.I_est[cP_act] ** 2 * self.Res
                    if self.f_est[cP_act] == 0:
                        B_act = 0
                    else:
                        B_act = self.V_est[cP_act] / (
                            self.f_est[cP_act] * self.A_eq
                        )  # in the region 2 the flux density is lower than B
                    Iron_losses = (
                        B_act ** 2
                        * (
                            self.sigma_h * self.f_est[cP_act] / 100
                            + self.sigma_e
                            * (self.thick_max * self.f_est[cP_act] / 100) ** 2
                        )
                        * self.wind_mass_fraction
                        * self.Elect_mass
                    )
                    P_loss = mech_losses + cap_losses + winding_losses + Iron_losses + Magnet_losess

                    eff[cP_act] = (P_act[cP_act] - P_loss) / P_act[cP_act]
                else:
                    eff[cP_act] = 0
                    self.I_est[cP_act] = 0
                    cosphi[cP_act] = 0
              
                # Generator shutdown in case the power is too high
                if P_act[cP_act] > self.P_max:
                    eff[cP_act] = 0
                    self.I_est[cP_act] = 0
                    cosphi[cP_act] = 0
        # The negative efficiencies are converted to 0. It is reasonable since it is passing by zero power
        # values and already turned on.
        
        eff[eff < 0] = 0
        self.Elect_eff = eff
        
        self.Elect_I_est = self.I_est
        self.Elect_cosphi = cosphi


    # The life of the generator working at the corresponding currents to the operational conditions
    def reliability_props(self):
        """ 
        This method computes the admissible number of cycles at each environmental state
        
        :return: The method doesn't return values, it stores the results in the corresponding variables
        """         
        # At each point of AT call life equations
        SCIG_life = np.zeros([len(self.Elect_I_est)])
        # AP_vector
        for cont_AP in range(0, len(self.Elect_I_est), 1):
            if self.Elect_I_est[cont_AP] == 0:
                self.Elect_I_est[cont_AP] = 0.1
            Temp = min(180,(self.Life[2] / self.I_nom ** 2) * self.Elect_I_est[cont_AP] ** 2)
            SCIG_life[cont_AP] = np.exp(self.Life[0] * Temp + self.Life[1])
        SCIG_life[SCIG_life == 0] = 1
        self.N = SCIG_life

    def env_props(self):
        """
        The non-dimensional mass is obtained here from the Gen_mass_f in the database
        
        :return: The method doesn't return values, it stores the results in the corresponding variables
        """

        self.Elect_mass_unit = (
            self.Gen_mass_f[0] * self.P_nom + self.Gen_mass_f[1]
        ) / self.P_nom

    def cost_props(self):
        """
        Update of the Elect_cost_unit per environmental condition
        
        :return:
            none
        """

        self.Elect_cost_unit = (
            self.cost_f[0] * (self.P_nom / 1000) ** self.cost_f[1] * 1000
        ) / self.P_nom


class Elect_Simplified(Mech2Elect):
    
    """Class representing the Simplified Electrical transformation (generator) object.    

    Args:
        P_nom (int):  Nominal power of the generator
        V_nom (float, optional): rated voltage. Defaults to 690.0.
        pp (int, optional): Number of pole pairs. Defaults to 2.
        fnom (float, optional): nominal frequency. Defaults to 50.0.
        
    Attributes:
        Elect_eff (list): List of the computed efficiencies for all the power levels 
        Elect_I_est (list): Stator current of the generator:
        Elect_I_mean (list): Stator current with probability distribution
        f_est (list): Working frequency list
        pp (int): Pole pair number
        N (list): Number of cycles for fatigue calculation
        Elect_damage (list): Sum of the fractions of actual cycles over the cycle capacity 'N' (>1 failure occurs)
        Elect_mass_unit (float): Unitary mass of the generator (to be multiplied by the Pnom^2 for obtaining the mass)
        Elect_mass (list): Total mass of the generator [kg]
        Elect_cost_unit (float): Cost factor
        Elect_cost (float): Total cost of the generator [€]
        V_est (list): Generator voltge
        P_nom (int): Nominal power of the generator in [W].
        V_nom (int): Generator rated voltage
        f_nom (int): Generator nominal frequency
        Elect_cosphi (list): generator cos phi
        om_nom (float): Nominal speed of the generator
        om_shaft_norm (list): Normalized speed levels for efficiency calculation (X-axis of efficiency curve)
        eff_levels (list): Normalized efficiency levels for efficiency calculation (Y-axis of efficiency curve)
        
    Returns:
        none

    """ 

    def __init__(self, P_nom, V_nom=690.0, pp=2, fnom=50.0):

        # Data initialization for all variables of the class stored in 'self'
        # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        # Rules:
        #     - Variables stored/computed in the parent functions must always start with Mech_
        #     - At least the required variables in all methods inherited from the parent must be here defined
        # +++++++++++++++++++++++++++++++++++++++++++++++++++++++
        #  Data initialisation
        # -----------------------
        #  Required by the parent functions
        # --------------Performance
        self.Elect_eff = []
        self.Elect_I_est = []
        self.Elect_P_mean = []
        self.Elect_I_mean = []
        self.Elect_eff_mean = []
        self.f_est = []
        self.pp = pp
        # ---------------Reliability
        self.N = []  # Number of cycles
        self.Elect_damage = []
        # ---------------Environmental
        self.Elect_mass_unit = []
        self.Elect_mass = []
        # ---------------Cost
        self.Elect_cost_unit = []
        self.Elect_cost = []  # Total cost of the gearbox [€]
        # Other variables
        self.P_nom = P_nom * 1000 #Pnom is received in kW from the GUI
        self.V_nom = V_nom
        self.V_est = []
        self.f_nom = fnom
        self.Elect_cosphi = []
        self.om_nom = 2.0 * np.pi * fnom / pp

        # array with normalized power loads [0..1] [pu]
        self.om_shaft_norm = [0, 0.03, 0.06, 0.1, 0.185, 0.4, 0.6, 0.8, 1]
        self.eff_levels = [0, 0.4, 0.5, 0.6, 0.7, 0.8, 0.85, 0.88, 0.9]

        # efficiency curve for efficiency calculation
        def eff_fit(x, k1, k2, k3, k4):
            y = 1 - k1 * np.exp(k2 * x) - k3 * np.exp(k4 * x)
            return y

        # Array with efficiency values corresponding to each power load value [0..1] [pu]
        a_gen, b_gen = curve_fit(
            eff_fit,
            np.array(self.om_shaft_norm),
            np.array(self.eff_levels),
            bounds=([-10.0, -10.0, -10.0, -10.0], [10.0, 0.0, 10.0, 0.0]),
        )
        self.perf_f = a_gen  # k1 == a[0] ; k2 == a[1] in the eff_fit function
        self.eff_fit = (
            eff_fit  # The function is stored to be used in the perf_props function
        )

    # Efficiency of the generator computed for each operational condition. The efficiency is based
    # on mechanical, iron, winding and capacity lossess
    def eff(self, P_act, om_act):
        """Calculation of lossess in the simplified generator -- The inputs are power LEVELs at which the Generator is expected to be working, for the mean Power computation the parent method 'performance' is used

        Args:
            P_act (list): Power levels
            om_act (list): Rotational speed
        """        
        
        inds_zero = om_act <= 0
        inds_nonzero = om_act > 0
        self.Elect_I_est = np.zeros([len(P_act)])
        self.f_est = om_act * self.pp / (2 * np.pi)
        self.V_est = self.V_nom / self.f_nom * self.f_est
        self.Elect_eff = self.eff_fit(
            om_act / self.om_nom, *self.perf_f
        )  # Changed the interpolation by the
        # evaluation with the fitted function
       
        self.Elect_eff[self.Elect_eff > 1] = 1
        # self.Elect_eff = self.t_eff * np.ones(len(P_act))
        self.Elect_I_est[inds_nonzero] = P_act[inds_nonzero] / (
            self.V_est[inds_nonzero] * np.sqrt(3)
        )
        self.Elect_I_est[inds_zero] = 0
        self.Elect_cosphi =0.95* np.ones([len(P_act)])

        self.Elect_eff[P_act > self.P_nom*5] = 0
        self.Elect_I_est[P_act > self.P_nom*5] = 0

    # The life of the generator working at the corresponding currents to the operational conditions
    def reliability_props(self):
        """This method computes the admissible number of cycles at each environmental state. It assumes that there
           is a failure rate of 0.1 at the input power level and is linearly increased up to 1 for 10·input_power
           
           :return:
                none
        """
        self.tao = (
            self.Elect_I_est * self.V_nom * np.sqrt(3)*self.Elect_cosphi 
        )  # [W] --> equivalent power level
        # **Manual** correction for null torques, set to 0.1[MPa], which almost doesn't consume any life
        self.tao[self.tao < 1] = 1

        # Obtain the corresponding fatigue life curves from the DB for the two sections
        annual_cycles = 365.25 * 24 * 3600 / 7  # Assuming a mean period of 7s
        design_working_level = 1*self.P_nom  # 0.5 * self.P_nom  # -> It lasts the annual cycles
        extreme_working_level = 10*self.P_nom # 10 * self.P_nom  # -> It lasts 0.1% the annual cycles
        m = (np.log10(annual_cycles) - np.log10(0.001 * annual_cycles)) / (
            np.log10(extreme_working_level) - np.log10(design_working_level)
        )  # steepness
        log_a = np.log10(annual_cycles) + m * np.log10(
            design_working_level
        )  # Initial value
        first_sect = [m, log_a]
        # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        # Life of the tension ranges within the first section of the fatigue curves
        N_life = 10 ** (
            first_sect[1] - first_sect[0] * np.log10(np.abs(np.array(self.tao)))
        )

        # **Manual** Assigment of ONE cycle life to those tension ranges higher than the tension range corresponding
        # to 1000cycles (low cycle region, out of the scope of this work)
        sigma_max = 10 ** ((3 - first_sect[1]) * (-1 / first_sect[0]))
        N_life[self.tao > sigma_max] = 1

        # Admissible number of cycles for the stress ranges at the shaft
        self.N = N_life

    def env_props(self):
        """
        The non-dimensional mass is obtained here from the Gen_mass_f in the database
        
        :return: none
        """
        # Unitary mass
        self.Elect_mass_unit = 10 / 1000  # 100kg/kW

    def cost_props(self):
        """
        Update of the Elect_cost_unit per environmental condition
        
        :return: none
        """

        self.Elect_cost_unit = 100 / 1000  # 100€/kW
