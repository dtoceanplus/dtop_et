# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import numpy as np
import pandas as pd
from scipy.optimize import curve_fit, minimize
import os
from scipy.stats import ncx2, foldnorm, rayleigh
import matplotlib.pyplot as plt
from dtop_energytransf.utils import get_project_root


class Control:
    """Summary: Class representing Control

    The control object provides the Probability Density Functions of Power Levels, Load Levels and Load ranges
    for specific control strategies.

    Args:
        tech (str): Technology wave or tidal
        control_type (str): Control can be Passive for cmpx 1 and 2 or User defined for cmpx 3 of Mechanical stage
        n_sigma (int): from a normal distribution
        bins (int): Sea State resolution. (Number of points to define the Sea State)
        adim_vel_mean (list): Vector of the non-dimensional speeds, with respect to the std. Commonly 0 to 5·std is enough
        catalogue (dict): Control parameters coming from the catalogue

    Attributes:
        type (str): Passive or User_defined
        tech (str): Technology wave or tidal
        n_sigma (int): from a normal distribution
        bins (int): Sea State resolution. (Number of points to define the Sea State)
        adim_vel_mean (list): Vector of the non-dimensional speeds, with respect to the std. Commonly 0 to 5·std is enough
        adim_vel (list): calculated Non dimensional velocity
        pdf_P (list): Power levels 
        prob_P (list):
        pdf_T (list):
        prob_T (list):
        pdf_T_range (list):
        prob_T_range (list):
        catalogue (dict): Control parameters from the catalogue   

    """

    def __init__(self, tech, control_type, n_sigma, bins, adim_vel_mean, catalogue):


        # --------------------------------------------------------------------------------------------------------------
        # Data initialization for all variables of the class stored in 'self'
        # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        # Rules:
        #     - A function called as the control_type is executed to obtain the corresponding PDFs
        #     - In the user defined control the three PDFs must be set along with a reference non-dimensional speed
        # +++++++++++++++++++++++++++++++++++++++++++++++++++++++
        # Initialisation data
        self.type = control_type.replace(' ', '_')
        self.tech = tech
        self.n_sigma = n_sigma
        self.bins = bins
        self.adim_vel_mean = adim_vel_mean
        self.adim_vel = []
        self.pdf_P = []
        self.prob_P = []
        self.pdf_T = []
        self.prob_T = []
        self.pdf_T_range = []
        self.prob_T_range = []
        self.catalogue = catalogue
        
        if self.type == "Passive":
            if hasattr(self, self.type):
                self.Passive()
        elif self.type == "User_defined":
            if hasattr(self, self.type):
                self.User_defined()

    def Passive(self):
        """
        Power levels, load levels and load ranges are provided assuming a passive control is applied. It is the
        most common control strategy and represents a fully linear or linearized model
        
        :return: none
        """
        self.adim_vel = np.arange(
            0.01, self.n_sigma + self.adim_vel_mean, self.n_sigma / self.bins
        )
        # Power levels
        self.pdf_P = ncx2.pdf(
            self.adim_vel ** 2, 1, self.adim_vel_mean ** 2
        )  # noncentral chi square, 1dof and mean vel.
        # self.prob_P = self.pdf_P[0:-1] * np.diff(self.adim_vel ** 2)

        mean_value = (self.pdf_P[1:] + self.pdf_P[:-1]) / 2
        self.prob_P = mean_value * np.diff(self.adim_vel ** 2)

        # Load levels
        if self.tech == "Tidal":
            self.pdf_T = self.pdf_P
            self.prob_T = self.prob_P
        elif self.tech == "Wave":
            self.pdf_T = foldnorm.pdf(
                self.adim_vel, self.adim_vel_mean
            )  # Folded normal distribution
            mean_value_T = (self.pdf_T[1:] + self.pdf_T[:-1]) / 2
            self.prob_T = mean_value_T * np.diff(self.adim_vel)
            # self.prob_T = self.pdf_T[0:-1] * np.diff(self.adim_vel)

        # Load ranges
        self.pdf_T_range = rayleigh.pdf(self.adim_vel)  # Rayleigh distribution
        mean_value_T_range = (self.pdf_T_range[1:] + self.pdf_T_range[:-1]) / 2
        self.prob_T_range = mean_value_T_range * np.diff(self.adim_vel)

        # self.prob_T_range = self.pdf_T_range[0:-1] * np.diff(self.adim_vel)

    def User_defined(self):
        """
        Power levels, load levels and load ranges are provided assuming an advanced user loaded its PDFs in control catalogue
        
        :return:
            none
        """

        self.adim_vel = np.arange(
            0.01, self.n_sigma + self.adim_vel_mean, self.n_sigma / self.bins
        )
        #  -------------------------------------------------------------------------------------------------------------
        # Read non-dimensional turbine data from the DataBase
        # root = get_project_root()
        # folder = str(root) + "/business/DB/json_data/"
        # # path = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '\\DB\\json_data\\'
        # User_Control = pd.read_json(
        #     folder + "Control" + ".json", orient="records", lines=True
        # )
        #  -------------------------------------------------------------------------------------------------------------
        user_cat = self.catalogue
        adim_vel_ref = np.array(user_cat["adim_vel"])

        # Power levels
        pdf_P_ref = user_cat["Power_levels"]
        self.pdf_P = np.interp(self.adim_vel ** 2, adim_vel_ref ** 2, pdf_P_ref)
        mean_value = (self.pdf_P[1:] + self.pdf_P[:-1]) / 2
        self.prob_P = mean_value * np.diff(self.adim_vel ** 2)
        # self.prob_P = self.pdf_P[0:-1] * np.diff(self.adim_vel ** 2)

        # Load levels
        pdf_T_ref = user_cat["Load_levels"]
        self.pdf_T = np.interp(self.adim_vel, adim_vel_ref, pdf_T_ref)
        mean_value_T = (self.pdf_T[1:] + self.pdf_T[:-1]) / 2
        self.prob_T = mean_value_T * np.diff(self.adim_vel)
        # self.prob_T = self.pdf_T[0:-1] * np.diff(self.adim_vel)

        # Load ranges
        pdf_T_range_ref = user_cat["Load_ranges"]
        self.pdf_T_range = np.interp(self.adim_vel, adim_vel_ref, pdf_T_range_ref)
        # self.prob_T_range = self.pdf_T_range[0:-1] * np.diff(self.adim_vel)
        mean_value_T_range = (self.pdf_T_range[1:] + self.pdf_T_range[:-1]) / 2
        self.prob_T_range = mean_value_T_range * np.diff(self.adim_vel)
