# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os  # JLM
from scipy.stats import chi2
from scipy.optimize import curve_fit
from dtop_energytransf.utils import get_project_root


class Elect2Grid:
    """Parent class to force all classes compute the required metrics. It representes de power converter

    Parent class for the sub-class to compute the Electrical Energy object to Grid conditioned energy. It provides all sub-classes
    with functions to compute what the four assessments are expecting at each environmental state. The specified functions are:

    - performance()
    
    - reliability()
    
    - env_impact()
    
    - cost()
    
    The initialisation is to be done at sub-class level (child), and all the required properties are also defined at
    sub-class level so that the definitive parameters/metrics can be evaluated with the functions provided
    by this class (parent).

    """
    # Generic methods that all kinds of Elect2Grid objects should be able to compute to
    # feed the assessments
    def performance(self, P_gen, f_gen, I_gen, V_gen, cosphi_gen, Elect_P_mean):
        """Method to compute the performance subject to specific environmental conditions

        Function to compute the performance of a child class subject to specific environmental conditions,
        represented in the torque and power probabilities

        Args:
            P_gen (list): Working power levels coming from the generator
            f_gen (list): Working frequency levels coming from the generator
            I_gen (list): Working current levels coming from the generator
            V_gen (list): Working voltage levels coming from the generator
            cosphi_gen (list): generator cos phi
            Elect_P_mean (list): Output electrical power multiplied by probability distribution of the power levels

        Attributes:
            Grid_P_mean (list): Output grid conditioning active power multiplied by probability distribution of the power levels
            Grid_P_mean_r (list): Output grid conditioning reactive power multiplied by probability distribution of the power levels
            Grid_eff_mean (float): Mean grid conditioning efficiency value

        Returns:
            none    
        """        
        

        # If the efficiency needs to be updated it is done with the function in the child
        if hasattr(self, "eff"):
            self.eff(P_gen, f_gen, I_gen, V_gen, cosphi_gen)

        EP_power = np.zeros([len(Elect_P_mean)])
        for cP in range(0, len(Elect_P_mean)):
            EP_power[cP] = self.pe_eff[cP] * Elect_P_mean[cP]
        self.Grid_P_mean = EP_power  # Active power
        FI = np.arccos(self.Grid_cosfi)
        self.Grid_P_mean_r = self.Grid_P_mean * np.tan(FI)  # Reactive power
        # Negative current/power/efficiencies are made zero to avoid weird results
        self.Grid_P_mean[self.Grid_P_mean < 0] = 0
        if np.sum(Elect_P_mean) == 0:
            self.Grid_eff_mean = 0 * sum(Elect_P_mean)
        else:
            self.Grid_eff_mean = sum(self.Grid_P_mean) / sum(Elect_P_mean)

        # print('Grid_eff: ' + str(self.Grid_eff_mean))

    def reliability(self, P_levels, P_avg, load_prob, EC_T, Tz):
        """Method to compute the damage subject to a specific environmental condition

        Function to compute the damage of the load ranges on the child object during all working hours of the year
        of the corresponding environmental condition

        Args:
            P_levels (list): Power distribution. Available power levels
            P_avg (float): Average input power
            load_prob (list): Probability distribution of load ranges
            EC_T (int): Annual equivalent time of the corresponding environmental condition [s]
            Tz (float): Average period of the force ranges

        Attributes:
            Grid_damage (float): Damage of the grid conditioning object during the total time 'EC_T'

        Returns:
            none    

        """        
        
        # ********************************************************************************************************
        # Necessary to be DEFINED in the child:
        #  - self.N - 1D array
        # Necessary to be computed here:
        #  - self.Grid_damage - float
        # ********************************************************************************************************

        # If the child needs the life to be updated per EC it is done here
        if hasattr(self, "reliability_props"):
            self.reliability_props(P_levels, P_avg)

        cycles = EC_T / Tz  # Total number of cycles in the Environmental Condition
        T_cycles = load_prob * cycles  # Actual number of cycles of each range
        self.Grid_damage = np.sum(
            T_cycles / self.N
        )  # Damage during the EC total time 'EC_T'

        # print('Grid_damage: ' + str(self.Grid_damage))

    def env_impact(self):
        """Method to compute the environmental impact (mass) of the sub-class

        Function to calculate the total mass of the child object. It is based on a non-dimensional mass precomputed
        in the child where: total_mass=non_dim_mass*Pnom^2
        
        Attributes:
            Grid_mass (float): Mass of the grid conditioning object in [kg]

        :return: The method doesn't return values, it stores the results in the corresponding variables
        """        
        
        # ********************************************************************************************************
        # Necessary to be DEFINED in the child:
        #  - self.P_nom - float
        #  - self.PowerConverter_mass_unit - float
        # Necessary to be computed here:
        #  - self.Grid_mass - float - linear function
        # ********************************************************************************************************

        # If the child needs the PowerConverter_mass_unit to be updated per EC it is done here
        if hasattr(self, "env_props"):
            self.env_props()

        self.Grid_mass = round(self.PowerConverter_mass_unit * self.P_nom, 2)

        # print('Grid_mass: ' + str(self.Grid_mass))

    def cost(self):
        """Method to compute the cost of the subclass

        Function to calculate the total cost of the child object. It is based on a cost function pre-loaded
        in the child.

        Attributes:
           Grid_cost (float): Cost of the grid conditioning object in €

        :return: The method doesn't return values, it stores the results in the corresponding variables           
        """
        
        # ********************************************************************************************************
        # Necessary to be DEFINED in the child:
        #  - self.P_nom - float
        #  - self.B2Bcost - float
        # Necessary to be computed here:
        #  - self.Grid_cost - float - linear function
        # ********************************************************************************************************

        # If the child needs the B2Bcost_unit to be updated per EC it is done here
        if hasattr(self, "cost_props"):
            self.cost_props()

        self.Grid_cost = round(self.P_nom * self.B2Bcost_unit, 2)

        # print('Grid_cost: ' + str(self.Grid_cost))


class B2B2level(Elect2Grid):
    """
    Back to back 2 level converter (B2B) object with the parameters of both the generator and the grid.

      
    :param float Pnom: Power converter nominal power [W]
    :param int Vdc: DC Link voltage [V]
    :param int fsw: Converter's switching frequency [Hz]
    :param int VnomGen: Nominal voltage of the generator [V]
    :param float fnomGen: Nominal frequency of the geerator [Hz]
    :param float Lgen: Inductance of the generator [Hr]
    :param float Rgen: Resistance of the generator [ohm]
    :param int Vgrid: Grid rms voltage [V]
    :param float Rgrid: Grid connection resistance [ohm]
    :param float Lgrid: Grid connection inductance [Hr]
    :param float cosfi: Required grid cosfi

    Attributes:
        pe_eff (list): Calculated efficiency of the power converter at each power level
        Grid_eff_mean (list): calculated efficiency of the Power Converter considering input power and probability
        Grid_P_mean (list): Active power of the power converter
        PowerConverter_temp(list): Temperature data for life assessment temp[temp[ºC],temp_rated[ºC],temp_max[ºC]]
        PowerConverter_life (list): K, k0: life asessment parameters . (N=eKT+Ko)
        N (list): Number of cycles that the converter can work at each stress range
        PowerConverter_mass_unit (list):
        P_nom (int):  Power converter nominal power [W]. Pnom is received in kW from the GUI
        B2Bcost_unit (float): unitary cost of the converter €/W
        cosphi_gen (list): generator cos phi
        power_gen (list): Working power levels coming from the generator
        V1 (list): AC Voltage at the input of the converter
        I_grid (list): AC current inyected to grid
        V2 (list): AC Voltage at the output of the converter
        Vdc (int):  Bus voltage/DC voltage [V]
        fsw (float): Switching frequency in the power converter [Hz]
        V_nomGen (int): rms nominal voltage of the generator [V]
        I_nomGen (float): rms nominal current of the gen [A]
        f_nomGen (float): Nominal frequency of the generator [Hz]
        Lgen (float): Generator inductance [Hr]
        Rgen (float): generator resistance [ohm]
        R_grid (float): Grid connection resistance [ohm]
        L_grid (float): Grid connection inductance [Hr]
        V_grid (int): Grid rms voltage [V]
        Grid_cosfi (float): Required cosfi by the grid
        Fgrid (float):Grid frequency
        Igbt_Vceo (float): on-state zero-current collector-emitter voltage of the IGBT
        Igbt_Rce (float): collector emitter on-state resistance of the IGBT
        Igbt_a (float): a, b and c are the coefficients of the curve fitting of the switching energy curve given by the manufacturer
        Igbt_b (float):
        Igbt_c (float):
        Igbt_Vnom (int): nominal voltage of the IGBT
        Diod_Vfo (float): zero-current forward voltage of the diode
        Diod_Rt (float): forward resistance of the diode
        Diod_a (float): a, b and c are the coefficients of the curve fitting of the reverse recovery energy curve given by the manufacturer
        Diod_b (float):
        Diod_c (float):
        Diod_Vnom (int): nominal voltage of the Diode        

    Returns:
        none    

        """
    def __init__(
        self, Pnom, Vdc, fsw, VnomGen, fnomGen, Lgen, Rgen, Vgrid, Rgrid, cosfi, Lgrid, Fgrid, catalogue,
    ):     

        # Variable initialization for the assessments
        # --------------------Required by performance
        self.pe_eff = []
        self.Grid_eff_mean = (
            []
        )  # calculated efficiency of the Power Converter considering input power and probability
        self.Grid_P_mean = []
        # --------------------Required by reliability
        self.PowerConverter_temp = []
        self.PowerConverter_life = []
        self.N = []
        # --------------------Required by environmental
        self.Grid_mass = []
        self.PowerConverter_mass_unit = []
        self.P_nom = Pnom * 1000 # Power converter nominal power [kW]/ Pnom is received in kW from the GUI  
        # --------------------Required by cost
        self.Grid_cost = []
        self.B2Bcost_unit = []
        # --------------------Other variables
        self.cosphi_gen = []
        self.power_gen = []
        self.V1 = []
        self.I_grid = []
        self.V2 = []
        self.Vdc = Vdc  # Bus voltage/DC voltage [V]
        self.fsw = fsw  # switching frequency in the power converter [Hz]
        self.V_nomGen = VnomGen  # rms nominal voltage of the generator [V]
        self.I_nomGen = Pnom / (
            np.sqrt(3) * VnomGen
        )  # rms nominal current of the gen [A]. #TODO: cosphi is user input in performance, needed to compute Inomgen
        self.f_nomGen = fnomGen  # Nominal frequency of the generator [Hz]
        self.Lgen = Lgen  # Generator inductance [Hr]
        self.Rgen = Rgen  # generator resistance [ohm]
        self.R_grid = Rgrid
        self.L_grid = Lgrid
        self.V_grid = Vgrid  # Grid rms voltage [V]
        # is a escalar value
        self.Grid_cosfi = cosfi
        self.Fgrid = Fgrid

        # Read data from DB
        # ********************************
        # path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))+'\\DB\\json_data\\'
        # Read data from the DB useful to compute losses in the Converter (Performance)
        # root = get_project_root()
        # folder = str(root) + "/business/DB/json_data/"
        #PE_DB = pd.read_json(
        #    folder + "PowerConverter.json", orient="records", lines=True
        #)
        # Read data for reliability of the Power Converter
        PE_DB = catalogue
        self.PowerConverter_temp = PE_DB["temp"]
        self.PowerConverter_life = PE_DB["life"]
        # Read data of power converter costs
        self.B2Bcost_unit = PE_DB["cost"]
        # Read data of power converter mass
        self.PowerConverter_mass_unit = sum(PE_DB["mass"].values())

        # select IGBT module depending on the nominal peak current at the input
        if self.I_nomGen * np.sqrt(2) < 75:
            self.Igbt_Vceo = PE_DB["IGBT150"][0]  # on-state zero-current collector-emitter voltage of the IGBT
            self.Igbt_Rce = PE_DB["IGBT150"][1] # collector emitter on-state resistance of the IGBT
            self.Igbt_a = PE_DB["IGBT150"][2] # a, b and c are the coefficients of the curve fitting of the
            # switching energy curve given by the manufacturer
            self.Igbt_b = PE_DB["IGBT150"][3]
            self.Igbt_c = PE_DB["IGBT150"][4]
            self.Igbt_Vnom = PE_DB["IGBT150"][5]  # nominal voltage of the IGBT
            self.Diod_Vfo = PE_DB["Diode150"][0] # zero-current forward voltage of the diode
            self.Diod_Rt = PE_DB["Diode150"][1]  # forward resistance
            self.Diod_a = PE_DB["Diode150"][2] # a, b and c are the coefficients of the curve fitting of the
            # reverse recovery energy curve given by the manufacturer
            self.Diod_b = PE_DB["Diode150"][3]
            self.Diod_c = PE_DB["Diode150"][4]
            self.Diod_Vnom = PE_DB["Diode150"][5]  # nominal voltage of the Diode
        elif self.I_nomGen * np.sqrt(2) < 225:
            self.Igbt_Vceo = PE_DB["IGBT450"][0]
            self.Igbt_Rce = PE_DB["IGBT450"][1]
            self.Igbt_a = PE_DB["IGBT450"][2]
            self.Igbt_b = PE_DB["IGBT450"][3]
            self.Igbt_c = PE_DB["IGBT450"][4]
            self.Igbt_Vnom = PE_DB["IGBT450"][5]
            self.Diod_Vfo = PE_DB["Diode450"][0]
            self.Diod_Rt = PE_DB["Diode450"][1]
            self.Diod_a = PE_DB["Diode450"][2]
            self.Diod_b = PE_DB["Diode450"][3]
            self.Diod_c = PE_DB["Diode450"][4]
            self.Diod_Vnom = PE_DB["Diode450"][5]
        elif self.I_nomGen * np.sqrt(2) < 400:
            self.Igbt_Vceo = PE_DB["IGBT800"][0]
            self.Igbt_Rce = PE_DB["IGBT800"][1]
            self.Igbt_a = PE_DB["IGBT800"][2]
            self.Igbt_b = PE_DB["IGBT800"][3]
            self.Igbt_c = PE_DB["IGBT800"][4]
            self.Igbt_Vnom = PE_DB["IGBT800"][5]
            self.Diod_Vfo = PE_DB["Diode800"][0]
            self.Diod_Rt = PE_DB["Diode800"][1]
            self.Diod_a = PE_DB["Diode800"][2]
            self.Diod_b = PE_DB["Diode800"][3]
            self.Diod_c = PE_DB["Diode800"][4]
            self.Diod_Vnom = PE_DB["Diode800"][5]
        else: #self.I_nomGen * np.sqrt(2) < 800: as we don't  have data for bigger igbts we use the bigger ones for the rest of cases
            self.Igbt_Vceo = PE_DB["IGBT1600"][0]
            self.Igbt_Rce = PE_DB["IGBT1600"][1]
            self.Igbt_a = PE_DB["IGBT1600"][2]
            self.Igbt_b = PE_DB["IGBT1600"][3]
            self.Igbt_c = PE_DB["IGBT1600"][4]
            self.Igbt_Vnom = PE_DB["IGBT1600"][5]
            self.Diod_Vfo = PE_DB["Diode1600"][0]
            self.Diod_Rt = PE_DB["Diode1600"][1]
            self.Diod_a = PE_DB["Diode1600"][2]
            self.Diod_b = PE_DB["Diode1600"][3]
            self.Diod_c = PE_DB["Diode1600"][4]
            self.Diod_Vnom = PE_DB["Diode1600"][5]
        # else:
        #     print("No module found for such a high power")

    def eff(self, P_gen, f_gen, I_gen, V_gen, cosphi_gen):
        """
        Calculation of losses in the power converter -- The inputs are power LEVELs at which the Power Converter
        is expected to be working, and generator output voltages for the mean Power computation
        the parent method 'performance' is used

        :param list P_gen: Active power from the generator [W]
        :param list f_gen: Frequency from the generator [Hz]
        :param list I_gen: Input current (output from the generator) [A]
        :param list V_gen: Generator output voltage [V]
        :param list cosphi_gen: Generator output cosfi
        
        :return: none
        """

        # Data from generator--------------------------------------------------------------------------------------------
        # self.cosphi_gen = cosphi_gen  # The assignation has been moved to the end of the function
        # self.f_gen = f_gen  # It is not used outside neither inside?!?!?!
        # self.I_gen = I_gen  # It is not used outside.
        # self.V_gen = V_gen  # It is not used outside.
        # ---------------------------------------------------------------------------------------------------------------

        eff = np.zeros([len(P_gen)])
        V_1 = np.zeros([len(P_gen)])
        Pdc = np.zeros([len(P_gen)])
        Igrid = np.zeros([len(P_gen)])
        V_2 = np.zeros([len(P_gen)])
        #TODO V_gen and I_gen are rms and for these calculous should be peak value. They are L-L (checked)
        for cP_act in range(0, len(P_gen), 1):
            if P_gen[cP_act] > 0:
                # Losses of the Rectifier: 1st IGBT bridge
                # Needed data for calculating losses
                V_1[cP_act] = (V_gen[cP_act] - self.Rgen * I_gen[cP_act]) / cosphi_gen[
                    cP_act
                ]  # Voltage at the input of the converter
                m1 = (2 / np.sqrt(3)) * (
                    V_1[cP_act] / self.Vdc
                )  # converter modulation index
                # 1 IGBT Losses
                Cond_losses_igbt = 0.5 * (
                    self.Igbt_Vceo * I_gen[cP_act] / np.pi
                    + self.Igbt_Rce * I_gen[cP_act] ** 2 / 4
                ) + m1 * cosphi_gen[cP_act] * (
                    self.Igbt_Vceo * I_gen[cP_act] / 8
                    + (self.Igbt_Rce * I_gen[cP_act] ** 2) / (3 * np.pi)
                )
                Sw_losses_igbt = (
                    self.fsw
                    * (
                        self.Igbt_a / 2
                        + self.Igbt_b * I_gen[cP_act] / np.pi
                        + self.Igbt_c * I_gen[cP_act] ** 2 / 4
                    )
                    * self.Vdc
                    / self.Igbt_Vnom
                )
                # 1 Diode Losses
                Cond_losses_diode = 0.5 * (
                    self.Diod_Vfo * I_gen[cP_act] / np.pi
                    + self.Diod_Rt * I_gen[cP_act] ** 2 / 4
                ) - m1 * cosphi_gen[cP_act] * (
                    self.Diod_Vfo * I_gen[cP_act] / 8
                    + (self.Diod_Rt * I_gen[cP_act] ** 2) / (3 * np.pi)
                )
                Sw_losses_diode = (
                    self.fsw
                    * (
                        self.Diod_a / 2
                        + self.Diod_b * I_gen[cP_act] / np.pi
                        + self.Diod_c * I_gen[cP_act] ** 2 / 4
                    )
                    * self.Vdc
                    / self.Diod_Vnom
                )
                # 1 2Level bridge Losses: RECTIFIER
                Cond_losses = 6 * (Cond_losses_igbt + Cond_losses_diode)
                Sw_losses = 6 * (Sw_losses_igbt + Sw_losses_diode)
                Total_losses_rect = Cond_losses + Sw_losses

                # Losses of the Inverter, 2nd IGBT Bridge
                # Input power to Inverter: Power from generator - losses in rectifier
                Pdc[cP_act] = P_gen[cP_act] - Total_losses_rect
                Vgrid_peak = (
                    np.sqrt(2) * self.V_grid
                )  # Grid peak voltage (input is in rms)
                # AC current to grid
                Igrid[cP_act] = (
                    2
                    * (Pdc[cP_act] - Total_losses_rect)
                    / (np.sqrt(3) * Vgrid_peak * self.Grid_cosfi)
                )
                FI = np.arccos(self.Grid_cosfi)
                Grid_sinfi = np.sin(FI)
                X_aux = Vgrid_peak * self.Grid_cosfi + self.R_grid * Igrid[cP_act]
                Y_aux = (
                    2 * np.pi * self.Fgrid * self.L_grid * Igrid[cP_act]
                    + Vgrid_peak * Grid_sinfi
                )
                THETA = np.arctan(Y_aux / X_aux)
                Cos_theta = np.cos(THETA)
                V_2[cP_act] = X_aux / Cos_theta
                m2 = (2 / np.sqrt(3)) * (
                    V_2[cP_act] / self.Vdc
                )  # converter modulation index
                # 1 IGBT Losses
                Cond_losses_igbt = 0.5 * (
                    self.Igbt_Vceo * Igrid[cP_act] / np.pi
                    + self.Igbt_Rce * Igrid[cP_act] ** 2 / 4
                ) + m2 * Cos_theta * (
                    self.Igbt_Vceo * Igrid[cP_act] / 8
                    + (self.Igbt_Rce * Igrid[cP_act] ** 2) / (3 * np.pi)
                )
                Sw_losses_igbt = (
                    self.fsw
                    * (
                        self.Igbt_a / 2
                        + self.Igbt_b * Igrid[cP_act] / np.pi
                        + self.Igbt_c * Igrid[cP_act] ** 2 / 4
                    )
                    * self.Vdc
                    / self.Igbt_Vnom
                )
                # 1 Diode Losses
                Cond_losses_diode = 0.5 * (
                    self.Diod_Vfo * Igrid[cP_act] / np.pi
                    + self.Diod_Rt * Igrid[cP_act] ** 2 / 4
                ) - m2 * Cos_theta * (
                    self.Diod_Vfo * Igrid[cP_act] / 8
                    + (self.Diod_Rt * Igrid[cP_act] ** 2) / (3 * np.pi)
                )
                Sw_losses_diode = (
                    self.fsw
                    * (
                        self.Diod_a / 2
                        + self.Diod_b * Igrid[cP_act] / np.pi
                        + self.Diod_c * Igrid[cP_act] ** 2 / 4
                    )
                    * self.Vdc
                    / self.Diod_Vnom
                )
                # 1 2Level bridge Losses: INVERTER
                Cond_losses = 6 * (Cond_losses_igbt + Cond_losses_diode)
                Sw_losses = 6 * (Sw_losses_igbt + Sw_losses_diode)
                Filter_losses = (3 / 2) * self.R_grid * Igrid[cP_act] * Igrid[cP_act]
                Total_losses_inv = Cond_losses + Sw_losses + Filter_losses
                # if needed, output electrical power for every input generator electrical power
                # P_ep[cP_act] = P_gen[cP_act] - Total_losses_rect - Total_losses_inv
                eff[cP_act] = (
                    P_gen[cP_act] - Total_losses_rect - Total_losses_inv
                ) / P_gen[cP_act]

        # The negative efficiencies are converted to 0. It is reasonable since it is passing by zero power
        # values and already turned on.
        eff[eff < 0] = 0
        self.pe_eff = eff
        self.power_gen = P_gen
        # peak values
        self.V1 = V_1
        self.I_grid = Igrid
        self.V2 = V_2
        # The cosphi of the generator is stored in the object to be used outside
        self.cosphi_gen = cosphi_gen  # sobra ERS

    def reliability_props(self, AP_vector, P_avg):
        """This method computes the admissible number of cycles at each environmental state

        Args:
            AP_vector (list): Input power levels
            P_avg (float): Average input power

        :return: The method doesn't return values, it stores the results in the corresponding variables    
        """        
        
        B2B_life_B10 = np.zeros([len(AP_vector)])

        for conta in range(
            0, len(AP_vector), 1
        ):  # self.PowerConverter_P.columns: #range(0, len(P_gen), 1):
            if AP_vector[conta] == 0:
                AT = 1
            else:
                P1 = P_avg + AP_vector[conta] / 2  # 30000P+AP/2
                P2 = P_avg - AP_vector[conta] / 2  # 40000#P-AP/2
                Temp_init = (
                    (self.PowerConverter_temp[2] - self.PowerConverter_temp[1])
                    / (self.P_nom - self.PowerConverter_temp[0])
                ) * P1 + self.PowerConverter_temp[2]
                Temp_final = (
                    (self.PowerConverter_temp[2] - self.PowerConverter_temp[1])
                    / (self.P_nom - self.PowerConverter_temp[0])
                ) * P2 + self.PowerConverter_temp[2]
                # AT = abs(Temp_final - Temp_init)
                AT = max(85,abs(Temp_final - Temp_init)) # if I have understood well form D5.4, the maximum difference in temperature is 85 degrees Celsius.
            # 1 Joins coductors
            B2B_life_B10[conta] = (
                self.PowerConverter_life[0] * AT ** self.PowerConverter_life[1]
            )
        self.N = B2B_life_B10


class Grid_Simplified(Elect2Grid):
    """
    Simplied converter object .

    :param float Pnom: Power converter nominal power [W]
    :param int Vgrid: Grid rms voltage [V]

    Attributes:
        pe_eff (list): Calculated efficiency of the power converter at each power level
        Grid_eff_mean (list): calculated efficiency of the Power Converter considering input power and probability
        Grid_P_mean (list): Active power of the power converter
        V_grid (int): Grid rms voltage [V]
        N (list): Number of cycles that the converter can work at each stress range
        PowerConverter_mass_unit (float): Fixed in the simplified converter (10 / 1000)
        P_nom (int):  Power converter nominal power [W]. Pnom is received in kW from the GUI
        B2Bcost_unit (float): unitary cost of the converter €/W (100 / 1000  --- 100€/kW)
        Grid_cosfi (int): Required grid cos phi assumed 1 in the simplified
        power_loads_norm (list): array with normalized power loads [0..1] [pu] (X-axis of the efficiency curve)
        eff_levels (list): normalized efficiency levels (Y-axis of the efficiency curve)
        perf_f (list): Array with efficiency values corresponding to each power load value [0..1] [pu]

    Returns:
        none
     """    

    def __init__(self, Pnom, V_grid):
        """
                Initialisation of the B2B object with the parameters of both the generator and the grid.
                Description of the input parameters is pending!?!?!
                :param Pnom:
                """

        # Variable initialization for the assessments
        # --------------------Required by performance
        self.V_grid = V_grid
        self.pe_eff = []
        self.Grid_eff_mean = (
            []
        )  # calculated efficiency of the Power Converter considering input power and probability
        self.Grid_P_mean = []
        # --------------------Required by reliability
        self.N = []
        # --------------------Required by environmental
        self.Grid_mass = []
        self.PowerConverter_mass_unit = []
        self.P_nom = Pnom *1000 # Power converter nominal power [kW]
        # --------------------Required by cost
        self.Grid_cost = []
        self.B2Bcost_unit = []
        # --------------------Other variables
        self.Grid_cosfi = 1  # Assumed in the simplified

        # array with normalized power loads [0..1] [pu]
        self.power_loads_norm = [0.1, 0.2, 0.3, 0.5, 0.8, 1]
        self.eff_levels = [0.97, 0.975, 0.98, 0.982, 0.981, 0.978]

        # efficiency curve for efficiency calculation
        def eff_fit(x, k1, k2, k3, k4):
            y = 1 - k1 * np.exp(k2 * x) - k3 * np.exp(k4 * x)
            return y

        # Array with efficiency values corresponding to each power load value [0..1] [pu]
        a_b2b, b_b2b = curve_fit(
            eff_fit,
            np.array(self.power_loads_norm),
            np.array(self.eff_levels),
            bounds=([-10.0, -10.0, -10.0, -10.0], [10.0, 0.0, 10.0, 0.0]),
        )
        self.perf_f = a_b2b  # k1 == a[0] ; k2 == a[1] in the eff_fit function
        self.eff_fit = (
            eff_fit  # The function is stored to be used in the perf_props function
        )

    def eff(self, P_gen, f_gen, I_gen, V_gen, cosphi_gen):

        """
        Calculation of the efficiency from an efficiency curve. The inputs are power LEVELs at which the Power Converter is expected to
        be working, for the mean Power computation the parent method 'performance' is used

        :param P_gen: Actual Input power
        :param cosphi_gen: Not used in the simplified
        :param f_gen: Not used in the simplified
        :param I_gen: Not used in the simplified
        :param V_gen: Not used in the simplified
        :return: none
        """

        eff = self.eff_fit(
            P_gen / self.P_nom, *self.perf_f
        )  # Changed the interpolation by the
        # evaluation with the fitted function
        # The negative efficiencies are converted to 0. It is reasonable since it is passing by zero power
        # values and already turned on.
        eff[eff < 0] = 0
        eff[
            eff > 1
        ] = 1  # Careful with the analytical fitting that may provide higher values
        self.pe_eff = eff
        self.pe_eff[P_gen > self.P_nom*5] = 0  # For peaks bigger than 5 times nominal power it stops

    def reliability_props(self, AP_vector, P_avg):
        """
        Summary: This method computes the admissible number of cycles at each environmental state. It assumes that there
        is a failure rate of 0.1 at the input power level and is linearly increased up to 1 for 10·input_power
        
        Args:
            AP_vector (list): Input power levels
            P_avg (float): Average input power. Not used in this method

        :return: The method doesn't return values, it stores the results in the corresponding variables    
        """

        self.tao = AP_vector  # [W] --> equivalent power level

        # **Manual** correction for null torques, set to 0.1[MPa], which almost doesn't consume any life
        self.tao[self.tao < 1] = 1

        # Obtain the corresponding fatigue life curves from the DB for the two sections
        annual_cycles = 365.25 * 24 * 3600 / 7  # Assuming a mean period of 7s
        design_working_level = 1 * self.P_nom # 0.5 * self.P_nom  # -> It lasts the annual cycles
        extreme_working_level = 10* self.P_nom # 10 * self.P_nom  # -> It lasts 0.1% the annual cycles
        m = (np.log10(annual_cycles) - np.log10(0.001 * annual_cycles)) / (
            np.log10(extreme_working_level) - np.log10(design_working_level)
        )  # steepness
        log_a = np.log10(annual_cycles) + m * np.log10(
            design_working_level
        )  # Initial value
        first_sect = [m, log_a]
        # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        # Life of the tension ranges within the first section of the fatigue curves
        N_life = 10 ** (
            first_sect[1] - first_sect[0] * np.log10(np.abs(np.array(self.tao)))
        )

        # **Manual** Assigment of ONE cycle life to those tension ranges higher than the tension range corresponding
        # to 1000cycles (low cycle region, out of the scope of this work)
        sigma_max = 10 ** ((3 - first_sect[1]) * (-1 / first_sect[0]))
        N_life[self.tao > sigma_max] = 1

        # Admissible number of cycles for the stress ranges at the shaft
        self.N = N_life

    def env_props(self):
        """
        The non-dimensional mass is obtained for the simplified converter

        :return: none
        """
        # Unitary mass
        self.PowerConverter_mass_unit = 10 / 1000  # 100kg/W

    def cost_props(self):
        """
        Update of the B2Bcost_unit 

        :return: none
        """

        self.B2Bcost_unit = 100 / 1000  # 100€/kW
