# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import copy
import types

import pandas as pd

import os
import glob
from dtop_energytransf.utils import get_project_root

from .cross_module_data import *
from .preprocessor import Preprocessor
from dtop_energytransf.business.Array import Array
from dtop_energytransf.business.DB import input_json_gen as json_gen
from dtop_energytransf.storage.models.models import EnergyTransformationStudy
from dtop_energytransf.business.output_management import NpEncoder



class RunAnalysis:
    """
    Main class for running the analysis mode of ET module.

    The class functions as follows

    *  loads the cross module data,  

    *  loads the user input data from the DB,

    *  pre-processes the data,

    *  initialises the Array class with the pre-processed inputs,

    *  evaluates the array performance using the Array class.

    Note that there are two optional steps that are currently commented and should only be used for testing.
    The first is to generate the intermediate JSON data, which is unnecessary for CPX 1 but may be useful for other
    complexity levels.
    The second is to write the JSON outputs, as was done in main_thread.py.
    Note that neither of these should be performed in the final version of the module.
    The database should be used for all persistence.
    
    Args:
        et_id (int): Identifier ID of the Energy Transformation study

    Attributes:
        study (inst): Instance of the Energy Transformation Class
        et_id (int): Identifier ID of the Energy Transformation study
        array_results (dict): Array results after running the analysis
        device_results (list): Device results after running the analysis
        pto_results (list): PTO results after running the analysis
        _representation_data (dict): Diccionary with the representation data
        additional_properties (list): List with the additional properties
        _ec_data (inst): instance of EnergyCaptureData. all the parameters processed from Energy Capture module for the study
        _mc_data (inst): instance of MachineCharacterisationData. all the parameters processed from Machine Characterisation module for the study
        _sc_data (inst): instance of SiteCharacterisationData. all the parameters processed from Site Characterisation module for the study
        _general_inputs (inst): instance of GeneralInputs. General input parameters from the GUI
        _mechanical_inputs (inst): instance of MechanicalInputs. Mechanical inputs from the GUI
        _electrical_inputs (inst): instance of ElectricalInputs. Electrical inputs from the GUI
        _grid_inputs (inst): instance of GridInputs. Grid inputs from the GUI        
        _control_inputs (inst): instance of ControlInputs. Control inputs from the GUI
        _preprocessor (inst): instance of the Preprocessor Class
        _array (class): Array Class

    Returns: 
        none    

    """

    def __init__(self, et_id):
    
        # Initialise result fields
        self.study = EnergyTransformationStudy.query.get(et_id)
        self.et_id = et_id
        self.array_results = {}
        self.device_results = []
        self.pto_results = []
        self._representation_data = {}
        self.additional_properties = []

        # Load from DB cross module data, user inputs and catalogue inputs
        self._ec_data = self.study.energy_capture_data
        self._mc_data = self.study.machine_characterisation_data    
        self._sc_data = self.study.site_characterisation_data
        self._general_inputs = self.study.general_inputs
        self._mechanical_inputs = self.study.mechanical_inputs
        self._electrical_inputs = self.study.electrical_inputs
        self._grid_inputs = self.study.grid_inputs
        self._control_inputs = self.study.control_inputs 
        # Pre-process data
        self._preprocessor = None
        self._pre_process_data()
        # Generate JSON  # temporary writing of JSON files. Not needed
        # self._generate_json()  
        # Initialise array class
        self._array = None
        self._initialise_array()
        # Generate performance inputs and evaluate array performance
        self._evaluate_array_performance()
        # TODO Format results and (TEMPORARY) write analysis results to JSON
        self._format_results()
        # self._write_analysis_results_to_json()  # TODO: REMOVE; temporary method for testing

    def _pre_process_data(self):
        """
        Preprocess the input data to ET, to be used by the Array class; the main entry-point to the business logic
        
        """        
                
        pre_process_inputs = (
            self._ec_data,
            self._mc_data,
            self._sc_data,
            self._general_inputs,
            self._mechanical_inputs,
            self._electrical_inputs,
            self._grid_inputs,
            self._control_inputs,
        )
        self._preprocessor = Preprocessor(*pre_process_inputs)
        self._preprocessor.preprocess()
        #print("Processing complete")

    def _generate_json(self):
        """Optional method for generating intermediate JSON files"""
        json_gen.input_json_gen(
            self._electrical_inputs.complexity_level,
            self._grid_inputs.complexity_level,
            self._mechanical_inputs.complexity_level,
            "Mech_Simplified",
            "None",
            "Passive"
        )
        #print("JSON generated")

    def _initialise_array(self):
        """Initialise the Array class with the pre-processes inputs"""
        array_class_inputs = self._preprocessor.array_class_inputs
        self._array = Array(*array_class_inputs)
        # print("Array initialised")
        
    def _evaluate_array_performance(self):
        """Evaluate the performance of the array using the array performance inputs and the array class"""
        self._preprocessor.generate_performance_inputs()
        array_perf_inputs = self._preprocessor.array_perf_inputs
        self._array.performance(*array_perf_inputs, self.et_id)
        # print("Array performance evaluated")

    def _format_results(self):
        """
        Loop through the results and extract the array, device and pto data.
        Digital representation formatting
        """
        array_copy = copy.deepcopy(self._array)
        i = 0
        for k in array_copy.Devices:
            self.device_results.append(array_copy.Devices[k]["Device"].__dict__)
            self.additional_properties.append([])
            self.pto_results.append([])
            for k_PTO in array_copy.Devices[k]["Device"].PTOs:
                self.additional_properties[i].append(
                    array_copy.Devices[k]["Device"].PTOs[k_PTO].pop("PTO").__dict__
                )
                self.pto_results[i].append(array_copy.Devices[k]["Device"].PTOs[k_PTO])
            self.device_results[i].pop("PTOs")
            i = i + 1
        
        del array_copy.Devices
        self.array_results = array_copy

        ###DIGITAL REPRESENTATION FORMATTING

        array_json = json.loads(json.dumps(self.array_results.__dict__, sort_keys=True, cls=NpEncoder))
        device_json = json.loads(json.dumps(self.device_results, sort_keys=True, cls=NpEncoder))
        pto_json = json.loads(json.dumps(self.pto_results, sort_keys=True, cls=NpEncoder))

        array_dict = copy.deepcopy(array_json)

        array = [
            {
                "id": "1",
                "properties": {
                    "array_mass": array_dict["Array_mass"],
                    "array_cost": array_dict["Array_cost"]
                }, 
                "assessment": {
                    "array_annual_mean_mechanical_power": array_dict["Array_P_Mech"],
                    "array_annual_mean_electrical_power": array_dict["Array_P_Elect"],
                    "array_annual_mean_grid_conditioning_active_power": array_dict["Array_Pa_Grid"],
                    "array_annual_mean_grid_conditioning_reactive_power": array_dict["Array_Pr_Grid"],
                    "array_annual_transformed_mechanical_energy": array_dict["Array_E_Mech"],
                    "array_annual_transformed_electrical_energy": array_dict["Array_E_Elect"],
                    "array_annual_grid_conditioning_energy": array_dict["Array_E_Grid"],
                }             
            }
        ]

        array[0]["id"] = "1"

        for k in array[0]["properties"]:
            array[0]["properties"][k]["origin"] = "ET"
            array[0]["properties"][k].pop("label")
            
        for k in array[0]["assessment"]:
            array[0]["assessment"][k]["origin"] = "ET"
            array[0]["assessment"][k].pop("label")
     
        device_list = copy.deepcopy(device_json)
        number_device = len(device_list)
        pto_list = copy.deepcopy(pto_json)
        # print(pto_list)
        number_of_ptos_per_device = len(pto_list[0])

        print (number_device)
        print(number_of_ptos_per_device)
        # print(pto_list[0])

        device = []
        pto = []
        mechanical_conversion = []
        electrical_conversion = []
        grid_conditionining = []

        for i in range(number_device):
            device.append({})
            device[i]["id"] = "1_" + str(i + 1)
            device[i]["properties"] = {}
            device[i]["properties"]["number_of_ptos_per_device"]  = copy.deepcopy(device_list[i]["Dev_par_PTOs"])
            device[i]["properties"]["cut_in_out"] = copy.deepcopy(device_list[i]["Dev_cut_in_out"])
            device[i]["assessment"] = {}
            device[i]["assessment"]["device_annual_transformed_mechanical_power"] = copy.deepcopy(device_list[i]["Dev_P_Mech"])
            device[i]["assessment"]["device_annual_transformed_electrical_power"] = copy.deepcopy(device_list[i]["Dev_P_Elect"])
            device[i]["assessment"]["device_annual_grid_conditioning_active_power"] = copy.deepcopy(device_list[i]["Dev_Pa_Grid"])
            device[i]["assessment"]["device_annual_grid_conditioning_reactive_power"] = copy.deepcopy(device_list[i]["Dev_Pr_Grid"])
            device[i]["assessment"]["device_annual_mechanical_transformed_energy"] = copy.deepcopy(device_list[i]["Dev_E_Mech"])
            device[i]["assessment"]["device_annual_electrical_transformed_energy"] = copy.deepcopy(device_list[i]["Dev_E_Elect"])              
            for k in range(number_of_ptos_per_device):
                pto.append({})
                pto[i*number_of_ptos_per_device+k]["id"] = "1_" + str(i + 1) + "_" + str(k + 1)
                pto[i*number_of_ptos_per_device+k]["assessment"] = {}
                # pto[i*number_of_ptos_per_device+k]["assessment"]["stress_load"] = copy.deepcopy(pto_list[i][k]["Stress_Loads"])
                # pto[i*number_of_ptos_per_device+k]["assessment"]["stress_load_range"] = copy.deepcopy(pto_list[i][k]["Stress_Load_Range"])
                # pto[i*number_of_ptos_per_device+k]["assessment"]["s_n"] = copy.deepcopy(pto_list[i][k]["S_N"])
                mechanical_conversion.append({})
                mechanical_conversion[i*number_of_ptos_per_device+k]["id"] = "1_" + str(i + 1) + "_" + str(k + 1) + "_" + "1"
                mechanical_conversion[i*number_of_ptos_per_device+k]["properties"] = {}
                mechanical_conversion[i*number_of_ptos_per_device+k]["properties"]["mechanical_conversion_size"] = copy.deepcopy(pto_list[i][k]["Mech_size"]) 
                mechanical_conversion[i*number_of_ptos_per_device+k]["properties"]["mechanical_conversion_cost"] = copy.deepcopy(pto_list[i][k]["Mech_cost"])
                mechanical_conversion[i*number_of_ptos_per_device+k]["properties"]["mechanical_conversion_mass"] = copy.deepcopy(pto_list[i][k]["Mech_mass"])
                mechanical_conversion[i*number_of_ptos_per_device+k]["assessment"] = {}
                mechanical_conversion[i*number_of_ptos_per_device+k]["assessment"]["mechanical_mean_transformed_power"] = copy.deepcopy(pto_list[i][k]["MechT_Power"])
                mechanical_conversion[i*number_of_ptos_per_device+k]["assessment"]["mechanical_transformed_energy"] = copy.deepcopy(pto_list[i][k]["MechT_Energy"])
                mechanical_conversion[i*number_of_ptos_per_device+k]["assessment"]["mechanical_conversion_annual_failure_rate"] = copy.deepcopy(pto_list[i][k]["MechT_Damage"])
                electrical_conversion.append({})
                electrical_conversion[i*number_of_ptos_per_device+k]["id"] = "1_" + str(i + 1) + "_" + str(k + 1) + "_" + "1"
                electrical_conversion[i*number_of_ptos_per_device+k]["properties"] = {}
                electrical_conversion[i*number_of_ptos_per_device+k]["properties"]["electrical_conversion_size"] = copy.deepcopy(pto_list[i][k]["Elect_P_rated"]) 
                electrical_conversion[i*number_of_ptos_per_device+k]["properties"]["electrical_conversion_cost"] = copy.deepcopy(pto_list[i][k]["Elect_cost"]) 
                electrical_conversion[i*number_of_ptos_per_device+k]["properties"]["electrical_conversion_mass"] = copy.deepcopy(pto_list[i][k]["Elect_mass"])
                electrical_conversion[i*number_of_ptos_per_device+k]["assessment"] = {}
                electrical_conversion[i*number_of_ptos_per_device+k]["assessment"]["electrical_mean_transformed_power"] = copy.deepcopy(pto_list[i][k]["ElectT_Power"])
                electrical_conversion[i*number_of_ptos_per_device+k]["assessment"]["electrical_transformed_energy"] = copy.deepcopy(pto_list[i][k]["ElectT_Energy"])
                electrical_conversion[i*number_of_ptos_per_device+k]["assessment"]["electrical_conversion_annual_failure_rate"] = copy.deepcopy(pto_list[i][k]["ElectT_Damage"])
                grid_conditionining.append({})
                grid_conditionining[i*number_of_ptos_per_device+k]["id"] = "1_" + str(i + 1) + "_" + str(k + 1) + "_" + "1"
                grid_conditionining[i*number_of_ptos_per_device+k]["properties"] = {}
                grid_conditionining[i*number_of_ptos_per_device+k]["properties"]["grid_conditiong_rated_power"] = copy.deepcopy(pto_list[i][k]["Grid_P_rated"])
                grid_conditionining[i*number_of_ptos_per_device+k]["properties"]["grid_conditiong_cost"] = copy.deepcopy(pto_list[i][k]["Grid_cost"])
                grid_conditionining[i*number_of_ptos_per_device+k]["properties"]["grid_conditioning_mass"] = copy.deepcopy(pto_list[i][k]["Grid_mass"]) 
                grid_conditionining[i*number_of_ptos_per_device+k]["assessment"] = {}
                grid_conditionining[i*number_of_ptos_per_device+k]["assessment"]["grid_conditioning_mean_active_power"] = copy.deepcopy(pto_list[i][k]["GridC_Active_Power"]) 
                grid_conditionining[i*number_of_ptos_per_device+k]["assessment"]["grid_conditioning_mean_reactive_power"] = copy.deepcopy(pto_list[i][k]["GridC_reactive_Power"]) 
                grid_conditionining[i*number_of_ptos_per_device+k]["assessment"]["grid_conditioning_energy"] = copy.deepcopy(pto_list[i][k]["GridC_Energy"]) 

        for k in device[0]["properties"]:
            device[0]["properties"][k]["origin"] = "ET"
            device[0]["properties"][k].pop("label")
        
        for k in device[0]["assessment"]:
            device[0]["assessment"][k]["origin"] = "ET"
            device[0]["assessment"][k].pop("label")
        
        for k in pto[0]["assessment"]:
            pto[0]["assessment"][k]["origin"] = "ET"
            pto[0]["assessment"][k].pop("label")

        for k in mechanical_conversion[0]["properties"]:
            mechanical_conversion[0]["properties"][k]["origin"] = "ET"
            mechanical_conversion[0]["properties"][k].pop("label")
        
        for k in mechanical_conversion[0]["assessment"]:
            mechanical_conversion[0]["assessment"][k]["origin"] = "ET"
            mechanical_conversion[0]["assessment"][k].pop("label")

        for k in electrical_conversion[0]["properties"]:
            electrical_conversion[0]["properties"][k]["origin"] = "ET"
            electrical_conversion[0]["properties"][k].pop("label")
        
        for k in electrical_conversion[0]["assessment"]:
            electrical_conversion[0]["assessment"][k]["origin"] = "ET"
            electrical_conversion[0]["assessment"][k].pop("label")

        for k in grid_conditionining[0]["properties"]:
            grid_conditionining[0]["properties"][k]["origin"] = "ET"
            grid_conditionining[0]["properties"][k].pop("label")
        
        for k in grid_conditionining[0]["assessment"]:
            grid_conditionining[0]["assessment"][k]["origin"] = "ET"
            grid_conditionining[0]["assessment"][k].pop("label")

        self._representation_data = {"array" : array, "device": device, "pto" : pto, "mechanical_conversion" : mechanical_conversion, "electrical_conversion" : electrical_conversion, "grid_conditioning" : grid_conditionining}

        # with open("pto.json", "w") as f:
        #     json.dump(self.pto_results, f, indent=4, sort_keys=True, cls=NpEncoder)
        #
        # with open("uparray.json", "w") as f:
        #      json.dump(self.array_results.__dict__, f, indent=4, sort_keys=True, cls=NpEncoder)

    def _write_analysis_results_to_json(self):
        """Optional function for writing the results to output JSON files"""
        # TODO: eventually, do not write to JSON files, save to DB and return as JSONIFIED response
        with open("arraytest2.json", "w") as f:
            json.dump(self.array_results.__dict__, f, indent=4, sort_keys=True, cls=NpEncoder)
        # with open("devicetest.json", "w") as f:
        #     json.dump(self.device_results, f, indent=4, sort_keys=True, cls=NpEncoder)
        # with open("ptotest.json", "w") as f:
        #     json.dump(self.pto_results, f, indent=4, sort_keys=True, cls=NpEncoder)
