# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import numpy as np
import pandas as pd

from dtop_energytransf.business import Device as Device_class


class Array:
    """
    This class initialises an array of wave or tidal devices from the Energy Transformation perspective.
    Here each device is initialised and assessed given the Environmental Conditions. Also the information at array level
    is built up, such as the hierarchy, bill of materials, transformed energy and the complexity level at which it is
    assessed is provided.
    
    The array consists of several devices, each device of several PTOs and each PTO of one MechT, one ElectT and one GridC object.

    All the outputs are given in diccionaries with the following structure:
    
        variable name = {
            "label": "Displayed name",

            "unit": "-",
            
            "description": "Description of the variable",
            
            "value": value of the variable,
        
        }

    Args:
        array_id (int): identifier of the array
        array_inputs (dict): Required inputs to define all required info at array level
        device_inputs (dict): Required inputs at device level
        pto_inputs (dict): Required inputs at pto level

    Attributes:
        Array_Tech (dict): structure with the Technology. Wave or Tidal technology
        Array_number_devices (dict): structure with the number of devices in the array
        id (dict): structure with the Array Id
        Devices (dict): structure with the All device objects within the array
        Hierarchy (dict): structure with the Hierarchy
        BoM (dict): structure with the Bill of materials of the Energy Transformation subsystem
        Array_mass(dict): structure with the total weight of the components of the Energy Transformation subsystem
        Array_cost(dict): structure with the Total cost of the components of the Energy Transformation subsystem
        Array_P_Capt (dict): structure with the Captured Power per sea state in kW
        Array_P_Mech (dict): structure with the Mechanical Power per sea state in kW
        Array_P_Elect (dict): structure with the Electrical Power per sea state in kW
        Array_Pa_Grid (dict): structure with the Grid Active Power per sea state in kW
        Array_Pr_Grid (dict): structure with the Grid Reactive Power per sea state in kW
        Array_E_Capt (dict): structure with the Total Annual Captured Energy in kWh
        Array_E_Mech (dict): structure with the Total Annual Mechanical Energy in kWh
        Array_E_Elect (dict): structure with the Total Annual Electrical Energy in kWh
        Array_E_Grid (dict): structure with the Total Annual Grid Conditioned Active Energy in kWh
        Array_Env_Conditions (dict): structure with the Environmental Conditions
        Materials (dict): structure with the Materials for ESA assesment
        SeaStates (list): list of numbered Sea states
        pto_subsystems (dict): structure with the PTO subsystems for the Hyerarchy: ["MechT", "ElectT", "GridC"]

    Returns:
        none

    """

    def __init__(self, array_id, array_inputs, device_inputs, pto_inputs):
        """This method initialises the whole array consisting of several devices, each device of several PTOs and each
        PTO of one MechT, one ElectT and one GridC object.
      
        """        

        # Inputted variables at array level
        self.Array_Tech = {
            "label": "Technology",
            "unit": "-",
            "description": "Wave or Tidal Energy Array",
            "value": array_inputs["Technology"],
        }
        self.Array_number_devices = {
            "label": "Number of devices",
            "unit": "-",
            "description": "Total number of devices in the array",
            "value": array_inputs["Number of devices"],
        }
        self.id = {
            "label": "Array ID",
            "unit": "-",
            "description": "Identifier of the Array",
            "value": array_id,
        }

        # Generated variables at array level
        self.Devices = {}  # All device objects within the array
        self.Hierarchy = {
            "label": "Hierarchy",
            "unit": "-",
            "description": "Hierarchy of the Energy Transformation subsystem ",
            "value": {},
        }  # The dependency of all components along with its failure rates
        self.BoM = {
            "label": "Bill of materials",
            "unit": "-",
            "description": "Bill of materials of the Energy Transformation subsystem",
            "value": [],
        }  # Bill of Materials of the Array
        # self.cmpx = []  # Complexity level of the model
        self.Array_mass = {"label": "Weight of the components", "unit" : "kg" , "description": "Total weight of the components of the Energy Transformation subsystem", "value" :0}  # Total mass of the PTO components
        self.Array_cost = {"label": "Cost of the components", "unit" : "Euros" , "description": "Total cost of the components of the Energy Transformation subsystem", "value" :0}  # Total cost of the PTO components
        self.Array_P_Capt = {"label": "Captured Power", "unit" : "kW" , "description": "Captured Power per sea state", "value" :[]}
        self.Array_P_Mech = {"label": "Mechanical Power", "unit" : "kW" , "description": "Mechanical Power per sea state", "value" :[]}  # Total Mechanical annual mean power per sea state
        self.Array_P_Elect = {"label": "Electrical Power", "unit" : "kW" , "description": "Electrical Power per sea state", "value" :[]}  # Total Electrical annual mean power per sea state
        self.Array_Pa_Grid = {"label": "Grid Active Power", "unit" : "kW" , "description": "Grid Active Power per sea state", "value" :[]}  # Total Grid annual active mean power per sea state
        self.Array_Pr_Grid = {"label": "Grid Reactive Power", "unit" : "kW" , "description": "Grid Reactive Power per sea state", "value" :[]}  # Total Grid annual reactive mean power per sea state
        self.Array_E_Capt = {"label": "Captured Energy", "unit" : "kWh" , "description": "Total Annual Captured Energy", "value" :[]}
        self.Array_E_Mech = {"label": "Mechanical Energy", "unit" : "kWh" , "description": "Total Annual Mechanical Energy", "value" :[] } # Total annual Mechanical Energy
        self.Array_E_Elect = {"label": "Electrical Energy", "unit" : "kWh" , "description": "Total Annual Electrical Energy", "value" :[] } # Total annual Electrical energy
        self.Array_E_Grid = {"label": "Grid conditioned Energy", "unit" : "kWh" , "description": "Total Annual Grid Conditioned Active Energy", "value" :[]}  # Total annual Grid Conditioned active energy
        self.Array_Env_Conditions = {"label": "Environmental Conditions", "unit" : "-" , "description": "Environmental Conditions", "value" :[] } # Environmental conditions the array will be subject to
        self.materials = {"label": "Materials", "unit" : "-" , "description": "Materials for ESA assesment", "value" :[]}
        self.SeaStates = []
        # self.rated_power_devices = {"label": "Device Rated Power", "unit" : "kW" , "description": "Rated Power of Each device", "value" :[]}
        # self.rated_voltage = {"label": "Device Rated Voltage", "unit" : "V" , "description": "Rated Voltage of Each device", "value" :[]}

        # Hierarchy variable fields initialisation
        self.Hierarchy["value"] = {
                            'system': ['ET'],
                            'name_of_node': [self.id["value"]],
                            'design_id': [self.id["value"]],
                            'node_type': ['System'],
                            'node_subtype' : ['NA'],
                            'category': ['Level 3'],
                            'parent': ['NA'],
                            'child': [['ET' + str(c_dev) for c_dev in np.arange(1, self.Array_number_devices["value"]+1, 1)]],
                            'gate_type': ['AND'],
                            'failure_rate_repair': ['NA'],
                            'failure_rate_replacement': ['NA']
                          }

        # Bill of Materials variable fields initialisation
        self.BoM["value"] = {
            "id": [],
            "name": [],
            "qnt": [],
            "uom": [],
            "unit_cost": [],
            "total_cost": [],
        }

        # Auxiliary variable for the hierarchy
        self.pto_subsystems = {
            "label": "Subsystems",
            "unit": "-",
            "description": "Subsystems",
            "value": ["MechT", "ElectT", "GridC"],
        }
        comp_types = []
        comp_costs = []
        comp_sizes = []
        pd_BoM = pd.DataFrame()

        # Device initialisation
        for c_dev in np.arange(0, self.Array_number_devices["value"], 1):
            dev_str = str(c_dev+1)
            # Device output dictionary
            Dev_ID = str(c_dev)
            self.Devices[Dev_ID] = {'Device': Device_class.Device(Dev_ID, self.Array_Tech["value"], device_inputs, pto_inputs)}
            self.Devices[Dev_ID]['Technology'] = self.Devices[Dev_ID]['Device'].Dev_tech["value"]
            self.Devices[Dev_ID]['Parallel_PTOs'] = self.Devices[Dev_ID]['Device'].Dev_par_PTOs
            self.Devices[Dev_ID]['dof_PTOs'] = self.Devices[Dev_ID]['Device'].Dev_dof_PTO["value"]
            self.Devices[Dev_ID]['Control_Strategy'] = self.Devices[Dev_ID]['Device'].Dev_Control_strat["value"]
            self.Devices[Dev_ID]['MechT_type'] = self.Devices[Dev_ID]['Device'].Dev_Mech_type["value"]
            self.Devices[Dev_ID]['ElectT_type'] = self.Devices[Dev_ID]['Device'].Dev_Elect_type["value"]
            self.Devices[Dev_ID]['GridC_type'] = self.Devices[Dev_ID]['Device'].Dev_Grid_type["value"]
            self.Devices[Dev_ID]['Shutdown_flag'] = self.Devices[Dev_ID]['Device'].Dev_shutdown_flag["value"]
            self.Devices[Dev_ID]['Cut_in_out'] = self.Devices[Dev_ID]['Device'].Dev_cut_in_out["value"]
            self.Devices[Dev_ID]['PTO mass'] = self.Devices[Dev_ID]['Device'].Dev_PTO_mass["value"]
            self.Devices[Dev_ID]['PTO_cost'] = self.Devices[Dev_ID]['Device'].Dev_PTO_cost["value"]
            self.Devices[Dev_ID]['Captured_Power'] = []
            self.Devices[Dev_ID]['MechT_Power'] = []
            self.Devices[Dev_ID]['ElectT_Power'] = []
            self.Devices[Dev_ID]['GridC_Active_Power'] = []
            self.Devices[Dev_ID]['GridC_reactive_Power'] = []
            self.Devices[Dev_ID]['Captured_Energy'] = []
            self.Devices[Dev_ID]['MechT_Energy'] = []
            self.Devices[Dev_ID]['ElectT_Energy'] = []
            self.Devices[Dev_ID]['GridC_Energy'] = []
            #print('Device with ID ' + Dev_ID + ' has been created')

            # Total mass and cost
            self.Array_mass["value"] = (
                self.Array_mass["value"]
                + self.Devices[Dev_ID]["Device"].Dev_PTO_mass["value"]
            )
            self.Array_cost["value"] = (
                self.Array_cost["value"]
                + self.Devices[Dev_ID]["Device"].Dev_PTO_cost["value"]
            )

            # Add the device to the hierarchy

            self.Hierarchy["value"]['system'].append('ET')
            self.Hierarchy["value"]['name_of_node'].append("ET"+dev_str)
            self.Hierarchy["value"]['design_id'].append(self.id["value"])
            self.Hierarchy["value"]['node_type'].append('Device')
            self.Hierarchy["value"]['node_subtype'].append('NA')
            self.Hierarchy["value"]['category'].append('Level 2')
            self.Hierarchy["value"]['parent'].append(self.id["value"])
            self.Hierarchy["value"]['child'].append(["ET"+dev_str+"_"+ x for x in list(self.Devices[Dev_ID]['Device'].PTOs.keys())])
            self.Hierarchy["value"]['gate_type'].append(str(int(self.Devices[Dev_ID]['Device'].Dev_shutdown_flag["value"])) + '/' +
                                               str(self.Devices[Dev_ID]['Device'].Dev_dof_PTO["value"]))
            self.Hierarchy["value"]['failure_rate_repair'].append('NA')
            self.Hierarchy["value"]['failure_rate_replacement'].append('NA')

            for c_PTO in self.Devices[Dev_ID]["Device"].PTOs.keys():
                # Add the PTO to the hierarchy
                name_node = "ET"+dev_str + '_' +\
                            self.Devices[Dev_ID]['Device'].PTOs[c_PTO]['PTO'].id
                self.Hierarchy["value"]['system'].append('ET')
                self.Hierarchy["value"]['name_of_node'].append(name_node)
                self.Hierarchy["value"]['design_id'].append(self.id["value"])
                self.Hierarchy["value"]['node_type'].append('PTO')
                self.Hierarchy["value"]['node_subtype'].append('NA')
                self.Hierarchy["value"]['category'].append('Level 1')
                self.Hierarchy["value"]['parent'].append("ET"+dev_str)
                self.Hierarchy["value"]['child'].append([name_node + '_' + self.pto_subsystems["value"][0],
                                                name_node + '_' + self.pto_subsystems["value"][1],
                                                name_node + '_' + self.pto_subsystems["value"][2]])
                self.Hierarchy["value"]['gate_type'].append('AND')
                self.Hierarchy["value"]['failure_rate_repair'].append('NA')
                self.Hierarchy["value"]['failure_rate_replacement'].append('NA')

                # Collecting types of transformation stages for the BoM
                comp_types.append(self.Devices[Dev_ID]["MechT_type"])
                comp_types.append(self.Devices[Dev_ID]["ElectT_type"])
                comp_types.append(self.Devices[Dev_ID]["GridC_type"])
                comp_sizes.append(
                    self.Devices[Dev_ID]["Device"].PTOs[c_PTO]["Mech_size"]["value"]
                )
                comp_sizes.append(
                    self.Devices[Dev_ID]["Device"].PTOs[c_PTO]["Elect_P_rated"]["value"]
                )
                comp_sizes.append(
                    self.Devices[Dev_ID]["Device"].PTOs[c_PTO]["Grid_P_rated"]["value"]
                )
                comp_costs.append(
                    self.Devices[Dev_ID]["Device"].PTOs[c_PTO]["Mech_cost"]["value"]
                )
                comp_costs.append(
                    self.Devices[Dev_ID]["Device"].PTOs[c_PTO]["Elect_cost"]["value"]
                )
                comp_costs.append(
                    self.Devices[Dev_ID]["Device"].PTOs[c_PTO]["Grid_cost"]["value"]
                )

                # self.Hierarchy['type'] = 'Object'
                for c_comp in self.pto_subsystems["value"]:
                    # Add PTO subsystems to the hierarchy

                    # self.Hierarchy['properties']['type'].append('Object')
                    # self.Hierarchy['properties']['properties'].append('Object')
                    self.Hierarchy["value"]["system"].append("ET")
                    self.Hierarchy["value"]["name_of_node"].append(
                        name_node + "_" + c_comp
                    )
                    self.Hierarchy["value"]["design_id"].append(self.id["value"])
                    self.Hierarchy["value"]["node_type"].append("Component")
                    self.Hierarchy["value"]["node_subtype"].append("NA")
                    self.Hierarchy["value"]["category"].append("Level 0")
                    self.Hierarchy["value"]["parent"].append(name_node)
                    self.Hierarchy["value"]["child"].append("NA")
                    self.Hierarchy["value"]["gate_type"].append("AND")
                    self.Hierarchy["value"]["failure_rate_repair"].append(
                        0.0
                    )  # The corresponding failure rate needs to be updated in
                    self.Hierarchy["value"]["failure_rate_replacement"].append(0.0)
                    #  the performance method.

        
        self.Array_mass["value"] = round (self.Array_mass["value"],2)
        self.Array_cost["value"] = round (self.Array_cost["value"],2)

        # Bill of Materials variable fields initialisation
        pd_BoM["Types"] = comp_types
        pd_BoM["Sizes"] = comp_sizes
        pd_BoM["Costs"] = comp_costs

        a = pd_BoM.groupby("Types").all()  # Detect types
        for comp in list(a.index):
            ind_id = list(a.index).index(comp)
            type_filtered = pd_BoM[pd_BoM["Types"] == comp]  # Filter by type
            size_filtered = type_filtered.groupby(
                "Sizes"
            ).all()  # Detect sizes of the type
            for c_size in list(size_filtered.index):
                type_size_found = type_filtered[
                    type_filtered["Sizes"] == c_size
                ]  # Filter by size

                # Populate the Bill of Materials
                self.BoM["value"]['id'].append('Cat_ID_'+str(ind_id))
                self.BoM["value"]['name'].append(comp)
                self.BoM["value"]['uom'].append('NA')
                self.BoM["value"]['qnt'].append(type_size_found['Types'].count())
                self.BoM["value"]['unit_cost'].append(list(type_size_found['Costs'])[0])
                self.BoM["value"]['total_cost'].append(list(type_size_found['Costs'])[0]*type_size_found['Types'].count())
    
    
        self.materials["value"] = [{"material_name" : "steel", "material_quantity": self.Array_mass["value"]} ]
        # self.rated_power_devices["value"] = self.Devices["0"]['Device'].Dev_rated_power["value"]
        # self.rated_voltage["value"] = self.Devices["0"]['Device'].PTOs["PTO_0_0"]['Grid_V']["value"]
        

    def performance(self, perf_inputs, env_conds, et_id):
        """This function evaluates the performance of all devices subject to the env_conditions shared by all of them.

        Args:
            perf_inputs (List): List of performance inputs required to evaluate the performance of the Array class for all devices. Includes sigma_v, Cpto and inverse of omega
            env_conds (List): The environmental conditions, coherent with each perf_inputs item to assess all devices: Hs (wave), Vc (tidal), Occ, Tz and site id 
            et_id (int): Energy transformation study identifier

        Returns:
            none

        """        
         # --------------------------------------------------------------------------------------------------------------
        # Reset of all required variables, the performance results are updated with only one set of env_conditions and
        # are NOT added to some other previous ones.
        self.Array_Env_Conditions["value"] = env_conds[0]
        self.Array_P_Capt["value"] = {"Power" : np.zeros(len(env_conds[0]["Occ"]))}
        self.Array_P_Mech["value"] = {"Power" : np.zeros(len(env_conds[0]["Occ"]))}
        self.Array_P_Elect["value"] = {"Power" : np.zeros(len(env_conds[0]["Occ"]))}
        self.Array_Pa_Grid["value"] = {"Power" : np.zeros(len(env_conds[0]["Occ"]))}
        self.Array_Pr_Grid["value"] = {"Power" : np.zeros(len(env_conds[0]["Occ"]))}
        self.Array_E_Capt["value"] = 0
        self.Array_E_Mech["value"] = 0  # Total annual Mechanical Energy
        self.Array_E_Elect["value"] = 0  # Total annual Electrical energy
        self.Array_E_Grid["value"] = 0  # Total annual Grid Conditioned active energy

        seas = len(env_conds[0]["id"])

        
        for contSS in range (0, seas):
            SeaState_data= {
                "value" : env_conds[0]["id"][contSS],
                "label" : "Sea State "+ str(env_conds[0]["id"][contSS])
            }
            self.SeaStates.append(SeaState_data)

        
        # --------------------------------------------------------------------------------------------------------------

        # Performance assessment of all devices within the array
        dev_count = 0
        for c_dev in self.Devices.keys():
            # Device performance assessment
            self.Devices[c_dev]["Device"].performance(perf_inputs[dev_count], env_conds[dev_count], et_id)
            dev_count = dev_count + 1

            decimal_points = 3
            # Array mean power
            self.Array_P_Capt["value"]['Power'] = np.round((
                self.Array_P_Capt["value"]['Power']
                + self.Devices[c_dev]["Device"].Dev_P_Capt["value"]['Power']
            ), decimal_points)
            self.Array_P_Mech["value"]['Power'] = np.round((
                self.Array_P_Mech["value"]['Power']
                + self.Devices[c_dev]["Device"].Dev_P_Mech["value"]['Power']
            ), decimal_points)
            self.Array_P_Elect["value"]['Power'] = np.round((
                self.Array_P_Elect["value"]['Power']
                + self.Devices[c_dev]["Device"].Dev_P_Elect["value"]['Power']
            ), decimal_points)
            self.Array_Pa_Grid["value"]['Power'] = np.round((
                self.Array_Pa_Grid["value"]['Power']
                + self.Devices[c_dev]["Device"].Dev_Pa_Grid["value"]['Power']
            ), decimal_points)
            self.Array_Pr_Grid["value"]['Power'] = np.round((
                self.Array_Pr_Grid["value"]['Power']
                + self.Devices[c_dev]["Device"].Dev_Pr_Grid["value"]['Power']
            ), decimal_points)

            self.Array_P_Capt["value"]['id'] = self.Array_Env_Conditions["value"]["id"]

            self.Array_P_Mech["value"]['id'] = self.Array_Env_Conditions["value"]["id"]

            self.Array_P_Elect["value"]['id'] = self.Array_Env_Conditions["value"]["id"]
            
            self.Array_Pa_Grid["value"]['id'] = self.Array_Env_Conditions["value"]["id"]


            self.Array_Pr_Grid["value"]['id'] = self.Array_Env_Conditions["value"]["id"]

            # Array produced annual energy
            self.Array_E_Capt["value"] = np.round((
                self.Array_E_Capt["value"]
                + self.Devices[c_dev]["Device"].Dev_E_Capt["value"]
            ), decimal_points)
            self.Array_E_Mech["value"] = np.round((
                self.Array_E_Mech["value"]
                + self.Devices[c_dev]["Device"].Dev_E_Mech["value"]
            ), decimal_points)
            self.Array_E_Elect["value"] = np.round((
                self.Array_E_Elect["value"]
                + self.Devices[c_dev]["Device"].Dev_E_Elect["value"]
            ), decimal_points)
            self.Array_E_Grid["value"] = np.round((
                self.Array_E_Grid["value"]
                + self.Devices[c_dev]["Device"].Dev_E_Grid["value"]
            ), decimal_points)

            # Device output dictionary
            self.Devices[c_dev]["Captured_Power"] = self.Devices[c_dev][
                "Device"
            ].Dev_P_Capt["value"]
            self.Devices[c_dev]["MechT_Power"] = self.Devices[c_dev][
                "Device"
            ].Dev_P_Mech["value"]
            self.Devices[c_dev]["ElectT_Power"] = self.Devices[c_dev][
                "Device"
            ].Dev_P_Elect["value"]
            self.Devices[c_dev]["GridC_Active_Power"] = self.Devices[c_dev][
                "Device"
            ].Dev_Pa_Grid["value"]
            self.Devices[c_dev]["GridC_reactive_Power"] = self.Devices[c_dev][
                "Device"
            ].Dev_Pr_Grid["value"]
            self.Devices[c_dev]["Captured_Energy"] = self.Devices[c_dev][
                "Device"
            ].Dev_E_Capt["value"]
            self.Devices[c_dev]["MechT_Energy"] = self.Devices[c_dev][
                "Device"
            ].Dev_E_Mech["value"]
            self.Devices[c_dev]["ElectT_Energy"] = self.Devices[c_dev][
                "Device"
            ].Dev_E_Elect["value"]
            self.Devices[c_dev]["GridC_Energy"] = self.Devices[c_dev][
                "Device"
            ].Dev_E_Grid["value"]

            # Populate the hierarchy with the corresponding damages
            for c_pto in self.Devices[c_dev]["Device"].PTOs.keys():
                for c_comp in self.pto_subsystems["value"]:
                    # Corresponding ID in the hierarchy
                    pto_id_hier = "ET"+str(int(self.Devices[c_dev]['Device'].id["value"])+1) + '_' +\
                                self.Devices[c_dev]['Device'].PTOs[c_pto]['PTO'].id + '_' + c_comp
                    # Index in which the component is stored
                    ind_comp = self.Hierarchy["value"]['name_of_node'].index(pto_id_hier)
                    if c_comp == 'MechT':
                        self.Hierarchy["value"]['failure_rate_repair'][ind_comp] = \
                            np.round(
                                self.Devices[c_dev]['Device'].PTOs[c_pto]['MechT_Damage']['value'],
                                decimal_points
                            )
                        self.Hierarchy["value"]['failure_rate_replacement'][ind_comp] = "NA"
                    elif c_comp == 'ElectT':
                        self.Hierarchy["value"]['failure_rate_repair'][ind_comp] = \
                            np.round(
                                self.Devices[c_dev]['Device'].PTOs[c_pto]['ElectT_Damage']['value'],
                                decimal_points
                            )
                        self.Hierarchy["value"]['failure_rate_replacement'][ind_comp] = "NA"
                    elif c_comp == 'GridC':
                        self.Hierarchy["value"]['failure_rate_repair'][ind_comp] = \
                            np.round(
                                self.Devices[c_dev]['Device'].PTOs[c_pto]['GridC_Damage']['value'],
                                decimal_points
                            )
                        self.Hierarchy["value"]['failure_rate_replacement'][ind_comp] = "NA"
            #print('Device performance assessment with ID ' + c_dev + ' has been done')
