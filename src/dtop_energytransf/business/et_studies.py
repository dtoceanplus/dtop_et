# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from sqlalchemy.exc import IntegrityError

from dtop_energytransf.service import db
from dtop_energytransf.storage.models.models import (
    EnergyTransformationStudy,
    GeneralInputs,
    MechanicalInputs,
    ElectricalInputs,
    GridInputs,
    ControlInputs,
    EnergyCaptureData,
    MachineCharacterisationData,
    SiteCharacterisationData
)
from dtop_energytransf.storage.schemas.schemas import EnergyTransformationStudySchema


def create_study(data):
    """
    Create a new Energy Transformation study and add it to the local storage database.

    Returns an error message and rolls back the session if the new ET study has the same name as an existing
    study in the database.

    :param dict data: dictionary required to create the Energy Transformation study object.
    :return: the newly created Energy Transformation study instance
    :rtype: dtop_energytransf.storage.models.models.EnergyTransformationStudy
    """
    et_study_schema = EnergyTransformationStudySchema()
    et_study = et_study_schema.load(data)
    et_study.general_inputs = GeneralInputs()
    et_study.mechanical_inputs = MechanicalInputs()
    et_study.electrical_inputs = ElectricalInputs()
    et_study.grid_inputs = GridInputs()
    et_study.control_inputs = ControlInputs()
    et_study.energy_capture_data = EnergyCaptureData()
    et_study.machine_characterisation_data = MachineCharacterisationData()
    et_study.site_characterisation_data = SiteCharacterisationData()
    et_study.status = 40
    try:
        db.session.add(et_study)
        db.session.commit()  # line added to fix bug introduced in sqlalchemy=1.3.14; Query-invoked auto-flush
    except IntegrityError:
        db.session.rollback()
        return "Energy Transformation study with that name already exists."

    db.session.commit()

    return et_study


def update_study(data, study_id):
    """
    Update an Energy Transformation study entry in the local storage database.

    Returns an error message if;

    - the ET study ID that is trying to be updated does not exist or

    - an ET study with the same name already exists.

    :param dict data: the ET study data required to update a study in the local database.
    :param str study_id: the ID of the ET study in the local storage database
    :return: the updated ET instance
    :rtype: dtop_energytransf.storage.models.EnergyTransformationStudy
    """
    s = EnergyTransformationStudy.query.get(study_id)
    if s is None:
        return "Energy Transformation study with that ID does not exist."
    data = {**data}
    s.update(**data)
    try:
        db.session.commit()
        return s
    except IntegrityError:
        db.session.rollback()
        return "Energy Transformation study with that name already exists."


def delete_study(study_id):
    """
    Delete an Energy Transformation study from the local storage database.

    If study with the specified ID does not exist, returns an error message that leads to a HTTP 404 error.

    :param str study_id: the ID of the Energy Transformation study in local storage database to be deleted
    :return: error message if ID does not exist, dictionary with deleted status set to True if deleted successfully
    :rtype: dict
    """
    s = EnergyTransformationStudy.query.get(study_id)
    if s is None:
        return "Energy Transformation study with that ID does not exist."
    else:
        db.session.delete(s)
        db.session.commit()
        return "Study deleted"
    
