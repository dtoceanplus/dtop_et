# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from dtop_energytransf.business.transf_stages import (
    Elect_Transf as ElectT,
    Mech_Transf as MechT,
    Grid_Cond as GridC,
    Control as ctrl_obj,
)

import matplotlib.pyplot as plt

# Required libraries


class PTO:
    """
    This class defines a PTO. It is initialised with its three transformation objects and has two methods, one for
    performance and the other one for reliability assessment.

    The initialisation function builds up the objects for the three transformation stages, Mechanical, Electrical
    and Grid conditioning, with the corresponding control strategy.
    
    Args:
        pto_id (int): identificator number of the
        technology (str): Wave or Tidal technology
        mechT_params (dict): Required initialisation parameters for the mechanical transformation coming from the GUI
        electT_params (dict): Required initialisation parameters for the electrical transformation coming from the GUI
        gridC_params (dict): Required initialisation parameters for the grid conditioning coming from the GUI
        control_params (dict): Required initialisation parameters for the control coming from the GUI   

    Attributes:
        id (int): Identifier of the PTO
        tech (str): Techonology "Wave" or "Tidal"
        MechT_type (str): Type of mechanical transformation 
        MechT_args (list): List of initialitation parameters of the mechanical object introduced in the GUI
        MechT_perf_args (list): List of performance parameters of the mechanical object introduced in the GUI
        MechT_t_ratio (float): Speed ratio relation. Will be one if direct drive is selected. 
        MechT_obj (obj): Mechanical transformation object
        ElectT_type (str): Type of electrical transformation
        ElectT_args(list): List of initialitation parameters of the electrical object introduced in the GUI
        ElectT_obj(obj): electrical transformation object
        GridC_type(str): Type of Grid transformation
        GridC_args(list): List of initialitation parameters of the Grid conditioning object introduced in the GUI
        GridC_obj(obj): Grid conditioning transformation object
        Control_type(str): Type of control 
        Control_args(list): List of initialitation parameters of the control object introduced in the GUI
        Control_obj(obj): control object

    Returns: 
        none

    """

    def __init__(
        self,
        pto_id,
        technology,
        mechT_params,
        electT_params,
        gridC_params,
        control_params,
    ):

        # ID storing
        self.id = pto_id
        self.tech = technology

        # Initialisation of the Mechanical Transformation object
        self.MechT_type = mechT_params["Type"]
        self.MechT_args = mechT_params[
            "init_args"
        ]  # All arguments, list in the same order as required by the object
        self.MechT_perf_args = mechT_params[
            "perf_args"
        ]  # Additional arguments to compute object performance.
        self.MechT_t_ratio = mechT_params["mech_t_ratio"]  # Hypotetic Gearbox without efficiency, only speed ratio relation
        self.MechT_obj = getattr(MechT, self.MechT_type)(*self.MechT_args)
        # After initialisation the cost and environmental impact is computed
        self.MechT_obj.env_impact()
        self.MechT_obj.cost()

        # Initialisation of the Electrical Transformation object
        self.ElectT_type = electT_params["Type"]
        self.ElectT_args = electT_params[
            "init_args"
        ]  # All arguments, list in the same order as required by the object
        self.ElectT_obj = getattr(ElectT, self.ElectT_type)(*self.ElectT_args)
        # After initialisation the cost and environmental impact is computed
        self.ElectT_obj.env_impact()
        self.ElectT_obj.cost()

        # Initialisation of the Grid Conditioning object
        self.GridC_type = gridC_params["Type"]
        self.GridC_args = gridC_params[
            "init_args"
        ]  # All arguments, list in the same order as required by the object
        self.GridC_obj = getattr(GridC, self.GridC_type)(*self.GridC_args)
        # After initialisation the cost and environmental impact is computed
        self.GridC_obj.env_impact()
        self.GridC_obj.cost()

        #  Initialisation of the Control object
        self.Control_type = control_params["Type"]
        self.Control_args = control_params["init_args"]
        self.Control_obj = ctrl_obj.Control(
            self.tech, self.Control_type, *self.Control_args
        )

    def performance(self, Cpto, sigma_v):
        """Here the performance of all components of the PTO is computed

        Args:
            Cpto (float): PTO damping applied by the PTO to get the mean power and velocity std
            sigma_v (float): 0.1 of hub velocity (Tidal) or sqrt (Captured Power / Cpto) (Wave)
        
        Returns: 
            none
        
        """        
        

        # Velocity vector defining velocity ranges occurring within the sea state.

        vels = sigma_v * self.Control_obj.adim_vel[0:-1]
        # P_levels = Cpto * vels**2
        # P_mean = self.Control_obj.prob_P * P_levels
        
        # Performance of the Mechanical Transformation Stage is computed in the Sea State
        self.MechT_obj.performance(
            Cpto,
            vels,
            self.Control_obj.prob_T,
            self.Control_obj.prob_P,
            self.MechT_perf_args,
        )

        # Performance of the Electrical Transformation Stage is computed in the Sea State
        mech_Power = self.MechT_obj.Mech_T * self.MechT_obj.omega
        omega_elect = self.MechT_obj.omega / self.MechT_t_ratio
        self.ElectT_obj.performance(
            mech_Power, omega_elect, self.MechT_obj.Mech_P_mean, self.Control_obj.prob_T
        )
        
        # Performance of the Grid Conditioning Stage is computed in the Sea State
        elect_power = mech_Power * self.ElectT_obj.Elect_eff

        self.GridC_obj.performance(
            elect_power,
            self.ElectT_obj.f_est,
            self.ElectT_obj.Elect_I_est,
            self.ElectT_obj.V_est,
            self.ElectT_obj.Elect_cosphi,
            self.ElectT_obj.Elect_P_mean,
        )
        grid_power = elect_power * self.GridC_obj.pe_eff
       
        # P_levels = (Cpto * vels**2)
        # P_mean = self.Control_obj.prob_P * P_levels
        # plt.plot(P_levels, self.ElectT_obj.Elect_eff)
        # plt.legend(['eff']) 
        # plt.show()   
        # plt.plot(P_levels, P_mean, P_levels, self.MechT_obj.Mech_P_mean, P_levels, self.ElectT_obj.Elect_P_mean, P_levels, self.GridC_obj.Grid_P_mean),\
        # plt.legend(['P_in', 'P_mech', 'P_elect', 'P_grid']) #,\
        # plt.savefig ('dibu.png')
        # plt.show()

        """Old 27052020 self.GridC_obj.performance(elect_power,
                                   self.ElectT_obj.Elect_I_est, self.ElectT_obj.Elect_cosphi, self.ElectT_obj.V_est,
                                   self.ElectT_obj.f_est, self.ElectT_obj.Elect_P_mean)"""

    def reliability(self, EC_time, EC_Tz):
        """The reliability of all components of the PTO is computed

        Args:
            EC_time (float): Total annual time of the Environmental Condition [s]
            EC_Tz (float): Mean period of the environmental condition [s]

        Returns: 
            none

        """       
       
        #  Reliability of the mechanical transformation stage
        self.MechT_obj.reliability(self.Control_obj.prob_T_range, EC_time, EC_Tz)

        #  Reliability of the Electrical transformation stage
        self.ElectT_obj.reliability(self.Control_obj.prob_T_range, EC_time)

        #  Reliability of the Grid Conditioning object
        self.GridC_obj.reliability(
            self.MechT_obj.Mech_T * self.MechT_obj.omega * self.ElectT_obj.Elect_eff,
            sum(self.ElectT_obj.Elect_P_mean),
            self.Control_obj.prob_T_range,
            EC_time,
            EC_Tz,
        )
