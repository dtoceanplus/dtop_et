# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# from dtop_energycapt.service import ma
# Modified JLM
from ..models.models import *

from flask_marshmallow import Marshmallow
from marshmallow import post_dump
from marshmallow import fields, validates, ValidationError

ma = Marshmallow()


class ProjectSchema(ma.ModelSchema):
    class Meta:
        model = Project


class ArraySchema(ma.ModelSchema):
    class Meta:
        model = Array


class DeviceSchema(ma.ModelSchema):
    class Meta:
        model = Device


class PTOSchema(ma.ModelSchema):
    class Meta:
        model = PTO


class EnergyTransformationStudySchema(ma.ModelSchema):
    """Marshmallow schema for Energy Transformation Study database model"""

    site_characterisation_id = fields.Integer(required=False, missing=None)
    machine_characterisation_id = fields.Integer(required=False, missing=None)
    energy_capture_id = fields.Integer(required=False, missing=None)
    site_characterisation_study_id = fields.Integer(required=False, missing=None)
    machine_characterisation_study_id = fields.Integer(required=False, missing=None)
    energy_capture_study_id = fields.Integer(required=False, missing=None)
    status = fields.Integer(required=False, missing=None)

    class Meta:
        model = EnergyTransformationStudy


class SimpleStudySchema(ma.Schema):

    id = fields.Integer()
    name = fields.String()
    description = fields.String()
    standalone = fields.Boolean(required=True)
    status = fields.Integer()
    site_characterisation_study_id = fields.Integer()
    machine_characterisation_study_id = fields.Integer()
    energy_capture_study_id = fields.Integer()

class SiteCharacterisationSchema(ma.ModelSchema):
    
    class Meta:
        model = SiteCharacterisationData

class MachineCharacterisationSchema(ma.ModelSchema):
    
    class Meta:
        model = MachineCharacterisationData

class EnergyCaptureSchema(ma.ModelSchema):
    
    class Meta:
        model = EnergyCaptureData    


class CrossmoduleSchema(ma.ModelSchema):
    """Marshmallow schema for crossmodule data to the Energy Transformation module"""

    site_characterisation_data = fields.Nested(SiteCharacterisationSchema(exclude=("id", "et_study")), required=True)
    machine_characterisation_data = fields.Nested(MachineCharacterisationSchema(exclude=("id", "et_study")), required=True)
    energy_capture_data = fields.Nested(EnergyCaptureSchema(exclude=("id", "et_study")), required=True)

    class Meta:
        model = EnergyTransformationStudy                    

class GeneralInputsSchema(ma.ModelSchema):

    class Meta:
        model = GeneralInputs


class MechanicalInputsSchema(ma.ModelSchema):

    class Meta:
        model = MechanicalInputs


class ElectricalInputsSchema(ma.ModelSchema):

    class Meta:
        model = ElectricalInputs


class GridInputsSchema(ma.ModelSchema):
    class Meta:
        model = GridInputs

class ControlInputsSchema(ma.ModelSchema):
    class Meta:
        model = ControlInputs

class InputsSchema(ma.ModelSchema):
    """Marshmallow schema for inputs to the Energy Transformation module"""

    general_inputs = fields.Nested(GeneralInputsSchema(exclude=("id", "et_study")), required=True)
    mechanical_inputs = fields.Nested(MechanicalInputsSchema(exclude=("id", "et_study")), required=True)
    electrical_inputs = fields.Nested(ElectricalInputsSchema(exclude=("id", "et_study")), required=True)
    grid_inputs = fields.Nested(GridInputsSchema(exclude=("id", "et_study")), required=True)
    control_inputs = fields.Nested(ControlInputsSchema(exclude=("id", "et_study")), required=True)

    class Meta:
        model = EnergyTransformationStudy


class ArrayResultsSchema(ma.ModelSchema):

    class Meta:
        model = ArrayResults


class DeviceResultsSchema(ma.ModelSchema):

    class Meta:
        model = DeviceResults


class PtoResultsSchema(ma.ModelSchema):

    class Meta:
        model = PtoResults

class RepresentationSchema(ma.ModelSchema):

    class Meta:
        model = Representation
