# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# from dtop_energytransf.storage.models import models as DB_models
# from sqlalchemy import create_engine
# from sqlalchemy.orm import sessionmaker
# import os
# import json

# #  Database Creation / Connection (if it already exists)
# # The engine is the core ***interface*** to the database (interface means both for creating and accessing)
# PWD = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))) + '\\Databases'
# SQLALCHEMY_DATABASE_URI = 'sqlite:///'+PWD+'\\ET_DB_test.db'.format(PWD)
# engine = create_engine(SQLALCHEMY_DATABASE_URI, echo=True)
# Session = sessionmaker(bind=engine)
# session = Session()  # --> Connection???

# # Create the databse table
# DB_models.Base.metadata.create_all(engine)

# # *********************Database Population**************************
# # Populating and loading from the databse is done through json.dumps and json.loads respectively
# ET_1 = DB_models.Project(name='ET_01')

# Array_1 = DB_models.Array_db(name='Array_0', Array_Tech='Wave', Array_number_devices=5, cmpx=1)

# Device_1 = DB_models.Device_db(name='Dev_0', Technology='Wave', Control_Strategy='Passive', MechT_type='AirTurbine',
#                      ElectT_type='SCIG', GridC_type='B2B2Level')

# PTO_1 = DB_models.PTO_db(name='PTO_0', Mech_size=json.dumps(1.52), Elect_P_rated=json.dumps(500e3), Grid_P_rated=json.dumps(300e3))

# Device_1.pto_table = [PTO_1]
# Array_1.device_table = [Device_1]
# ET_1.array_table = [Array_1]

# session.add(ET_1)  # careful when adding to an existing field as it may be doubled
# session.commit()
