# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
mecht_air = {"data":[{"attributes":{"id":1,"name":"MechT_AirT_0"},"id":1,"links":{"self":"http://localhost/api/mecht_air_turbine/1"},"relationships":{},"type":"MechtAirTurbine"},{"attributes":{"id":2,"name":"MechT_AirT_1"},"id":2,"links":{"self":"http://localhost/api/mecht_air_turbine/2"},"relationships":{},"type":"MechtAirTurbine"}],"jsonapi":{"version":"1.0"},"links":{"self":"http://localhost/api/mecht_air_turbine/?fields[MechtAirTurbine]=id,name&page[offset]=0&page[limit]=250"},"meta":{"count":2,"limit":250}}