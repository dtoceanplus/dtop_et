# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from typing import Type
from sqlalchemy.ext import mutable
from sqlalchemy.orm import backref
import json
from flask_sqlalchemy import SQLAlchemy

import pandas as pd
import matplotlib.pyplot as plt

from dtop_energytransf.storage.mecanic_data import wells_default, impulse_default, gearbox_default, hydraulic_default
from dtop_energytransf.storage.control_data import aux_adim_vel, aux_power_levels, aux_load_levels, aux_load_ranges
from ...service import db




class Project(db.Model):
    __tablename__ = "project_table"
    etId = db.Column(db.Integer, primary_key=True)
    Description = db.Column(db.JSON, nullable=False)
    Technology = db.Column(db.JSON, nullable=False)
    Type_sim = db.Column(db.JSON, nullable=False)
    ControlStrategy = db.Column(db.JSON, nullable=False)
    Mech_type = db.Column(db.JSON, nullable=False)
    Elect_type = db.Column(db.JSON, nullable=False)
    Grid_type = db.Column(db.JSON, nullable=False)
    CpxMech = db.Column(db.JSON, nullable=False)
    CpxElec = db.Column(db.JSON, nullable=False)
    CpxGrid = db.Column(db.JSON, nullable=False)
    Date = db.Column(db.JSON)
    array_table = db.relationship(
        "Array",
        uselist=False,
        back_populates="project_table",
        cascade="all, delete-orphan",
    )
    device_table = db.relationship(
        "Device", back_populates="project_table", cascade="all, delete-orphan",
    )
    pto_table = db.relationship(
        "PTO", back_populates="project_table", cascade="all, delete-orphan",
    )

    def __repr__(self):
        return "<Project(name='%s')>" % self.name

    def update(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)


class Array(db.Model):
    __tablename__ = "array_table"
    array_id = db.Column(db.Integer, primary_key=True)
    project_id = db.Column(
        db.Integer, db.ForeignKey("project_table.etId", onupdate="cascade")
    )
    id = db.Column(db.JSON)
    pto_subsystems = db.Column(db.JSON)
    Array_number_devices = db.Column(db.JSON)
    Array_Tech = db.Column(db.JSON)
    Array_Env_Conditions = db.Column(db.JSON)
    Array_P_Capt = db.Column(db.JSON)
    BoM = db.Column(db.JSON)
    Hierarchy = db.Column(db.JSON)
    Array_cost = db.Column(db.JSON)
    Array_mass = db.Column(db.JSON)
    Array_P_Mech = db.Column(db.JSON)
    Array_P_Elect = db.Column(db.JSON)
    Array_Pa_Grid = db.Column(db.JSON)
    Array_Pr_Grid = db.Column(db.JSON)
    Array_E_Mech = db.Column(db.JSON)
    Array_E_Elect = db.Column(db.JSON)
    Array_E_Grid = db.Column(db.JSON)
    Array_E_Capt = db.Column(db.JSON)  # aggiungere openapi
    materials = db.Column(db.JSON)
    Devices = db.Column(db.JSON)
    device_table = db.relationship(
        "Device", back_populates="array_table", cascade="all, delete-orphan",
    )
    project_table = db.relationship("Project", back_populates="array_table")
    pto_table = db.relationship("PTO", back_populates="array_table")


#     def __repr__(self):
#        return "<Array_db(name='%s', Technology='%s', Number of devices='%s', Complexity Level='%s')>" % (
#                             self.name, self.Array_Tech, self.Array_number_devices, self.cmpx)


class Device(db.Model):
    __tablename__ = "device_table"
    device_id = db.Column(db.Integer, primary_key=True)
    array_id = db.Column(
        db.Integer, db.ForeignKey("array_table.array_id", onupdate="cascade")
    )
    project_id = db.Column(
        db.Integer, db.ForeignKey("project_table.etId", onupdate="cascade")
    )

    Dev_Captured_Power = db.Column(db.JSON)
    Dev_Control_strat = db.Column(db.JSON)
    Dev_E_Capt = db.Column(db.JSON)
    Dev_E_Elect = db.Column(db.JSON)
    Dev_E_Grid = db.Column(db.JSON)
    Dev_E_Mech = db.Column(db.JSON)
    Dev_Elect_type = db.Column(db.JSON)
    Dev_Env_Conditions = db.Column(db.JSON)
    Dev_Grid_type = db.Column(db.JSON)
    Dev_Mech_type = db.Column(db.JSON)
    Dev_PTO_cost = db.Column(db.JSON)
    Dev_PTO_mass = db.Column(db.JSON)

    Dev_P_Capt = db.Column(db.JSON)
    Dev_P_Elect = db.Column(db.JSON)
    Dev_P_Mech = db.Column(db.JSON)
    Dev_Pa_Grid = db.Column(db.JSON)
    Dev_Pr_Grid = db.Column(db.JSON)
    Dev_cut_in_out = db.Column(db.JSON)

    Dev_dof_PTO = db.Column(db.JSON)
    Dev_elect_mass = db.Column(db.JSON)
    Dev_grid_mass = db.Column(db.JSON)
    Dev_mech_mass = db.Column(db.JSON)
    Dev_par_PTOs = db.Column(db.JSON)
    Dev_rated_power = db.Column(db.JSON)
    Dev_shutdown_flag = db.Column(db.JSON)

    Dev_tech = db.Column(db.JSON)

    id = db.Column(db.JSON)

    pto_table = db.relationship(
        "PTO", back_populates="device_table", cascade="all, delete-orphan",
    )

    # Device relationship with array
    array_table = db.relationship("Array", back_populates="device_table")

    project_table = db.relationship("Project", back_populates="device_table")


#     def __repr__(self):
#        return "<Device_db(name='%s', Technology='%s', Control strategy='%s', MechT_type='%s', ElectT_type='%s'," \
#               " GridC_type='%s')>" % (
#                             self.name, self.Technology, self.Control_Strategy, self.MechT_type, self.ElectT_type
#                             , self.GridC_type)


class PTO(db.Model):
    __tablename__ = "pto_table"
    pto_id = db.Column(db.Integer, primary_key=True)
    device_id = db.Column(
        db.Integer, db.ForeignKey("device_table.device_id", onupdate="cascade")
    )
    array_id = db.Column(
        db.Integer, db.ForeignKey("array_table.array_id", onupdate="cascade")
    )
    project_id = db.Column(
        db.Integer, db.ForeignKey("project_table.etId", onupdate="cascade")
    )

    Captured_Energy = db.Column(db.JSON)
    Captured_Power = db.Column(db.JSON)
    ElectT_Damage = db.Column(db.JSON)
    ElectT_Energy = db.Column(db.JSON)
    ElectT_Power = db.Column(db.JSON)
    Elect_P_rated = db.Column(db.JSON)
    Elect_cost = db.Column(db.JSON)
    Elect_mass = db.Column(db.JSON)
    GridC_Active_Power = db.Column(db.JSON)
    GridC_Damage = db.Column(db.JSON)
    GridC_Energy = db.Column(db.JSON)
    GridC_reactive_Power = db.Column(db.JSON)
    Grid_P_rated = db.Column(db.JSON)
    Grid_cost = db.Column(db.JSON)
    Grid_mass = db.Column(db.JSON)
    MechT_Damage = db.Column(db.JSON)
    MechT_Energy = db.Column(db.JSON)
    MechT_Load_Range = db.Column(db.JSON)
    MechT_Loads = db.Column(db.JSON)
    MechT_Power = db.Column(db.JSON)
    Mech_cost = db.Column(db.JSON)
    Mech_mass = db.Column(db.JSON)
    Mech_size = db.Column(db.JSON)
    PTO = db.Column(db.JSON)
    S_N = db.Column(db.JSON)
    ultimate_stress = db.Column(db.JSON)
    number_cycles = db.Column(db.JSON)
    maximum_stress_probability = db.Column(db.JSON)
    fatigue_stress_probability = db.Column(db.JSON)
    Grid_V = db.Column(db.JSON)
    id = db.Column(db.JSON)

    device_table = db.relationship("Device", back_populates="pto_table")

    array_table = db.relationship("Array", back_populates="pto_table")

    project_table = db.relationship("Project", back_populates="pto_table")


# New model classes below


class EnergyTransformationStudy(db.Model):
    """
    An SQLAlchemy database model class for a Energy Transformation study.
    """

    __tablename__ = "energy_transformation_study"
    id = db.Column(db.Integer, primary_key=True)
    """The ID of the Energy Transformation study"""
    name = db.Column(db.String(64), index=True,  nullable=False)
    """The name of the ET study"""
    description = db.Column(db.String(120), index=True)
    """A description of the ET study"""
    standalone = db.Column(db.Boolean, default=True)
    """Boolean value for whether ET is to be run in standalone mode"""
    status = db.Column(db.Integer, default=0)
    """Value to check the percentage of fulfilment of the study: 30% created with crossmodule data, 70% ET input data, 100% output data obtained"""
    site_characterisation_study_id = db.Column(db.Integer, default=0)
    """The ID of the linked Site Characterisation study"""
    site_characterisation_id = db.Column(db.Integer, db.ForeignKey("site_characterisation_data.id"))
    """The ID for the site characterisation data associated with the ET study"""
    site_characterisation_data = db.relationship(
        "SiteCharacterisationData",
        uselist=False,
        cascade="all, delete",
        back_populates="et_study"
    )
    """One-to-one relationship with the Site characterisation data to use"""
    machine_characterisation_study_id = db.Column(db.Integer, default=0)
    """The ID of the linked Machine Characterisation study"""
    machine_characterisation_id = db.Column(db.Integer, db.ForeignKey("machine_characterisation_data.id"))
    """The ID for the machine characterisation data associated with the ET study"""
    machine_characterisation_data = db.relationship(
        "MachineCharacterisationData",
        uselist=False,
        cascade="all, delete",
        back_populates="et_study"
    )
    """One-to-one relationship with the Machine characterisation data to use"""
    energy_capture_study_id = db.Column(db.Integer, default=0)
    """The ID of the linked Energy Capture study"""
    energy_capture_id = db.Column(db.Integer, db.ForeignKey("energy_capture_data.id"))
    """The ID for the energy capture data associated with the ET study"""
    energy_capture_data = db.relationship(
        "EnergyCaptureData",
        uselist=False,
        cascade="all, delete",
        back_populates="et_study"
    )
    """One-to-one relationship with the energy capture data to use"""
    general_inputs_id = db.Column(db.Integer, db.ForeignKey("general_inputs.id"))
    """The ID for the set of general inputs associated with the ET study"""
    general_inputs = db.relationship(
        "GeneralInputs",
        uselist=False,
        cascade="all, delete",
        back_populates="et_study"
    )
    """One-to-one relationship with the set of general inputs to use"""
    mechanical_inputs_id = db.Column(db.Integer, db.ForeignKey("mechanical_inputs.id"))
    """The ID for the set of mechanical inputs associated with the ET study"""
    mechanical_inputs = db.relationship(
        "MechanicalInputs",
        uselist=False,
        cascade="all, delete",
        back_populates="et_study"
    )
    """One-to-one relationship with the set of mechanical inputs to use"""
    electrical_inputs_id = db.Column(db.Integer, db.ForeignKey("electrical_inputs.id"))
    """The ID for the set of electrical inputs associated with the ET study"""
    electrical_inputs = db.relationship(
        "ElectricalInputs",
        uselist=False,
        cascade="all, delete",
        back_populates="et_study"
    )
    """One-to-one relationship with the set of electrical inputs to use"""
    grid_inputs_id = db.Column(db.Integer, db.ForeignKey("grid_inputs.id"))
    """The ID for the set of grid inputs associated with the ET study"""
    grid_inputs = db.relationship(
        "GridInputs",
        uselist=False,
        cascade="all, delete",
        back_populates="et_study"
    )
    """One-to-one relationship with the set of grid inputs to use"""
    control_inputs_id = db.Column(db.Integer, db.ForeignKey("control_inputs.id"))
    """The ID for the set of control inputs associated with the ET study"""
    control_inputs = db.relationship(
        "ControlInputs",
        uselist=False,
        cascade="all, delete",
        back_populates="et_study"
    )
    """One-to-one relationship with the set of control inputs to use"""
    array_results = db.relationship(
        "ArrayResults",
        uselist=False,
        cascade="all, delete",
        back_populates="et_study"
    )
    """One-to-one relationship between an ET study and ArrayResults"""
    device_results = db.relationship(
        "DeviceResults",
        cascade="all, delete",
        back_populates="et_study"
    )
    """One-to-many relationship between an ET study and DeviceResults"""
    pto_results = db.relationship(
        "PtoResults",
        cascade="all, delete",
        back_populates="et_study"
    )
    """One-to-many relationship between an ET study and PtoResults"""

    representation = db.relationship(
        "Representation",
        uselist=False,
        cascade="all, delete",
        back_populates="et_study"
    )
    """One-to-one relationship between an ET study and Representation"""

    def update(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)


class SiteCharacterisationData(db.Model):
    __tablename__ = "site_characterisation_data"
    id = db.Column(db.Integer, primary_key=True)
    et_study = db.relationship(
        "EnergyTransformationStudy",
        back_populates="site_characterisation_data",
        uselist=False,
    )
    """The relationship to an ET study; site characterisation data cannot exist without a linked ET study"""

    sc_id = db.Column(db.JSON, default=[0])
    sc_cmpx = db.Column(db.Integer, default=1)
    Hs = db.Column(db.JSON, default=[1])
    Tz = db.Column(db.JSON, default=[1])
    Occ = db.Column(db.JSON, default=[1])

    def update(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)    


class MachineCharacterisationData(db.Model):
    __tablename__ = "machine_characterisation_data"
    id = db.Column(db.Integer, primary_key=True)
    et_study = db.relationship(
        "EnergyTransformationStudy",
        back_populates="machine_characterisation_data",
        uselist=False,
    )
    """The relationship to an ET study; machine characterisation data cannot exist without a linked ET study"""

    mc_id = db.Column(db.Integer, default=0)
    technology = db.Column(db.String(20), default= "Wave")
    """Technology under study: Wave/Tidal"""
    mc_cmpx =db.Column(db.Integer, default=1)
    """Complexity level of the Machine Characterization study"""
    Cpto = db.Column(db.JSON, default=[300])
    """Cpto PTO damping. It is a float for cmpx 1 and 2, and an array for cmpx 3"""
    # variables for Wave case
    dof = db.Column(db.Integer, default=1)
    """Degrees of Freedom of the device"""
    # Variables for Tidal case
    cut_in_out = db.Column(db.JSON, default=[0.5, 5])
    """Cut in (start) and cut out (stop) speed"""
    Cp = db.Column(db.JSON, default= [0.3])
    """Power coefficient"""
    number_rotor = db.Column(db.Integer, default=1)
    # Number of rotors of the tidal device. Will be number of parallel PTOs (in wave it is GUI input) 
    rotor_diameter = db.Column(db.Float, default= 5)
    """Rotor diameter in Tidal device"""
    TSR = db.Column(db.Float, default= 5.8)
    """Tip speed ratio"""
    Ct_tidal = db.Column(db.JSON, default= [0.1, 0.3, 0.3, 0.3, 0.5, 0.1])
    """inverse of the mean rotational speed of the turbine rotor. Will be a unique value for complexities 1 and 2, and an array for complexity 3"""
    cp_ct_velocity = db.Column(db.JSON, default= [0, 0.5, 1, 2, 5, 10])
    """Velocities at which cp and ct are given. Only for cmpx3"""

    def update(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)


class EnergyCaptureData(db.Model):
    __tablename__ = "energy_capture_data"
    id = db.Column(db.Integer, primary_key=True)
    et_study = db.relationship(
        "EnergyTransformationStudy",
        back_populates="energy_capture_data",
        uselist=False,
    )
    """The relationship to an ET study; energy capture data cannot exist without a linked ET study"""

    ec_id = db.Column(db.Integer, default=0)
    ec_cmpx =db.Column(db.Integer, default=1)
    """complexity level of the Energy Capture study"""
    num_devices = db.Column(db.Integer, default= 2)
    """number of devices"""
    Captured_Power= db.Column(db.JSON, default=[[100000], [150000]])
    """Captured power per device [W]. In cmpx 3 is an array per Sea state"""
    sigma_v = db.Column(db.JSON, default= [2.3])
    """Hub velocity in Tidal devices. It is a float for cmpx 1 and 2, and an array for cmpx 3"""
    siteConditionID = db.Column(db.JSON, default=[[0],[0]])
    """Site Condition ID of the sea state. It is an integer for cmpx 1 and 2, and an array for cmpx 3"""

    def update(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

class GeneralInputs(db.Model):
    __tablename__ = "general_inputs"
    id = db.Column(db.Integer, primary_key=True)
    et_study = db.relationship(
        "EnergyTransformationStudy",
        back_populates="general_inputs",
        uselist=False,
    )
    """The relationship to an ET study; general inputs cannot exist without a linked ET study"""
    parallel_ptos = db.Column(db.Integer, default=1)
    """Number of parallel PTOs in the device"""
    dev_shut_flag = db.Column(db.Integer, default=1)
    """Minimum number of active PTOs required for the device to operate"""

    def update(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)


class MechanicalInputs(db.Model):
    __tablename__ = "mechanical_inputs"
    id = db.Column(db.Integer, primary_key=True)
    et_study = db.relationship(
        "EnergyTransformationStudy",
        back_populates="mechanical_inputs",
        uselist=False,
    )
    """The relationship to an ET study; mechanical inputs cannot exist without a linked ET study"""
    complexity_level = db.Column(db.Integer)
    """Mechanical complexity level; can be 1, 2 or 3"""
    direct_drive = db.Column(db.Boolean, default=False)
    """Boolean variable to determine if direct drive option or gearbox is used"""
    rated_power = db.Column(db.Float, default=500)
    """Simplified and Gearbox: Rated power in kW"""
    t_ratio = db.Column(db.Integer, default=10)
    """Simplified: Transmission ratio; relationship between speed of device and rotating speed for a simplified mechanical case"""
    """Gearbox: Gearbox ratio"""
    mech_t_ratio = db.Column(db.Integer, default=1)
    """Airturbine/Hydraulic: relationship between the mechanical rotation obtained from the mechanical transformation and the speed of the generator shaft"""
    diameter = db.Column(db.Float, default=1)
    """AirTurbine: Turbine diameter [m]"""
    Sowc = db.Column(db.Float, default=20)
    """AirTurbine: internal surface water level area [m2]"""
    size = db.Column(db.Float, default=1.4e-4)
    """Hydraulic: motor size [m3/rad]"""
    Ap = db.Column(db.Float, default=0.2)
    """Hydraulic: Cross section of the piston [m2]"""
    flow = db.Column(db.Float, default=1)
    """Flow portion allowed to pass through the hydraulic motor"""
    Type = db.Column(db.String(20), default= "Gearbox")
    """Mechanical Transformation Type [AirTurbine, Hydraulic, Gearbox]"""
    turb_type = db.Column(db.String(20), default= "Wells")
    """If Mechanical Transformation is Airturbine, turbine type [Wells/Impulse]"""   
    # variables from catalogue
    name = db.Column(db.String(20), default= "Please select")
    """Name of the introduced mechanical object for identification"""
    catalogue_turb = db.Column(db.JSON, default = ["turb1", "turb2"])
    catalogue_hyd = db.Column(db.JSON, default = ["hyd1", "hyd2"])
    catalogue_gb = db.Column(db.JSON, default = ["gb1", "g2"])
    wells_data = db.Column(db.JSON, default = wells_default)
    """Diccionary with all the catalogue input data for the wells turbine mechanical stage"""
    impulse_data = db.Column(db.JSON, default = impulse_default)
    """Diccionary with all the catalogue input data for the impulse turbine mechanical stage"""
    hydraulic_data = db.Column(db.JSON, default = hydraulic_default)
    """Diccionary with all the catalogue input data for the hydraulic mechanical stage"""
    gearbox_data = db.Column(db.JSON, default = gearbox_default)
    """Diccionary with all the catalogue input data for the gearbox mechanical stage"""

    def update(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)


class ElectricalInputs(db.Model):
    __tablename__ = "electrical_inputs"
    id = db.Column(db.Integer, primary_key=True)
    et_study = db.relationship(
        "EnergyTransformationStudy",
        back_populates="electrical_inputs",
        uselist=False,
    )
    """The relationship to an ET study; electrical inputs cannot exist without a linked ET study"""

    complexity_level = db.Column(db.Integer)
    """Electrical complexity level; can be 1, 2 or 3"""
    rated_power = db.Column(db.Float, default=500)
    """Electrical conversion rated power in kW"""
    # Variables from the GUI in cmpx 2 and cmpx 3
    Vnom = db.Column(db.Integer, default=400)
    """RMS L-L nominal voltage of the generator [V]"""
    fnom = db.Column(db.Integer, default=50)
    """Nominal frequency of the generator [Hz]"""
    Lgen = db.Column(db.Float, default=0.0005)
    """Inductance of the generator [Hr]"""
    Rgen = db.Column(db.Float, default=0.0001)
    """Resistance of the generator [ohm]"""
    pp = db.Column(db.Integer, default=2)
    """Number of pole pair in the generator"""
    rel_T_maxnom = db.Column(db.Float, default=2)
    """Ratio of maximum to nominal Torque"""
    rel_V_maxnom = db.Column(db.Float, default=1.725)
    """Ratio of maximum to nominal Voltage"""
    gen_class = db.Column(db.String(20), default= "Class_A")
    """Generator Class, for life calculation"""
    Type = db.Column(db.String(20), default= "SCIG")
    """Generator Type: SCIG or PMSG"""
    cos_phi = db.Column(db.JSON, default=[[0.8047,-2.6691,2.8468,-0.0541],[0.2743,-1.4198,2.1163,-0.0649],[0.409,-1.8776,2.4187,-0.0673],[-0.1545,-0.33935,1.24835,-0.0372],[-0.718,1.1989,0.078,-0.0071],[-0.735,1.25695,0.02425,-0.0044], [-0.752,1.315,-0.0295,-0.0017]])
    """obtains de cos_phi depending on the load factor and pole pairs in SCIG generators. Ref: "Impact of Pole Pair Number on the Efficiency of an Induction
    Generator for a Mini Hydro Power Plant" """
    max_cos_phi = db.Column(db.JSON, default=[0.92, 0.90, 0.88, 0.71, 0.55, 0.54, 0.53])
    """maximum cos_phi depending on the pole pairs for SCIG generators"""    
    # Variables below come from the catalogue
    catalogue_gen = db.Column(db.JSON, default = ["scig1", "pmsg2"])
    name = db.Column(db.String(20), default= "Please select")
    """Name of the introduced generator for identification"""
    x_f = db.Column(db.JSON, default=[2e-7, -1e-5, 6e-4])
    """x1, x2 and x3 linear functions coeffs of the nominal rot_speed [rpm] of the generator"""
    shaft_diam = db.Column(db.JSON, default=[0.00000000000002, -0.000000007, 0.001, 27.451])
    """Cubic function coefficients with nominal power [W]"""
    phi_cos = db.Column(db.JSON, default=[8e-15, -0.000000001, 0.00005, 0.0827])
    """cosphi[cP_act] = self.phi_cos[0] * P_act[cP_act] ** 3 + self.phi_cos[1] * P_act[cP_act] ** 2 + \
    self.phi_cos[2] * P_act[cP_act] + self.phi_cos[3]"""
    """Assumed phi_cos relation with the mechanical power [W]. Only for PMSG"""
    sigma_h = db.Column(db.Float, default= 5.01)
    sigma_e = db.Column(db.Float, default= 44.8)
    """Histeresis and eddy current loss coefficients"""
    B = db.Column(db.Float, default= 0.8)
    """Average magnetic flux density in the air gap [T]"""
    Gen_mass = db.Column(db.JSON, default=[0.0056, 55.041])
    """Generator mass [kg/W]. a and b factors in a*P_nom+b"""
    wind_mass_fraction  = db.Column(db.Float, default=0.6)
    """mass of the stator and rotor ratio from the total mass of the generator"""
    I_nom = db.Column(db.JSON, default=[0.0018, 0.934])
    """I0 and I1 factors in I0*Pnom+I1. For reliability calculations"""
    Res = db.Column(db.JSON, default=[72584, -1.236])
    """Generator R0 and R1 stator resistance factors in R0*Pnom^R1. for winding losses calculationn"""
    Life = db.Column(db.JSON, default= {"Class_A" : [-0.069, 17.1288228, 105], "Class_B" : [-0.069, 18.8974785, 130], "Class_F" : [-0.069, 20.7619675, 155], "Class_H" : [-0.069, 22.1243926, 180]})
    """ [k, k0, Temp_max] for life assessment depending on the class of the generator A, B, F, H"""
    cost = db.Column(db.JSON, default=[2.5, 0.7])
    """	B and X cost factors in  B*P_nom^(X)"""
    thick_max = db.Column(db.Float, default= 0.64)
    """Maximum thickness of the iron core steel plate"""
    Manufacturer = db.Column(db.String(50), default = "ABB")
    """Generator manufacturer"""
    Date = db.Column(db.String(20), default = "21102020")
    """Generator date of input"""

    def update(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)


class GridInputs(db.Model):
    __tablename__ = "grid_inputs"
    id = db.Column(db.Integer, primary_key=True)
    et_study = db.relationship(
        "EnergyTransformationStudy",
        back_populates="grid_inputs",
        uselist=False,
    )
    """The relationship to an ET study; grid inputs cannot exist without a linked ET study"""
    complexity_level = db.Column(db.Integer)
    """Grid complexity level; can be 1, 2 or 3"""
    rated_power = db.Column(db.Float, default=500)
    """Grid conditioning rated power in kW"""
    Type = db.Column(db.String(20), default= "B2B2level")
    """Type of the introduced power converter. For the moment only Back to Back converter"""
    #  Internal parameters for cmpx1. Not modifiable
    power_loads_norm = db.Column(db.JSON, default=[0.1, 0.2, 0.3, 0.5, 0.8, 1])
    eff_levels = db.Column(db.JSON, default=[0.97, 0.975, 0.98, 0.982, 0.981, 0.978])
    failure_rate_cpx1 = db.Column(db.Float, default=0.27)
    # Variables from the GUI in cmpx 2 and cmpx 3
    Vdc = db.Column(db.Integer, default=1200)
    """DC link voltage [V]"""
    fsw = db.Column(db.Integer, default=5000)
    """Power Converter switching frequency [Hz]"""
    Vgrid = db.Column(db.Integer, default=690)
    """Grid rms voltage [V]"""
    Rgrid = db.Column(db.Float, default=0.0001)
    """Grid connection resistance [ohm]"""
    cosfi = db.Column(db.Float, default=1.0)
    """Required cosfi in the injected power into the grid"""
    Lgrid = db.Column(db.Float, default=0.001)
    """Grid connection inductance [Hr]"""
    Fgrid = db.Column(db.Integer, default=50)
    """Grid Frequency [Hz]"""

    # Variables below come from the catalogue
    catalogue_grid = db.Column(db.JSON, default = ["B2B2level1", "B2B2level2"])
    name = db.Column(db.String(20), default= "Please select")
    """Name of the introduced power converter for identification"""
    Manufacturer = db.Column(db.String(50), default = "INFINEON")
    """Grid converter manufacturer"""
    Date = db.Column(db.String(20), default=21102020)
    """Grid conditioning date of input"""
    cost = db.Column(db.Float, default=0.1)
    """Grid conditioning cost [€/W]"""
    life = db.Column(db.JSON, default=[2E+19, -7.885])
    """Grid conditioning data for life assessment k[] and k0[]"""
    temp = db.Column(db.JSON, default=[0, 40, 120])
    """Grid conditioning temperature data for life assessment temp[temp[ºC],temp_rated[ºC],temp_max[ºC]]"""
    mass = db.Column(db.JSON, default= {"Copper":0.000137, "Plastic":0.00006, "Steel": 0.000326, "Iron": 0.00005, "Zink": 0.00002})
    "Grid conditioning mass data for different materials in [kg/W]"
    IGBT150 = db.Column(db.JSON, default= [0.85,0.011497,0.008490,0.0006578,-0.0000002294,900])
    """IGBT "Vceo", "Rce", "a", "b", "c", "Vnom" values for a 150 A IGBT in Grid conditioning """
    Diode150 = db.Column(db.JSON, default=[0.81, 0.006625, 0.0110730, 0.0002368, -0.0000003392, 900])
    """Diode "Vfo", "Rt", "a", "b", "c", "Vnom" values for a 150 A Diode in Grid conditioning """
    IGBT450 = db.Column(db.JSON, default= [0.85, 0.00358333, 0.06026, 0.0003002, 0.0000006978, 900])
    """IGBT "Vceo", "Rce", "a", "b", "c", "Vnom" values for a 450 A IGBT in Grid conditioning """
    Diode450 = db.Column(db.JSON, default=[0.8, 0.00271605, 0.02424, 0.0002509, 0.0000001585, 900])
    """Diode "Vfo", "Rt", "a", "b", "c", "Vnom" values for a 450 A Diode in Grid conditioning """
    IGBT800 = db.Column(db.JSON, default=[1.15, 0.00178125, 0.0977, 0.00028, 0.000000347, 900])
    """IGBT "Vceo", "Rce", "a", "b", "c", "Vnom" values for a 800 A IGBT in Grid conditioning """
    Diode800 = db.Column(db.JSON, default=[0.875, 0.0011, 0.049, 0.000352, -0.000000105, 900])
    """Diode "Vfo", "Rt", "a", "b", "c", "Vnom" values for a 800 A Diode in Grid conditioning """
    IGBT1600 = db.Column(db.JSON, default=[1, 0.001, 0.258, 0.000275, 0.000000163, 900])
    """IGBT "Vceo", "Rce", "a", "b", "c", "Vnom" values for a 1600 A IGBT in Grid conditioning """
    Diode1600 = db.Column(db.JSON, default=[0.85, 0.000589, 0.095, 0.000314, -0.00000004, 900])
    """Diode "Vfo", "Rt", "a", "b", "c", "Vnom" values for a 1600 A Diode in Grid conditioning """

    def update(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

class ControlInputs(db.Model):
    __tablename__ = "control_inputs"
    id = db.Column(db.Integer, primary_key=True)
    et_study = db.relationship(
        "EnergyTransformationStudy",
        back_populates="control_inputs",
        uselist=False,
    )
    """The relationship to an ET study; control inputs cannot exist without a linked ET study"""
    complexity_level = db.Column(db.Integer)
    """Control complexity level will be the same as for Mechanical; can be 1, 2 or 3. Only cmpx 3 will allow user defined control"""
    # Default inputs for Control CPX level 1, GUI inputs in cmpx 2 and 3
    control_type = db.Column(db.String(15), default="Passive")
    """Control can be Passive for cmpx 1 and 2 or User defined for cmpx 3 of Mechanical stage"""
    n_sigma = db.Column(db.Integer, default=5)
    # viene de una distribución normal
    bins = db.Column(db.Integer, default=500)
    # Sea State resolution. (Number of points to define the Sea State)
    vel_mean = db.Column(db.Float, default=1e-6)
    # fixed non-zero minimum value to compute the control 
    # Inputs from the catalogue
    catalogue_cont = db.Column(db.JSON, default = ["control1"])
    adim_vel = db.Column(db.JSON, default= aux_adim_vel)
    power_levels = db.Column(db.JSON, default= aux_power_levels)
    """One degree of freedom Chi Square distribution"""
    load_levels = db.Column(db.JSON, default= aux_load_levels)
    """Folded normal distribution"""
    load_ranges = db.Column(db.JSON, default= aux_load_ranges)
    """Rayleigh distribution"""
    date = db.Column(db.String(20), default = "21102020")
    """Control date of input"""
    name = db.Column(db.String(20), default= "Please select")
    """ Name of the introduced control for identification"""    
    
    def update(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)


class ArrayResults(db.Model):
    __tablename__ = "array_results"
    array_id = db.Column(db.Integer, primary_key=True)
    et_study_id = db.Column(
        db.Integer, db.ForeignKey("energy_transformation_study.id", onupdate="cascade")
    )
    et_study = db.relationship("EnergyTransformationStudy", back_populates="array_results")

    device_results = db.relationship(
        "DeviceResults", back_populates="array_results", cascade="all, delete-orphan",
    )
    pto_results = db.relationship("PtoResults", back_populates="array_results")

    id = db.Column(db.JSON)
    pto_subsystems = db.Column(db.JSON)
    Array_number_devices = db.Column(db.JSON)
    Array_Tech = db.Column(db.JSON)
    Array_Env_Conditions = db.Column(db.JSON)
    Array_P_Capt = db.Column(db.JSON)
    BoM = db.Column(db.JSON)
    Hierarchy = db.Column(db.JSON)
    Array_cost = db.Column(db.JSON)
    Array_mass = db.Column(db.JSON)
    Array_P_Mech = db.Column(db.JSON)
    Array_P_Elect = db.Column(db.JSON)
    Array_Pa_Grid = db.Column(db.JSON)
    Array_Pr_Grid = db.Column(db.JSON)
    Array_E_Mech = db.Column(db.JSON)
    Array_E_Elect = db.Column(db.JSON)
    Array_E_Grid = db.Column(db.JSON)
    Array_E_Capt = db.Column(db.JSON)  # aggiungere openapi
    materials = db.Column(db.JSON)
    Devices = db.Column(db.JSON)
    SeaStates = db.Column(db.JSON) #new, add to openapi


class DeviceResults(db.Model):
    __tablename__ = "device_results"
    device_id = db.Column(db.Integer, primary_key=True)
    array_results_id = db.Column(
        db.Integer, db.ForeignKey("array_results.array_id", onupdate="cascade")
    )
    array_results = db.relationship("ArrayResults", back_populates="device_results")
    et_study_id = db.Column(
        db.Integer, db.ForeignKey("energy_transformation_study.id", onupdate="cascade")
    )
    et_study = db.relationship("EnergyTransformationStudy", back_populates="device_results")

    pto_results = db.relationship(
        "PtoResults", back_populates="device_results", cascade="all, delete-orphan",
    )

    Dev_Captured_Power = db.Column(db.JSON)
    Dev_Control_strat = db.Column(db.JSON)
    Dev_E_Capt = db.Column(db.JSON)
    Dev_E_Elect = db.Column(db.JSON)
    Dev_E_Grid = db.Column(db.JSON)
    Dev_E_Mech = db.Column(db.JSON)
    Dev_Elect_type = db.Column(db.JSON)
    Dev_Env_Conditions = db.Column(db.JSON)
    Dev_Grid_type = db.Column(db.JSON)
    Dev_Mech_type = db.Column(db.JSON)
    Dev_PTO_cost = db.Column(db.JSON)
    Dev_PTO_mass = db.Column(db.JSON)

    Dev_P_Capt = db.Column(db.JSON)
    Dev_P_Elect = db.Column(db.JSON)
    Dev_P_Mech = db.Column(db.JSON)
    Dev_Pa_Grid = db.Column(db.JSON)
    Dev_Pr_Grid = db.Column(db.JSON)
    Dev_cut_in_out = db.Column(db.JSON)

    Dev_dof_PTO = db.Column(db.JSON)
    Dev_elect_mass = db.Column(db.JSON)
    Dev_grid_mass = db.Column(db.JSON)
    Dev_mech_mass = db.Column(db.JSON)
    Dev_par_PTOs = db.Column(db.JSON)
    Dev_rated_power = db.Column(db.JSON)
    Dev_shutdown_flag = db.Column(db.JSON)

    Dev_tech = db.Column(db.JSON)

    id = db.Column(db.JSON)


class PtoResults(db.Model):
    __tablename__ = "pto_results"
    pto_id = db.Column(db.Integer, primary_key=True)
    device_results_id = db.Column(
        db.Integer, db.ForeignKey("device_results.device_id", onupdate="cascade")
    )
    device_results = db.relationship("DeviceResults", back_populates="pto_results")
    array_results_id = db.Column(
        db.Integer, db.ForeignKey("array_results.array_id", onupdate="cascade")
    )
    array_results = db.relationship("ArrayResults", back_populates="pto_results")
    et_study_id = db.Column(
        db.Integer, db.ForeignKey("energy_transformation_study.id", onupdate="cascade")
    )
    et_study = db.relationship("EnergyTransformationStudy", back_populates="pto_results")

    Captured_Energy = db.Column(db.JSON)
    Captured_Power = db.Column(db.JSON)
    ElectT_Damage = db.Column(db.JSON)
    ElectT_Energy = db.Column(db.JSON)
    ElectT_Power = db.Column(db.JSON)
    Elect_P_rated = db.Column(db.JSON)
    Elect_cost = db.Column(db.JSON)
    Elect_mass = db.Column(db.JSON)
    GridC_Active_Power = db.Column(db.JSON)
    GridC_Damage = db.Column(db.JSON)
    GridC_Energy = db.Column(db.JSON)
    GridC_reactive_Power = db.Column(db.JSON)
    Grid_P_rated = db.Column(db.JSON)
    Grid_cost = db.Column(db.JSON)
    Grid_mass = db.Column(db.JSON)
    MechT_Damage = db.Column(db.JSON)
    MechT_Energy = db.Column(db.JSON)
    MechT_Load_Range = db.Column(db.JSON)
    MechT_Loads = db.Column(db.JSON)
    MechT_Power = db.Column(db.JSON)
    Mech_cost = db.Column(db.JSON)
    Mech_mass = db.Column(db.JSON)
    Mech_size = db.Column(db.JSON)
    MechT_omega = db.Column(db.JSON) #new, add to openapi
    ElecT_omega = db.Column(db.JSON) #new, add to openapi
    MechT_eff = db.Column(db.JSON) #new, add to openapi
    ElecT_eff = db.Column(db.JSON) #new, add to openapi
    GridC_eff = db.Column(db.JSON) #new, add to openapi
    PTO = db.Column(db.JSON)
    S_N = db.Column(db.JSON)
    ultimate_stress = db.Column(db.JSON)
    number_cycles = db.Column(db.JSON)
    maximum_stress_probability = db.Column(db.JSON)
    fatigue_stress_probability = db.Column(db.JSON)
    Grid_V = db.Column(db.JSON)
    id = db.Column(db.JSON)

class Representation(db.Model):
    __tablename__ = "representation"
    representation_id = db.Column(db.Integer, primary_key=True)
    et_study_id = db.Column(
        db.Integer, db.ForeignKey("energy_transformation_study.id", onupdate="cascade")
    )
    et_study = db.relationship("EnergyTransformationStudy", back_populates="representation")


    array = db.Column(db.JSON)
    device = db.Column(db.JSON)
    pto = db.Column(db.JSON)
    mechanical_conversion = db.Column(db.JSON)
    electrical_conversion = db.Column(db.JSON)
    grid_conditioning = db.Column(db.JSON)

    def update(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

