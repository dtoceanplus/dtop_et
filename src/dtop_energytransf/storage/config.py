# This is is the Energy Transformation Module (ET) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Eider Robles, Imanol Touzon, Joseba Lopez, Luca Grispiani, Vincenzo Nava, Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from dtop_energytransf import business

from os import environ, path

basedir = path.abspath(path.dirname(__file__))


class Config:
    """Set Flask configuration vars from .env file."""

    # General Config
    SECRET_KEY = environ.get("SECRET_KEY")
    FLASK_APP = environ.get("FLASK_APP")
    FLASK_ENV = environ.get("FLASK_ENV")

    # Database
    SQLALCHEMY_DATABASE_URI = environ.get("DATABASE_URL") or "sqlite:///" + path.join(
        basedir, "..", "Databases", "ET_DB_test.db"
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False


# TODO: move this global variable to the database
PROJECT_OBJECTS = {}

# TODO: REVIEW
"""empty_Study = {"name": ""}
empty_Wells = {"name": ""}
empty_Impulse = {"name": ""}
empty_SCIG = {"name": ""}
empty_PowerConverter = {"name": ""}
empty_Gearbox = {"name": ""}
empty_Hydraulic = {"name": ""}
empty_Control = {"name": ""}
empty_Outputs = {"name": ""}"""
empty_ET_db = {"name": ""}
empty_Array_db = {"name": ""}
empty_Device_db = {"name": ""}
empty_PTO_db = {"name": ""}

# __all_projects = Project.query.all()

# populate the PROJECT_OBJECTS dictionary with the test projects
# for project in __all_projects:
#     project_object = business.ArrayFactory.get_array(project.type.upper(),
#                                                      str(project.complexity))

#     PROJECT_OBJECTS[str(project.id)] = project_object
