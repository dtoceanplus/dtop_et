class  SpeyStudyPage{
    // static STUDY_TITLE = 'Study name'
    // static STUDY_DESC = 'Study description'
    // static STUDY_TITLE_EDIT = 'Study edited name'
    // static STUDY_DESC_EDIT = 'Study edited description'

    // configureRoute() {
    //     cy.server()
    //     cy.route('GET', '/spey').as('getStudies')
    // }

    // createStudy() {
    //     this.configureRoute();
    //     cy.visit('/#/spey')
    //     cy.get('[data-cy-spey=createSpeyStudyBtn]').click()
    //     cy.get('[data-cy-spey=createSpeyName]').type(SpeyStudyPage.STUDY_TITLE)
    //     cy.get('[data-cy-spey=createSpeyDesc]').type(SpeyStudyPage.STUDY_DESC)
    //     cy.get('[data-cy-spey=submitCreateButton]').click()
    //     cy.wait('@getStudies')
    //     cy.contains(SpeyStudyPage.STUDY_TITLE)
    //     cy.contains(SpeyStudyPage.STUDY_DESC)
    // }

    // editStudy(studyId) {
    //     this.configureRoute();
    //     cy.get(`[data-cy-spey=editStudyBtn${studyId}]`).click()
    //     cy.get('[data-cy-spey=editStudyFormName]').clear().type(SpeyStudyPage.STUDY_TITLE_EDIT)
    //     cy.get('[data-cy-spey=editStudyFormDesc]').clear().type(SpeyStudyPage.STUDY_DESC_EDIT)
    //     cy.get('[data-cy-spey=confirmUpdateStudyBtn]').click()
    //     // cy.wait('@getStudies')
    //     cy.contains(SpeyStudyPage.STUDY_TITLE_EDIT)
    //     cy.contains(SpeyStudyPage.STUDY_DESC_EDIT)
    // }

    // deleteStudy(studyId) {
    //     this.configureRoute();
    //     cy.get(`[data-cy-spey=deleteStudyBtn${studyId}]`).click()
    //     cy.get('[data-cy-spey=submitDeleteButton]').click()
    //     // cy.wait('@getStudies')
    //     cy.get('.el-table__row').should('not.contain', SpeyStudyPage.STUDY_TITLE_EDIT)
    // }

}
export default SpeyStudyPage;
