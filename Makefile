export DTOP_DOMAIN = dto.test

run_moks:
	docker-compose --f docker-compose.test.yml -f docker-compose.yml up sc.dto.test

impose:
	make -f ci.makefile et-deploy-docker
	docker-compose run --rm contracts

impose-local:
	make -f ci.makefile et-deploy
	docker-compose run --rm contracts

verify:
	make -f ci.makefile et-deploy-docker
	docker-compose --project-name et -f verifiable.docker-compose -f verifier.docker-compose run verifier
	docker-compose --project-name et -f verifiable.docker-compose stop et
	docker cp `docker-compose --project-name et -f verifiable.docker-compose ps -q`:/app/.coverage.make-verify
