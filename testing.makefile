SHELL:=bash

clean:
	conda remove --name dtop_energytransf --all

reset: clean
	conda env create --name dtop_energytransf --file environment.yml --force
	conda env list


# -------------------------------------------
# Run Pytest tests locally

x-rm-dist:
	rm -rf ./dist

x-create-dist:
	python setup.py sdist

x-uninstall:
	pip uninstall --yes dtop_energytransf

x-install: x-uninstall x-rm-dist x-create-dist
	pip install dtop_energytransf --no-cache-dir --find-links dist

pytest: x-pytest-prepare x-pytest

x-pytest-prepare:
	pip install jsonschema
	pip install jsonpath-rw
	pip install pact-python
	pip install pyyaml
	pip install pytest pytest-cov
	pip install dtop_energytransf --no-cache-dir --find-links dist

x-pytest:
	export DATABASE_URL=sqlite:///../Databases/ET_DB_test.db; python -m pytest --cov=dtop_energytransf --junitxml=report.xml

pytest-local:
	export DATABASE_URL=sqlite:///../Databases/ET_DB_test.db; python -m pytest --cov=dtop_energytransf test --junitxml=report.xml


# -------------------------------------------
# Run Dredd tests locally

dredd-local: x-dredd-prepare x-dredd-local

x-dredd-prepare:
	pip install python-dotenv
	pip install dredd_hooks
	npm uninstall dredd --global
	npm install dredd@12.2.1 --global

x-dredd-local:
	flask init-db
	bash -c "dredd --config dredd-local.yml"
