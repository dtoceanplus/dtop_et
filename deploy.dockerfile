FROM python:3.8-slim-buster

COPY requirements.txt .
COPY setup.py .
COPY src/dtop_energytransf/ ./src/dtop_energytransf/


RUN apt-get update && \
    apt-get install --yes --no-install-recommends gcc libc6-dev && \
    pip install --requirement requirements.txt && \
    pip install --editable . && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get purge --auto-remove --yes gcc libc6-dev

ENV FLASK_APP dtop_energytransf.service
ENV DATABASE_URL sqlite:///..\Databases\ET_DB_test.db
EXPOSE 5000
RUN flask init-db

CMD flask run --host 0.0.0.0
