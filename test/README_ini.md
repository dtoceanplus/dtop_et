# Overview

For testing we will use [pytest](https://docs.pytest.org/) framework.

Each fest file starts with `test_` (e.g. [business/test_foo.py](business/test_foo.py)).

You can use subdirectories (e.g. [business](business)).

Each test function starts with `test_` (e.g. [def test_foo(): ...](business/test_foo.py)).

Documentation:
- [pytest](https://docs.pytest.org/)
- [Install pytest](https://docs.pytest.org/en/latest/getting-started.html#install-pytest)
- [Run pytest](https://docs.pytest.org/en/latest/usage.html#calling-pytest-through-python-m-pytest)
