import atexit

import pytest
from pact import Consumer, Like, Provider, Term, EachLike

from dtop_energytransf.service import create_app


@pytest.fixture
def app():
    app = create_app({"TESTING": True})
    yield app


@pytest.fixture
def client(app):
    client = app.test_client()
    yield client

pact = Consumer("et").has_pact_with(Provider("sc"), port=1236)
pact.start_service()
atexit.register(pact.stop_service)

# @pytest.mark.skip()
def test_sc_cpx1(client):
    pact.given("sc 1 exists and it has basic wave statistics").upon_receiving(
        "a request for sc parameters at cpx1"
    ).with_request("GET", Term(r"/sc/\d+/point/statistics/waves/Basic", "/sc/1/point/statistics/waves/Basic")).will_respond_with(
        200, body=Like({"hs":Like({"mean":1}),"tp":Like({"mean":1})})
    )

    with pact:

        response = client.post("/sc_1", json={"sc_1": f"{pact.uri}/sc/1/point/statistics/waves/Basic"})
        assert response.status_code == 201

# @pytest.mark.skip()
def test_sc_wec_cpx2_3(client):
    pact.given("sc 1 exists and it has hs_tp_dp_flattened").upon_receiving(
        "a request for sc WEC parameters at cpx 2 3"
    ).with_request("GET", Term(r"/sc/\d+/point/statistics/waves/EJPD3v/hs_tp_dp_flattened", "/sc/1/point/statistics/waves/EJPD3v/hs_tp_dp_flattened")).will_respond_with(
        200, body=Like({"bins1":EachLike("0.0-0.5"),"bins2": EachLike("0.0-0.5"), "id": EachLike(1),"pdf": EachLike(0.5)})
    )

    with pact:
        response = client.post("/sc_wec_2_3", json={"sc_wec_2_3": f"{pact.uri}/sc/1/point/statistics/waves/EJPD3v/hs_tp_dp_flattened"})
        assert response.status_code == 201


# @pytest.mark.skip()
def test_sc_tec_cpx3(client):
    pact.given("sc 1 exists and it has currents scenarii matrices").upon_receiving(
        "a request for sc TEC parameters at cpx 3"
    ).with_request("GET", Term(r"/sc/\d+/farm/scenarii_matrices/currents", "/sc/1/farm/scenarii_matrices/currents")).will_respond_with(
        200, body=Like({"p": EachLike(0.4)})
    )

    with pact:
        response = client.post("sc_tec_cpx3", json={"sc_tec_cpx3": f"{pact.uri}/sc/1/farm/scenarii_matrices/currents"})
        assert response.status_code == 201
