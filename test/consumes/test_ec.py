import atexit

import pytest
from pact import Consumer, Like, Provider, Term, EachLike

from dtop_energytransf.service import create_app


@pytest.fixture
def app():
    app = create_app({"TESTING": True})
    yield app


@pytest.fixture
def client(app):
    client = app.test_client()
    yield client


# @pytest.mark.skip()
pact = Consumer("et").has_pact_with(Provider("ec"), port=1234)
pact.start_service()
atexit.register(pact.stop_service)

def test_ec_id(client):
    pact.given("ec 1 exists").upon_receiving(
        "a request for id"
    ).with_request("GET", Term(r"/ec/\d", "/ec/1")).will_respond_with(
        200, body=Like({"complexity": 1, "type": "WEC"})
    )
    
    with pact:

        response = client.post("/ec_id", json={"ec_id": f"{pact.uri}/ec/1"})
        assert response.status_code == 201

# @pytest.mark.skip()
def test_ec_farm(client):
    pact.given("ec 1 exists and it has farm").upon_receiving(
        "a request for farm"
    ).with_request("GET", Term(r"/ec/\d+/farm", "/ec/1/farm")).will_respond_with(
        200, body=Like({"number_devices": 2})
    )

    with pact:

        response = client.post("/ec_farm", json={"ec_farm": f"{pact.uri}/ec/1/farm"})
        assert response.status_code == 201

# @pytest.mark.skip()
def test_ec_devices(client):

    pact.given("ec 1 exists and it has devices").upon_receiving(
        "a request for devices"
    ).with_request("GET", Term(r"/ec/\d+/devices", "/ec/1/devices")).will_respond_with(
        200, body= ([{"captured_power_per_condition": Like({"capturedPower": EachLike(10001), "siteConditionID": EachLike(2)})}])
    )

    with pact:

        response = client.post("/ec_devices", json={"ec_devices": f"{pact.uri}/ec/1/devices"})
        assert response.status_code == 201



# @pytest.mark.skip()
def test_ec_tec_cpx1_velocity_hub(client):

    pact.given("ec 1 exists and it has tidal at complexity 1").upon_receiving(
        "a request for hub velocity at cpx 1"
    ).with_request("GET", Term(r"/ec/\d+/results/tec/complexity1", "/ec/1/results/tec/complexity1")).will_respond_with(
        200, body= Like({"array_velocity_field": EachLike({"rotorID": 1, "hub_velocity": EachLike(2.3)})})
    )

    with pact:

        response = client.post("/ec_tec1_hub", json={"ec_tec1_hub": f"{pact.uri}/ec/1/results/tec/complexity1"})
        assert response.status_code == 201


# @pytest.mark.skip()
def test_ec_tec_cpx2_velocity_hub(client):

    pact.given("ec 1 exists and it has tidal at complexity 2").upon_receiving(
        "a request for hub velocity at cpx 2"
    ).with_request("GET", Term(r"/ec/\d+/results/tec/complexity2", "/ec/1/results/tec/complexity2")).will_respond_with(
        200, body= Like({"array_velocity_field": EachLike({"rotorID": 1, "hub_velocity": EachLike(2.3)})})
    )

    with pact:

        response = client.post("/ec_tec2_hub", json={"ec_tec2_hub": f"{pact.uri}/ec/1/results/tec/complexity2"})
        assert response.status_code == 201


# @pytest.mark.skip()
def test_ec_tec_cpx3_velocity_hub(client):

    pact.given("ec 1 exists and it has tidal at complexity 3").upon_receiving(
        "a request for hub velocity at cpx 3"
    ).with_request("GET", Term(r"/ec/\d+/results/tec/complexity3", "/ec/1/results/tec/complexity3")).will_respond_with(
       200, body= Like({"array_velocity_field": EachLike({"rotorID": 1, "hub_velocity": EachLike(2.3)})})
    )

    with pact:

        response = client.post("/ec_tec3_hub", json={"ec_tec3_hub": f"{pact.uri}/ec/1/results/tec/complexity3"})
        assert response.status_code == 201




# @pytest.mark.skip()
# def test_ec_tec_main_dim_device(client):

#     pact.given("ec 1 exists and it has input tidal at complexity 1").upon_receiving(
#         "a request for main dimension device"
#     ).with_request("GET", Term(r"/ec/\d+/inputs/machine/tec/complexity1", "/ec/1/inputs/machine/tec/complexity1")).will_respond_with(
#         200, body=Like({"main_dim_device": 5})
#     )

#     with pact:
#         response = client.post(
#             "/ec_tec1", json={"ec_tec1": f"{pact.uri}/ec/1/inputs/machine/tec/complexity1"}
#         )
#         assert response.status_code == 201

# @pytest.mark.skip()
# def test_ec_tec_cpx2(client):
#     pact.given("ec 1 exists and it has input tidal at complexity 2").upon_receiving(
#         "a request for rotor diameter at cpx2"
#     ).with_request("GET", Term(r"/ec/\d+/inputs/machine/tec/complexity2", "/ec/1/inputs/machine/tec/complexity2")).will_respond_with(
#         200, body=Like({"rotor_diameter": 10})
#     )

#     with pact:
#         response = client.post(
#             "/ec_tec2", json={"ec_tec2": f"{pact.uri}/ec/1/inputs/machine/tec/complexity2"})
#         assert response.status_code == 201

# @pytest.mark.skip()
# def test_ec_tec_cpx3(client):
#     pact.given("ec 1 exists and it has input tidal at complexity 3").upon_receiving(
#         "a request for rotor diameter at cpx3"
#     ).with_request("GET", Term(r"/ec/\d+/inputs/machine/tec/complexity3", "/ec/1/inputs/machine/tec/complexity3")).will_respond_with(
#         200, body=Like({"rotor_diameter": 10})
#     )

#     with pact:
#         response = client.post(
#             "/ec_tec3", json={"ec_tec3": f"{pact.uri}/ec/1/inputs/machine/tec/complexity3"})
#         assert response.status_code == 201
