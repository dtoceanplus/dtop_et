import atexit

import pytest
from pact import Consumer, Like, Provider, Term, EachLike

from dtop_energytransf.service import create_app


@pytest.fixture
def app():
    app = create_app({"TESTING": True})
    yield app


@pytest.fixture
def client(app):
    client = app.test_client()
    yield client

pact = Consumer("et").has_pact_with(Provider("mc"), port=1235)
pact.start_service()
atexit.register(pact.stop_service)

# @pytest.mark.skip()
def test_mc_id(client):
    pact.given("mc 1 exists").upon_receiving(
        "a request for id"
    ).with_request("GET", Term(r"/mc/\d", "/mc/1")).will_respond_with(
        200, body=Like({"complexity": 1, "type": "WEC"})
    )
    
    with pact:

        response = client.post("/mc_id", json={"mc_id": f"{pact.uri}/mc/1"})
        assert response.status_code == 201

# @pytest.mark.skip()
def test_mc_dim(client):
    pact.given("mc 1 exists and it has dimensions").upon_receiving(
        "a request for id"
    ).with_request("GET", Term(r"/mc/\d+/dimensions", "/mc/1/dimensions")).will_respond_with(
        200, body=Like({"characteristic_dimension": 8})
    )
    
    with pact:

        response = client.post("/mc_dim", json={"mc_dim": f"{pact.uri}/mc/1/dimensions"})
        assert response.status_code == 201

# @pytest.mark.skip()
def test_mc_pto_damping_cpx1(client):
    pact.given("mc 1 exists and it has model wec1").upon_receiving(
        "a request for pto damping at cpx1"
    ).with_request("GET", Term(r"/mc/\d+/model/wec/complexity1", "/mc/1/model/wec/complexity1")).will_respond_with(
        200, body=Like({"pto_damping": EachLike(1000000)})
    )

    with pact:

        response = client.post("/mc_pto_damping_cpx1", json={"mc_pto_damping_cpx1": f"{pact.uri}/mc/1/model/wec/complexity1"})
        assert response.status_code == 201

# @pytest.mark.skip()
def test_mc_pto_damping_cpx2(client):

    pact.given("mc 1 exists and it has model wec2").upon_receiving(
        "a request for pto damping at cpx2"
    ).with_request("GET", Term(r"/mc/\d+/model/wec/complexity2", "/mc/1/model/wec/complexity2")).will_respond_with(
        200, body=Like({"pto_damping": EachLike(1000000)})
    )

    with pact:

        response = client.post("/mc_pto_damping_cpx2", json={"mc_pto_damping_cpx2": f"{pact.uri}/mc/1/model/wec/complexity2"})
        assert response.status_code == 201

# @pytest.mark.skip()
def test_mc_pto_damping_cpx3(client):

    pact.given("mc 1 exists and it has model wec3").upon_receiving(
        "a request for pto damping at cpx3"
    ).with_request("GET", Term(r"/mc/\d+/model/wec/complexity3", "/mc/1/model/wec/complexity3")).will_respond_with(
        200, body=Like({"pto_damping": EachLike(EachLike(EachLike(EachLike(EachLike(1000000)))))})
    )

    with pact:

        response = client.post("/mc_pto_damping_cpx3", json={"mc_pto_damping_cpx3": f"{pact.uri}/mc/1/model/wec/complexity3"})
        assert response.status_code == 201

# @pytest.mark.skip()
def test_mc_tec_cpx1(client):
    pact.given("mc 1 exists and it has tec at cpx1").upon_receiving(
        "a request for cpx1 parameters"
    ).with_request("GET", Term(r"/mc/\d+/model/tec/complexity1", "/mc/1/model/tec/complexity1")).will_respond_with(
        200, body=Like({"cp": 0.3, "number_rotor": 1})
    )

    with pact:

        response = client.post("/mc_tec_cpx1", json={"mc_tec_cpx1": f"{pact.uri}/mc/1/model/tec/complexity1"})
        assert response.status_code == 201

# @pytest.mark.skip()
def test_mc_tec_cpx2(client):
    pact.given("mc 1 exists and it has tec at cpx2").upon_receiving(
        "a request for cpx2 parameters"
    ).with_request("GET", Term(r"/mc/\d+/model/tec/complexity2", "/mc/1/model/tec/complexity2")).will_respond_with(
        200, body=Like({"cp": 0.3, "number_rotor": 1, "tip_speed_ratio": 1, "ct": 0.4, "cut_in_velocity": 1, "cut_out_velocity": 10})
    )

    with pact:

        response = client.post("/mc_tec_cpx2", json={"mc_tec_cpx2": f"{pact.uri}/mc/1/model/tec/complexity2"})
        assert response.status_code == 201

# @pytest.mark.skip()
def test_mc_tec_cpx3(client):
    pact.given("mc 1 exists and it has tec at cpx3").upon_receiving(
        "a request for cpx3 parameters"
    ).with_request("GET", Term(r"/mc/\d+/model/tec/complexity3", "/mc/1/model/tec/complexity3")).will_respond_with(
        200, body=Like({"cp": EachLike(0.5), "number_rotor": 1, "tip_speed_ratio": 1, "ct": EachLike(0.5), "cut_in_velocity": 1, "cut_out_velocity": 10, "cp_ct_velocity": EachLike(0.5)})
    )

    with pact:
        response = client.post("/mc_tec_cpx3", json={"mc_tec_cpx3": f"{pact.uri}/mc/1/model/tec/complexity3"})
        assert response.status_code == 201
