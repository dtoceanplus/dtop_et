from dtop_energytransf.business.et_studies import (
    create_study,
    update_study,
    delete_study,
)
from dtop_energytransf.service.api.studies import get_et_study_list


STUDY_DATA = {
    "name": "test study",
    "description": "study description",
    "standalone": True,
    "status": 0,
    "site_characterisation_study_id": 1,
    "machine_characterisation_study_id": 2,
    "energy_capture_study_id": 3,
}
STUDY_DATA_ID = STUDY_DATA.copy()
STUDY_DATA_ID["id"] = 1
STUDY_DATA_ID["status"] = 40
UPDATED_STUDY_DATA = {
    "name": "updated test study",
    "description": "new study description",
    "standalone": True,
    "status": 0,
    "site_characterisation_study_id": 2,
    "machine_characterisation_study_id": 1,
    "energy_capture_study_id": 4,
}
UPDATED_STUDY_DATA_ID = UPDATED_STUDY_DATA.copy()
UPDATED_STUDY_DATA_ID["id"] = 1

DEFAULT_SC_DATA = {
    "technology" : "WEC",
    "complexity": 1,
    "Hs": [1],
    "Tp": [1],
    "Occ": [1],
    "id": [1],
}
DEFAULT_MC_DATA = {
    "technology": "WEC",
    "complexity": 1,
    "id": 1,
    "pto_damping": [1000000]
}
DEFAULT_EC_DATA = {
    "technology": "WEC",    
    "complexity": 1,
    "id": 1,
    "number_devices": 2,
    "captured_power_per_condition": {
        "capturedPower": [
            10001]
    },
    "siteConditionID": 2,
}
UPDATED_SC_DATA = {
    "technology": "WEC",
    "complexity": 2,
    "Hs": [2],
    "Tp": [2],
    "Occ": [2],
    "id": [2],
}
UPDATED_MC_DATA = {
    "technology": "WEC",
    "complexity": 2,
    "id": 2,
    "pto_damping": [1000000]
}
UPDATED_EC_DATA = {
    "complexity": 1,
    "id": 2,
    "number_devices": 3,
    "captured_power_per_condition": {
        "capturedPower": [
            10001]
    },
    "siteConditionID": 2,
}


def get_study_list():

    return get_et_study_list()["et_studies"]


def test_no_studies_at_start(app):
    with app.app_context():
        study_list = get_study_list()
        assert study_list == []


def test_create_study(app):
    with app.app_context():
        create_study(STUDY_DATA)
        study_list = get_study_list()
        assert study_list == [STUDY_DATA_ID]


def test_create_study_integrity_error(app):
    with app.app_context():
        create_study(STUDY_DATA)
        error = create_study(STUDY_DATA)
        assert error == "Energy Transformation study with that name already exists."


def test_update_study(app):
    with app.app_context():
        create_study(STUDY_DATA)
        update_study(UPDATED_STUDY_DATA, "1")
        study_list = get_study_list()
        assert study_list == [UPDATED_STUDY_DATA_ID]


def test_update_study_wrong_id(app):
    with app.app_context():
        error = update_study(UPDATED_STUDY_DATA, "1")
        assert error == "Energy Transformation study with that ID does not exist."


def test_update_study_same_name(app):
    with app.app_context():
        create_study(STUDY_DATA)
        create_study(UPDATED_STUDY_DATA)
        error = update_study(STUDY_DATA, "2")
        assert error == "Energy Transformation study with that name already exists."


def test_delete_study(app):
    with app.app_context():
        create_study(STUDY_DATA)
        study_list = get_study_list()
        assert len(study_list) == 1
        msg = delete_study("1")
        assert msg == "Study deleted"
        study_list = get_study_list()
        assert len(study_list) == 0


def test_delete_study_wrong_id(app):
    with app.app_context():
        error = delete_study("1")
        assert error == "Energy Transformation study with that ID does not exist."


