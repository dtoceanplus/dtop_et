from dtop_energytransf.business.cross_module_data import *
from dtop_energytransf.business.et_studies import create_study
from dtop_energytransf.service.api.studies import get_crossmodule_data
from test.business.test_et_studies import UPDATED_STUDY_DATA, STUDY_DATA

import copy

# External data for Wave cmpx 1
EC_DATA_W1 = {
    "technology": "WEC",    
    "complexity": 1,
    "id": 3,
    "number_devices": 2,
    "captured_power_per_condition":[ 
    {"deviceID": 0, 
     "capturedPower": [100], 
     "siteConditionID": [0]},
     {"deviceID": 1, 
     "capturedPower": [150], 
      "siteConditionID": [0]}]
}

SC_DATA_W1 = {
    "technology": "WEC",  
    "complexity": 1,
    "HS": [1],
    "TP": [1],
    "p": [1],
    "id": [1]
}

MC_DATA_W1 = {
    "technology": "WEC",
    "complexity": 1,
    "id": 3,
    "pto_damping": [1600000] 
}

# External data for Wave cmpx 2
EC_DATA_W2 = {
    "technology": "WEC",    
    "complexity": 2,
    "id": 3,
    "number_devices": 2,
    "captured_power_per_condition":[ 
    {"deviceID": 0, 
     "capturedPower": [100], 
     "siteConditionID": [0]},
     {"deviceID": 1, 
     "capturedPower": [150], 
      "siteConditionID": [0]}]
}

SC_DATA_W2 = {
    "technology": "WEC",  
    "complexity": 2,
    "HS": [1],
    "TP": [1],
    "p": [1],
    "id": [1]
}

MC_DATA_W2={
    "technology": "WEC",
    "complexity": 2,
    "id": 3,
    "pto_damping": [1600000]
}

# External data for Wave cmpx 3
EC_DATA_W3 = {
    "technology": "WEC",    
    "complexity": 3,
    "id": 3,
    "number_devices": 5,
    "captured_power_per_condition": 
    [
    {"deviceID": 0, 
     "capturedPower": [100, 100, 100, 100, 100, 100], 
     "siteConditionID": [0, 1, 2, 3, 4, 5]}, 
    {"deviceID": 1, 
        "capturedPower": [100, 100, 100, 100, 100, 100], 
        "siteConditionID": [0, 1, 2, 3, 4, 5]},
    {"deviceID": 2, 
        "capturedPower": [100, 100, 100, 100, 100, 100], 
        "siteConditionID": [0, 1, 2, 3, 4, 5]},
    {"deviceID": 3, 
        "capturedPower": [100, 100, 100, 100, 100, 100], 
        "siteConditionID": [0, 1, 2, 3, 4, 5]},
    {"deviceID": 4, 
        "capturedPower": [100, 100, 100, 100, 100, 100], 
        "siteConditionID": [0, 1, 2, 3, 4, 5]}                                   
    ] 
}

SC_DATA_W3 = {
    "technology": "WEC",
    "complexity": 3,  
    "HS": [1],
    "TP": [1],
    "p": [1],
    "id": [1, 2, 3, 4, 5, 6]
}

MC_DATA_W3={
    "technology": "WEC",
    "complexity": 3,
    "id": 3,
    "pto_damping": [[1600000, 0, 0, 0, 0, 0], [0, 1600000, 0, 0, 0, 0],[0, 0, 1600000, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0]]
}

# External data for Tidal cmpx 1
EC_DATA_T1 = {
    "technology": "TEC",    
    "complexity": 1,
    "id": 3,
    "number_devices": 5,
    "captured_power_per_condition": 
    [
    {"deviceID": 0, 
     "capturedPower": [100], 
     "siteConditionID": [0]}, 
    {"deviceID": 1, 
        "capturedPower": [100], 
        "siteConditionID": [0]}, 
    {"deviceID": 2, 
        "capturedPower": [100], 
        "siteConditionID": [0]}, 
    {"deviceID": 3, 
        "capturedPower": [100], 
        "siteConditionID": [0]}, 
    {"deviceID": 4, 
        "capturedPower": [100], 
        "siteConditionID": [0]}                                
    ],    
    "array_velocity_field": 
    [
    {"deviceID": 0, 
        "hub_velocity": [2.75016301683051],
        "siteID": [0]}, 
    {"deviceID": 1, 
        "hub_velocity": [2.75016301683052],
        "siteID": [0]}, 
    {"deviceID": 2, 
        "hub_velocity": [2.75016301683053],
        "siteID": [0]}, 
    {"deviceID": 3, 
        "hub_velocity": [2.75016301683054],
        "siteID": [0]}, 
    {"deviceID": 4, 
        "hub_velocity": [2.75016301683055],
        "siteID": [0]} 
    ],
    "main_dim_device": 20
}

SC_DATA_T1 = {
    "technology": "TEC",  
    "complexity": 1,
    "p": [1],
    "id": [1]
}

MC_DATA_T1={
    "technology": "TEC",
    "complexity": 1,
    "id": 3,
    "cp": 0.3,  
    "number_rotor": 3
}

# External data for Tidal cmpx 2
EC_DATA_T2 = {
    "technology": "TEC",    
    "complexity": 2,
    "id": 1,
    "number_devices": 5,
    "captured_power_per_condition": 
    [
    {"deviceID": 0, 
     "capturedPower": [100], 
     "siteConditionID": [0]}, 
    {"deviceID": 1, 
        "capturedPower": [100], 
        "siteConditionID": [0]}, 
    {"deviceID": 2, 
        "capturedPower": [100], 
        "siteConditionID": [0]}, 
    {"deviceID": 3, 
        "capturedPower": [100], 
        "siteConditionID": [0]}, 
    {"deviceID": 4, 
        "capturedPower": [100], 
        "siteConditionID": [0]}                                
    ],    
    "array_velocity_field": 
    [
    {"deviceID": 0, 
        "hub_velocity": [2.75016301683051],
        "siteID": [0]}, 
    {"deviceID": 1, 
        "hub_velocity": [2.75016301683052],
        "siteID": [0]}, 
    {"deviceID": 2, 
        "hub_velocity": [2.75016301683053],
        "siteID": [0]}, 
    {"deviceID": 3, 
        "hub_velocity": [2.75016301683054],
        "siteID": [0]}, 
    {"deviceID": 4, 
        "hub_velocity": [2.75016301683055],
        "siteID": [0]} 
    ],
    "rotor_diameter": 20
}

SC_DATA_T2 = {
    "technology": "TEC",
    "complexity": 2,
    "p": [1],
    "id": [1]
}

MC_DATA_T2={
    "technology": "TEC",
    "complexity": 2,
    "id": 3,
    "cp": 0.3,  
    "number_rotor": 3,
    "tip_speed_ratio": 1, 
    "ct": 0.4, 
    "cut_in_velocity": 1,  
    "cut_out_velocity": 10
}

# External data for Tidal cmpx 3
EC_DATA_T3 = {
    "technology": "TEC",    
    "complexity": 3,
    "id": 1,
    "number_devices": 5,
    "captured_power_per_condition": 
    [
    {"deviceID": 0, 
     "capturedPower": [100, 100, 100, 100, 100, 100], 
     "siteConditionID": [0, 1, 2, 3, 4, 5]}, 
    {"deviceID": 1, 
     "capturedPower": [100, 100, 100, 100, 100, 100],  
     "siteConditionID": [0, 1, 2, 3, 4, 5]},
    {"deviceID": 2, 
     "capturedPower": [100, 100, 100, 100, 100, 100],  
     "siteConditionID": [0, 1, 2, 3, 4, 5]},
    {"deviceID": 3, 
     "capturedPower": [100, 100, 100, 100, 100, 100],  
     "siteConditionID": [0, 1, 2, 3, 4, 5]},
    {"deviceID": 4, 
     "capturedPower": [100, 100, 100, 100, 100, 100], 
     "siteConditionID": [0, 1, 2, 3, 4, 5]}                             
    ],    
    "array_velocity_field": [
    {"deviceID": 0, 
        "hub_velocity": [2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553],
        "siteID": [0, 1, 2, 3, 4, 5]}, 
    {"deviceID": 1, 
        "hub_velocity": [2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553],
        "siteID": [0, 1, 2, 3, 4, 5]}, 
    {"deviceID": 2, 
        "hub_velocity": [2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553],
        "siteID": [0, 1, 2, 3, 4, 5]},
    {"deviceID": 3, 
        "hub_velocity": [2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553],
        "siteID": [0, 1, 2, 3, 4, 5]},
    {"deviceID": 4, 
        "hub_velocity": [2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553],
        "siteID": [0, 1, 2, 3, 4, 5]}
    ],
    "rotor_diameter": 20
}

SC_DATA_T3 = {
    "technology": "TEC",
    "complexity": 3,
    "p": [0.16, 0.16, 0.16, 0.16, 0.16, 0.16],
    "id": [1, 2, 3, 4, 5, 6]
}

MC_DATA_T3={
    "technology": "TEC",
    "complexity": 3,
    "id": 3,
    "cp": [0, 0.3, 1, 1, 0.5, 0],  
    "number_rotor": 3,
    "tip_speed_ratio": 1, 
    "ct": [0, 0.3, 0.3, 0.3, 0.5, 0], 
    "cut_in_velocity": 1,  
    "cut_out_velocity": 10,
    "cp_ct_velocity": [0, 0.5, 1, 2, 5, 10]
}


def test_process_cross_module_w1(app):
    with app.app_context():
        
        s = create_study(STUDY_DATA)
        ec = ProcessEnergyCapture(s, EC_DATA_W1)
        mc = ProcessMachineCharacterisation(s, MC_DATA_W1)
        sc = ProcessSiteCharacterisation(s, SC_DATA_W1)
        result = get_crossmodule_data(1)
        assert result == {'site_characterisation_data': {'sc_id': [0], 'sc_cmpx': 1, 'Hs': [1], 'Occ': [1], 'Tz': [1]}, 'machine_characterisation_data': {'TSR': 5.8, 'Cp': [0.3], 'Cpto': [1600000], 'mc_id': 3, 'technology': 'Wave', 'Ct_tidal': [0.1, 0.3, 0.3, 0.3, 0.5, 0.1], 'mc_cmpx': 1, 'cut_in_out': [], 'cp_ct_velocity': [0, 0.5, 1, 2, 5, 10], 'dof': 1, 'number_rotor': 1}, 'energy_capture_data': {'sigma_v': [2.3], 'ec_cmpx': 1, 'rotor_diameter': 5.0, 'num_devices': 2, 'Captured_Power': [[100000], [150000]], 'ec_id': 3, 'siteConditionID': [[0], [0]]}} 

# def test_process_cross_module_w2(app):
#     with app.app_context():
        
#         s = create_study(UPDATED_STUDY_DATA)
#         ec = ProcessEnergyCapture(s, EC_DATA_W2)
#         mc = ProcessMachineCharacterisation(s, MC_DATA_W2)
#         sc = ProcessSiteCharacterisation(s, SC_DATA_W2)
#         result = get_crossmodule_data(1)
#         assert result == {'site_characterisation_data': {'Occ': [1], 'sc_id': [0], 'Hs': [1], 'Tz': [1], 'sc_cmpx': 2}, 'energy_capture_data': {'sigma_v': [2.3], 'ec_id': 3, 'rotor_diameter': 5.0, 'num_devices': 2, 'siteConditionID': [[0], [0]], 'Captured_Power': [[100000], [150000]], 'ec_cmpx': 2}, 'machine_characterisation_data': {'TSR': 5.8, 'mc_id': 3, 'Cpto': [1600000], 'number_rotor': 1, 'technology': 'Wave', 'cut_in_out': [], 'dof': 1, 'Cp': [0.3], 'Ct_tidal': [0.1, 0.3, 0.3, 0.3, 0.5, 0.1], 'cp_ct_velocity': [0, 0.5, 1, 2, 5, 10], 'mc_cmpx': 2}}

# def test_process_cross_module_w3(app):
#     with app.app_context():
        
#         s = create_study(UPDATED_STUDY_DATA)
#         ec = ProcessEnergyCapture(s, EC_DATA_W3)
#         mc = ProcessMachineCharacterisation(s, MC_DATA_W3)
#         sc = ProcessSiteCharacterisation(s, SC_DATA_W3)
#         result = get_crossmodule_data(1)
#         assert result == {'energy_capture_data': {'sigma_v': [2.3], 'ec_id': 3, 'Captured_Power': [[100000, 100000, 100000, 100000, 100000, 100000], [100000, 100000, 100000, 100000, 100000, 100000], [100000, 100000, 100000, 100000, 100000, 100000], [100000, 100000, 100000, 100000, 100000, 100000], [100000, 100000, 100000, 100000, 100000, 100000]], 'ec_cmpx': 3, 'siteConditionID': [[0, 1, 2, 3, 4, 5], [0, 1, 2, 3, 4, 5], [0, 1, 2, 3, 4, 5], [0, 1, 2, 3, 4, 5], [0, 1, 2, 3, 4, 5]], 'rotor_diameter': 5.0, 'num_devices': 5}, 'machine_characterisation_data': {'mc_cmpx': 3, 'Ct_tidal': [0.1, 0.3, 0.3, 0.3, 0.5, 0.1], 'TSR': 5.8, 'Cpto': [[1600000, 0, 0, 0, 0, 0], [0, 1600000, 0, 0, 0, 0], [0, 0, 1600000, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0]], 'dof': 3, 'cp_ct_velocity': [0, 0.5, 1, 2, 5, 10], 'Cp': [0.3], 'cut_in_out': [], 'technology': 'Wave', 'number_rotor': 1, 'mc_id': 3}, 'site_characterisation_data': {'Tz': [1], 'Hs': [1], 'Occ': [1], 'sc_id': [1, 2, 3, 4, 5, 6], 'sc_cmpx': 3}}

# def test_process_cross_module_t1(app):
#     with app.app_context():
       
#         s = create_study(UPDATED_STUDY_DATA)
#         ec = ProcessEnergyCapture(s, EC_DATA_T1)
#         mc = ProcessMachineCharacterisation(s, MC_DATA_T1)
#         sc = ProcessSiteCharacterisation(s, SC_DATA_T1)
#         result = get_crossmodule_data(1)
#         assert result == {'site_characterisation_data': {'sc_cmpx': 1, 'Tz': [1], 'Occ': [1], 'Hs': [1], 'sc_id': [1]}, 'machine_characterisation_data': {'number_rotor': 3, 'TSR': 5.8, 'cp_ct_velocity': [0, 0.5, 1, 2, 5, 10], 'mc_id': 3, 'technology': 'Tidal', 'mc_cmpx': 1, 'Ct_tidal': [1.2538441688605653], 'dof': 1, 'cut_in_out': [1, 5], 'Cp': [0.3], 'Cpto': [[530834.960169927], [530834.960169929], [530834.9601699308], [530834.9601699328], [530834.9601699347]]}, 'energy_capture_data': {'sigma_v': [[2.75016301683051], [2.75016301683052], [2.75016301683053], [2.75016301683054], [2.75016301683055]], 'siteConditionID': [[0], [0], [0], [0], [0]], 'Captured_Power': [[100000], [100000], [100000], [100000], [100000]], 'ec_id': 3, 'num_devices': 5, 'ec_cmpx': 1, 'rotor_diameter': 20.0}}

# def test_process_cross_module_t2(app):
#     with app.app_context():
        
#         s = create_study(UPDATED_STUDY_DATA)
#         ec = ProcessEnergyCapture(s, EC_DATA_T2)
#         mc = ProcessMachineCharacterisation(s, MC_DATA_T2)
#         sc = ProcessSiteCharacterisation(s, SC_DATA_T2)
#         result = get_crossmodule_data(1)
#         assert result == {'site_characterisation_data': {'Occ': [1], 'Tz': [1], 'sc_id': [1], 'Hs': [1], 'sc_cmpx': 2}, 'energy_capture_data': {'ec_cmpx': 2, 'rotor_diameter': 20.0, 'sigma_v': [[2.75016301683051], [2.75016301683052], [2.75016301683053], [2.75016301683054], [2.75016301683055]], 'num_devices': 5, 'ec_id': 1, 'siteConditionID': [[0], [0], [0], [0], [0]], 'Captured_Power': [[100000], [100000], [100000], [100000], [100000]]}, 'machine_characterisation_data': {'dof': 1, 'Ct_tidal': [0.4], 'cp_ct_velocity': [0, 0.5, 1, 2, 5, 10], 'mc_cmpx': 2, 'mc_id': 3, 'cut_in_out': [1, 10], 'Cp': [0.3], 'TSR': 1.0, 'Cpto': [[530834.960169927], [530834.960169929], [530834.9601699308], [530834.9601699328], [530834.9601699347]], 'technology': 'Tidal', 'number_rotor': 3}}
# def test_process_cross_module_t3(app):
#     with app.app_context():
        
#         s = create_study(UPDATED_STUDY_DATA)
#         ec = ProcessEnergyCapture(s, EC_DATA_T3)
#         mc = ProcessMachineCharacterisation(s, MC_DATA_T3)
#         sc = ProcessSiteCharacterisation(s, SC_DATA_T3)
#         result = get_crossmodule_data(1)
#         assert result == {'machine_characterisation_data': {'mc_id': 3, 'TSR': 1.0, 'Ct_tidal': [0.1, 0.3, 0.3, 0.3, 0.5, 0.1], 'Cpto': [[0.0, 485131.5096967493, 1617105.0323224973, 1518452.369626761, 739137.7489631724, 0.0], [0.0, 485131.5096967493, 1617105.0323224973, 1518452.369626761, 739137.7489631724, 0.0], [0.0, 485131.5096967493, 1617105.0323224973, 1518452.369626761, 739137.7489631724, 0.0], [0.0, 485131.5096967493, 1617105.0323224973, 1518452.369626761, 739137.7489631724, 0.0], [0.0, 485131.5096967493, 1617105.0323224973, 1518452.369626761, 739137.7489631724, 0.0]], 'cp_ct_velocity': [0, 0.5, 1, 2, 5, 10], 'mc_cmpx': 3, 'technology': 'Tidal', 'dof': 1, 'number_rotor': 3, 'cut_in_out': [1, 10], 'Cp': [0, 0.3, 1, 1, 0.5, 0]}, 'site_characterisation_data': {'Hs': [1], 'Occ': [0.16, 0.16, 0.16, 0.16, 0.16, 0.16], 'sc_id': [1, 2, 3, 4, 5, 6], 'Tz': [1, 1, 1, 1, 1, 1], 'sc_cmpx': 3}, 'energy_capture_data': {'sigma_v': [[2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553], [2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553], [2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553], [2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553], [2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553]], 'rotor_diameter': 20.0, 'ec_cmpx': 3, 'siteConditionID': [[0, 1, 2, 3, 4, 5], [0, 1, 2, 3, 4, 5], [0, 1, 2, 3, 4, 5], [0, 1, 2, 3, 4, 5], [0, 1, 2, 3, 4, 5]], 'Captured_Power': [[100000, 100000, 100000, 100000, 100000, 100000], [100000, 100000, 100000, 100000, 100000, 100000], [100000, 100000, 100000, 100000, 100000, 100000], [100000, 100000, 100000, 100000, 100000, 100000], [100000, 100000, 100000, 100000, 100000, 100000]], 'ec_id': 1, 'num_devices': 5}}

def test__cross_module_validation_error(app):
    with app.app_context():
        
        s = create_study(UPDATED_STUDY_DATA)
        invalid_ec_data = copy.deepcopy(EC_DATA_T1)
        del invalid_ec_data["technology"]
        invalid_mc_data = copy.deepcopy(MC_DATA_T1)
        del invalid_mc_data["number_rotor"]
        invalid_sc_data = copy.deepcopy(SC_DATA_T1)
        del invalid_sc_data["p"]
        ec = ProcessEnergyCapture(s, invalid_ec_data)
        mc = ProcessMachineCharacterisation(s, invalid_mc_data)
        sc = ProcessSiteCharacterisation(s, invalid_sc_data)
              
        assert ec.validation_error
        assert mc.validation_error
        assert sc.validation_error

"""
def test_process_site_characterisation(app):

    # TODO: update, very simple test now
    sc = ProcessSiteCharacterisation()
    assert sc.sc_data == {
        "Hs": [1],
        "Tz": [1],
        "Occ": [1],
        "id": [1]
    }


def test_process_machine_characterisation(app):

    # TODO: update, very simple test now
    mc = ProcessMachineCharacterisation()
    assert mc.mc_data["cut_in_cut_out"] == [
        0.5,
        5
    ]
    v = mc.mc_data["pto_matrix"] == np.array(
        [
            [0.,  0.,  0.,  0.,  0.,  0.],
            [0., 10.,  0.,  0.,  0.,  0.],
            [0.,  0.,  0.,  0.,  0.,  0.],
            [0.,  0.,  0., 10.,  0.,  0.],
            [0.,  0.,  0.,  0.,  0.,  0.],
            [0.,  0.,  0.,  0.,  0., 10.]
        ]
    )
    assert v.all()
    assert mc.mc_data["dof"] == 3

"""

