import copy
import json
import os

from dtop_energytransf.business.et_studies import create_study
from dtop_energytransf.business.output_management import AnalysisResults
from dtop_energytransf.business.input_management import InputManagement
from dtop_energytransf.business.analysis import RunAnalysis
from test.business.test_et_studies import STUDY_DATA
from test.business.test_input_management import  UPDATED_INPUT_DATA
from dtop_energytransf.storage.schemas.schemas import ArrayResultsSchema, DeviceResultsSchema, PtoResultsSchema
from dtop_energytransf.storage.models.models import ArrayResults, DeviceResults, PtoResults

from dtop_energytransf.business.cross_module_data import *

from dtop_energytransf.service import db

# External data for Wave cmpx 1
EC_DATA_W1 = {
    "technology": "WEC",    
    "complexity": 1,
    "id": 3,
    "number_devices": 2,
    "captured_power_per_condition":[ 
    {"deviceID": 0, 
     "capturedPower": [100], 
     "siteConditionID": [0]},
     {"deviceID": 1, 
     "capturedPower": [150], 
      "siteConditionID": [0]}]
}

SC_DATA_W1 = {
    "technology" : "WEC",
    "complexity": 1,
    "Hs": [1],
    "Tp": [1],
    "Occ": [1],
    "id": [1]
}

MC_DATA_W1 = {
    "technology": "WEC",
    "complexity": 1,
    "id": 3,
    "pto_damping": [1600000] 
}

def json_loader(f_name):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    with open(f"{dir_path}/{f_name}.json", "r") as json_file:
        data = json.load(json_file)

    return data


# def test_output_management_init(app):
#     with app.app_context():
#         s = create_study(STUDY_DATA)

#         ec = ProcessEnergyCapture(s, EC_DATA_W1)
#         mc = ProcessMachineCharacterisation(s, MC_DATA_W1)
#         sc = ProcessSiteCharacterisation(s, SC_DATA_W1)
#         # after creating the study, update the inputs for setting complexity levels that are not set
#         # by default
       
#         input_mgmt = InputManagement(s, UPDATED_INPUT_DATA)
#         input_mgmt.update_db()        

#         analysis = RunAnalysis(1)
#         AnalysisResults(analysis)
#         array_results_db = ArrayResults.query.filter(ArrayResults.et_study_id == 1)
#         array_schema = ArrayResultsSchema(many=True)
#         array_dict = array_schema.dump(array_results_db)
#         device_results_db = DeviceResults.query.filter(DeviceResults.et_study_id == 1)
#         device_schema = DeviceResultsSchema(many=True)
#         device_dict = device_schema.dump(device_results_db)
#         pto_results_db = PtoResults.query.filter(PtoResults.et_study_id == 1)
#         pto_schema = PtoResultsSchema(many=True)
#         pto_dict = pto_schema.dump(pto_results_db)

#         ar = array_dict[0]["Array_Pa_Grid"]["value"]["Power"]
#         assert ar[0] ==  4.146
#         dev = device_dict[0]["Dev_Pa_Grid"]["value"]["Power"]
#         assert round(dev[0],4) == round(2.197769842680461,4)
#         pto = pto_dict[0]["GridC_Active_Power"]["value"]
#         assert round(pto[0], 6) == round(2.197769842680461, 6)
#         assert round(pto_dict[0]["GridC_Damage"]["value"], 6) == round(0.02841104372716904, 6)


#         UPDATED_INPUT_DATA2PTO = UPDATED_INPUT_DATA.copy()
#         UPDATED_INPUT_DATA2PTO['general_inputs']['parallel_ptos']=2
#         # test that if the analysis is re-run, rows are deleted and written again, but not just added
#         input_mgmt = InputManagement(s, UPDATED_INPUT_DATA2PTO)
#         input_mgmt.update_db()
#         analysis = RunAnalysis(1)
#         AnalysisResults(analysis)
#         array_results_db = ArrayResults.query.filter(ArrayResults.et_study_id == 1)
#         array_schema = ArrayResultsSchema(many=True)
#         array_dict = array_schema.dump(array_results_db)
#         device_results_db = DeviceResults.query.filter(DeviceResults.et_study_id == 1)
#         device_schema = DeviceResultsSchema(many=True)
#         device_dict = device_schema.dump(device_results_db)
#         pto_results_db = PtoResults.query.filter(PtoResults.et_study_id == 1)
#         pto_schema = PtoResultsSchema(many=True)
#         pto_dict = pto_schema.dump(pto_results_db)


#         ar = array_dict[0]["Array_Pa_Grid"]["value"]["Power"]
#         assert ar[0] ==  10.617
#         dev = device_dict[0]["Dev_Pa_Grid"]["value"]["Power"]
#         assert round(dev[0],4) == round(5.333325837775419,4)
#         pto = pto_dict[0]["GridC_Active_Power"]["value"]
#         assert round(pto[0], 6) == round(2.6666629188877096, 6)
#         assert round(pto_dict[0]["GridC_Damage"]["value"], 6) == round(0.05474565905129964, 6)



