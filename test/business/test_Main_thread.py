# Database required libraries
from dtop_energytransf.storage.models import models as DB_models
from dtop_energytransf.business.DB import input_json_gen as json_gen
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import os
import json
from scipy.stats import rayleigh

# Generic libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os
import time as t_crono
from scipy.stats import rayleigh

# Libraries from within the module
from dtop_energytransf.business import PTO as PTO_class
from dtop_energytransf.business import Device as Dev_class

# from dtop_energytransf.business import Array as Array_class

# #THIS SET OF TESTS IS TOTALLY WRONG AND MUST BE CANCELLED ASAP


# def test_Wave_analisis_Array(read_config):

#     # We also need the complexity level used at EC ==>  / [ec ]  "complexity": 1,
#     energy_capture_cmpx = 3

#     Elect_CMPX = 2  # USER INPUT
#     Grid_CMPX = 2  # USER INPUT
#     Mech_CMPX = 2  # USER INPUT

#     # Type_mech_transf = "AirTurbine"  # USER INPUT ==> (AirTurbine - Hydraulic - Gearbox / CMPX2-3) - Mech_Simplified CMPX3
#     # Type_elect_transf = 'SCIG'       # USER INPUT CMPX1-2-3  SCIG/ Elect_Simplified
#     # Type_grid_transf = 'B2B2level'   # USER INPUT CMPX 1-2-3  B2B2level-Grid_Simplified

#     mode = "analysis"  # User input 'analysis' / 'design'
#     # ARRAY - INPUTS
#     # *************************
#     Technology = "Wave"  #  CMPX 1-2-3 Wave-Tidal ==> /ec/  ["type"]
#     Number_of_devices = (
#         1  #  CMPX 1-2-3 REVIEW!! ==>  /ec/{ecId}/farm   ["number_devices"]
#     )
#     # Initialisation INPUTS for an Array
#     Array_inputs = {"Technology": Technology, "Number of devices": Number_of_devices}
#     # ----------------------------------
#     Parallel_PTOs = 2  # USER INPUT CMPX1-2-3 In case there are two turbines in each dof-PTO, with its Elect and
#     # Grid conversion stages
#     CUT_IN_OUT = [0.5, 5]  # MODULE OUTPUT MC CMPX1-2-3
#     Dev_shut_flag = 1.0  # USER INPUT CMPX 1-2-3
#     # ----------------------------------------------------------------
#     PTO_matrix = np.zeros([6, 6])  # MODULE OUTPUT MC CMPX1-2-3
#     PTO_matrix[1, 1] = 10
#     PTO_matrix[3, 3] = 10
#     PTO_matrix[5, 5] = 10
#     GDL = 0
#     # /mc/{mcId}/model/wec/complexity3
#     # "shared_dof":  [0,0,1,0,0,0]
#     for cont1 in range(1, 6):
#         for cont2 in range(1, 6):
#             if PTO_matrix[cont1, cont2] > 0:
#                 GDL = GDL + 1

#     # Initialisation INPUTS
#     Device_INPUTS = {
#         "PTO_matrix": PTO_matrix,
#         "Parallel_PTOs": Parallel_PTOs,
#         "Shutdown_flag": Dev_shut_flag,
#         "cut_in_out": CUT_IN_OUT,
#     }
#     # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#     if Technology == "Wave":
#         # **********************************************************************************************************************
#         # DUMMY INPUTS from the Energy Capture (EC) and Site Characterisation (SC) modules -- WAVE Example
#         # ************************************************************************************************
#         # / ec / {ecId} / inputs / machine / wec / complexity1
#         # "device_capture_width_ratio": 0.3,
#         # /ec/{ecId}/inputs/machine/wec/complexity2
#         """"device_capture_width_ratio":[],
#         "tp_capture_width": [],Peak Period vector associated with the capture width ratio matrix
#         "hs_capture_width":[], Significant Wave Height vector associated with the capture width ratio matrix
#         "wave_angle_capture_width":[0],Incident Wave Angle vector associated with the capture width ratio matrix"""
#         # /ec/{ecId}/inputs/machine/wec/complexity3
#         """
#         "dir_pm":[0],
#         "hs_pm":[0.5,1.5,2.5],
#         "tp_pm":[2,3,4],
#         "scatter_diagram":[[]],
#         "power_matrix":[[]"""
#         """
#         path = os.path.dirname(os.path.abspath(__file__)) + "\\DB\\json_data\\"
#         EC_file = "EC_out_OES_Sphere"
#         EC_out = pd.read_json(
#             path + EC_file + ".json", orient="records", lines=True
#         )  # Data generated with the OES sphere

#         # Initialisation of variables
#         # /mc/{mcId}/model/wec/complexity2
#         # "pto_damping": It seems that is only 1 general parameter
#         # /mc/{mcId}/model/wec/complexity3
#         # "pto_damping":
#         Cpto = np.zeros(len(EC_out["Hs[m]"]))  # MODULE OUTPUT EC
#         Pcapt = np.zeros(
#             len(EC_out["Hs[m]"])
#         )  # MODULE OUTPUT EC /ec/{ecId}/devices/{deviceId} / captured_power_per_condition / capturedPower
#         Hs = EC_out["Hs[m]"]  # MODULE OUTPUT EC
#         Tp = EC_out["Tp[s]"]  # MODULE OUTPUT EC
#         Occ = EC_out["Occ"]  # MODULE OUTPUT EC
#         for c_SS in range(0, len(EC_out["Hs[m]"])):
#             # Maximum Captured power in each Sea State
#             ind_Pmax = EC_out["Pmean[W]"][c_SS][:].index(max(EC_out["Pmean[W]"][c_SS][:]))
#             Cpto[c_SS] = EC_out["Cpto[Ns/m]"][c_SS][ind_Pmax]
#             Pcapt[c_SS] = EC_out["Pmean[W]"][c_SS][ind_Pmax]"""
#         Cpto = np.zeros(len(read_config["Hs[m]"]))
#         Pcapt = np.zeros(len(read_config["Hs[m]"]))
#         Hs = read_config["Hs[m]"]
#         Tp = read_config["Tp[s]"]
#         Occ = read_config["Occ"]
#         for c_SS in range(0, len(read_config["Hs[m]"])):
#             # Maximum Captured power in each Sea State
#             ind_Pmax = read_config["Pmean[W]"][c_SS][:].index(
#                 max(read_config["Pmean[W]"][c_SS][:])
#             )
#             Cpto[c_SS] = read_config["Cpto[Ns/m]"][c_SS][ind_Pmax]
#             Pcapt[c_SS] = read_config["Pmean[W]"][c_SS][ind_Pmax]
#         # *****Energy Transformation (ET) Module -- INPUTS to be gathered from the ET-GUI *****
#         # **********************************************************************************************************************
#         # PTO - INPUTS
#         # ----------------------------------------------------------------------------------------------------------------------
#         # Required parameters to initialise the Mechanical Transformation object
#         # --- Air Turbine ---
#         Type_mech_transf = "AirTurbine"  # USER INPUT ==> (AirTurbine - Hydraulic - Gearbox / CMPX2-3) - Mech_Simplified CMPX3
#         if Mech_CMPX == 1:
#             Type_mech_transf = "Mech_Simplified"
#         if Type_mech_transf != "AirTurbine":
#             Type_turbine = "none"
#         else:
#             Type_turbine = "Impulse"  # USER INPUT ==> Impulse - Wells CMPX2-CMPX3
#         turb_D = 1  # USER INPUT ==> m
#         S_owc_c = np.pi * 2.5 ** 2  # OWC

#         obj_args = (
#             []
#         )  # This variable is required in case the object needs aditional inputs for performance
#         # -------------------
#         # Generic Dictionary input for the Mechanical Transformation
#         mechT_params = {
#             "Type": Type_mech_transf,  # The name of the object
#             "init_args": [
#                 Type_turbine,
#                 turb_D,
#                 S_owc_c,
#                 Technology,
#             ],  # Required arguments to initialise the object
#             "perf_args": obj_args,  # Required arguments to perform the performance of specific objects
#             "mech_t_ratio": 10,
#         }  # REVIEW THIS PARAMETER!!!!!!!!!!!!!!!!
#         # ******************************************************************************************************************
#         # ENVIRONMENTAL CONDITIONS
#         Env_Conditions = {"Hs": Hs, "Tz": Tp, "Occ": Occ}
#         # ARRAY - INPUTS
#         lst_random = [0.9, 1.1, 1.2, 0.95, 1.05]
#         lst_sigma2 = []
#         lst_cpto2 = []
#         Array_perf_inputs = []
#         dic1 = {}
#         dic2 = {}
#         for cont in range(0, GDL):
#             lst_sigma2.append(Pcapt / Cpto)
#             lst_cpto2.append(Cpto)
#             dic1 = {"sigma_v": lst_sigma2, "C_pto": lst_cpto2}
#             Array_perf_inputs.append(dic1)
#         # DEVICE
#         lst_cpto = []
#         lst_sigma = []
#         Device_perf_inputs = []
#         for cont2 in range(0, Number_of_devices):  # Degree of freedom
#             lst_cpto.append(Cpto)
#             lst_sigma.append(np.sqrt(Pcapt / Cpto))
#             dic2 = {"sigma_v": lst_sigma, "C_pto": lst_cpto}
#             Device_perf_inputs.append(dic2)
#     else:  # *********************TIDAL****************************************************************************************
#         # DUMMY INPUTS from the Energy Capture (EC) and Site Characterisation (SC) modules -- TIDAL example
#         # **************************************************************************************************
#         # /ec/{ecId}/inputs/machine/tec/complexity1  /
#         # "cp"

#         # /ec/{ecId}/inputs/machine/tec/complexity2  /  "rotor_diameter": 10,
#         # "cut_in_velocity": 1,
#         # "cut_out_velocity": 10,
#         # "cp": 0.3,
#         # "ct": 1,
#         # "rated_pow_device": 500,
#         # "number_rotor": 1,
#         # / ec / {ecId} / results / tec / complexity2
#         """"array_velocity_field":
#         "deviceID": 1,
#         "hub_velocity": 2.3"""
#         # / ec / {ecId} / inputs / site / tec / complexity2
#         # "velocity_field":[ 0,3],
#         # /ec/{ecId}/inputs/site/tec/complexity3
#         # "V": [
#         # "probability":[
#         # / mc / {mcId} / model / tec / complexity2
#         # "tip_speed_ratio": 1,

#         # / ec / {ecId} / inputs / machine / tec / complexity3
#         """"rotor_diameter": 10,
#         "hub_height": 20,
#         "floating": false,
#         "cut_in_velocity": 2,
#         "cut_out_velocity": 5,
#         "cp":   [       0.3,       0.8   ],
#         "ct":   [        0.3,        0.3    ],
#         "cp_ct_velocity":[ 2, 5 ],
#         "rated_pow_device": 500,
#         "number_rotor": 1,
#         """
#         # / ec / {ecId} / results / tec / complexity3
#         """"array_velocity_field":
#         "deviceID": 1,
#         "hub_velocity": 2.3"""
#         # /mc/{mcId}/model/tec/complexity3
#         # "tip_speed_ratio": 1,
#         Type_turbine = 0
#         current_vel = np.arange(0.1, 5, 0.05)  # m/s
#         turbine_radius = 10  # m
#         rho_water = 1025.0
#         vmean_max_occ = 5  # m/s
#         current_pdf = rayleigh.pdf(5 * current_vel / vmean_max_occ)
#         current_prob = current_pdf * np.mean(np.diff(current_vel))
#         P_rated = 2e6  # W
#         omega_rated = 14 * 2 * np.pi / 60  # rad/s
#         v_curr_rated = 3  # m/s
#         #  Power curve
#         power_exp = 3
#         power_low = (
#             P_rated
#             / v_curr_rated ** power_exp
#             * current_vel[current_vel <= v_curr_rated] ** power_exp
#         )
#         power_up = P_rated * np.ones(len(current_vel[current_vel > v_curr_rated]))
#         mean_power = np.concatenate([power_low, power_up])
#         #  Rotational speed curve
#         omega_exp = 1.5
#         omega_low = (
#             omega_rated
#             / v_curr_rated ** omega_exp
#             * current_vel[current_vel <= v_curr_rated] ** omega_exp
#         )
#         omega_up = omega_rated * np.ones(len(current_vel[current_vel > v_curr_rated]))
#         mean_omega = np.concatenate([omega_low, omega_up])
#         # Cp (power coefficient) and TSR (tip speed ratio)
#         Cp = mean_power / (
#             0.5 * rho_water * np.pi * turbine_radius ** 2 * current_vel ** 3
#         )
#         TSR = mean_omega * turbine_radius / current_vel
#         Cpto = current_vel * Cp * 0.5 * rho_water * np.pi * turbine_radius ** 2
#         Ct_tidal = (
#             1 / mean_omega
#         )  #          OUTPUT FROM EC  = turbine_radius / (TSR * current_vel)
#         # ******************************************************************************************************************
#         # *****Energy Transformation (ET) Module -- INPUTS to be gathered from the ET-GUI *****
#         # ******************************************************************************************************************
#         # PTO - INPUTS
#         # *************************
#         # ------------------------------------------------------------------------------------------------------------------
#         # Required parameters to initialise the Mechanical Transformation object
#         # --- Gearbox ---
#         GB_tech = Technology
#         GB_Pnom = 5e5  # USER INPUT CMPX1-2-3 W
#         GB_t_ratio = 1 / 100  # USER INPUT CMPX1-2-3 relation
#         obj_args = {
#             "Ct_tidal": 0
#         }  # For tidal devices the torque relation with the speed changes w.r.t. wave devices. The corresponding
#         # coefficientes are provided through this variable to compute the torque on the shaft. It just needs to be initialised,
#         # and will be updated per current speed with the corresponding Ct_tidal set in the perf_inputs
#         # -------------------
#         # Generic Dictionary input for the Mechanical Transformation
#         Type_mech_transf = "Gearbox"  # USER INPUT CMPX 1-2-3 Gearbox - Mech_Simplified
#         if Mech_CMPX == 1:
#             Type_mech_transf = "Mech_Simplified"
#         mechT_params = {
#             "Type": Type_mech_transf,  # The name of the object
#             "init_args": [
#                 # GB_tech,
#                 GB_Pnom,
#                 GB_t_ratio,
#                 Technology,
#             ],  # Required arguments to initialise the object
#             "perf_args": obj_args,  # Required arguments to perform the performance of specific objects
#             "mech_t_ratio": 1,
#         }  # Always 1 with the gearbox as it is already defined in the init_args
#         # ------------------------------------------------------------------------------------------------------------------
#         # *******************************************************************************************************************
#         # Dummy inputs from Energy Capture for the 3 dof-PTOs of the device
#         # A Turb. intensity of a 10% is assumed - D.R.J. Sutherland et al.
#         lst_current_vel = []
#         lst_Cpto3 = []
#         lst_Ct_tidal = []
#         Device_perf_inputs = []
#         dic3 = {}
#         for cont2 in range(0, Number_of_devices):
#             lst_current_vel.append(0.1 * current_vel)
#             lst_Cpto3.append(Cpto)
#             lst_Ct_tidal.append(Ct_tidal)
#             dic3 = {
#                 "sigma_v": lst_current_vel,
#                 "C_pto": lst_Cpto3,
#                 "Ct_tidal": lst_Ct_tidal,
#             }
#             Device_perf_inputs.append(dic3)
#         # ******************************************************************************************************************
#         # The set of Environmental Conditions, for all devices
#         Env_Conditions = {
#             "Vc": current_vel,
#             "Tz": 10 * np.ones(len(current_vel)),
#             "Occ": current_prob,
#         }
#         lst_current_vel1 = []
#         lst_Cpto4 = []
#         lst_Ct_tidal1 = []
#         Array_perf_inputs = []
#         dic4 = {}
#         for cont in range(0, GDL):
#             lst_current_vel1.append(0.1 * current_vel)
#             lst_Cpto4.append(Cpto)
#             lst_Ct_tidal1.append(Ct_tidal)
#             dic4 = {
#                 "sigma_v": lst_current_vel1,
#                 "C_pto": lst_Cpto4,
#                 "Ct_tidal": lst_Ct_tidal1,
#             }
#             Array_perf_inputs.append(dic4)
#         lst_devices = []
#         Array_perf_inputs1 = []
#     # **********************************************************************************************************************
#     # *****Energy Transformation (ET) Module -- INPUTS to be gathered from the ET-GUI *****
#     # **********************************************************************************************************************
#     # PTO - INPUTS
#     # *************************
#     # ----------------------------------------------------------------------------------------------------------------------
#     # Required parameters to initialise the Electrical Transformation object
#     # --- Squirrel Cage Induction Generator ---
#     Gen_Nom_P = 100e3  # USER INPUT (WATS) CMPX-1-2-3
#     pp = 2  # USER INPUT  CMPX-2-3
#     Vnom = 400.0  # USER INPUT  CMPX-2-3
#     fnom = 50.0  # USER INPUT  CMPX-2-3
#     rel_T_maxnom = 2.0  # USER INPUT  CMPX-2-3
#     rel_V_maxnom = 1.725  # USER INPUT  CMPX-2-3
#     gen_class = "Class_F"  # USER INPUT  CMPX-2-3 Class_A-Class_B-Class_F-Class_G
#     Type_elect_transf = "SCIG"  # USER INPUT CMPX1-2-3  SCIG/ Elect_Simplified
#     if Elect_CMPX == 1:
#         Type_elect_transf = "Elect_Simplified"
#     electT_params = {
#         "Type": Type_elect_transf,
#         "init_args": [Gen_Nom_P, pp, Vnom, fnom, rel_T_maxnom, rel_V_maxnom, gen_class],
#     }
#     # ----------------------------------------------------------------------------------------------------------------------
#     # Required parameters to initialise the Grid Conditioning object
#     # --- Back to back to level ---
#     B2B_Nom_P = 100e3  # USER INPUT CMPX 1-2-3
#     Vdc = 1200  # USER INPUT CMPX 2-3
#     fsw = 5000  # USER INPUT CMPX 2-3
#     VnomGen = Vnom * np.sqrt(3)  # USER INPUT CMPX 2-3
#     fnomGen = 50.0  # USER INPUT CMPX 2-3
#     Lgen = 0.0005  # USER INPUT CMPX 2-3
#     Rgen = 0.0001  # USER INPUT CMPX 2-3
#     Vgrid = 690  # USER INPUT CMPX 2-3 /mc/{mcId}/general/ "rated_voltage"
#     Rgrid = 0.0001  # USER INPUT CMPX 2-3
#     Lgrid = 0.001  # USER INPUT CMPX 2-3
#     cosfi_grid = 0.91  # USER INPUT CMPX 2-3
#     Type_grid_transf = "B2B2level"  # USER INPUT CMPX 1-2-3  B2B2level-Grid_Simplified
#     if Mech_CMPX == 1:
#         Type_grid_transf = "Grid_Simplified"
#     gridC_params = {
#         "Type": "B2B2level",
#         "init_args": [
#             B2B_Nom_P,
#             Vdc,
#             fsw,
#             VnomGen,
#             fnomGen,
#             Lgen,
#             Rgen,
#             Vgrid,
#             Rgrid,
#             cosfi_grid,
#             Lgrid,
#         ],
#     }
#     # ----------------------------------------------------------------------------------------------------------------------
#     # Required parameters to initialise a Control object
#     # --- Passive Control ---
#     type_control = "Passive"  # USER INPUT CMPX 2-3 User_defined - Passive
#     n_sigma = 5  # USER INPUT CMPX 2-3
#     bins = 500  # USER INPUT CMPX 2-3 points per sea state (500)
#     vel_mean = 1e-6
#     control_params = {"Type": type_control, "init_args": [n_sigma, bins, vel_mean]}
#     # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#     # ----- ARRAY object example -----
#     # Initialisation

#     # Gen_Nom_P = 100e3                       # USER INPUT (WATS) CMPX-1-2-3
#     # B2B_Nom_P = 100e3                       # USER INPUT CMPX 1-2-3
#     # turb_D = 1                          # USER INPUT ==> m
#     if mode == "analysis":
#         pto_inputs = [mechT_params, electT_params, gridC_params, control_params]
#         # ----------------------------------------------------------------------------------------------------------------------
#         # json data input depending on the CMPX
#         json_gen.input_json_gen(
#             Elect_CMPX,
#             Grid_CMPX,
#             Mech_CMPX,
#             Type_mech_transf,
#             Type_turbine,
#             type_control,
#         )
#         Array_ID = "Array_01"
#         t_init = t_crono.time()
#         Array = Array_class.Array(Array_ID, Array_inputs, Device_INPUTS, pto_inputs)
#         print(str(t_crono.time() - t_init) + " [s]")
#         print(str((t_crono.time() - t_init) / 60) + " [min]")
#         # Performance assessment
#         t_perf = t_crono.time()
#         Array.performance(Array_perf_inputs, Env_Conditions)
#         print(str(t_crono.time() - t_perf) + " [s]")
#         print(str((t_crono.time() - t_perf) / 60) + " [min]")
#         # ----------------------------------------------------------------------------------------------
#         # BACKEND auxiliar
#         """from backend_aux import et_array_to_jsondict

#         dic_results = et_array_to_jsondict(Array)
#         et_results = json.dumps(dic_results)
#         # with debug purposes, the output is dump to a file
#         with open("et_results.json", "w") as f:
#             json.dump(dic_results, f, indent=4, sort_keys=True)
#         print("END")"""
#     else:  # DESIGN MODE
#         range_min = int(50e3)  # user inputs
#         range_max = int(150e3)  # user inputs
#         range_step = int(50e3)  # user inputs
#         range_aux = []
#         total_sims = range_max / range_min
#         range_aux = np.arange(range_min, range_max + range_step, range_step)

#         # Gen_Nom_P = 100e3                         # USER INPUT (WATS) CMPX-1-2-3
#         # B2B_Nom_P = 100e3                         # USER INPUT CMPX 1-2-3
#         # turb_D = 1                                # USER INPUT ==> m
#         parameter_sim = 1  # 1mech - 2elec - 3grid
#         for cont10 in range(0, len(range_aux)):
#             # MODIFICATIONS ON THE INPUTS************************************************************************************
#             if parameter_sim == 1:  # mechanical sim

#                 if Technology == "Wave":  # WAVE TECHNOLOGY
#                     turb_D = range_aux[cont10]  # to be modified for other elements
#                     mechT_params = {
#                         "Type": Type_mech_transf,  # The name of the object
#                         "init_args": [
#                             Type_turbine,
#                             turb_D,
#                             S_owc_c,
#                             Technology,
#                         ],  # Required arguments to initialise the object
#                         "perf_args": obj_args,  # Required arguments to perform the performance of specific objects
#                         "mech_t_ratio": 10,
#                     }  # REVIEW THIS PARAMETER!!!!!!!!!!!!!!!!
#                 else:  # TIDAL TECHNOLOGY
#                     GB_Pnom = range_aux[cont10]  # USER INPUT CMPX1-2-3 W
#                     Type_mech_transf = (
#                         "Gearbox"  # USER INPUT CMPX 1-2-3 Gearbox - Mech_Simplified
#                     )
#                     mechT_params = {
#                         "Type": "Gearbox",  # The name of the object
#                         "init_args": [
#                             # GB_tech,
#                         GB_Pnom, GB_t_ratio, Technology],
#                         # Required arguments to initialise the object
#                         "perf_args": obj_args,
#                         # Required arguments to perform the performance of specific objects
#                         "mech_t_ratio": 1,
#                     }  # Always 1 with the gearbox as it is already defined in the init_args
#             else:
#                 if parameter_sim == 2:  # Electrical sim
#                     Gen_Nom_P = range_aux[cont10]
#                     electT_params = {
#                         "Type": Type_elect_transf,
#                         "init_args": [
#                             Gen_Nom_P,
#                             pp,
#                             Vnom,
#                             fnom,
#                             rel_T_maxnom,
#                             rel_V_maxnom,
#                             gen_class,
#                         ],
#                     }
#                 else:  # grid sim
#                     B2B_Nom_P = range_aux[cont10]
#                     gridC_params = {
#                         "Type": "B2B2level",
#                         "init_args": [
#                             B2B_Nom_P,
#                             Vdc,
#                             fsw,
#                             VnomGen,
#                             fnomGen,
#                             Lgen,
#                             Rgen,
#                             Vgrid,
#                             Rgrid,
#                             cosfi_grid,
#                             Lgrid,
#                         ],
#                     }
#             # ***************************************************************************************************************
#             pto_inputs = [mechT_params, electT_params, gridC_params, control_params]
#             json_gen.input_json_gen(
#                 Elect_CMPX,
#                 Grid_CMPX,
#                 Mech_CMPX,
#                 Type_mech_transf,
#                 Type_turbine,
#                 type_control,
#             )
#             Array_ID = "Array_0" + str(cont10)
#             t_init = t_crono.time()
#             Array = Array_class.Array(Array_ID, Array_inputs, Device_INPUTS, pto_inputs)
#             print(str(t_crono.time() - t_init) + " [s]")
#             print(str((t_crono.time() - t_init) / 60) + " [min]")
#             # Performance assessment
#             t_perf = t_crono.time()
#             Array.performance(Array_perf_inputs, Env_Conditions)
#             print(str(t_crono.time() - t_perf) + " [s]")
#             print(str((t_crono.time() - t_perf) / 60) + " [min]")


# def test_Tidal_analisis_Array(read_config):
#     # We also need the complexity level used at EC ==>  / [ec ]  "complexity": 1,
#     energy_capture_cmpx = 3

#     Elect_CMPX = 2  # USER INPUT
#     Grid_CMPX = 2  # USER INPUT
#     Mech_CMPX = 1  # USER INPUT

#     # Type_mech_transf = "AirTurbine"  # USER INPUT ==> (AirTurbine - Hydraulic - Gearbox / CMPX2-3) - Mech_Simplified CMPX3
#     # Type_elect_transf = 'SCIG'       # USER INPUT CMPX1-2-3  SCIG/ Elect_Simplified
#     # Type_grid_transf = 'B2B2level'   # USER INPUT CMPX 1-2-3  B2B2level-Grid_Simplified

#     mode = "analysis"  # User input 'analysis' / 'design'
#     # ARRAY - INPUTS
#     # *************************
#     Technology = "Tidal"  # CMPX 1-2-3 Wave-Tidal ==> /ec/  ["type"]
#     Number_of_devices = (
#         1  # CMPX 1-2-3 REVIEW!! ==>  /ec/{ecId}/farm   ["number_devices"]
#     )
#     # Initialisation INPUTS for an Array
#     Array_inputs = {"Technology": Technology, "Number of devices": Number_of_devices}
#     # ----------------------------------
#     Parallel_PTOs = 2  # USER INPUT CMPX1-2-3 In case there are two turbines in each dof-PTO, with its Elect and
#     # Grid conversion stages
#     CUT_IN_OUT = [0.5, 5]  # MODULE OUTPUT MC CMPX1-2-3
#     Dev_shut_flag = 1.0  # USER INPUT CMPX 1-2-3
#     # ----------------------------------------------------------------
#     PTO_matrix = np.zeros([6, 6])  # MODULE OUTPUT MC CMPX1-2-3
#     PTO_matrix[1, 1] = 10
#     PTO_matrix[3, 3] = 10
#     PTO_matrix[5, 5] = 10
#     GDL = 0
#     # /mc/{mcId}/model/wec/complexity3
#     # "shared_dof":  [0,0,1,0,0,0]
#     for cont1 in range(1, 6):
#         for cont2 in range(1, 6):
#             if PTO_matrix[cont1, cont2] > 0:
#                 GDL = GDL + 1

#     # Initialisation INPUTS
#     Device_INPUTS = {
#         "PTO_matrix": PTO_matrix,
#         "Parallel_PTOs": Parallel_PTOs,
#         "Shutdown_flag": Dev_shut_flag,
#         "cut_in_out": CUT_IN_OUT,
#     }
#     # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#     if Technology == "Wave":
#         # **********************************************************************************************************************
#         # DUMMY INPUTS from the Energy Capture (EC) and Site Characterisation (SC) modules -- WAVE Example
#         # ************************************************************************************************
#         # / ec / {ecId} / inputs / machine / wec / complexity1
#         # "device_capture_width_ratio": 0.3,
#         # /ec/{ecId}/inputs/machine/wec/complexity2
#         """"device_capture_width_ratio":[],
#         "tp_capture_width": [],Peak Period vector associated with the capture width ratio matrix
#         "hs_capture_width":[], Significant Wave Height vector associated with the capture width ratio matrix
#         "wave_angle_capture_width":[0],Incident Wave Angle vector associated with the capture width ratio matrix"""
#         # /ec/{ecId}/inputs/machine/wec/complexity3
#         """
#         "dir_pm":[0],
#         "hs_pm":[0.5,1.5,2.5],
#         "tp_pm":[2,3,4],
#         "scatter_diagram":[[]],
#         "power_matrix":[[]"""
#         """
#         path = os.path.dirname(os.path.abspath(__file__)) + "\\DB\\json_data\\"
#         EC_file = "EC_out_OES_Sphere"
#         EC_out = pd.read_json(
#             path + EC_file + ".json", orient="records", lines=True
#         )  # Data generated with the OES sphere

#         # Initialisation of variables
#         # /mc/{mcId}/model/wec/complexity2
#         # "pto_damping": It seems that is only 1 general parameter
#         # /mc/{mcId}/model/wec/complexity3
#         # "pto_damping":
#         Cpto = np.zeros(len(EC_out["Hs[m]"]))  # MODULE OUTPUT EC
#         Pcapt = np.zeros(
#             len(EC_out["Hs[m]"])
#         )  # MODULE OUTPUT EC /ec/{ecId}/devices/{deviceId} / captured_power_per_condition / capturedPower
#         Hs = EC_out["Hs[m]"]  # MODULE OUTPUT EC
#         Tp = EC_out["Tp[s]"]  # MODULE OUTPUT EC
#         Occ = EC_out["Occ"]  # MODULE OUTPUT EC
#         for c_SS in range(0, len(EC_out["Hs[m]"])):
#             # Maximum Captured power in each Sea State
#             ind_Pmax = EC_out["Pmean[W]"][c_SS][:].index(max(EC_out["Pmean[W]"][c_SS][:]))
#             Cpto[c_SS] = EC_out["Cpto[Ns/m]"][c_SS][ind_Pmax]
#             Pcapt[c_SS] = EC_out["Pmean[W]"][c_SS][ind_Pmax]"""
#         Cpto = np.zeros(len(read_config["Hs[m]"]))
#         Pcapt = np.zeros(len(read_config["Hs[m]"]))
#         Hs = read_config["Hs[m]"]
#         Tp = read_config["Tp[s]"]
#         Occ = read_config["Occ"]
#         for c_SS in range(0, len(read_config["Hs[m]"])):
#             # Maximum Captured power in each Sea State
#             ind_Pmax = read_config["Pmean[W]"][c_SS][:].index(
#                 max(read_config["Pmean[W]"][c_SS][:])
#             )
#             Cpto[c_SS] = read_config["Cpto[Ns/m]"][c_SS][ind_Pmax]
#             Pcapt[c_SS] = read_config["Pmean[W]"][c_SS][ind_Pmax]
#         # *****Energy Transformation (ET) Module -- INPUTS to be gathered from the ET-GUI *****
#         # **********************************************************************************************************************
#         # PTO - INPUTS
#         # ----------------------------------------------------------------------------------------------------------------------
#         # Required parameters to initialise the Mechanical Transformation object
#         # --- Air Turbine ---
#         Type_mech_transf = "AirTurbine"  # USER INPUT ==> (AirTurbine - Hydraulic - Gearbox / CMPX2-3) - Mech_Simplified CMPX3
#         if Mech_CMPX == 1:
#             Type_mech_transf = "Mech_Simplified"
#         if Type_mech_transf != "AirTurbine":
#             Type_turbine = "none"
#         else:
#             Type_turbine = "Impulse"  # USER INPUT ==> Impulse - Wells CMPX2-CMPX3
#         turb_D = 1  # USER INPUT ==> m
#         S_owc_c = np.pi * 2.5 ** 2  # OWC

#         obj_args = (
#             []
#         )  # This variable is required in case the object needs aditional inputs for performance
#         # -------------------
#         # Generic Dictionary input for the Mechanical Transformation
#         mechT_params = {
#             "Type": Type_mech_transf,  # The name of the object
#             "init_args": [
#                 Type_turbine,
#                 turb_D,
#                 S_owc_c,
#                 Technology,
#             ],  # Required arguments to initialise the object
#             "perf_args": obj_args,  # Required arguments to perform the performance of specific objects
#             "mech_t_ratio": 10,
#         }  # REVIEW THIS PARAMETER!!!!!!!!!!!!!!!!
#         # ******************************************************************************************************************
#         # ENVIRONMENTAL CONDITIONS
#         Env_Conditions = {"Hs": Hs, "Tz": Tp, "Occ": Occ}
#         # ARRAY - INPUTS
#         lst_random = [0.9, 1.1, 1.2, 0.95, 1.05]
#         lst_sigma2 = []
#         lst_cpto2 = []
#         Array_perf_inputs = []
#         dic1 = {}
#         dic2 = {}
#         for cont in range(0, GDL):
#             lst_sigma2.append(Pcapt / Cpto)
#             lst_cpto2.append(Cpto)
#             dic1 = {"sigma_v": lst_sigma2, "C_pto": lst_cpto2}
#             Array_perf_inputs.append(dic1)
#         # DEVICE
#         lst_cpto = []
#         lst_sigma = []
#         Device_perf_inputs = []
#         for cont2 in range(0, Number_of_devices):  # Degree of freedom
#             lst_cpto.append(Cpto)
#             lst_sigma.append(np.sqrt(Pcapt / Cpto))
#             dic2 = {"sigma_v": lst_sigma, "C_pto": lst_cpto}
#             Device_perf_inputs.append(dic2)
#     else:  # *********************TIDAL****************************************************************************************
#         # DUMMY INPUTS from the Energy Capture (EC) and Site Characterisation (SC) modules -- TIDAL example
#         # **************************************************************************************************
#         # /ec/{ecId}/inputs/machine/tec/complexity1  /
#         # "cp"

#         # /ec/{ecId}/inputs/machine/tec/complexity2  /  "rotor_diameter": 10,
#         # "cut_in_velocity": 1,
#         # "cut_out_velocity": 10,
#         # "cp": 0.3,
#         # "ct": 1,
#         # "rated_pow_device": 500,
#         # "number_rotor": 1,
#         # / ec / {ecId} / results / tec / complexity2
#         """"array_velocity_field":
#         "deviceID": 1,
#         "hub_velocity": 2.3"""
#         # / ec / {ecId} / inputs / site / tec / complexity2
#         # "velocity_field":[ 0,3],
#         # /ec/{ecId}/inputs/site/tec/complexity3
#         # "V": [
#         # "probability":[
#         # / mc / {mcId} / model / tec / complexity2
#         # "tip_speed_ratio": 1,

#         # / ec / {ecId} / inputs / machine / tec / complexity3
#         """"rotor_diameter": 10,
#         "hub_height": 20,
#         "floating": false,
#         "cut_in_velocity": 2,
#         "cut_out_velocity": 5,
#         "cp":   [       0.3,       0.8   ],
#         "ct":   [        0.3,        0.3    ],
#         "cp_ct_velocity":[ 2, 5 ],
#         "rated_pow_device": 500,
#         "number_rotor": 1,
#         """
#         # / ec / {ecId} / results / tec / complexity3
#         """"array_velocity_field":
#         "deviceID": 1,
#         "hub_velocity": 2.3"""
#         # /mc/{mcId}/model/tec/complexity3
#         # "tip_speed_ratio": 1,
#         Type_turbine = 0
#         current_vel = np.arange(0.1, 5, 0.05)  # m/s
#         turbine_radius = 10  # m
#         rho_water = 1025.0
#         vmean_max_occ = 5  # m/s
#         current_pdf = rayleigh.pdf(5 * current_vel / vmean_max_occ)
#         current_prob = current_pdf * np.mean(np.diff(current_vel))
#         P_rated = 2e6  # W
#         omega_rated = 14 * 2 * np.pi / 60  # rad/s
#         v_curr_rated = 3  # m/s
#         #  Power curve
#         power_exp = 3
#         power_low = (
#             P_rated
#             / v_curr_rated ** power_exp
#             * current_vel[current_vel <= v_curr_rated] ** power_exp
#         )
#         power_up = P_rated * np.ones(len(current_vel[current_vel > v_curr_rated]))
#         mean_power = np.concatenate([power_low, power_up])
#         #  Rotational speed curve
#         omega_exp = 1.5
#         omega_low = (
#             omega_rated
#             / v_curr_rated ** omega_exp
#             * current_vel[current_vel <= v_curr_rated] ** omega_exp
#         )
#         omega_up = omega_rated * np.ones(len(current_vel[current_vel > v_curr_rated]))
#         mean_omega = np.concatenate([omega_low, omega_up])
#         # Cp (power coefficient) and TSR (tip speed ratio)
#         Cp = mean_power / (
#             0.5 * rho_water * np.pi * turbine_radius ** 2 * current_vel ** 3
#         )
#         TSR = mean_omega * turbine_radius / current_vel
#         Cpto = current_vel * Cp * 0.5 * rho_water * np.pi * turbine_radius ** 2
#         Ct_tidal = (
#             1 / mean_omega
#         )  # OUTPUT FROM EC  = turbine_radius / (TSR * current_vel)
#         # ******************************************************************************************************************
#         # *****Energy Transformation (ET) Module -- INPUTS to be gathered from the ET-GUI *****
#         # ******************************************************************************************************************
#         # PTO - INPUTS
#         # *************************
#         # ------------------------------------------------------------------------------------------------------------------
#         # Required parameters to initialise the Mechanical Transformation object
#         # --- Gearbox ---
#         GB_tech = Technology
#         GB_Pnom = 5e5  # USER INPUT CMPX1-2-3 W
#         GB_t_ratio = 1 / 100  # USER INPUT CMPX1-2-3 relation
#         obj_args = {
#             "Ct_tidal": 0
#         }  # For tidal devices the torque relation with the speed changes w.r.t. wave devices. The corresponding
#         # coefficientes are provided through this variable to compute the torque on the shaft. It just needs to be initialised,
#         # and will be updated per current speed with the corresponding Ct_tidal set in the perf_inputs
#         # -------------------
#         # Generic Dictionary input for the Mechanical Transformation
#         Type_mech_transf = "Gearbox"  # USER INPUT CMPX 1-2-3 Gearbox - Mech_Simplified
#         if Mech_CMPX == 1:
#             Type_mech_transf = "Mech_Simplified"
#         mechT_params = {
#             "Type": Type_mech_transf,  # The name of the object
#             "init_args": [
#                 # GB_tech,
#                 GB_Pnom,
#                 GB_t_ratio,
#                 Technology,
#             ],  # Required arguments to initialise the object
#             "perf_args": obj_args,  # Required arguments to perform the performance of specific objects
#             "mech_t_ratio": 1,
#         }  # Always 1 with the gearbox as it is already defined in the init_args
#         # ------------------------------------------------------------------------------------------------------------------
#         # *******************************************************************************************************************
#         # Dummy inputs from Energy Capture for the 3 dof-PTOs of the device
#         # A Turb. intensity of a 10% is assumed - D.R.J. Sutherland et al.
#         lst_current_vel = []
#         lst_Cpto3 = []
#         lst_Ct_tidal = []
#         Device_perf_inputs = []
#         dic3 = {}
#         for cont2 in range(0, Number_of_devices):
#             lst_current_vel.append(0.1 * current_vel)
#             lst_Cpto3.append(Cpto)
#             lst_Ct_tidal.append(Ct_tidal)
#             dic3 = {
#                 "sigma_v": lst_current_vel,
#                 "C_pto": lst_Cpto3,
#                 "Ct_tidal": lst_Ct_tidal,
#             }
#             Device_perf_inputs.append(dic3)
#         # ******************************************************************************************************************
#         # The set of Environmental Conditions, for all devices
#         Env_Conditions = {
#             "Vc": current_vel,
#             "Tz": 10 * np.ones(len(current_vel)),
#             "Occ": current_prob,
#         }
#         lst_current_vel1 = []
#         lst_Cpto4 = []
#         lst_Ct_tidal1 = []
#         Array_perf_inputs = []
#         dic4 = {}
#         for cont in range(0, GDL):
#             lst_current_vel1.append(0.1 * current_vel)
#             lst_Cpto4.append(Cpto)
#             lst_Ct_tidal1.append(Ct_tidal)
#             dic4 = {
#                 "sigma_v": lst_current_vel1,
#                 "C_pto": lst_Cpto4,
#                 "Ct_tidal": lst_Ct_tidal1,
#             }
#             Array_perf_inputs.append(dic4)
#         lst_devices = []
#         Array_perf_inputs1 = []
#     # **********************************************************************************************************************
#     # *****Energy Transformation (ET) Module -- INPUTS to be gathered from the ET-GUI *****
#     # **********************************************************************************************************************
#     # PTO - INPUTS
#     # *************************
#     # ----------------------------------------------------------------------------------------------------------------------
#     # Required parameters to initialise the Electrical Transformation object
#     # --- Squirrel Cage Induction Generator ---
#     Gen_Nom_P = 100e3  # USER INPUT (WATS) CMPX-1-2-3
#     pp = 2  # USER INPUT  CMPX-2-3
#     Vnom = 400.0  # USER INPUT  CMPX-2-3
#     fnom = 50.0  # USER INPUT  CMPX-2-3
#     rel_T_maxnom = 2.0  # USER INPUT  CMPX-2-3
#     rel_V_maxnom = 1.725  # USER INPUT  CMPX-2-3
#     gen_class = "Class_F"  # USER INPUT  CMPX-2-3 Class_A-Class_B-Class_F-Class_G
#     Type_elect_transf = "SCIG"  # USER INPUT CMPX1-2-3  SCIG/ Elect_Simplified
#     if Elect_CMPX == 1:
#         Type_elect_transf = "Elect_Simplified"
#     electT_params = {
#         "Type": Type_elect_transf,
#         "init_args": [Gen_Nom_P, pp, Vnom, fnom, rel_T_maxnom, rel_V_maxnom, gen_class],
#     }
#     # ----------------------------------------------------------------------------------------------------------------------
#     # Required parameters to initialise the Grid Conditioning object
#     # --- Back to back to level ---
#     B2B_Nom_P = 100e3  # USER INPUT CMPX 1-2-3
#     Vdc = 1200  # USER INPUT CMPX 2-3
#     fsw = 5000  # USER INPUT CMPX 2-3
#     VnomGen = Vnom * np.sqrt(3)  # USER INPUT CMPX 2-3
#     fnomGen = 50.0  # USER INPUT CMPX 2-3
#     Lgen = 0.0005  # USER INPUT CMPX 2-3
#     Rgen = 0.0001  # USER INPUT CMPX 2-3
#     Vgrid = 690  # USER INPUT CMPX 2-3 /mc/{mcId}/general/ "rated_voltage"
#     Rgrid = 0.0001  # USER INPUT CMPX 2-3
#     Lgrid = 0.001  # USER INPUT CMPX 2-3
#     cosfi_grid = 0.91  # USER INPUT CMPX 2-3
#     Type_grid_transf = "B2B2level"  # USER INPUT CMPX 1-2-3  B2B2level-Grid_Simplified
#     if Mech_CMPX == 1:
#         Type_grid_transf = "Grid_Simplified"
#     gridC_params = {
#         "Type": "B2B2level",
#         "init_args": [
#             B2B_Nom_P,
#             Vdc,
#             fsw,
#             VnomGen,
#             fnomGen,
#             Lgen,
#             Rgen,
#             Vgrid,
#             Rgrid,
#             cosfi_grid,
#             Lgrid,
#         ],
#     }
#     # ----------------------------------------------------------------------------------------------------------------------
#     # Required parameters to initialise a Control object
#     # --- Passive Control ---
#     type_control = "Passive"  # USER INPUT CMPX 2-3 User_defined - Passive
#     n_sigma = 5  # USER INPUT CMPX 2-3
#     bins = 500  # USER INPUT CMPX 2-3 points per sea state (500)
#     vel_mean = 1e-6
#     control_params = {"Type": type_control, "init_args": [n_sigma, bins, vel_mean]}
#     # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#     # ----- ARRAY object example -----
#     # Initialisation

#     # Gen_Nom_P = 100e3                       # USER INPUT (WATS) CMPX-1-2-3
#     # B2B_Nom_P = 100e3                       # USER INPUT CMPX 1-2-3
#     # turb_D = 1                          # USER INPUT ==> m
#     if mode == "analysis":
#         pto_inputs = [mechT_params, electT_params, gridC_params, control_params]
#         # ----------------------------------------------------------------------------------------------------------------------
#         # json data input depending on the CMPX
#         json_gen.input_json_gen(
#             Elect_CMPX,
#             Grid_CMPX,
#             Mech_CMPX,
#             Type_mech_transf,
#             Type_turbine,
#             type_control,
#         )
#         Array_ID = "Array_01"
#         t_init = t_crono.time()
#         Array = Array_class.Array(Array_ID, Array_inputs, Device_INPUTS, pto_inputs)
#         print(str(t_crono.time() - t_init) + " [s]")
#         print(str((t_crono.time() - t_init) / 60) + " [min]")
#         # Performance assessment
#         t_perf = t_crono.time()
#         Array.performance(Array_perf_inputs, Env_Conditions)
#         print(str(t_crono.time() - t_perf) + " [s]")
#         print(str((t_crono.time() - t_perf) / 60) + " [min]")
#         # ----------------------------------------------------------------------------------------------
#         # BACKEND auxiliar
#         """from backend_aux import et_array_to_jsondict

#         dic_results = et_array_to_jsondict(Array)
#         et_results = json.dumps(dic_results)
#         # with debug purposes, the output is dump to a file
#         with open("et_results.json", "w") as f:
#             json.dump(dic_results, f, indent=4, sort_keys=True)
#         print("END")"""
#     else:  # DESIGN MODE
#         range_min = int(50e3)  # user inputs
#         range_max = int(150e3)  # user inputs
#         range_step = int(50e3)  # user inputs
#         range_aux = []
#         total_sims = range_max / range_min
#         range_aux = np.arange(range_min, range_max + range_step, range_step)

#         # Gen_Nom_P = 100e3                         # USER INPUT (WATS) CMPX-1-2-3
#         # B2B_Nom_P = 100e3                         # USER INPUT CMPX 1-2-3
#         # turb_D = 1                                # USER INPUT ==> m
#         parameter_sim = 1  # 1mech - 2elec - 3grid
#         for cont10 in range(0, len(range_aux)):
#             # MODIFICATIONS ON THE INPUTS************************************************************************************
#             if parameter_sim == 1:  # mechanical sim

#                 if Technology == "Wave":  # WAVE TECHNOLOGY
#                     turb_D = range_aux[cont10]  # to be modified for other elements
#                     mechT_params = {
#                         "Type": Type_mech_transf,  # The name of the object
#                         "init_args": [
#                             Type_turbine,
#                             turb_D,
#                             S_owc_c,
#                             Technology,
#                         ],  # Required arguments to initialise the object
#                         "perf_args": obj_args,  # Required arguments to perform the performance of specific objects
#                         "mech_t_ratio": 10,
#                     }  # REVIEW THIS PARAMETER!!!!!!!!!!!!!!!!
#                 else:  # TIDAL TECHNOLOGY
#                     GB_Pnom = range_aux[cont10]  # USER INPUT CMPX1-2-3 W
#                     Type_mech_transf = (
#                         "Gearbox"  # USER INPUT CMPX 1-2-3 Gearbox - Mech_Simplified
#                     )
#                     mechT_params = {
#                         "Type": "Gearbox",  # The name of the object
#                         "init_args": [
#                             # GB_tech,
#                             GB_Pnom, GB_t_ratio, Technology],
#                         # Required arguments to initialise the object
#                         "perf_args": obj_args,
#                         # Required arguments to perform the performance of specific objects
#                         "mech_t_ratio": 1,
#                     }  # Always 1 with the gearbox as it is already defined in the init_args
#             else:
#                 if parameter_sim == 2:  # Electrical sim
#                     Gen_Nom_P = range_aux[cont10]
#                     electT_params = {
#                         "Type": Type_elect_transf,
#                         "init_args": [
#                             Gen_Nom_P,
#                             pp,
#                             Vnom,
#                             fnom,
#                             rel_T_maxnom,
#                             rel_V_maxnom,
#                             gen_class,
#                         ],
#                     }
#                 else:  # grid sim
#                     B2B_Nom_P = range_aux[cont10]
#                     gridC_params = {
#                         "Type": "B2B2level",
#                         "init_args": [
#                             B2B_Nom_P,
#                             Vdc,
#                             fsw,
#                             VnomGen,
#                             fnomGen,
#                             Lgen,
#                             Rgen,
#                             Vgrid,
#                             Rgrid,
#                             cosfi_grid,
#                             Lgrid,
#                         ],
#                     }
#             # ***************************************************************************************************************
#             pto_inputs = [mechT_params, electT_params, gridC_params, control_params]
#             json_gen.input_json_gen(
#                 Elect_CMPX,
#                 Grid_CMPX,
#                 Mech_CMPX,
#                 Type_mech_transf,
#                 Type_turbine,
#                 type_control,
#             )
#             Array_ID = "Array_0" + str(cont10)
#             t_init = t_crono.time()
#             Array = Array_class.Array(Array_ID, Array_inputs, Device_INPUTS, pto_inputs)
#             print(str(t_crono.time() - t_init) + " [s]")
#             print(str((t_crono.time() - t_init) / 60) + " [min]")
#             # Performance assessment
#             t_perf = t_crono.time()
#             Array.performance(Array_perf_inputs, Env_Conditions)
#             print(str(t_crono.time() - t_perf) + " [s]")
#             print(str((t_crono.time() - t_perf) / 60) + " [min]")


# def test_Wave_design_Array(read_config):
#     # We also need the complexity level used at EC ==>  / [ec ]  "complexity": 1,
#     energy_capture_cmpx = 3

#     Elect_CMPX = 2  # USER INPUT
#     Grid_CMPX = 2  # USER INPUT
#     Mech_CMPX = 2  # USER INPUT

#     # Type_mech_transf = "AirTurbine"  # USER INPUT ==> (AirTurbine - Hydraulic - Gearbox / CMPX2-3) - Mech_Simplified CMPX3
#     # Type_elect_transf = 'SCIG'       # USER INPUT CMPX1-2-3  SCIG/ Elect_Simplified
#     # Type_grid_transf = 'B2B2level'   # USER INPUT CMPX 1-2-3  B2B2level-Grid_Simplified

#     mode = "design"  # User input 'analysis' / 'design'
#     # ARRAY - INPUTS
#     # *************************
#     Technology = "Wave"  # CMPX 1-2-3 Wave-Tidal ==> /ec/  ["type"]
#     Number_of_devices = (
#         1  # CMPX 1-2-3 REVIEW!! ==>  /ec/{ecId}/farm   ["number_devices"]
#     )
#     # Initialisation INPUTS for an Array
#     Array_inputs = {"Technology": Technology, "Number of devices": Number_of_devices}
#     # ----------------------------------
#     Parallel_PTOs = 2  # USER INPUT CMPX1-2-3 In case there are two turbines in each dof-PTO, with its Elect and
#     # Grid conversion stages
#     CUT_IN_OUT = [0.5, 5]  # MODULE OUTPUT MC CMPX1-2-3
#     Dev_shut_flag = 1.0  # USER INPUT CMPX 1-2-3
#     # ----------------------------------------------------------------
#     PTO_matrix = np.zeros([6, 6])  # MODULE OUTPUT MC CMPX1-2-3
#     PTO_matrix[1, 1] = 10
#     PTO_matrix[3, 3] = 10
#     PTO_matrix[5, 5] = 10
#     GDL = 0
#     # /mc/{mcId}/model/wec/complexity3
#     # "shared_dof":  [0,0,1,0,0,0]
#     for cont1 in range(1, 6):
#         for cont2 in range(1, 6):
#             if PTO_matrix[cont1, cont2] > 0:
#                 GDL = GDL + 1

#     # Initialisation INPUTS
#     Device_INPUTS = {
#         "PTO_matrix": PTO_matrix,
#         "Parallel_PTOs": Parallel_PTOs,
#         "Shutdown_flag": Dev_shut_flag,
#         "cut_in_out": CUT_IN_OUT,
#     }
#     # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#     if Technology == "Wave":
#         # **********************************************************************************************************************
#         # DUMMY INPUTS from the Energy Capture (EC) and Site Characterisation (SC) modules -- WAVE Example
#         # ************************************************************************************************
#         # / ec / {ecId} / inputs / machine / wec / complexity1
#         # "device_capture_width_ratio": 0.3,
#         # /ec/{ecId}/inputs/machine/wec/complexity2
#         """"device_capture_width_ratio":[],
#         "tp_capture_width": [],Peak Period vector associated with the capture width ratio matrix
#         "hs_capture_width":[], Significant Wave Height vector associated with the capture width ratio matrix
#         "wave_angle_capture_width":[0],Incident Wave Angle vector associated with the capture width ratio matrix"""
#         # /ec/{ecId}/inputs/machine/wec/complexity3
#         """
#         "dir_pm":[0],
#         "hs_pm":[0.5,1.5,2.5],
#         "tp_pm":[2,3,4],
#         "scatter_diagram":[[]],
#         "power_matrix":[[]"""
#         """
#         path = os.path.dirname(os.path.abspath(__file__)) + "\\DB\\json_data\\"
#         EC_file = "EC_out_OES_Sphere"
#         EC_out = pd.read_json(
#             path + EC_file + ".json", orient="records", lines=True
#         )  # Data generated with the OES sphere

#         # Initialisation of variables
#         # /mc/{mcId}/model/wec/complexity2
#         # "pto_damping": It seems that is only 1 general parameter
#         # /mc/{mcId}/model/wec/complexity3
#         # "pto_damping":
#         Cpto = np.zeros(len(EC_out["Hs[m]"]))  # MODULE OUTPUT EC
#         Pcapt = np.zeros(
#             len(EC_out["Hs[m]"])
#         )  # MODULE OUTPUT EC /ec/{ecId}/devices/{deviceId} / captured_power_per_condition / capturedPower
#         Hs = EC_out["Hs[m]"]  # MODULE OUTPUT EC
#         Tp = EC_out["Tp[s]"]  # MODULE OUTPUT EC
#         Occ = EC_out["Occ"]  # MODULE OUTPUT EC
#         for c_SS in range(0, len(EC_out["Hs[m]"])):
#             # Maximum Captured power in each Sea State
#             ind_Pmax = EC_out["Pmean[W]"][c_SS][:].index(max(EC_out["Pmean[W]"][c_SS][:]))
#             Cpto[c_SS] = EC_out["Cpto[Ns/m]"][c_SS][ind_Pmax]
#             Pcapt[c_SS] = EC_out["Pmean[W]"][c_SS][ind_Pmax]"""
#         Cpto = np.zeros(len(read_config["Hs[m]"]))
#         Pcapt = np.zeros(len(read_config["Hs[m]"]))
#         Hs = read_config["Hs[m]"]
#         Tp = read_config["Tp[s]"]
#         Occ = read_config["Occ"]
#         for c_SS in range(0, len(read_config["Hs[m]"])):
#             # Maximum Captured power in each Sea State
#             ind_Pmax = read_config["Pmean[W]"][c_SS][:].index(
#                 max(read_config["Pmean[W]"][c_SS][:])
#             )
#             Cpto[c_SS] = read_config["Cpto[Ns/m]"][c_SS][ind_Pmax]
#             Pcapt[c_SS] = read_config["Pmean[W]"][c_SS][ind_Pmax]
#         # *****Energy Transformation (ET) Module -- INPUTS to be gathered from the ET-GUI *****
#         # **********************************************************************************************************************
#         # PTO - INPUTS
#         # ----------------------------------------------------------------------------------------------------------------------
#         # Required parameters to initialise the Mechanical Transformation object
#         # --- Air Turbine ---
#         Type_mech_transf = "AirTurbine"  # USER INPUT ==> (AirTurbine - Hydraulic - Gearbox / CMPX2-3) - Mech_Simplified CMPX3
#         if Mech_CMPX == 1:
#             Type_mech_transf = "Mech_Simplified"
#         if Type_mech_transf != "AirTurbine":
#             Type_turbine = "none"
#         else:
#             Type_turbine = "Impulse"  # USER INPUT ==> Impulse - Wells CMPX2-CMPX3
#         turb_D = 1  # USER INPUT ==> m
#         S_owc_c = np.pi * 2.5 ** 2  # OWC

#         obj_args = (
#             []
#         )  # This variable is required in case the object needs aditional inputs for performance
#         # -------------------
#         # Generic Dictionary input for the Mechanical Transformation
#         mechT_params = {
#             "Type": Type_mech_transf,  # The name of the object
#             "init_args": [
#                 Type_turbine,
#                 turb_D,
#                 S_owc_c,
#                 Technology,
#             ],  # Required arguments to initialise the object
#             "perf_args": obj_args,  # Required arguments to perform the performance of specific objects
#             "mech_t_ratio": 10,
#         }
#         # ******************************************************************************************************************
#         # ENVIRONMENTAL CONDITIONS
#         Env_Conditions = {"Hs": Hs, "Tz": Tp, "Occ": Occ}
#         # ARRAY - INPUTS
#         lst_random = [0.9, 1.1, 1.2, 0.95, 1.05]
#         lst_sigma2 = []
#         lst_cpto2 = []
#         Array_perf_inputs = []
#         dic1 = {}
#         dic2 = {}
#         for cont in range(0, GDL):
#             lst_sigma2.append(Pcapt / Cpto)
#             lst_cpto2.append(Cpto)
#             dic1 = {"sigma_v": lst_sigma2, "C_pto": lst_cpto2}
#             Array_perf_inputs.append(dic1)
#         # DEVICE
#         lst_cpto = []
#         lst_sigma = []
#         Device_perf_inputs = []
#         for cont2 in range(0, Number_of_devices):  # Degree of freedom
#             lst_cpto.append(Cpto)
#             lst_sigma.append(np.sqrt(Pcapt / Cpto))
#             dic2 = {"sigma_v": lst_sigma, "C_pto": lst_cpto}
#             Device_perf_inputs.append(dic2)
#     else:  # *********************TIDAL****************************************************************************************
#         # DUMMY INPUTS from the Energy Capture (EC) and Site Characterisation (SC) modules -- TIDAL example
#         # **************************************************************************************************
#         # /ec/{ecId}/inputs/machine/tec/complexity1  /
#         # "cp"

#         # /ec/{ecId}/inputs/machine/tec/complexity2  /  "rotor_diameter": 10,
#         # "cut_in_velocity": 1,
#         # "cut_out_velocity": 10,
#         # "cp": 0.3,
#         # "ct": 1,
#         # "rated_pow_device": 500,
#         # "number_rotor": 1,
#         # / ec / {ecId} / results / tec / complexity2
#         """"array_velocity_field":
#         "deviceID": 1,
#         "hub_velocity": 2.3"""
#         # / ec / {ecId} / inputs / site / tec / complexity2
#         # "velocity_field":[ 0,3],
#         # /ec/{ecId}/inputs/site/tec/complexity3
#         # "V": [
#         # "probability":[
#         # / mc / {mcId} / model / tec / complexity2
#         # "tip_speed_ratio": 1,

#         # / ec / {ecId} / inputs / machine / tec / complexity3
#         """"rotor_diameter": 10,
#         "hub_height": 20,
#         "floating": false,
#         "cut_in_velocity": 2,
#         "cut_out_velocity": 5,
#         "cp":   [       0.3,       0.8   ],
#         "ct":   [        0.3,        0.3    ],
#         "cp_ct_velocity":[ 2, 5 ],
#         "rated_pow_device": 500,
#         "number_rotor": 1,
#         """
#         # / ec / {ecId} / results / tec / complexity3
#         """"array_velocity_field":
#         "deviceID": 1,
#         "hub_velocity": 2.3"""
#         # /mc/{mcId}/model/tec/complexity3
#         # "tip_speed_ratio": 1,
#         Type_turbine = 0
#         current_vel = np.arange(0.1, 5, 0.05)  # m/s
#         turbine_radius = 10  # m
#         rho_water = 1025.0
#         vmean_max_occ = 5  # m/s
#         current_pdf = rayleigh.pdf(5 * current_vel / vmean_max_occ)
#         current_prob = current_pdf * np.mean(np.diff(current_vel))
#         P_rated = 2e6  # W
#         omega_rated = 14 * 2 * np.pi / 60  # rad/s
#         v_curr_rated = 3  # m/s
#         #  Power curve
#         power_exp = 3
#         power_low = (
#             P_rated
#             / v_curr_rated ** power_exp
#             * current_vel[current_vel <= v_curr_rated] ** power_exp
#         )
#         power_up = P_rated * np.ones(len(current_vel[current_vel > v_curr_rated]))
#         mean_power = np.concatenate([power_low, power_up])
#         #  Rotational speed curve
#         omega_exp = 1.5
#         omega_low = (
#             omega_rated
#             / v_curr_rated ** omega_exp
#             * current_vel[current_vel <= v_curr_rated] ** omega_exp
#         )
#         omega_up = omega_rated * np.ones(len(current_vel[current_vel > v_curr_rated]))
#         mean_omega = np.concatenate([omega_low, omega_up])
#         # Cp (power coefficient) and TSR (tip speed ratio)
#         Cp = mean_power / (
#             0.5 * rho_water * np.pi * turbine_radius ** 2 * current_vel ** 3
#         )
#         TSR = mean_omega * turbine_radius / current_vel
#         Cpto = current_vel * Cp * 0.5 * rho_water * np.pi * turbine_radius ** 2
#         Ct_tidal = (
#             1 / mean_omega
#         )  # OUTPUT FROM EC  = turbine_radius / (TSR * current_vel)
#         # ******************************************************************************************************************
#         # *****Energy Transformation (ET) Module -- INPUTS to be gathered from the ET-GUI *****
#         # ******************************************************************************************************************
#         # PTO - INPUTS
#         # *************************
#         # ------------------------------------------------------------------------------------------------------------------
#         # Required parameters to initialise the Mechanical Transformation object
#         # --- Gearbox ---
#         GB_tech = Technology
#         GB_Pnom = 5e5  # USER INPUT CMPX1-2-3 W
#         GB_t_ratio = 1 / 100  # USER INPUT CMPX1-2-3 relation
#         obj_args = {
#             "Ct_tidal": 0
#         }  # For tidal devices the torque relation with the speed changes w.r.t. wave devices. The corresponding
#         # coefficientes are provided through this variable to compute the torque on the shaft. It just needs to be initialised,
#         # and will be updated per current speed with the corresponding Ct_tidal set in the perf_inputs
#         # -------------------
#         # Generic Dictionary input for the Mechanical Transformation
#         Type_mech_transf = "Gearbox"  # USER INPUT CMPX 1-2-3 Gearbox - Mech_Simplified
#         if Mech_CMPX == 1:
#             Type_mech_transf = "Mech_Simplified"
#         mechT_params = {
#             "Type": Type_mech_transf,  # The name of the object
#             "init_args": [
#                 # GB_tech,
#                 GB_Pnom,
#                 GB_t_ratio,
#                 Technology,
#             ],  # Required arguments to initialise the object
#             "perf_args": obj_args,  # Required arguments to perform the performance of specific objects
#             "mech_t_ratio": 1,
#         }  # Always 1 with the gearbox as it is already defined in the init_args
#         # ------------------------------------------------------------------------------------------------------------------
#         # *******************************************************************************************************************
#         # Dummy inputs from Energy Capture for the 3 dof-PTOs of the device
#         # A Turb. intensity of a 10% is assumed - D.R.J. Sutherland et al.
#         lst_current_vel = []
#         lst_Cpto3 = []
#         lst_Ct_tidal = []
#         Device_perf_inputs = []
#         dic3 = {}
#         for cont2 in range(0, Number_of_devices):
#             lst_current_vel.append(0.1 * current_vel)
#             lst_Cpto3.append(Cpto)
#             lst_Ct_tidal.append(Ct_tidal)
#             dic3 = {
#                 "sigma_v": lst_current_vel,
#                 "C_pto": lst_Cpto3,
#                 "Ct_tidal": lst_Ct_tidal,
#             }
#             Device_perf_inputs.append(dic3)
#         # ******************************************************************************************************************
#         # The set of Environmental Conditions, for all devices
#         Env_Conditions = {
#             "Vc": current_vel,
#             "Tz": 10 * np.ones(len(current_vel)),
#             "Occ": current_prob,
#         }
#         lst_current_vel1 = []
#         lst_Cpto4 = []
#         lst_Ct_tidal1 = []
#         Array_perf_inputs = []
#         dic4 = {}
#         for cont in range(0, GDL):
#             lst_current_vel1.append(0.1 * current_vel)
#             lst_Cpto4.append(Cpto)
#             lst_Ct_tidal1.append(Ct_tidal)
#             dic4 = {
#                 "sigma_v": lst_current_vel1,
#                 "C_pto": lst_Cpto4,
#                 "Ct_tidal": lst_Ct_tidal1,
#             }
#             Array_perf_inputs.append(dic4)
#         lst_devices = []
#         Array_perf_inputs1 = []
#     # **********************************************************************************************************************
#     # *****Energy Transformation (ET) Module -- INPUTS to be gathered from the ET-GUI *****
#     # **********************************************************************************************************************
#     # PTO - INPUTS
#     # *************************
#     # ----------------------------------------------------------------------------------------------------------------------
#     # Required parameters to initialise the Electrical Transformation object
#     # --- Squirrel Cage Induction Generator ---
#     Gen_Nom_P = 100e3  # USER INPUT (WATS) CMPX-1-2-3
#     pp = 2  # USER INPUT  CMPX-2-3
#     Vnom = 400.0  # USER INPUT  CMPX-2-3
#     fnom = 50.0  # USER INPUT  CMPX-2-3
#     rel_T_maxnom = 2.0  # USER INPUT  CMPX-2-3
#     rel_V_maxnom = 1.725  # USER INPUT  CMPX-2-3
#     gen_class = "Class_F"  # USER INPUT  CMPX-2-3 Class_A-Class_B-Class_F-Class_G
#     Type_elect_transf = "SCIG"  # USER INPUT CMPX1-2-3  SCIG/ Elect_Simplified
#     if Elect_CMPX == 1:
#         Type_elect_transf = "Elect_Simplified"
#     electT_params = {
#         "Type": Type_elect_transf,
#         "init_args": [Gen_Nom_P, pp, Vnom, fnom, rel_T_maxnom, rel_V_maxnom, gen_class],
#     }
#     # ----------------------------------------------------------------------------------------------------------------------
#     # Required parameters to initialise the Grid Conditioning object
#     # --- Back to back to level ---
#     B2B_Nom_P = 100e3  # USER INPUT CMPX 1-2-3
#     Vdc = 1200  # USER INPUT CMPX 2-3
#     fsw = 5000  # USER INPUT CMPX 2-3
#     VnomGen = Vnom * np.sqrt(3)  # USER INPUT CMPX 2-3
#     fnomGen = 50.0  # USER INPUT CMPX 2-3
#     Lgen = 0.0005  # USER INPUT CMPX 2-3
#     Rgen = 0.0001  # USER INPUT CMPX 2-3
#     Vgrid = 690  # USER INPUT CMPX 2-3 /mc/{mcId}/general/ "rated_voltage"
#     Rgrid = 0.0001  # USER INPUT CMPX 2-3
#     Lgrid = 0.001  # USER INPUT CMPX 2-3
#     cosfi_grid = 0.91  # USER INPUT CMPX 2-3
#     Type_grid_transf = "B2B2level"  # USER INPUT CMPX 1-2-3  B2B2level-Grid_Simplified
#     if Mech_CMPX == 1:
#         Type_grid_transf = "Grid_Simplified"
#     gridC_params = {
#         "Type": "B2B2level",
#         "init_args": [
#             B2B_Nom_P,
#             Vdc,
#             fsw,
#             VnomGen,
#             fnomGen,
#             Lgen,
#             Rgen,
#             Vgrid,
#             Rgrid,
#             cosfi_grid,
#             Lgrid,
#         ],
#     }
#     # ----------------------------------------------------------------------------------------------------------------------
#     # Required parameters to initialise a Control object
#     # --- Passive Control ---
#     type_control = "Passive"  # USER INPUT CMPX 2-3 User_defined - Passive
#     n_sigma = 5  # USER INPUT CMPX 2-3
#     bins = 500  # USER INPUT CMPX 2-3 points per sea state (500)
#     vel_mean = 1e-6
#     control_params = {"Type": type_control, "init_args": [n_sigma, bins, vel_mean]}
#     # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#     # ----- ARRAY object example -----
#     # Initialisation

#     # Gen_Nom_P = 100e3                       # USER INPUT (WATS) CMPX-1-2-3
#     # B2B_Nom_P = 100e3                       # USER INPUT CMPX 1-2-3
#     # turb_D = 1                          # USER INPUT ==> m
#     if mode == "analysis":
#         pto_inputs = [mechT_params, electT_params, gridC_params, control_params]
#         # ----------------------------------------------------------------------------------------------------------------------
#         # json data input depending on the CMPX
#         json_gen.input_json_gen(
#             Elect_CMPX,
#             Grid_CMPX,
#             Mech_CMPX,
#             Type_mech_transf,
#             Type_turbine,
#             type_control,
#         )
#         Array_ID = "Array_01"
#         t_init = t_crono.time()
#         Array = Array_class.Array(Array_ID, Array_inputs, Device_INPUTS, pto_inputs)
#         print(str(t_crono.time() - t_init) + " [s]")
#         print(str((t_crono.time() - t_init) / 60) + " [min]")
#         # Performance assessment
#         t_perf = t_crono.time()
#         Array.performance(Array_perf_inputs, Env_Conditions)
#         print(str(t_crono.time() - t_perf) + " [s]")
#         print(str((t_crono.time() - t_perf) / 60) + " [min]")
#         # ----------------------------------------------------------------------------------------------
#         # BACKEND auxiliar
#         """from backend_aux import et_array_to_jsondict

#         dic_results = et_array_to_jsondict(Array)
#         et_results = json.dumps(dic_results)
#         # with debug purposes, the output is dump to a file
#         with open("et_results.json", "w") as f:
#             json.dump(dic_results, f, indent=4, sort_keys=True)
#         print("END")"""
#     else:  # DESIGN MODE
#         range_min = int(50e3)  # user inputs
#         range_max = int(150e3)  # user inputs
#         range_step = int(50e3)  # user inputs
#         range_aux = []
#         total_sims = range_max / range_min
#         range_aux = np.arange(range_min, range_max + range_step, range_step)

#         # Gen_Nom_P = 100e3                         # USER INPUT (WATS) CMPX-1-2-3
#         # B2B_Nom_P = 100e3                         # USER INPUT CMPX 1-2-3
#         # turb_D = 1                                # USER INPUT ==> m
#         parameter_sim = 1  # 1mech - 2elec - 3grid
#         for cont10 in range(0, len(range_aux)):
#             # MODIFICATIONS ON THE INPUTS************************************************************************************
#             if parameter_sim == 1:  # mechanical sim

#                 if Technology == "Wave":  # WAVE TECHNOLOGY
#                     turb_D = range_aux[cont10]  # to be modified for other elements
#                     mechT_params = {
#                         "Type": Type_mech_transf,  # The name of the object
#                         "init_args": [
#                             Type_turbine,
#                             turb_D,
#                             S_owc_c,
#                             Technology,
#                         ],  # Required arguments to initialise the object
#                         "perf_args": obj_args,  # Required arguments to perform the performance of specific objects
#                         "mech_t_ratio": 10,
#                     }  # REVIEW THIS PARAMETER!!!!!!!!!!!!!!!!
#                 else:  # TIDAL TECHNOLOGY
#                     GB_Pnom = range_aux[cont10]  # USER INPUT CMPX1-2-3 W
#                     Type_mech_transf = (
#                         "Gearbox"  # USER INPUT CMPX 1-2-3 Gearbox - Mech_Simplified
#                     )
#                     mechT_params = {
#                         "Type": "Gearbox",  # The name of the object
#                         "init_args": [
#                             # GB_tech,
#                             GB_Pnom, GB_t_ratio, Technology],
#                         # Required arguments to initialise the object
#                         "perf_args": obj_args,
#                         # Required arguments to perform the performance of specific objects
#                         "mech_t_ratio": 1,
#                     }  # Always 1 with the gearbox as it is already defined in the init_args
#             else:
#                 if parameter_sim == 2:  # Electrical sim
#                     Gen_Nom_P = range_aux[cont10]
#                     electT_params = {
#                         "Type": Type_elect_transf,
#                         "init_args": [
#                             Gen_Nom_P,
#                             pp,
#                             Vnom,
#                             fnom,
#                             rel_T_maxnom,
#                             rel_V_maxnom,
#                             gen_class,
#                         ],
#                     }
#                 else:  # grid sim
#                     B2B_Nom_P = range_aux[cont10]
#                     gridC_params = {
#                         "Type": "B2B2level",
#                         "init_args": [
#                             B2B_Nom_P,
#                             Vdc,
#                             fsw,
#                             VnomGen,
#                             fnomGen,
#                             Lgen,
#                             Rgen,
#                             Vgrid,
#                             Rgrid,
#                             cosfi_grid,
#                             Lgrid,
#                         ],
#                     }
#             # ***************************************************************************************************************
#             pto_inputs = [mechT_params, electT_params, gridC_params, control_params]
#             json_gen.input_json_gen(
#                 Elect_CMPX,
#                 Grid_CMPX,
#                 Mech_CMPX,
#                 Type_mech_transf,
#                 Type_turbine,
#                 type_control,
#             )
#             Array_ID = "Array_0" + str(cont10)
#             t_init = t_crono.time()
#             Array = Array_class.Array(Array_ID, Array_inputs, Device_INPUTS, pto_inputs)
#             print(str(t_crono.time() - t_init) + " [s]")
#             print(str((t_crono.time() - t_init) / 60) + " [min]")
#             # Performance assessment
#             t_perf = t_crono.time()
#             Array.performance(Array_perf_inputs, Env_Conditions)
#             print(str(t_crono.time() - t_perf) + " [s]")
#             print(str((t_crono.time() - t_perf) / 60) + " [min]")


# def test_Tidal_design_Array(read_config):
#     # We also need the complexity level used at EC ==>  / [ec ]  "complexity": 1,
#     energy_capture_cmpx = 3

#     Elect_CMPX = 2  # USER INPUT
#     Grid_CMPX = 2  # USER INPUT
#     Mech_CMPX = 1  # USER INPUT

#     # Type_mech_transf = "AirTurbine"  # USER INPUT ==> (AirTurbine - Hydraulic - Gearbox / CMPX2-3) - Mech_Simplified CMPX3
#     # Type_elect_transf = 'SCIG'       # USER INPUT CMPX1-2-3  SCIG/ Elect_Simplified
#     # Type_grid_transf = 'B2B2level'   # USER INPUT CMPX 1-2-3  B2B2level-Grid_Simplified

#     mode = "design"  # User input 'analysis' / 'design'
#     # ARRAY - INPUTS
#     # *************************
#     Technology = "Tidal"  # CMPX 1-2-3 Wave-Tidal ==> /ec/  ["type"]
#     Number_of_devices = (
#         1  # CMPX 1-2-3 REVIEW!! ==>  /ec/{ecId}/farm   ["number_devices"]
#     )
#     # Initialisation INPUTS for an Array
#     Array_inputs = {"Technology": Technology, "Number of devices": Number_of_devices}
#     # ----------------------------------
#     Parallel_PTOs = 2  # USER INPUT CMPX1-2-3 In case there are two turbines in each dof-PTO, with its Elect and
#     # Grid conversion stages
#     CUT_IN_OUT = [0.5, 5]  # MODULE OUTPUT MC CMPX1-2-3
#     Dev_shut_flag = 1.0  # USER INPUT CMPX 1-2-3
#     # ----------------------------------------------------------------
#     PTO_matrix = np.zeros([6, 6])  # MODULE OUTPUT MC CMPX1-2-3
#     PTO_matrix[1, 1] = 10
#     PTO_matrix[3, 3] = 10
#     PTO_matrix[5, 5] = 10
#     GDL = 0
#     # /mc/{mcId}/model/wec/complexity3
#     # "shared_dof":  [0,0,1,0,0,0]
#     for cont1 in range(1, 6):
#         for cont2 in range(1, 6):
#             if PTO_matrix[cont1, cont2] > 0:
#                 GDL = GDL + 1

#     # Initialisation INPUTS
#     Device_INPUTS = {
#         "PTO_matrix": PTO_matrix,
#         "Parallel_PTOs": Parallel_PTOs,
#         "Shutdown_flag": Dev_shut_flag,
#         "cut_in_out": CUT_IN_OUT,
#     }
#     # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#     if Technology == "Wave":
#         # **********************************************************************************************************************
#         # DUMMY INPUTS from the Energy Capture (EC) and Site Characterisation (SC) modules -- WAVE Example
#         # ************************************************************************************************
#         # / ec / {ecId} / inputs / machine / wec / complexity1
#         # "device_capture_width_ratio": 0.3,
#         # /ec/{ecId}/inputs/machine/wec/complexity2
#         """"device_capture_width_ratio":[],
#         "tp_capture_width": [],Peak Period vector associated with the capture width ratio matrix
#         "hs_capture_width":[], Significant Wave Height vector associated with the capture width ratio matrix
#         "wave_angle_capture_width":[0],Incident Wave Angle vector associated with the capture width ratio matrix"""
#         # /ec/{ecId}/inputs/machine/wec/complexity3
#         """
#         "dir_pm":[0],
#         "hs_pm":[0.5,1.5,2.5],
#         "tp_pm":[2,3,4],
#         "scatter_diagram":[[]],
#         "power_matrix":[[]"""
#         """
#         path = os.path.dirname(os.path.abspath(__file__)) + "\\DB\\json_data\\"
#         EC_file = "EC_out_OES_Sphere"
#         EC_out = pd.read_json(
#             path + EC_file + ".json", orient="records", lines=True
#         )  # Data generated with the OES sphere

#         # Initialisation of variables
#         # /mc/{mcId}/model/wec/complexity2
#         # "pto_damping": It seems that is only 1 general parameter
#         # /mc/{mcId}/model/wec/complexity3
#         # "pto_damping":
#         Cpto = np.zeros(len(EC_out["Hs[m]"]))  # MODULE OUTPUT EC
#         Pcapt = np.zeros(
#             len(EC_out["Hs[m]"])
#         )  # MODULE OUTPUT EC /ec/{ecId}/devices/{deviceId} / captured_power_per_condition / capturedPower
#         Hs = EC_out["Hs[m]"]  # MODULE OUTPUT EC
#         Tp = EC_out["Tp[s]"]  # MODULE OUTPUT EC
#         Occ = EC_out["Occ"]  # MODULE OUTPUT EC
#         for c_SS in range(0, len(EC_out["Hs[m]"])):
#             # Maximum Captured power in each Sea State
#             ind_Pmax = EC_out["Pmean[W]"][c_SS][:].index(max(EC_out["Pmean[W]"][c_SS][:]))
#             Cpto[c_SS] = EC_out["Cpto[Ns/m]"][c_SS][ind_Pmax]
#             Pcapt[c_SS] = EC_out["Pmean[W]"][c_SS][ind_Pmax]"""
#         Cpto = np.zeros(len(read_config["Hs[m]"]))
#         Pcapt = np.zeros(len(read_config["Hs[m]"]))
#         Hs = read_config["Hs[m]"]
#         Tp = read_config["Tp[s]"]
#         Occ = read_config["Occ"]
#         for c_SS in range(0, len(read_config["Hs[m]"])):
#             # Maximum Captured power in each Sea State
#             ind_Pmax = read_config["Pmean[W]"][c_SS][:].index(
#                 max(read_config["Pmean[W]"][c_SS][:])
#             )
#             Cpto[c_SS] = read_config["Cpto[Ns/m]"][c_SS][ind_Pmax]
#             Pcapt[c_SS] = read_config["Pmean[W]"][c_SS][ind_Pmax]
#         # *****Energy Transformation (ET) Module -- INPUTS to be gathered from the ET-GUI *****
#         # **********************************************************************************************************************
#         # PTO - INPUTS
#         # ----------------------------------------------------------------------------------------------------------------------
#         # Required parameters to initialise the Mechanical Transformation object
#         # --- Air Turbine ---
#         Type_mech_transf = "AirTurbine"  # USER INPUT ==> (AirTurbine - Hydraulic - Gearbox / CMPX2-3) - Mech_Simplified CMPX3
#         if Mech_CMPX == 1:
#             Type_mech_transf = "Mech_Simplified"
#         if Type_mech_transf != "AirTurbine":
#             Type_turbine = "none"
#         else:
#             Type_turbine = "Impulse"  # USER INPUT ==> Impulse - Wells CMPX2-CMPX3
#         turb_D = 1  # USER INPUT ==> m
#         S_owc_c = np.pi * 2.5 ** 2  # OWC

#         obj_args = (
#             []
#         )  # This variable is required in case the object needs aditional inputs for performance
#         # -------------------
#         # Generic Dictionary input for the Mechanical Transformation
#         mechT_params = {
#             "Type": Type_mech_transf,  # The name of the object
#             "init_args": [
#                 Type_turbine,
#                 turb_D,
#                 S_owc_c,
#                 Technology,
#             ],  # Required arguments to initialise the object
#             "perf_args": obj_args,  # Required arguments to perform the performance of specific objects
#             "mech_t_ratio": 10,
#         }  # REVIEW THIS PARAMETER!!!!!!!!!!!!!!!!
#         # ******************************************************************************************************************
#         # ENVIRONMENTAL CONDITIONS
#         Env_Conditions = {"Hs": Hs, "Tz": Tp, "Occ": Occ}
#         # ARRAY - INPUTS
#         lst_random = [0.9, 1.1, 1.2, 0.95, 1.05]
#         lst_sigma2 = []
#         lst_cpto2 = []
#         Array_perf_inputs = []
#         dic1 = {}
#         dic2 = {}
#         for cont in range(0, GDL):
#             lst_sigma2.append(Pcapt / Cpto)
#             lst_cpto2.append(Cpto)
#             dic1 = {"sigma_v": lst_sigma2, "C_pto": lst_cpto2}
#             Array_perf_inputs.append(dic1)
#         # DEVICE
#         lst_cpto = []
#         lst_sigma = []
#         Device_perf_inputs = []
#         for cont2 in range(0, Number_of_devices):  # Degree of freedom
#             lst_cpto.append(Cpto)
#             lst_sigma.append(np.sqrt(Pcapt / Cpto))
#             dic2 = {"sigma_v": lst_sigma, "C_pto": lst_cpto}
#             Device_perf_inputs.append(dic2)
#     else:  # *********************TIDAL****************************************************************************************
#         # DUMMY INPUTS from the Energy Capture (EC) and Site Characterisation (SC) modules -- TIDAL example
#         # **************************************************************************************************
#         # /ec/{ecId}/inputs/machine/tec/complexity1  /
#         # "cp"

#         # /ec/{ecId}/inputs/machine/tec/complexity2  /  "rotor_diameter": 10,
#         # "cut_in_velocity": 1,
#         # "cut_out_velocity": 10,
#         # "cp": 0.3,
#         # "ct": 1,
#         # "rated_pow_device": 500,
#         # "number_rotor": 1,
#         # / ec / {ecId} / results / tec / complexity2
#         """"array_velocity_field":
#         "deviceID": 1,
#         "hub_velocity": 2.3"""
#         # / ec / {ecId} / inputs / site / tec / complexity2
#         # "velocity_field":[ 0,3],
#         # /ec/{ecId}/inputs/site/tec/complexity3
#         # "V": [
#         # "probability":[
#         # / mc / {mcId} / model / tec / complexity2
#         # "tip_speed_ratio": 1,

#         # / ec / {ecId} / inputs / machine / tec / complexity3
#         """"rotor_diameter": 10,
#         "hub_height": 20,
#         "floating": false,
#         "cut_in_velocity": 2,
#         "cut_out_velocity": 5,
#         "cp":   [       0.3,       0.8   ],
#         "ct":   [        0.3,        0.3    ],
#         "cp_ct_velocity":[ 2, 5 ],
#         "rated_pow_device": 500,
#         "number_rotor": 1,
#         """
#         # / ec / {ecId} / results / tec / complexity3
#         """"array_velocity_field":
#         "deviceID": 1,
#         "hub_velocity": 2.3"""
#         # /mc/{mcId}/model/tec/complexity3
#         # "tip_speed_ratio": 1,
#         Type_turbine = 0
#         current_vel = np.arange(0.1, 5, 0.05)  # m/s
#         turbine_radius = 10  # m
#         rho_water = 1025.0
#         vmean_max_occ = 5  # m/s
#         current_pdf = rayleigh.pdf(5 * current_vel / vmean_max_occ)
#         current_prob = current_pdf * np.mean(np.diff(current_vel))
#         P_rated = 2e6  # W
#         omega_rated = 14 * 2 * np.pi / 60  # rad/s
#         v_curr_rated = 3  # m/s
#         #  Power curve
#         power_exp = 3
#         power_low = (
#             P_rated
#             / v_curr_rated ** power_exp
#             * current_vel[current_vel <= v_curr_rated] ** power_exp
#         )
#         power_up = P_rated * np.ones(len(current_vel[current_vel > v_curr_rated]))
#         mean_power = np.concatenate([power_low, power_up])
#         #  Rotational speed curve
#         omega_exp = 1.5
#         omega_low = (
#             omega_rated
#             / v_curr_rated ** omega_exp
#             * current_vel[current_vel <= v_curr_rated] ** omega_exp
#         )
#         omega_up = omega_rated * np.ones(len(current_vel[current_vel > v_curr_rated]))
#         mean_omega = np.concatenate([omega_low, omega_up])
#         # Cp (power coefficient) and TSR (tip speed ratio)
#         Cp = mean_power / (
#             0.5 * rho_water * np.pi * turbine_radius ** 2 * current_vel ** 3
#         )
#         TSR = mean_omega * turbine_radius / current_vel
#         Cpto = current_vel * Cp * 0.5 * rho_water * np.pi * turbine_radius ** 2
#         Ct_tidal = (
#             1 / mean_omega
#         )  # OUTPUT FROM EC  = turbine_radius / (TSR * current_vel)
#         # ******************************************************************************************************************
#         # *****Energy Transformation (ET) Module -- INPUTS to be gathered from the ET-GUI *****
#         # ******************************************************************************************************************
#         # PTO - INPUTS
#         # *************************
#         # ------------------------------------------------------------------------------------------------------------------
#         # Required parameters to initialise the Mechanical Transformation object
#         # --- Gearbox ---
#         GB_tech = Technology
#         GB_Pnom = 5e5  # USER INPUT CMPX1-2-3 W
#         GB_t_ratio = 1 / 100  # USER INPUT CMPX1-2-3 relation
#         obj_args = {
#             "Ct_tidal": 0
#         }  # For tidal devices the torque relation with the speed changes w.r.t. wave devices. The corresponding
#         # coefficientes are provided through this variable to compute the torque on the shaft. It just needs to be initialised,
#         # and will be updated per current speed with the corresponding Ct_tidal set in the perf_inputs
#         # -------------------
#         # Generic Dictionary input for the Mechanical Transformation
#         Type_mech_transf = "Gearbox"  # USER INPUT CMPX 1-2-3 Gearbox - Mech_Simplified
#         if Mech_CMPX == 1:
#             Type_mech_transf = "Mech_Simplified"
#         mechT_params = {
#             "Type": Type_mech_transf,  # The name of the object
#             "init_args": [
#                 # GB_tech,
#                 GB_Pnom,
#                 GB_t_ratio,
#                 Technology,
#             ],  # Required arguments to initialise the object
#             "perf_args": obj_args,  # Required arguments to perform the performance of specific objects
#             "mech_t_ratio": 1,
#         }  # Always 1 with the gearbox as it is already defined in the init_args
#         # ------------------------------------------------------------------------------------------------------------------
#         # *******************************************************************************************************************
#         # Dummy inputs from Energy Capture for the 3 dof-PTOs of the device
#         # A Turb. intensity of a 10% is assumed - D.R.J. Sutherland et al.
#         lst_current_vel = []
#         lst_Cpto3 = []
#         lst_Ct_tidal = []
#         Device_perf_inputs = []
#         dic3 = {}
#         for cont2 in range(0, Number_of_devices):
#             lst_current_vel.append(0.1 * current_vel)
#             lst_Cpto3.append(Cpto)
#             lst_Ct_tidal.append(Ct_tidal)
#             dic3 = {
#                 "sigma_v": lst_current_vel,
#                 "C_pto": lst_Cpto3,
#                 "Ct_tidal": lst_Ct_tidal,
#             }
#             Device_perf_inputs.append(dic3)
#         # ******************************************************************************************************************
#         # The set of Environmental Conditions, for all devices
#         Env_Conditions = {
#             "Vc": current_vel,
#             "Tz": 10 * np.ones(len(current_vel)),
#             "Occ": current_prob,
#         }
#         lst_current_vel1 = []
#         lst_Cpto4 = []
#         lst_Ct_tidal1 = []
#         Array_perf_inputs = []
#         dic4 = {}
#         for cont in range(0, GDL):
#             lst_current_vel1.append(0.1 * current_vel)
#             lst_Cpto4.append(Cpto)
#             lst_Ct_tidal1.append(Ct_tidal)
#             dic4 = {
#                 "sigma_v": lst_current_vel1,
#                 "C_pto": lst_Cpto4,
#                 "Ct_tidal": lst_Ct_tidal1,
#             }
#             Array_perf_inputs.append(dic4)
#         lst_devices = []
#         Array_perf_inputs1 = []
#     # **********************************************************************************************************************
#     # *****Energy Transformation (ET) Module -- INPUTS to be gathered from the ET-GUI *****
#     # **********************************************************************************************************************
#     # PTO - INPUTS
#     # *************************
#     # ----------------------------------------------------------------------------------------------------------------------
#     # Required parameters to initialise the Electrical Transformation object
#     # --- Squirrel Cage Induction Generator ---
#     Gen_Nom_P = 100e3  # USER INPUT (WATS) CMPX-1-2-3
#     pp = 2  # USER INPUT  CMPX-2-3
#     Vnom = 400.0  # USER INPUT  CMPX-2-3
#     fnom = 50.0  # USER INPUT  CMPX-2-3
#     rel_T_maxnom = 2.0  # USER INPUT  CMPX-2-3
#     rel_V_maxnom = 1.725  # USER INPUT  CMPX-2-3
#     gen_class = "Class_F"  # USER INPUT  CMPX-2-3 Class_A-Class_B-Class_F-Class_G
#     Type_elect_transf = "SCIG"  # USER INPUT CMPX1-2-3  SCIG/ Elect_Simplified
#     if Elect_CMPX == 1:
#         Type_elect_transf = "Elect_Simplified"
#     electT_params = {
#         "Type": Type_elect_transf,
#         "init_args": [Gen_Nom_P, pp, Vnom, fnom, rel_T_maxnom, rel_V_maxnom, gen_class],
#     }
#     # ----------------------------------------------------------------------------------------------------------------------
#     # Required parameters to initialise the Grid Conditioning object
#     # --- Back to back to level ---
#     B2B_Nom_P = 100e3  # USER INPUT CMPX 1-2-3
#     Vdc = 1200  # USER INPUT CMPX 2-3
#     fsw = 5000  # USER INPUT CMPX 2-3
#     VnomGen = Vnom * np.sqrt(3)  # USER INPUT CMPX 2-3
#     fnomGen = 50.0  # USER INPUT CMPX 2-3
#     Lgen = 0.0005  # USER INPUT CMPX 2-3
#     Rgen = 0.0001  # USER INPUT CMPX 2-3
#     Vgrid = 690  # USER INPUT CMPX 2-3 /mc/{mcId}/general/ "rated_voltage"
#     Rgrid = 0.0001  # USER INPUT CMPX 2-3
#     Lgrid = 0.001  # USER INPUT CMPX 2-3
#     cosfi_grid = 0.91  # USER INPUT CMPX 2-3
#     Type_grid_transf = "B2B2level"  # USER INPUT CMPX 1-2-3  B2B2level-Grid_Simplified
#     if Mech_CMPX == 1:
#         Type_grid_transf = "Grid_Simplified"
#     gridC_params = {
#         "Type": "B2B2level",
#         "init_args": [
#             B2B_Nom_P,
#             Vdc,
#             fsw,
#             VnomGen,
#             fnomGen,
#             Lgen,
#             Rgen,
#             Vgrid,
#             Rgrid,
#             cosfi_grid,
#             Lgrid,
#         ],
#     }
#     # ----------------------------------------------------------------------------------------------------------------------
#     # Required parameters to initialise a Control object
#     # --- Passive Control ---
#     type_control = "Passive"  # USER INPUT CMPX 2-3 User_defined - Passive
#     n_sigma = 5  # USER INPUT CMPX 2-3
#     bins = 500  # USER INPUT CMPX 2-3 points per sea state (500)
#     vel_mean = 1e-6
#     control_params = {"Type": type_control, "init_args": [n_sigma, bins, vel_mean]}
#     # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#     # ----- ARRAY object example -----
#     # Initialisation

#     # Gen_Nom_P = 100e3                       # USER INPUT (WATS) CMPX-1-2-3
#     # B2B_Nom_P = 100e3                       # USER INPUT CMPX 1-2-3
#     # turb_D = 1                          # USER INPUT ==> m
#     if mode == "analysis":
#         pto_inputs = [mechT_params, electT_params, gridC_params, control_params]
#         # ----------------------------------------------------------------------------------------------------------------------
#         # json data input depending on the CMPX
#         json_gen.input_json_gen(
#             Elect_CMPX,
#             Grid_CMPX,
#             Mech_CMPX,
#             Type_mech_transf,
#             Type_turbine,
#             type_control,
#         )
#         Array_ID = "Array_01"
#         t_init = t_crono.time()
#         Array = Array_class.Array(Array_ID, Array_inputs, Device_INPUTS, pto_inputs)
#         print(str(t_crono.time() - t_init) + " [s]")
#         print(str((t_crono.time() - t_init) / 60) + " [min]")
#         # Performance assessment
#         t_perf = t_crono.time()
#         Array.performance(Array_perf_inputs, Env_Conditions)
#         print(str(t_crono.time() - t_perf) + " [s]")
#         print(str((t_crono.time() - t_perf) / 60) + " [min]")
#         # ----------------------------------------------------------------------------------------------
#         # BACKEND auxiliar
#         """from backend_aux import et_array_to_jsondict

#         dic_results = et_array_to_jsondict(Array)
#         et_results = json.dumps(dic_results)
#         # with debug purposes, the output is dump to a file
#         with open("et_results.json", "w") as f:
#             json.dump(dic_results, f, indent=4, sort_keys=True)
#         print("END")"""
#     else:  # DESIGN MODE
#         range_min = int(50e3)  # user inputs
#         range_max = int(150e3)  # user inputs
#         range_step = int(50e3)  # user inputs
#         range_aux = []
#         total_sims = range_max / range_min
#         range_aux = np.arange(range_min, range_max + range_step, range_step)

#         # Gen_Nom_P = 100e3                         # USER INPUT (WATS) CMPX-1-2-3
#         # B2B_Nom_P = 100e3                         # USER INPUT CMPX 1-2-3
#         # turb_D = 1                                # USER INPUT ==> m
#         parameter_sim = 1  # 1mech - 2elec - 3grid
#         for cont10 in range(0, len(range_aux)):
#             # MODIFICATIONS ON THE INPUTS************************************************************************************
#             if parameter_sim == 1:  # mechanical sim

#                 if Technology == "Wave":  # WAVE TECHNOLOGY
#                     turb_D = range_aux[cont10]  # to be modified for other elements
#                     mechT_params = {
#                         "Type": Type_mech_transf,  # The name of the object
#                         "init_args": [
#                             Type_turbine,
#                             turb_D,
#                             S_owc_c,
#                             Technology,
#                         ],  # Required arguments to initialise the object
#                         "perf_args": obj_args,  # Required arguments to perform the performance of specific objects
#                         "mech_t_ratio": 10,
#                     }  # REVIEW THIS PARAMETER!!!!!!!!!!!!!!!!
#                 else:  # TIDAL TECHNOLOGY
#                     GB_Pnom = range_aux[cont10]  # USER INPUT CMPX1-2-3 W
#                     Type_mech_transf = (
#                         "Gearbox"  # USER INPUT CMPX 1-2-3 Gearbox - Mech_Simplified
#                     )
#                     mechT_params = {
#                         "Type": "Gearbox",  # The name of the object
#                         "init_args": [
#                             # GB_tech,
#                             GB_Pnom, GB_t_ratio, Technology],
#                         # Required arguments to initialise the object
#                         "perf_args": obj_args,
#                         # Required arguments to perform the performance of specific objects
#                         "mech_t_ratio": 1,
#                     }  # Always 1 with the gearbox as it is already defined in the init_args
#             else:
#                 if parameter_sim == 2:  # Electrical sim
#                     Gen_Nom_P = range_aux[cont10]
#                     electT_params = {
#                         "Type": Type_elect_transf,
#                         "init_args": [
#                             Gen_Nom_P,
#                             pp,
#                             Vnom,
#                             fnom,
#                             rel_T_maxnom,
#                             rel_V_maxnom,
#                             gen_class,
#                         ],
#                     }
#                 else:  # grid sim
#                     B2B_Nom_P = range_aux[cont10]
#                     gridC_params = {
#                         "Type": "B2B2level",
#                         "init_args": [
#                             B2B_Nom_P,
#                             Vdc,
#                             fsw,
#                             VnomGen,
#                             fnomGen,
#                             Lgen,
#                             Rgen,
#                             Vgrid,
#                             Rgrid,
#                             cosfi_grid,
#                             Lgrid,
#                         ],
#                     }
#             # ***************************************************************************************************************
#             pto_inputs = [mechT_params, electT_params, gridC_params, control_params]
#             json_gen.input_json_gen(
#                 Elect_CMPX,
#                 Grid_CMPX,
#                 Mech_CMPX,
#                 Type_mech_transf,
#                 Type_turbine,
#                 type_control,
#             )
#             Array_ID = "Array_0" + str(cont10)
#             t_init = t_crono.time()
#             Array = Array_class.Array(Array_ID, Array_inputs, Device_INPUTS, pto_inputs)
#             print(str(t_crono.time() - t_init) + " [s]")
#             print(str((t_crono.time() - t_init) / 60) + " [min]")
#             # Performance assessment
#             t_perf = t_crono.time()
#             Array.performance(Array_perf_inputs, Env_Conditions)
#             print(str(t_crono.time() - t_perf) + " [s]")
#             print(str((t_crono.time() - t_perf) / 60) + " [min]")
