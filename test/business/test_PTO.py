from dtop_energytransf.business import PTO as PTO_class
from dtop_energytransf.storage.control_data import aux_adim_vel, aux_power_levels, aux_load_levels, aux_load_ranges
from dtop_energytransf.storage.mecanic_data import wells_default, impulse_default, gearbox_default, hydraulic_default

import numpy as np
import os
import pandas as pd
from scipy.stats import rayleigh
from dtop_energytransf.utils import get_project_root


# def test_foo():
#     assert foo() == "bar"
# ----------------------------------------------------------------------------------------------------------------------
# This is a function to check how pytest works. It can be ran either with the command pytest in a terminal,
# which will run all .py files within the 'tests' folder or just executing each script with the pychram IDE.
# The result appears to be the percentage of passed tests. But it appears that there is not a check of how many lines
# have been executed of the actual code.


# **********************************************************************************************************************
# DUMMY INPUTS from the Energy Capture (EC) and Site Characterisation (SC) modules -- WAVE Example
# ************************************************************************************************
# root= Path(__file__).parent
# filename = str(root) + '/EC_out_OES_Sphere.json'

# EC_out = pd.read_json(filename, orient='records', lines=True) # Data generated with the OES sphere


# **********************************************************************************************************************

# **********************************************************************************************************************
# DUMMY INPUTS from the Energy Capture (EC) and Site Characterisation (SC) modules -- TIDAL example
# **************************************************************************************************
current_vel = np.arange(0.1, 5, 0.05)  # m/s
turbine_radius = 10  # m
rho_water = 1025.0
vmean_max_occ = 5  # m/s
current_pdf = rayleigh.pdf(5 * current_vel / vmean_max_occ)
current_prob = current_pdf * np.mean(np.diff(current_vel))

P_rated = 2e6  # W
omega_rated = 14 * 2 * np.pi / 60  # rad/s
v_curr_rated = 3  # m/s

#  Power curve
power_exp = 3
power_low = (
    P_rated
    / v_curr_rated ** power_exp
    * current_vel[current_vel <= v_curr_rated] ** power_exp
)
power_up = P_rated * np.ones(len(current_vel[current_vel > v_curr_rated]))
mean_power = np.concatenate([power_low, power_up])

#  Rotational speed curve
omega_exp = 1.5
omega_low = (
    omega_rated
    / v_curr_rated ** omega_exp
    * current_vel[current_vel <= v_curr_rated] ** omega_exp
)
omega_up = omega_rated * np.ones(len(current_vel[current_vel > v_curr_rated]))
mean_omega = np.concatenate([omega_low, omega_up])

# Cp (power coefficient) and TSR (tip speed ratio)
Cp = mean_power / (0.5 * rho_water * np.pi * turbine_radius ** 2 * current_vel ** 3)
TSR = mean_omega * turbine_radius / current_vel

Cpto = current_vel * Cp * 0.5 * rho_water * np.pi * turbine_radius ** 2
Ct_tidal = 1 / mean_omega  # = turbine_radius / (TSR * current_vel)
Cpto_tidal = Cpto[10]
sigma_v_tidal = 0.1 * current_vel[10]
# **********************************************************************************************************************

#-----------------------------------------------------------------------------------------------------------------------
# Catalogue inputs
#***********************************************************************************************************************
elec_cat = {
            "x_f" : [2e-7, -1e-5, 6e-4],
            "shaft_diam" : [0.00000000000002, -0.000000007, 0.001, 27.451],
            "phi_cos" : [8e-15, -0.000000001, 0.00005, 0.0827],
            "sigma_h" : 5.01,
            "sigma_e" : 44.8,
            "B" : 0.8,
            "Gen_mass" : [0.0056, 55.041],
            "wind_mass_fraction" : 0.6,
            "I_nom" : [0.0018, 0.934],
            "Res" : [72584, -1.236],
            "Life" : {"Class_A" : [-0.069, 17.1288228, 105], "Class_B" : [-0.069, 18.8974785, 130], "Class_F" : [-0.069, 20.7619675, 155], "Class_H" : [-0.069, 22.1243926, 180]},
            "cost" : [2.5, 0.7],
            "thick_max" : 0.64
}

grid_cat = {
            "temp" : [0, 40, 120],
            "cost" : 0.1,
            "life" : [2E+19, -7.885],
            "mass" : {"Copper":0.000137, "Plastic":0.00006, "Steel": 0.000326, "Iron": 0.00005, "Zink": 0.00002},
            "IGBT150" : [0.85,0.011497,0.008490,0.0006578,-0.0000002294,900],
            "IGBT450" : [0.85, 0.00358333, 0.06026, 0.0003002, 0.0000006978, 900],
            "IGBT800" : [1.15, 0.00178125, 0.0977, 0.00028, 0.000000347, 900],
            "IGBT1600" : [1, 0.001, 0.258, 0.000275, 0.000000163, 900],
            "Diode150" : [0.81, 0.006625, 0.0110730, 0.0002368, -0.0000003392, 900],
            "Diode450" : [0.8, 0.00271605, 0.02424, 0.0002509, 0.0000001585, 900],
            "Diode800" : [1.15, 0.00178125, 0.0977, 0.00028, 0.000000347, 900],
            "Diode1600" : [0.85, 0.000589, 0.095, 0.000314, -0.00000004, 900],
}
control_cat = {
            "adim_vel" : aux_adim_vel,
            "Power_levels" : aux_power_levels,
            "Load_levels" : aux_load_levels,
            "Load_ranges" : aux_load_ranges,
}

# ----------------------------------------------------------------------------------------------------------------------
# Mechanical Transformation Object inputs
# ***************************************************
# --- Air Turbine - Wells ---
Type_turbine = "Wells"  # "Impulse" / "Wells"
turb_D = 1
S_owc_c = np.pi * 2.5 ** 2  # OWC
mech_cat = {"at_cat": wells_default}
obj_args = (
    []
)  # This variable is required in case the object needs aditional inputs for performance
# -------------------
# Generic Dictionary input for the Mechanical Transformation
Technology = "Wave"  #  CMPX 1-2-3 Wave-Tidal ==> /ec/  ["type"]
Type_mech_transf = "AirTurbine"  # USER INPUT ==> (AirTurbine - Hydraulic - Gearbox / CMPX2-3) - Mech_Simplified CMPX3
mechT_AirTw_params = {
    "Type": Type_mech_transf,  # The name of the object
    "init_args": [
        Type_turbine,
        turb_D,
        S_owc_c,
        mech_cat,
    ],  # Required arguments to initialise the object
    "perf_args": obj_args,  # Required arguments to perform the performance of specific objects
    "mech_t_ratio": 10,
}
# ----------------------------------------------------------------------------------------------------------------------
# --- Air Turbine ---
Type_turbine = "Impulse"  # "Impulse" / "Wells"
turb_D = 1
S_owc_c = np.pi * 2.5 ** 2  # OWC
mech_cat = {"at_cat": impulse_default}
obj_args = (
    []
)  # This variable is required in case the object needs aditional inputs for performance
# -------------------
# Generic Dictionary input for the Mechanical Transformation
Technology = "Wave"  #  CMPX 1-2-3 Wave-Tidal ==> /ec/  ["type"]

mechT_AirTI_params = {
    "Type": "AirTurbine",  # The name of the object
    "init_args": [
        Type_turbine,
        turb_D,
        S_owc_c,
        mech_cat,
    ],  # Required arguments to initialise the object
    "perf_args": obj_args,  # Required arguments to perform the performance of specific objects
    "mech_t_ratio": 10,
}
# ----------------------------------------------------------------------------------------------------------------------
# --- Hydraulic ---
D_hyd = 1.4e-4
Ap = 0.2
mech_cat = {"hy_cat": hydraulic_default}
obj_args = [1]
# -------------------
# Generic Dictionary input for the Mechanical Transformation
mechT_Hyd_params = {
    "Type": "Hydraulic",  # The name of the object
    "init_args": [D_hyd, Ap, mech_cat],  # Required arguments to initialise the object
    "perf_args": obj_args,  # Required arguments to perform the performance of specific objects
    "mech_t_ratio": 10,
}
# ----------------------------------------------------------------------------------------------------------------------
# --- Gearbox - Wave ---
GB_Pnom = 10000
GB_t_ratio = 10
mech_cat = {"gb_cat": gearbox_default}
# -------------------
# Generic Dictionary input for the Mechanical Transformation
mechT_GB_params = {
    "Type": "Gearbox",  # The name of the object
    "init_args": [
        # "Wave",
        GB_Pnom,
        GB_t_ratio,
        Technology,
        mech_cat,
    ],  # Required arguments to initialise the object
    "perf_args": obj_args,  # Required arguments to perform the performance of specific objects
    "mech_t_ratio": 1,
}
# ----------------------------------------------------------------------------------------------------------------------
# --- Gearbox - Tidal ---
GB_Pnom = 10000
GB_t_ratio = 10
mech_cat = {"gb_cat": gearbox_default}
obj_args = {"Ct_tidal": Ct_tidal[10]}
# -------------------
# Generic Dictionary input for the Mechanical Transformation
mechT_GB_tidal_params = {
    "Type": "Gearbox",  # The name of the object
    "init_args": [
        # "Tidal",
        GB_Pnom,
        GB_t_ratio,
        Technology,
        mech_cat,
    ],  # Required arguments to initialise the object
    "perf_args": obj_args,  # Required arguments to perform the performance of specific objects
    "mech_t_ratio": 1,
}
# ----------------------------------------------------------------------------------------------------------------------

# --- Mechanical Simplified - Wave ---
P_max_simplified = 500e3
t_ratio = 10
efficiency = 0.8
obj_args = []
Technology = "Wave"  #  CMPX 1-2-3 Wave-Tidal ==> /ec/  ["type"]
# Generic Dictionary input for the Mechanical Transformation
mechT_Simp_params = {
    "Type": "Mech_Simplified",  # The name of the object
    "init_args": [
        # "Wave",
        P_max_simplified,
        t_ratio,
        Technology,
        efficiency,
    ],  # Required arguments to initialise the object
    "perf_args": obj_args,  # Required arguments to perform the performance of specific objects
    "mech_t_ratio": 1,
}
# ----------------------------------------------------------------------------------------------------------------------
# --- Mechanical Simplified - Tidal ---
P_max_simplified = 500e3
t_ratio = 1 / 100
efficiency = 0.8
obj_args = {"Ct_tidal": Ct_tidal[10]}
# Generic Dictionary input for the Mechanical Transformation
Technology = "Tidal"  #  CMPX 1-2-3 Wave-Tidal ==> /ec/  ["type"]
mechT_Simp_tidal_params = {
    "Type": "Mech_Simplified",  # The name of the object
    "init_args": [
        # "Tidal",
        P_max_simplified,
        t_ratio,
        Technology,
        efficiency,
    ],  # Required arguments to initialise the object
    "perf_args": obj_args,  # Required arguments to perform the performance of specific objects
    "mech_t_ratio": 1,
}
# ----------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------
# Electrical Transformation Object inputs
# ***************************************************
# --- Squirrel Cage Induction Generator ---
Gen_Nom_P = 100e3  # W
pp = 2
Vnom = 400.0
fnom = 50.0
rel_T_maxnom = 2.0
rel_V_maxnom = 1.725
gen_class = "Class_F"
electT_SCIG_params = {
    "Type": "SCIG",
    "init_args": [Gen_Nom_P, pp, Vnom, fnom, rel_T_maxnom, rel_V_maxnom, gen_class, elec_cat],
}
# --- Simplified Generator ---
Simp_Nom_P = 50000
electT_Simp_params = {"Type": "Elect_Simplified", "init_args": [Simp_Nom_P]}
# ----------------------------------------------------------------------------------------------------------------------
# Grid Conditioning Object inputs
# ***************************************************
# --- Back to back to level ---
Vdc = 1200
fsw = 5000
VnomGen = 690
fnomGen = 50.0
Lgen = 0.0005
Rgen = 0.0001
Vgrid = 690
Rgrid = 0.0001
Lgrid = 0.001
cosfi_grid = 0.91
B2B_Nom_P = 100e3  # W
Fgrid = 50
gridC_B2B_params = {
    "Type": "B2B2level",
    "init_args": [
        B2B_Nom_P,
        Vdc,
        fsw,
        VnomGen,
        fnomGen,
        Lgen,
        Rgen,
        Vgrid,
        Rgrid,
        cosfi_grid,
        Lgrid,
        Fgrid,
        grid_cat,
    ],
}
# ----------------------------------------------------------------------------------------------------------------------
# --- Grid Conditioning Simplified ---
B2BSimp_Nom_P = 100e3  # W
B2BSimp_V_grid = 690
gridC_Simp_params = {
    "Type": "Grid_Simplified",
    "init_args": [B2BSimp_Nom_P, B2BSimp_V_grid],
}
# ----------------------------------------------------------------------------------------------------------------------
# Control Object inputs
# ***************************************************
# --- Passive Control ---
n_sigma = 5
bins = 500
vel_mean = 1e-6
control_Passive_params = {"Type": "Passive", "init_args": [n_sigma, bins, vel_mean, control_cat]}
# ----------------------------------------------------------------------------------------------------------------------
# --- User Defined Control ---
n_sigma = 5
bins = 500
vel_mean = 1e-6
control_Userdefined_params = {
    "Type": "User defined",
    "init_args": [n_sigma, bins, vel_mean, control_cat],
}
# ----------------------------------------------------------------------------------------------------------------------

pto_cmpx2_AirT_inputs = [
    mechT_AirTw_params,
    electT_SCIG_params,
    gridC_B2B_params,
    control_Passive_params,
]
pto_cmpx2_AirTI_inputs = [
    mechT_AirTI_params,
    electT_SCIG_params,
    gridC_B2B_params,
    control_Passive_params,
]
pto_cmpx2_Hyd_inputs = [
    mechT_Hyd_params,
    electT_SCIG_params,
    gridC_B2B_params,
    control_Passive_params,
]
pto_cmpx2_GB_inputs = [
    mechT_GB_params,
    electT_SCIG_params,
    gridC_B2B_params,
    control_Passive_params,
]
pto_cmpx2_AirT_userControl_inputs = [
    mechT_AirTw_params,
    electT_SCIG_params,
    gridC_B2B_params,
    control_Userdefined_params,
]
pto_cmpx1_Mech_inputs = [
    mechT_Simp_params,
    electT_SCIG_params,
    gridC_B2B_params,
    control_Passive_params,
]
pto_cmpx1_Gen_inputs = [
    mechT_AirTw_params,
    electT_Simp_params,
    gridC_B2B_params,
    control_Passive_params,
]
pto_cmpx1_Grid_inputs = [
    mechT_AirTw_params,
    electT_SCIG_params,
    gridC_Simp_params,
    control_Passive_params,
]
pto_cmpx2_GB_tidal_inputs = [
    mechT_GB_tidal_params,
    electT_SCIG_params,
    gridC_B2B_params,
    control_Passive_params,
]
pto_cmpx1_Mech_tidal_inputs = [
    mechT_Simp_tidal_params,
    electT_SCIG_params,
    gridC_B2B_params,
    control_Passive_params,
]

# --- WAVE PTO Cases testing ---


def test_pto_wave_cmpx2_airt_w(read_config):
    # Initialisation of variables
    Cpto = np.zeros(len(read_config["Hs[m]"]))
    Pcapt = np.zeros(len(read_config["Hs[m]"]))
    Hs = read_config["Hs[m]"]
    Tp = read_config["Tp[s]"]
    Occ = read_config["Occ"]
    for c_SS in range(0, len(read_config["Hs[m]"])):
        # Maximum Captured power in each Sea State
        ind_Pmax = read_config["Pmean[W]"][c_SS][:].index(
            max(read_config["Pmean[W]"][c_SS][:])
        )
        Cpto[c_SS] = read_config["Cpto[Ns/m]"][c_SS][ind_Pmax]
        Pcapt[c_SS] = read_config["Pmean[W]"][c_SS][ind_Pmax]

    # Wave inputs - Sea State for assessment
    Cpto_wave = Cpto[0]
    sigma_v_wave = np.sqrt(Pcapt[0] / Cpto_wave)
    EC_T = 365.25 * 24 * 3600 * Occ[0]
    Tz = Tp[0]
    PTO = PTO_class.PTO("PTO_Wave_AirT", "Wave", *pto_cmpx2_AirT_inputs)
    PTO.performance(Cpto_wave, sigma_v_wave)
    PTO.reliability(EC_T, Tz)
    P_mech = int(np.sum(PTO.MechT_obj.Mech_P_mean))
    D_mech = int(PTO.MechT_obj.Mech_damage * 1e17)
    P_elect = int(np.sum(PTO.ElectT_obj.Elect_P_mean))
    D_elect = int(PTO.ElectT_obj.Elect_damage * 1e10)
    P_grid = int(np.sum(PTO.GridC_obj.Grid_P_mean))
    D_grid = int(PTO.GridC_obj.Grid_damage * 1e17)

    P_sum = P_mech + P_elect + P_grid
    D_sum = D_mech + D_elect + D_grid

    assert P_sum == 778, D_sum == 71


def test_pto_wave_cmpx2_airt_i(read_config):
    # Initialisation of variables
    Cpto = np.zeros(len(read_config["Hs[m]"]))
    Pcapt = np.zeros(len(read_config["Hs[m]"]))
    Hs = read_config["Hs[m]"]
    Tp = read_config["Tp[s]"]
    Occ = read_config["Occ"]
    for c_SS in range(0, len(read_config["Hs[m]"])):
        # Maximum Captured power in each Sea State
        ind_Pmax = read_config["Pmean[W]"][c_SS][:].index(
            max(read_config["Pmean[W]"][c_SS][:])
        )
        Cpto[c_SS] = read_config["Cpto[Ns/m]"][c_SS][ind_Pmax]
        Pcapt[c_SS] = read_config["Pmean[W]"][c_SS][ind_Pmax]

    # Wave inputs - Sea State for assessment
    Cpto_wave = Cpto[0]
    sigma_v_wave = np.sqrt(Pcapt[0] / Cpto_wave)
    EC_T = 365.25 * 24 * 3600 * Occ[0]
    Tz = Tp[0]
    PTO = PTO_class.PTO("PTO_Wave_AirT", "Wave", *pto_cmpx2_AirTI_inputs)
    PTO.performance(Cpto_wave, sigma_v_wave)
    PTO.reliability(EC_T, Tz)
    P_mech = int(np.sum(PTO.MechT_obj.Mech_P_mean))
    D_mech = int(PTO.MechT_obj.Mech_damage * 1e17)
    P_elect = int(np.sum(PTO.ElectT_obj.Elect_P_mean))
    D_elect = int(PTO.ElectT_obj.Elect_damage * 1e10)
    P_grid = int(np.sum(PTO.GridC_obj.Grid_P_mean))
    D_grid = int(PTO.GridC_obj.Grid_damage * 1e17)

    P_sum = P_mech + P_elect + P_grid
    D_sum = D_mech + D_elect + D_grid

    assert P_sum == 1768, D_sum == 3299698



def test_pto_wave_cmpx2_Hyd_inputs(read_config):
    # Initialisation of variables
    Cpto = np.zeros(len(read_config["Hs[m]"]))
    Pcapt = np.zeros(len(read_config["Hs[m]"]))
    Hs = read_config["Hs[m]"]
    Tp = read_config["Tp[s]"]
    Occ = read_config["Occ"]
    for c_SS in range(0, len(read_config["Hs[m]"])):
        # Maximum Captured power in each Sea State
        ind_Pmax = read_config["Pmean[W]"][c_SS][:].index(
            max(read_config["Pmean[W]"][c_SS][:])
        )
        Cpto[c_SS] = read_config["Cpto[Ns/m]"][c_SS][ind_Pmax]
        Pcapt[c_SS] = read_config["Pmean[W]"][c_SS][ind_Pmax]

    # Wave inputs - Sea State for assessment
    Cpto_wave = Cpto[0]
    sigma_v_wave = np.sqrt(Pcapt[0] / Cpto_wave)
    EC_T = 365.25 * 24 * 3600 * Occ[0]
    Tz = Tp[0]
    PTO = PTO_class.PTO("PTO_Wave_AirT", "Wave", *pto_cmpx2_Hyd_inputs)
    PTO.performance(Cpto_wave, sigma_v_wave)
    PTO.reliability(EC_T, Tz)
    P_mech = int(np.sum(PTO.MechT_obj.Mech_P_mean))
    D_mech = int(PTO.MechT_obj.Mech_damage * 1e17)
    P_elect = int(np.sum(PTO.ElectT_obj.Elect_P_mean))
    D_elect = int(PTO.ElectT_obj.Elect_damage * 1e10)
    P_grid = int(np.sum(PTO.GridC_obj.Grid_P_mean))
    D_grid = int(PTO.GridC_obj.Grid_damage * 1e17)

    P_sum = P_mech + P_elect + P_grid
    D_sum = D_mech + D_elect + D_grid

    assert P_sum == 4790, D_sum == 13059170


def test_pto_wave_cmpx2_GB_inputs(read_config):
    # Initialisation of variables
    Cpto = np.zeros(len(read_config["Hs[m]"]))
    Pcapt = np.zeros(len(read_config["Hs[m]"]))
    Hs = read_config["Hs[m]"]
    Tp = read_config["Tp[s]"]
    Occ = read_config["Occ"]
    for c_SS in range(0, len(read_config["Hs[m]"])):
        # Maximum Captured power in each Sea State
        ind_Pmax = read_config["Pmean[W]"][c_SS][:].index(
            max(read_config["Pmean[W]"][c_SS][:])
        )
        Cpto[c_SS] = read_config["Cpto[Ns/m]"][c_SS][ind_Pmax]
        Pcapt[c_SS] = read_config["Pmean[W]"][c_SS][ind_Pmax]

    # Wave inputs - Sea State for assessment
    Cpto_wave = Cpto[0]
    sigma_v_wave = np.sqrt(Pcapt[0] / Cpto_wave)
    EC_T = 365.25 * 24 * 3600 * Occ[0]
    Tz = Tp[0]
    PTO = PTO_class.PTO("PTO_Wave_AirT", "Wave", *pto_cmpx2_GB_inputs)
    PTO.performance(Cpto_wave, sigma_v_wave)
    PTO.reliability(EC_T, Tz)
    P_mech = int(np.sum(PTO.MechT_obj.Mech_P_mean))
    D_mech = int(PTO.MechT_obj.Mech_damage * 1e17)
    P_elect = int(np.sum(PTO.ElectT_obj.Elect_P_mean))
    D_elect = int(PTO.ElectT_obj.Elect_damage * 1e10)
    P_grid = int(np.sum(PTO.GridC_obj.Grid_P_mean))
    D_grid = int(PTO.GridC_obj.Grid_damage * 1e17)

    P_sum = P_mech + P_elect + P_grid
    D_sum = D_mech + D_elect + D_grid

    assert P_sum == 2847, D_sum == 20507584116301


def test_pto_wave_cmpx2_airt_w_usercontr(read_config):
    # Initialisation of variables
    Cpto = np.zeros(len(read_config["Hs[m]"]))
    Pcapt = np.zeros(len(read_config["Hs[m]"]))
    Hs = read_config["Hs[m]"]
    Tp = read_config["Tp[s]"]
    Occ = read_config["Occ"]
    for c_SS in range(0, len(read_config["Hs[m]"])):
        # Maximum Captured power in each Sea State
        ind_Pmax = read_config["Pmean[W]"][c_SS][:].index(
            max(read_config["Pmean[W]"][c_SS][:])
        )
        Cpto[c_SS] = read_config["Cpto[Ns/m]"][c_SS][ind_Pmax]
        Pcapt[c_SS] = read_config["Pmean[W]"][c_SS][ind_Pmax]

    # Wave inputs - Sea State for assessment
    Cpto_wave = Cpto[0]
    sigma_v_wave = np.sqrt(Pcapt[0] / Cpto_wave)
    EC_T = 365.25 * 24 * 3600 * Occ[0]
    Tz = Tp[0]
    PTO = PTO_class.PTO("PTO_Wave_AirT", "Wave", *pto_cmpx2_AirT_userControl_inputs)
    PTO.performance(Cpto_wave, sigma_v_wave)
    PTO.reliability(EC_T, Tz)
    P_mech = int(np.sum(PTO.MechT_obj.Mech_P_mean))
    D_mech = int(PTO.MechT_obj.Mech_damage * 1e17)
    P_elect = int(np.sum(PTO.ElectT_obj.Elect_P_mean))
    D_elect = int(PTO.ElectT_obj.Elect_damage * 1e10)
    P_grid = int(np.sum(PTO.GridC_obj.Grid_P_mean))
    D_grid = int(PTO.GridC_obj.Grid_damage * 1e17)

    P_sum = P_mech + P_elect + P_grid
    D_sum = D_mech + D_elect + D_grid

    assert P_sum == 778, D_sum == 71


def test_pto_wave_cmpx1_Mech_inputs(read_config):
    # Initialisation of variables
    Cpto = np.zeros(len(read_config["Hs[m]"]))
    Pcapt = np.zeros(len(read_config["Hs[m]"]))
    Hs = read_config["Hs[m]"]
    Tp = read_config["Tp[s]"]
    Occ = read_config["Occ"]
    for c_SS in range(0, len(read_config["Hs[m]"])):
        # Maximum Captured power in each Sea State
        ind_Pmax = read_config["Pmean[W]"][c_SS][:].index(
            max(read_config["Pmean[W]"][c_SS][:])
        )
        Cpto[c_SS] = read_config["Cpto[Ns/m]"][c_SS][ind_Pmax]
        Pcapt[c_SS] = read_config["Pmean[W]"][c_SS][ind_Pmax]

    # Wave inputs - Sea State for assessment
    Cpto_wave = Cpto[0]
    sigma_v_wave = np.sqrt(Pcapt[0] / Cpto_wave)
    EC_T = 365.25 * 24 * 3600 * Occ[0]
    Tz = Tp[0]
    PTO = PTO_class.PTO("PTO_Wave_AirT", "Wave", *pto_cmpx1_Mech_inputs)
    PTO.performance(Cpto_wave, sigma_v_wave)
    PTO.reliability(EC_T, Tz)
    P_mech = int(np.sum(PTO.MechT_obj.Mech_P_mean))
    D_mech = int(PTO.MechT_obj.Mech_damage * 1e17)
    P_elect = int(np.sum(PTO.ElectT_obj.Elect_P_mean))
    D_elect = int(PTO.ElectT_obj.Elect_damage * 1e10)
    P_grid = int(np.sum(PTO.GridC_obj.Grid_P_mean))
    D_grid = int(PTO.GridC_obj.Grid_damage * 1e17)

    P_sum = P_mech + P_elect + P_grid
    D_sum = D_mech + D_elect + D_grid

    assert P_sum == 3035, D_sum == 13459177960


def test_pto_wave_cmpx1_Gen_inputs(read_config):
    # Initialisation of variables
    Cpto = np.zeros(len(read_config["Hs[m]"]))
    Pcapt = np.zeros(len(read_config["Hs[m]"]))
    Hs = read_config["Hs[m]"]
    Tp = read_config["Tp[s]"]
    Occ = read_config["Occ"]
    for c_SS in range(0, len(read_config["Hs[m]"])):
        # Maximum Captured power in each Sea State
        ind_Pmax = read_config["Pmean[W]"][c_SS][:].index(
            max(read_config["Pmean[W]"][c_SS][:])
        )
        Cpto[c_SS] = read_config["Cpto[Ns/m]"][c_SS][ind_Pmax]
        Pcapt[c_SS] = read_config["Pmean[W]"][c_SS][ind_Pmax]

    # Wave inputs - Sea State for assessment
    Cpto_wave = Cpto[0]
    sigma_v_wave = np.sqrt(Pcapt[0] / Cpto_wave)
    EC_T = 365.25 * 24 * 3600 * Occ[0]
    Tz = Tp[0]
    PTO = PTO_class.PTO("PTO_Wave_AirT", "Wave", *pto_cmpx1_Gen_inputs)
    PTO.performance(Cpto_wave, sigma_v_wave)
    PTO.reliability(EC_T, Tz)
    P_mech = int(np.sum(PTO.MechT_obj.Mech_P_mean))
    D_mech = int(PTO.MechT_obj.Mech_damage * 1e17)
    P_elect = int(np.sum(PTO.ElectT_obj.Elect_P_mean))
    D_elect = int(PTO.ElectT_obj.Elect_damage * 1e10)
    P_grid = int(np.sum(PTO.GridC_obj.Grid_P_mean))
    D_grid = int(PTO.GridC_obj.Grid_damage * 1e17)

    P_sum = P_mech + P_elect + P_grid
    D_sum = D_mech + D_elect + D_grid

    assert P_sum == 1130, D_sum == 2658


def test_pto_wave_cmpx1_Grid_inputs(read_config):
    # Initialisation of variables
    Cpto = np.zeros(len(read_config["Hs[m]"]))
    Pcapt = np.zeros(len(read_config["Hs[m]"]))
    Hs = read_config["Hs[m]"]
    Tp = read_config["Tp[s]"]
    Occ = read_config["Occ"]
    for c_SS in range(0, len(read_config["Hs[m]"])):
        # Maximum Captured power in each Sea State
        ind_Pmax = read_config["Pmean[W]"][c_SS][:].index(
            max(read_config["Pmean[W]"][c_SS][:])
        )
        Cpto[c_SS] = read_config["Cpto[Ns/m]"][c_SS][ind_Pmax]
        Pcapt[c_SS] = read_config["Pmean[W]"][c_SS][ind_Pmax]

    # Wave inputs - Sea State for assessment
    Cpto_wave = Cpto[0]
    sigma_v_wave = np.sqrt(Pcapt[0] / Cpto_wave)
    EC_T = 365.25 * 24 * 3600 * Occ[0]
    Tz = Tp[0]
    PTO = PTO_class.PTO("PTO_Wave_AirT", "Wave", *pto_cmpx1_Grid_inputs)
    PTO.performance(Cpto_wave, sigma_v_wave)
    PTO.reliability(EC_T, Tz)
    P_mech = int(np.sum(PTO.MechT_obj.Mech_P_mean))
    D_mech = int(PTO.MechT_obj.Mech_damage * 1e17)
    P_elect = int(np.sum(PTO.ElectT_obj.Elect_P_mean))
    D_elect = int(PTO.ElectT_obj.Elect_damage * 1e10)
    P_grid = int(np.sum(PTO.GridC_obj.Grid_P_mean))
    D_grid = int(PTO.GridC_obj.Grid_damage * 1e17)

    P_sum = P_mech + P_elect + P_grid
    D_sum = D_mech + D_elect + D_grid

    assert P_sum == 780, D_sum == 29506


# --- TIDAL Cases PTO testing ---


def test_pto_wave_cmpx2_GB_tidal_inputs():
    PTO = PTO_class.PTO("PTO_Wave_AirT", "Tidal", *pto_cmpx2_GB_tidal_inputs)
    PTO.performance(Cpto_tidal, sigma_v_tidal)
    PTO.reliability(365.25 * 24.0 * 3600.0 * 0.1, 12.0 * 3600.0)
    P_mech = int(np.sum(PTO.MechT_obj.Mech_P_mean))
    D_mech = int(PTO.MechT_obj.Mech_damage * 1e17)
    P_elect = int(np.sum(PTO.ElectT_obj.Elect_P_mean))
    D_elect = int(PTO.ElectT_obj.Elect_damage * 1e10)
    P_grid = int(np.sum(PTO.GridC_obj.Grid_P_mean))
    D_grid = int(PTO.GridC_obj.Grid_damage * 1e17)

    P_sum = P_mech + P_elect + P_grid
    D_sum = D_mech + D_elect + D_grid

    assert P_sum == 13, D_sum == 706237939


# def test_pto_wave_cmpx1_Mech_tidal_inputs():
#     PTO = PTO_class.PTO("PTO_Wave_AirT", "Wave", *pto_cmpx1_Mech_tidal_inputs)
#     PTO.performance(Cpto_tidal, sigma_v_tidal)
#     PTO.reliability(365.0 * 24.0 * 3600.0 * 0.1, 12.0 * 3600.0)
#     P_mech = int(np.sum(PTO.MechT_obj.Mech_P_mean))
#     D_mech = int(PTO.MechT_obj.Mech_damage * 1e17)
#     P_elect = int(np.sum(PTO.ElectT_obj.Elect_P_mean))
#     D_elect = int(PTO.ElectT_obj.Elect_damage * 1e10)
#     P_grid = int(np.sum(PTO.GridC_obj.Grid_P_mean))
#     D_grid = int(PTO.GridC_obj.Grid_damage * 1e17)

#     P_sum = P_mech + P_elect + P_grid
#     D_sum = D_mech + D_elect + D_grid

#     assert P_sum == 156, D_sum == 9183837
