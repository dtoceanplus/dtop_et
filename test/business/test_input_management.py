import copy

from dtop_energytransf.business.et_studies import create_study
from dtop_energytransf.business.input_management import InputManagement
from dtop_energytransf.service.api.inputs import get_input_data
from test.business.test_et_studies import STUDY_DATA
from dtop_energytransf.storage.mecanic_data import wells_default, impulse_default, gearbox_default, hydraulic_default
from dtop_energytransf.storage.control_data import aux_adim_vel, aux_power_levels, aux_load_levels, aux_load_ranges


DEFAULT_INPUT_DATA = {
    "electrical_inputs": {
        "complexity_level": None,
        "rated_power": 50000.0,
        "Vnom" : 400,
        "fnom" : 50,
        "Lgen" : 0.0005,
        "Rgen" : 0.0001,
        "pp": 2,
        "rel_T_maxnom": 2.0,
        "rel_V_maxnom": 1.725,
        "gen_class": "Class_A",
        "Type": "SCIG",
        "x_f" : [0.0000002, -0.00001, 0.0006],
        "shaft_diam" : [0.00000000000002, -0.000000007, 0.001, 27.451],
        "phi_cos" : [8e-15, -0.000000001, 0.00005, 0.0827],
        "sigma_h" : 5.01,
        "sigma_e" : 44.8,
        "B" : 0.8,
        "Gen_mass" : [0.0056, 55.041],
        "wind_mass_fraction" : 0.6,
        "I_nom" : [0.0018, 0.934],
        "Res" : [72584, -1.236],
        "Life" : {"Class_A" : [-0.069, 17.1288228, 105], "Class_B" : [-0.069, 18.8974785, 130], "Class_F" : [-0.069, 20.7619675, 155], "Class_H" : [-0.069, 22.1243926, 180]},
        "cost" : [2.5, 0.7],
        "om_shaft_norm" : [0, 0.03, 0.06, 0.1, 0.185, 0.4, 0.6, 0.8, 1],
        "eff_levels" : [0, 0.4, 0.5, 0.6, 0.7, 0.8, 0.85, 0.88, 0.9],
        "thick_max" : 0.64,
        "Manufacturer" : "ABB",
        "Date" : "21102020"
    },
    "general_inputs": {
        "dev_shut_flag": 1,
        "parallel_ptos": 1
    },
    "grid_inputs": {
        "complexity_level": None,
        "rated_power": 50000.0,
        "power_loads_norm" : [0.1, 0.2, 0.3, 0.5, 0.8, 1],
        "eff_levels" : [0.97, 0.975, 0.98, 0.982, 0.981, 0.978],
        "failure_rate_cpx1" : 0.27,
        "Vdc" : 1200,
        "fsw" : 5000,
        "Vgrid" : 690,
        "Rgrid" : 0.0001,
        "cosfi" : 1.0,
        "Lgrid" : 0.001,
        "Fgrid" : 50,
        "Type": "B2B2level",
        "Manufacturer" : "INFINEON",
        "Date" : "21102020",
        "cost" : 0.1,
        "life" : [2E+19, -7.885],
        "temp" : [0, 40, 120],
        "mass" : {"Copper": 0.000137, "Plastic": 0.00006, "Steel": 0.000326, "Iron": 0.00005, "Zink": 0.00002},
        "IGBT150" : [0.85, 0.011497, 0.008490, 0.0006578, -0.0000002294, 900],
        "Diode150" : [0.81, 0.006625, 0.0110730, 0.0002368, -0.0000003392, 900],
        "IGBT450" : [0.85, 0.00358333, 0.06026, 0.0003002, 0.0000006978, 900],
        "Diode450" : [0.8, 0.00271605, 0.02424, 0.0002509, 0.0000001585, 900],
        "IGBT800" : [1.15, 0.00178125, 0.0977, 0.00028, 0.000000347, 900],
        "Diode800" : [0.875, 0.0011, 0.049, 0.000352, -0.000000105, 900],
        "IGBT1600": [1, 0.001, 0.258, 0.000275, 0.000000163, 900],
        "Diode1600" :[0.85, 0.000589, 0.095, 0.000314, -0.00000004, 900]
    },
    "mechanical_inputs": {
        "complexity_level": None,
        "rated_power": 50000.0,
        "t_ratio": 10,
        "mech_t_ratio":10,
        "diameter" : 1.0,
        "Sowc" : 19.63,
        "size" : 1.4e-4,
        "Ap" : 0.2,
        "flow" : 1,
        "Type": "AirTurbine",
        "turb_type" : "Wells",
        "wells_data": wells_default,
        "impulse_data": impulse_default,
        "hydraulic_data": hydraulic_default,
        "gearbox_data": gearbox_default
    },
    "control_inputs": {
        "complexity_level": None,
         "control_type" :"Passive",
         "n_sigma" : 5,
         "bins" : 500,
         "vel_mean": 1e-6,
         "adim_vel" : aux_adim_vel,
         "power_levels" : aux_power_levels,
         "load_levels" : aux_load_levels,
         "load_ranges" : aux_load_ranges
    }
}
UPDATED_INPUT_DATA = {
    "electrical_inputs": {
        "complexity_level": 1,
        "rated_power": 100000.0,
        "Vnom": 400,
        "fnom": 50,
        "Lgen": 0.0005,
        "Rgen": 0.0001,
        "pp": 2,
        "rel_T_maxnom": 2.0,
        "rel_V_maxnom": 1.725,
        "gen_class": "Class_A",
        "Type": "SCIG",
        "x_f": [0.0000002, -0.00001, 0.0006],
        "shaft_diam": [0.00000000000002, -0.000000007, 0.001, 27.451],
        "phi_cos": [0.000000000000008, -0.000000001, 0.00005, 0.0827],
        "sigma_h": 5.01,
        "sigma_e": 44.8,
        "B": 0.8,
        "Gen_mass": [0.0056, 55.041],
        "wind_mass_fraction": 0.6,
        "I_nom": [0.0018, 0.934],
        "Res": [72584, -1.236],
        "Life": {"Class_A": [-0.069, 17.1288228, 105], "Class_B": [-0.069, 18.8974785, 130],
         "Class_F": [-0.069, 20.7619675, 155], "Class_H": [-0.069, 22.1243926, 180]},
        "cost": [2.5, 0.7],
        "om_shaft_norm": [0, 0.03, 0.06, 0.1, 0.185, 0.4, 0.6, 0.8, 1],
        "eff_levels": [0, 0.4, 0.5, 0.6, 0.7, 0.8, 0.85, 0.88, 0.9],
        "thick_max": 0.64,
        "Manufacturer": "ABB",
        "Date": "21102020"
      },
    "general_inputs": {
        "dev_shut_flag": 1,
        "parallel_ptos": 1
    },
    "grid_inputs": {
        "complexity_level": 1,
        "rated_power": 100000.0,
        "power_loads_norm" : [0.1, 0.2, 0.3, 0.5, 0.8, 1],
        "eff_levels" : [0.97, 0.975, 0.98, 0.982, 0.981, 0.978],
        "failure_rate_cpx1" : 0.27,
        "Vdc" : 1200,
        "fsw" : 5000,
        "Vgrid" : 690,
        "Rgrid" : 0.0001,
        "cosfi" : 1.0,
        "Lgrid" : 0.001,
        "Fgrid" : 50,
        "Type": "B2B2level",
        "Manufacturer" : "INFINEON",
        "Date" : "21102020",
        "cost" : 0.1,
        "life" : [2E+19, -7.885],
        "temp" : [0, 40, 120],
        "mass" : {"Copper": 0.000137, "Plastic": 0.00006, "Steel": 0.000326, "Iron": 0.00005, "Zink": 0.00002},
        "IGBT150" : [0.85, 0.011497, 0.008490, 0.0006578, -0.0000002294, 900],
        "Diode150" : [0.81, 0.006625, 0.0110730, 0.0002368, -0.0000003392, 900],
        "IGBT450" : [0.85, 0.00358333, 0.06026, 0.0003002, 0.0000006978, 900],
        "Diode450" : [0.8, 0.00271605, 0.02424, 0.0002509, 0.0000001585, 900],
        "IGBT800" : [1.15, 0.00178125, 0.0977, 0.00028, 0.000000347, 900],
        "Diode800" : [0.875, 0.0011, 0.049, 0.000352, -0.000000105, 900],
        "IGBT1600": [1, 0.001, 0.258, 0.000275, 0.000000163, 900],
        "Diode1600" :[0.85, 0.000589, 0.095, 0.000314, -0.00000004, 900]
    },
    "mechanical_inputs": {
        "complexity_level": 1,
        "rated_power": 100000.0,
        "t_ratio": 10,
        "mech_t_ratio":10,
        "diameter": 1.0,
        "Sowc": 19.63,
        "size": 1.4e-4,
        "Ap": 0.2,
        "flow" : 1.0,
        "Type": "AirTurbine",
        "turb_type" : "Wells",
        "wells_data" : wells_default,
        "impulse_data" : impulse_default,
        "hydraulic_data" : hydraulic_default,
        "gearbox_data" : gearbox_default
    },
    "control_inputs": {
        "complexity_level": 1,
        "control_type": "Passive",
        "n_sigma": 5,
        "bins": 500,
        "vel_mean": 1e-6,
        "adim_vel": aux_adim_vel,
        "power_levels": aux_power_levels,
        "load_levels": aux_load_levels,
        "load_ranges": aux_load_ranges
    }
}


def test_input_management_init(app):

    with app.app_context():
        s = create_study(STUDY_DATA)

        input_mgmt = InputManagement(s, DEFAULT_INPUT_DATA)
        assert not input_mgmt.validation_error


def test_input_management_init_validation_error(app):

    with app.app_context():
        s = create_study(STUDY_DATA)

        invalid_input_data = copy.deepcopy(DEFAULT_INPUT_DATA)
        del invalid_input_data["electrical_inputs"]
        input_mgmt = InputManagement(s, invalid_input_data)
        assert input_mgmt.validation_error


def test_validate_complexity_levels_and_inputs(app):

    with app.app_context():
        s = create_study(STUDY_DATA)

        input_mgmt = InputManagement(s, DEFAULT_INPUT_DATA)
        assert input_mgmt.validate_complexity_levels_and_inputs()
        # TODO: Right now it is just asserting that the function returns true, will eventually need to add proper unit
        #   tests when the function is actually doing something


# def test_update_db(app):

#     with app.app_context():
#         s = create_study(STUDY_DATA)

#         # First check that the inputs DB tables are full of default values (when study is initially created)
#         input_data = get_input_data(1)
#         assert input_data == DEFAULT_INPUT_DATA
#      #   np.testing.assert_equal(input_data, DEFAULT_INPUT_DATA)

#         # Then check that the updates have been performed correctly
#         input_mgmt = InputManagement(s, UPDATED_INPUT_DATA)
#         input_mgmt.update_db()
#         input_data = get_input_data(1)
#         assert input_data == UPDATED_INPUT_DATA
#       #  np.testing.assert_equal(input_data, UPDATED_INPUT_DATA)
