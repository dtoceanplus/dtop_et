import numpy as np

from dtop_energytransf.business.et_studies import create_study
from dtop_energytransf.business.input_management import InputManagement
from dtop_energytransf.business.preprocessor import Preprocessor
from test_et_studies import STUDY_DATA
from test_input_management import UPDATED_INPUT_DATA
from dtop_energytransf.storage.control_data import *
from test_cross_module_data import EC_DATA_W1, MC_DATA_W1, SC_DATA_W1, EC_DATA_T3, MC_DATA_T3, SC_DATA_T3
from dtop_energytransf.business.cross_module_data import *
from dtop_energytransf.storage.models.models import EnergyTransformationStudy
from dtop_energytransf.service.api.studies import get_crossmodule_data

# EC_DATA_W1 = {
#     "technology": "WEC",    
#     "complexity": 1,
#     "id": 3,
#     "number_devices": 2,
#     "captured_power_per_condition":[ 
#     {"deviceID": 0, 
#      "capturedPower": [100], 
#      "siteConditionID": [0]},
#      {"deviceID": 1, 
#      "capturedPower": [150], 
#       "siteConditionID": [0]}]
# }


EXPECTED_ARRAY_CLASS_INPUTS = (
    "Array_01",
    {
        "Technology": "Wave",
        "Number of devices": 2,
    },
    {
        "dof": 1,
        "Parallel_PTOs": 1,
        "Shutdown_flag": 1,
        "cut_in_out": [],
    },
    [{
        "Type": "Mech_Simplified",
        "init_args": [
            100000.0,
            10,
            "Wave",
        ],
        "perf_args": [],
        "mech_t_ratio": 1,
    },
    {
        "Type": "Elect_Simplified",
        "init_args": [100000.0]
    },
    {
        "Type": "Grid_Simplified",
        "init_args": [
            100000.0,
            690
        ],
    },
    {
        "Type": "Passive",
        "init_args": [
            5, 
            500, 
            1e-06,
            {"Load_levels": aux_load_levels,
            "Load_ranges": aux_load_ranges,
            "Power_levels": aux_power_levels,
            "adim_vel": aux_adim_vel
            }]}]
)


# define what we need to test

def create_study_with_inputs():

    s = create_study(STUDY_DATA)
    input_mgmt = InputManagement(s, UPDATED_INPUT_DATA)
    input_mgmt.update_db()

    return s        # here a dummy study is created

def preprocess_setup(tech):

    s = create_study_with_inputs()

    if tech == "Wave":
        ProcessEnergyCapture(s, EC_DATA_W1)
        MC_DATA_W1['technology'] = 'WEC'
        mc = ProcessMachineCharacterisation(s, MC_DATA_W1)
        ProcessSiteCharacterisation(s, SC_DATA_W1)
        result = get_crossmodule_data(1)
    else:
        ProcessEnergyCapture(s, EC_DATA_T3)
        MC_DATA_T3['technology'] = 'TEC'
        mc = ProcessMachineCharacterisation(s, MC_DATA_T3)
        ProcessSiteCharacterisation(s, SC_DATA_T3) 
        result = get_crossmodule_data(1)
    
    
    study = EnergyTransformationStudy.query.get(1)
    ec = study.energy_capture_data
    sc = study.site_characterisation_data
    mc = study.machine_characterisation_data
    general_inputs = s.general_inputs
    mechanical_inputs = s.mechanical_inputs
    electrical_inputs = s.electrical_inputs
    grid_inputs = s.grid_inputs
    control_inputs = s.control_inputs

    return (
        ec,
        mc,
        sc,
        general_inputs,
        mechanical_inputs,
        electrical_inputs,
        grid_inputs,
        control_inputs
    )

print("ok")

## here test the _init_ fuction in the preprocessor
def test_preprocess_init(app):
    with app.app_context():

        preprocess_inputs = preprocess_setup("Wave")
        preprocessor = Preprocessor(*preprocess_inputs)

        assert getattr(preprocessor, '_mech_processor').__name__ == "_process_mech_inputs_cpx1"
        assert getattr(preprocessor, '_elec_processor').__name__ == "_process_elec_inputs_cpx1"
        assert getattr(preprocessor, '_grid_processor').__name__ == "_process_grid_inputs_cpx1"
        assert getattr(preprocessor, '_control_processor').__name__ == "_process_control_inputs_cpx1"

# here test1 array perf input functions
def test_preprocess_array_perf_inputs(app):
    with app.app_context():
        preprocess_inputs = preprocess_setup("Wave")
        preprocessor = Preprocessor(*preprocess_inputs)

        preprocessor.generate_performance_inputs()
        
        assert preprocessor.array_perf_inputs == ([{'sigma_v': [[0.25]], 'C_pto': [[1600000]]}, {'sigma_v': [[0.30618621784789724]], 'C_pto': [[1600000]]}], [{'Hs': [1], 'Occ': [1], 'Tz': [1], 'id': [0]}, {'Hs': [1], 'Occ': [1], 'Tz': [1], 'id': [0]}])



## here test2 array perf input functions
# def test_preprocess_array_perf_inputs_2(app):

#     # Test array performance inputs with more complicated Pcapt and Cpto

#     with app.app_context():
#         preprocess_inputs = preprocess_setup("Tidal")
#         preprocessor = Preprocessor(*preprocess_inputs)

#         preprocessor.generate_performance_inputs()
        
#         assert preprocessor.array_perf_inputs == ([{'sigma_v': [[2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553], [2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553], [2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553]], 'C_pto': [[0.0, 485131.5096967493, 1617105.0323224973, 1518452.369626761, 739137.7489631724, 0.0], [0.0, 485131.5096967493, 1617105.0323224973, 1518452.369626761, 739137.7489631724, 0.0], [0.0, 485131.5096967493, 1617105.0323224973, 1518452.369626761, 739137.7489631724, 0.0]], 'Ct_tidal': [[0.1, 0.3, 0.3, 0.3, 0.5, 0.1], [0.1, 0.3, 0.3, 0.3, 0.5, 0.1], [0.1, 0.3, 0.3, 0.3, 0.5, 0.1]]}, {'sigma_v': [[2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553], [2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553], [2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553]], 'C_pto': [[0.0, 485131.5096967493, 1617105.0323224973, 1518452.369626761, 739137.7489631724, 0.0], [0.0, 485131.5096967493, 1617105.0323224973, 1518452.369626761, 739137.7489631724, 0.0], [0.0, 485131.5096967493, 1617105.0323224973, 1518452.369626761, 739137.7489631724, 0.0]], 'Ct_tidal': [[0.1, 0.3, 0.3, 0.3, 0.5, 0.1], [0.1, 0.3, 0.3, 0.3, 0.5, 0.1], [0.1, 0.3, 0.3, 0.3, 0.5, 0.1]]}, {'sigma_v': [[2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553], [2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553], [2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553]], 'C_pto': [[0.0, 485131.5096967493, 1617105.0323224973, 1518452.369626761, 739137.7489631724, 0.0], [0.0, 485131.5096967493, 1617105.0323224973, 1518452.369626761, 739137.7489631724, 0.0], [0.0, 485131.5096967493, 1617105.0323224973, 1518452.369626761, 739137.7489631724, 0.0]], 'Ct_tidal': [[0.1, 0.3, 0.3, 0.3, 0.5, 0.1], [0.1, 0.3, 0.3, 0.3, 0.5, 0.1], [0.1, 0.3, 0.3, 0.3, 0.5, 0.1]]}, {'sigma_v': [[2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553], [2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553], [2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553]], 'C_pto': [[0.0, 485131.5096967493, 1617105.0323224973, 1518452.369626761, 739137.7489631724, 0.0], [0.0, 485131.5096967493, 1617105.0323224973, 1518452.369626761, 739137.7489631724, 0.0], [0.0, 485131.5096967493, 1617105.0323224973, 1518452.369626761, 739137.7489631724, 0.0]], 'Ct_tidal': [[0.1, 0.3, 0.3, 0.3, 0.5, 0.1], [0.1, 0.3, 0.3, 0.3, 0.5, 0.1], [0.1, 0.3, 0.3, 0.3, 0.5, 0.1]]}, {'sigma_v': [[2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553], [2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553], [2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553]], 'C_pto': [[0.0, 485131.5096967493, 1617105.0323224973, 1518452.369626761, 739137.7489631724, 0.0], [0.0, 485131.5096967493, 1617105.0323224973, 1518452.369626761, 739137.7489631724, 0.0], [0.0, 485131.5096967493, 1617105.0323224973, 1518452.369626761, 739137.7489631724, 0.0]], 'Ct_tidal': [[0.1, 0.3, 0.3, 0.3, 0.5, 0.1], [0.1, 0.3, 0.3, 0.3, 0.5, 0.1], [0.1, 0.3, 0.3, 0.3, 0.5, 0.1]]}], [{'Vc': [2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553], 'Occ': [0.16, 0.16, 0.16, 0.16, 0.16, 0.16], 'Tz': [1, 1, 1, 1, 1, 1], 'id': 1}, {'Vc': [2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553], 'Occ': [0.16, 0.16, 0.16, 0.16, 0.16, 0.16], 'Tz': [1, 1, 1, 1, 1, 1], 'id': 1}, {'Vc': [2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553], 'Occ': [0.16, 0.16, 0.16, 0.16, 0.16, 0.16], 'Tz': [1, 1, 1, 1, 1, 1], 'id': 1}, {'Vc': [2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553], 'Occ': [0.16, 0.16, 0.16, 0.16, 0.16, 0.16], 'Tz': [1, 1, 1, 1, 1, 1], 'id': 1}, {'Vc': [2.75016301683056, 2.5133814393838345, 2.5133814393838345, 2.3600507858955155, 2.29760598385362, 2.116560480963553], 'Occ': [0.16, 0.16, 0.16, 0.16, 0.16, 0.16], 'Tz': [1, 1, 1, 1, 1, 1], 'id': 1}])


### here test the array class
def test_preprocess_array_class_inputs(app):
   with app.app_context():
       preprocess_inputs = preprocess_setup("Wave")
       preprocessor = Preprocessor(*preprocess_inputs)

       preprocessor.preprocess()
       assert preprocessor.array_class_inputs == EXPECTED_ARRAY_CLASS_INPUTS
