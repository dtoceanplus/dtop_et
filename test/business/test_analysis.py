import json
import os

from dtop_energytransf.business.analysis import RunAnalysis
from dtop_energytransf.business.et_studies import create_study
from test_et_studies import STUDY_DATA
from test_input_management import UPDATED_INPUT_DATA
from dtop_energytransf.business.input_management import InputManagement
from dtop_energytransf.business.output_management import NpEncoder
# from test.business.test_preprocessor import create_study_with_inputs
from dtop_energytransf.business.cross_module_data import *

# External data for Wave cmpx 1
EC_DATA_W1 = {
    "technology": "WEC",    
    "complexity": 1,
    "id": 3,
    "number_devices": 2,
    "captured_power_per_condition":[ 
    {"deviceID": 0, 
     "capturedPower": [100], 
     "siteConditionID": [0]},
     {"deviceID": 1, 
     "capturedPower": [150], 
      "siteConditionID": [0]}]
}

SC_DATA_W1 = {
    "technology" : "WEC",
    "complexity": 1,
    "Hs": [2],
    "Tp": [2],
    "Occ": [1],
    "id": [1]
}

MC_DATA_W1 = {
    "technology": "WEC",
    "complexity": 1,
    "id": 3,
    "pto_damping": [1600000] 
}

def json_loader(f_name):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    with open(f"{dir_path}/{f_name}.json", "r") as json_file:
        data = json.load(json_file)

    return data

def create_study_with_inputs():
    
    s = create_study(STUDY_DATA)
    ProcessEnergyCapture(s, EC_DATA_W1)
    ProcessMachineCharacterisation(s, MC_DATA_W1)
    ProcessSiteCharacterisation(s, SC_DATA_W1)
    input_mgmt = InputManagement(s, UPDATED_INPUT_DATA)
    input_mgmt.update_db()

    return s        # here a dummy study is created

# def test_analysis_wave_cpx1_all(app):
#     with app.app_context():
#         create_study_with_inputs()

#         analysis = RunAnalysis(1)

#         array_json = json.loads(json.dumps(analysis.array_results.__dict__, sort_keys=True, cls=NpEncoder))
#         # expected_array_json = json_loader("arraytest2")
#         # assert array_json ==  expected_array_json
#         ar = array_json["Array_Pa_Grid"]["value"]["Power"]
#         assert ar[0] ==  4.146
#         ar = array_json["Array_mass"]["value"]
#         assert ar ==  46000.0

# def test_analysis_wave_cpx2_all(app):
#     with app.app_context():
#         create_study_with_inputs()

#         analysis = RunAnalysis(1)

#         array_json = json.loads(json.dumps(analysis.array_results.__dict__, sort_keys=True, cls=NpEncoder))
#         expected_array_json = json_loader("uparray")
#         assert array_json == expected_array_json

# def test_analysis_wave_cpx3_all(app):
#     with app.app_context():
#         create_study_with_inputs()

#         analysis = RunAnalysis(1)

#         array_json = json.loads(json.dumps(analysis.array_results.__dict__, sort_keys=True, cls=NpEncoder))
#         expected_array_json = json_loader("uparray")
#         assert array_json == expected_array_json
