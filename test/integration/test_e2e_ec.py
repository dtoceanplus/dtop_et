"""
This is a simple test that checks that all necessary services are available
After some real tests involving communication with MC and SC will be written, this won't be needed
"""

import requests


def test_ec_farm():
    resp = requests.get("http://ec/ec/1/farm")
    assert resp.status_code == 200, resp.text


def test_ec_devices():
    resp = requests.get("http://ec/ec/1/devices")
    assert resp.status_code == 200, resp.text
