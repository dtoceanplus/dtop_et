"""
This is a simple test that checks that all necessary services are available
After some real tests involving communication with MC and SC will be written, this won't be needed
"""

import requests


def test_mc_general():
    resp = requests.get("http://mc/mc/1/general")
    assert resp.status_code == 200, resp.text


def test_mc_dimensions():
    resp = requests.get("http://mc/mc/1/dimensions")
    assert resp.status_code == 200, resp.text


def test_mc_project():
    resp = requests.get("http://mc/mc/1")
    assert resp.status_code == 200, resp.text
