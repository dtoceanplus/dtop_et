"""
This is a simple test that checks that all necessary services are available
After some real tests involving communication with sc and SC will be written, this won't be needed
"""
import pytest
import requests


def test_sc_info():
    resp = requests.get("http://sc/sc/1/point/info")
    assert resp.status_code == 200, resp.text


def test_sc_current_stats():
    resp = requests.get("http://sc/sc/1/point/statistics/currents/Basic/Flux")
    assert resp.status_code == 200, resp.text


def test_sc_wave_stats():
    resp = requests.get("http://sc/sc/1/point/statistics/waves/Basic/CgE")
    assert resp.status_code == 200, resp.text


def test_sc_wave_JJPD():
    resp = requests.get(
        "http://sc/sc/1/point/statistics/waves/EJPD3v/hs_tp_dp_monthly_flattened"
    )
    assert resp.status_code == 200, resp.text


def test_sc_current_scenarii():
    resp = requests.get("http://sc/sc/1/farm/scenarii_matrices/currentsmonthly")
    assert resp.status_code == 200, resp.text
