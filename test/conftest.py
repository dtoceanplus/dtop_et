# main/conftest.py
import json
import pathlib
import pytest
import pandas as pd
import numpy as np
from scipy.stats import rayleigh

from dtop_energytransf.service import create_app
from dtop_energytransf.storage.db_utils import init_db


@pytest.fixture()
def read_config(request):
    file = pathlib.Path(request.node.fspath)
    config = file.with_name("EC_out_OES_Sphere.json")
    with config.open() as fp:
        EC_out = pd.read_json(fp, orient="records", lines=True)
        # Initialisation of variables
        Cpto = np.zeros(len(EC_out["Hs[m]"]))
        Pcapt = np.zeros(len(EC_out["Hs[m]"]))
        Hs = EC_out["Hs[m]"]
        Tp = EC_out["Tp[s]"]
        Occ = EC_out["Occ"]
        for c_SS in range(0, len(EC_out["Hs[m]"])):
            # Maximum Captured power in each Sea State
            ind_Pmax = EC_out["Pmean[W]"][c_SS][:].index(
                max(EC_out["Pmean[W]"][c_SS][:])
            )
            Cpto[c_SS] = EC_out["Cpto[Ns/m]"][c_SS][ind_Pmax]
            Pcapt[c_SS] = EC_out["Pmean[W]"][c_SS][ind_Pmax]

        # Wave inputs - Sea State for assessment
        Cpto_wave = Cpto[0]
        sigma_v_wave = np.sqrt(Pcapt[0] / Cpto_wave)
        EC_T = 365.25 * 24 * 3600 * Occ[0]
        Tz = Tp[0]
        # Initialisation of variables

        return EC_out


@pytest.fixture
def app():
    app = create_app(
        {
            "TESTING": True,
            "SQLALCHEMY_DATABASE_URI": "sqlite://",
            "SERVER_NAME": "localhost.dev",
        }
    )

    with app.app_context():
        init_db()

    yield app


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def runner(app):
    return app.test_cli_runner()
