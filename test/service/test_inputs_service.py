from test.business.test_input_management import (
    create_study,
    STUDY_DATA,
    DEFAULT_INPUT_DATA,
    UPDATED_INPUT_DATA,
    get_input_data,
)
import numpy as np

def test_get_input_data_not_found(client):

    response = client.get("/energy_transf/1/inputs")
    assert response.status_code == 404
    assert (
        response.json["message"]
        == "Energy Transformation study with that ID could not be found"
    )

# def test_get_input_data(client, app):

#     with app.app_context():
#         create_study(STUDY_DATA)

#     response = client.get("/energy_transf/1/inputs")
#     assert response.status_code == 200
#     #assert response.json == DEFAULT_INPUT_DATA
#     np.testing.assert_equal(response.json, DEFAULT_INPUT_DATA)



def test_send_input_data(client, app):

    with app.app_context():
        create_study(STUDY_DATA)

    response = client.put("/energy_transf/1/inputs", json=UPDATED_INPUT_DATA)
    assert response.status_code == 200
    assert response.json == {
        "message": "Inputs have been updated",
    }


def test_send_input_data_no_request_body(client, app):

    with app.app_context():
        create_study(STUDY_DATA)

    response = client.put("/energy_transf/1/inputs")
    assert response.status_code == 400
    assert response.json["message"] == "No request body provided"



def test_send_input_data_wrong_id(client, app):

    with app.app_context():
        create_study(STUDY_DATA)

    response = client.put("/energy_transf/2/inputs", json=UPDATED_INPUT_DATA)
    assert response.status_code == 404
    assert (
        response.json["message"]
        == "Energy Transformation study with that ID could not be found"
    )


def test_send_input_data_validation_error(client, app):

    with app.app_context():
        create_study(STUDY_DATA)

    input_data_no_general_inputs = UPDATED_INPUT_DATA.copy()
    del input_data_no_general_inputs["general_inputs"]
    response = client.put("/energy_transf/1/inputs", json=input_data_no_general_inputs)
    assert response.status_code == 400
    assert "Missing data for required field" in response.json["message"]

# TODO: validate_complexity_levels_and_inputs when function done-->  def test_send_input_data_validate_complexity_levels_and_inputs(client, app):


