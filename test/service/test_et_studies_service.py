from test.business.test_et_studies import (
    create_study,
    STUDY_DATA,
    STUDY_DATA_ID,
    UPDATED_STUDY_DATA,
    UPDATED_STUDY_DATA_ID,
    get_study_list,
)

expected_study = {
  "cmpx": 1, 
  "study_result": {
    "description": "study description", 
    "energy_capture_study_id": 3, 
    "id": 1, 
    "machine_characterisation_study_id": 2, 
    "name": "test study", 
    "site_characterisation_study_id": 1, 
    "standalone": True, 
    "status": 40
  }, 
  "technology": "Wave"
}

def test_get_empty_study_list(client):

    response = client.get("/energy_transf/")
    assert response.status_code == 200
    studies = response.json["et_studies"]
    assert studies == []


def test_get_et_study_list(client, app):

    with app.app_context():
        create_study(STUDY_DATA)

    response = client.get("/energy_transf/")
    assert response.status_code == 200
    studies = response.json["et_studies"]
    assert studies == [STUDY_DATA_ID]


def test_create_et_study(client):

    response = client.post("/energy_transf/", json=STUDY_DATA)
    assert response.status_code == 201
    assert response.json == STUDY_DATA_ID


def test_create_study_no_request_body(client):

    response = client.post("/energy_transf/")
    assert response.status_code == 400
    assert response.json["message"] == "No request body provided"


def test_create_study_validation_error(client):

    study_data_no_name = STUDY_DATA.copy()
    del study_data_no_name["name"]
    response = client.post("/energy_transf", json=study_data_no_name)
    assert response.status_code == 400
    assert "Missing data for required field" in response.json["message"]


def test_create_study_business_error(client, app):

    with app.app_context():
        create_study(STUDY_DATA)

    response = client.post("/energy_transf/", json=STUDY_DATA)
    assert response.status_code == 400
    assert (
        response.json["message"]
        == "Energy Transformation study with that name already exists."
    )


def test_get_et_study(client, app):

    with app.app_context():
        create_study(STUDY_DATA)

    response = client.get("/energy_transf/1")
    assert response.status_code == 200
    assert response.json == expected_study


def test_get_et_study_not_found(client):

    response = client.get("/energy_transf/1")
    assert response.status_code == 404
    assert (
        response.json["message"]
        == "Energy Transformation study with that ID could not be found"
    )


def test_update_et_study(client, app):

    with app.app_context():
        create_study(STUDY_DATA)

    response = client.put("/energy_transf/1", json=UPDATED_STUDY_DATA)
    assert response.status_code == 200
    assert response.json == {
        "updated_study": UPDATED_STUDY_DATA_ID,
        "message": "Study updated.",
    }


def test_update_et_study_no_request_body(client, app):

    with app.app_context():
        create_study(STUDY_DATA)

    response = client.put("/energy_transf/1")
    assert response.status_code == 400
    assert response.json["message"] == "No request body provided"


def test_update_et_study_validation_error(client, app):

    with app.app_context():
        create_study(STUDY_DATA)

    study_no_name = UPDATED_STUDY_DATA.copy()
    del study_no_name["name"]
    response = client.put("/energy_transf/1", json=study_no_name)
    assert response.status_code == 400
    assert "Missing data for required field" in response.json["message"]


def test_update_et_study_wrong_id(client, app):

    with app.app_context():
        create_study(STUDY_DATA)

    response = client.put("/energy_transf/2", json=UPDATED_STUDY_DATA)
    assert response.status_code == 404
    assert (
        response.json["message"]
        == "Energy Transformation study with that ID does not exist."
    )


def test_update_et_study_same_name(client, app):

    with app.app_context():
        create_study(STUDY_DATA)
        create_study(UPDATED_STUDY_DATA)

    study_data_same_name = UPDATED_STUDY_DATA.copy()
    study_data_same_name["name"] = STUDY_DATA["name"]
    response = client.put("/energy_transf/2", json=study_data_same_name)
    assert response.status_code == 400
    assert response.json["message"] == "Energy Transformation study with that name already exists."


def test_delete_et_study(client, app):

    with app.app_context():
        create_study(STUDY_DATA)
        studies = get_study_list()
        assert len(studies) == 1

        response = client.delete("/energy_transf/1")
        assert response.status_code == 200
        assert response.json == {"deleted_study_id": "1", "message": "Study deleted."}
        studies = get_study_list()
        assert len(studies) == 0


def test_delete_et_study_not_found(client):

    response = client.delete("/energy_transf/1")
    assert response.status_code == 400
    assert (
        response.json["message"]
        == "Energy Transformation study with that ID does not exist."
    )
