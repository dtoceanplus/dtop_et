import pytest
import json

from dtop_energytransf import service
from dtop_energytransf.storage.models import *


# def test_projects(client):
#     clean_db(client)
#     response = client.get("/energy_transf")
#     projects = json.loads(response.data)
#     assert type(projects) is list
#
#     # def test_add_project(client):
#     post_project = client.post("/energy_transf", json=project_body)
#     data_post_project = json.loads(post_project.data)
#     assert data_post_project["message"] == "Project added"
#
#
# def clean_db(client):
#     response = client.get("/energy_transf")
#     if response.status_code == 404:
#         response = client.post("/energy_transf", json=project_body)
#         response = client.get("/energy_transf")
#
#     for project in response.json:
#         response = client.delete(f'energy_transf/{project["etId"]}')
#         assert response.status_code == 200


project_body = {
    "Description": "Example Body",
    "Technology": "Wave",
    "Type_sim": "Design",
    "ControlStrategy": "Passive",
    "Mech_type": "Air Turbine",
    "Elect_type": "SCIG",
    "Grid_type": "B2B2Level",
    "CpxMech": 1,
    "CpxElec": 1,
    "CpxGrid": 1,
    "Date": "2020-06-07",
}
