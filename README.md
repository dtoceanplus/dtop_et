# Energy Transformation Module Overview
Energy Transformation Module of the DTOceanPlus project.
It assesses the performance of the system and subsystems in the projects,
in terms of energy production, efficiency and power quality.

ET is distributed under the AGPL license see LICENSE file for more details.

# Python Package
## Virtual Environment
Create a virtual environment to isolate/package the application development

```bash
conda env create --file environment.yml
```

Activate the environment to be used for the development
```bash
conda activate dtop_energytransf
```

Installing the package for development:
```bash
pip install -e .
```

Build package:
```bash
python setup.py sdist
```
Package will be in `dist` folder.

Install package from `dist`:
```bash
pip install dtop_energytransf --find-links dist
```

Documentation:
- [Python Packaging User Guide](https://packaging.python.org)
- [Installing Packages](https://packaging.python.org/tutorials/installing-packages/)
- [Packaging Python Projects](https://packaging.python.org/tutorials/packaging-projects/)

# Testing

See [test](test) folder.

The [pytest](https://docs.pytest.org/en/latest/) framework makes it easy to write tests.

Install pytest:
```bash
pip install pytest
```

Run tests:
```bash
python -m pytest
```

## Coverage

Coverage reports about code which should be tested.

Python has tool [Coverage.py](https://coverage.readthedocs.io/)
and PyTest has plugin for it [pytest-cov](https://pytest-cov.readthedocs.io/).

Install pytest-cov plugin
```bash
pip install pytest-cov
```

Run pytest with coverage:
```bash
python -m pytest --cov=dtop_energytransf --cov-report=html
```

# Languages

Python has tool [Babel](http://babel.pocoo.org/) that assist in internationalizing and localizing
and Flask has plugin for it [Flask-Babel](https://pythonhosted.org/Flask-Babel/).

See [babel.cfg](babel.cfg) file.

Extracting Text to Translate:
```bash
pybabel extract .
```
It will create `message.pot` file with translations.
This file should not be stored in repository.

Generating a Language Catalog:
```bash
pybabel init -i messages.pot -d src/dtop_example/service/translations -l fr
```
It will create [src/dtop_example/service/translations/fr/LC_MESSAGES/messages.po](src/service/gui/translations/fr/LC_MESSAGES/messages.po) text file.

Use tool [poEdit](https://www.poedit.net/) for catalog (.po file) editing.

Compile Catalog:
```bash
pybabel compile -d src/dtop_example/service/translations
```
It will create `src/dtop_example/service/translations/fr/LC_MESSAGES/messages.mo` binary file.
This file is used by [Flask-Babel](https://pythonhosted.org/Flask-Babel/).

Tools:
- [poEdit](https://www.poedit.net/)

Documentation:
- [I18n and L10n](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-xiii-i18n-and-l10n)
- [Babel](http://babel.pocoo.org/)
- [Flask-Babel](https://pythonhosted.org/Flask-Babel/)


# Service API documentation

For service API description there is [OpenAPI Specification](https://openapis.org).
See [openapi.json](src/dtop_example/service/static/openapi.json).

For API documentation page there is [ReDoc](https://github.com/Rebilly/ReDoc) tool.
It is OpenAPI/Swagger-generated API Reference Documentation.
See [templates/api/index.html](src/dtop_example/service/templates/api/index.html) and
[api/foo/__init__.py](src/dtop_example/service/api/foo/__init__.py).

For generating OpenAPI files from code comments there is tool [oas](https://openap.is).

Install oas with [Node.js npm](https://nodejs.org/):
```bash
npm install oas -g
```
Init template:
```bash
oas init
```
See [swagger.yaml](swagger.yaml).

Generate OpenAPI description:
```bash
oas generate > src/dtop_example/service/static/openapi.json
```
See [openapi.json](src/dtop_example/service/static/openapi.json).

Documentation:
- [ReDoc](https://github.com/Rebilly/ReDoc)
- [OpenAPI Specification by Swagger](https://swagger.io/specification/)
- [oas](https://openap.is)
- [OpenAPI Tools](https://openapi.tools)


# GitLab CI

The [.gitlab-ci.yml](.gitlab-ci.yml) file tells the GitLab runner what to do.

GitLab offers a CI (Continuous Integration) service.
CI is for build and check each modification in repository.

Documentation:
- [Getting started with GitLab CI/CD](https://gitlab.com/help/ci/quick_start/README)
- [GitLab CI/CD Pipeline Configuration Reference](https://gitlab.com/help/ci/yaml/README.md)


# Start Service

Install `python-dotenv` for taking service default configuration from `.flaskenv`:

```DOS .bat
pip install python-dotenv
```

Without `.flaskenv` you should set environment variables for `flask`:

```DOS .bat
set FLASK_APP=dtop_energytransf.service
set FLASK_ENV=development
set FLASK_RUN_PORT=5000
```

Initialize database:
```DOS .bat
flask init-db
```
Database will be created in `instance/dtop_main.sqlite`.

Start the service:
```DOS .bat
flask run
```

Now you can open in browser this URL: http://localhost:5000/.

# Using with Docker

Install [Docker].

[Docker]: https://www.docker.com/products/docker-desktop

Start service:
```bash
docker-compose up -d
```

# Comments for OCC update
## Prerequisites and environment
The update fixing current local tests issues has been developed and tested in the following development environment and installed products:

1. Windows 10 Pro
2. Docker and Windows Gitlab CI
3. [miniconda3](https://docs.conda.io/en/latest/miniconda.html)
4. [anaconda make](https://anaconda.org/anaconda/make)
5. [Git Bash](https://gitforwindows.org/)
6. [Node.js](https://nodejs.org/en/)

Commands below have been tested using conda virtual environment and Powershell on Windows 10
- locally and Gitlab CI (based on OCC Windows 10 Group Runner)
- locally with Linux Dockers and Gitlab CI (based on the same Dockers)

Clean Conda virtual environment `dtop_spey` is used.

## Getting submodule sources :
```bash
git submodule update --init --recursive
```

## Setup/adjust the necessary environment variables :
```bash
$ENV:PATH="C:\ProgramData\Miniconda3;$ENV:PATH"
$ENV:PATH="C:\ProgramData\Miniconda3\Scripts;$ENV:PATH"
$ENV:PATH="C:\ProgramData\Miniconda3\Library\bin;$ENV:PATH"
$ENV:PATH="C:\Program Files\Git\bin;$ENV:PATH"
```

## Simple checks of the development and test environment :
```bash
where.exe conda
where.exe make
where.exe bash
where.exe node
where.exe npm
```

## Local environment tests
For simplification use the `testing.makefile` targets. If necessary also see `testing.makefile` and `.gitlab-ci.yml` to perform directly the commands of these targers.

### Recreate clean conda virtual environment `dtop_energytransf` :
```bash
make --file testing.makefile reset
```

### Activate the conda environment `dtop_energytransf`:
```bash
conda activate dtop_energytransf
    or
C:\ProgramData\Miniconda3\shell\condabin\conda-hook.ps1 ; conda activate C:\ProgramData\Miniconda3\envs\dtop_energytransf

conda env list
```

### Rebuild and install `dtop_energytransf` in the conda environment `dtop_energytransf`
```bash
bash -c "make --file testing.makefile x-install"
```

### Install pytest products and perform pytesting in the conda environment `dtop_energytransf` :
```bash
make --file testing.makefile pytest
```

### Install dredd products and perform dredd testing in the conda environment `dtop_energytransf` :
```bash
make --file testing.makefile dredd-local
```

Run auxiliary test if necessary :

The auxiliary test script creates new SPEY assessment for running module basing on the `body` test data defined in `dredd_utils.py`.

```bash
flask run
python populate_db.py
```

## Docker environment tests

Cleanup environment :
```bash
make --file ci.makefile clean
```

Perform docker based dredd testing :
```bash
make --file ci.makefile dredd
```

Perform pytesting (all pytests are passed) :
```bash
make --file ci.makefile pytest
```

Perform all main tasks (including clean pytest dredd) :
```bash
make --file ci.makefile all
```
