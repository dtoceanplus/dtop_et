ifeq ($(OS),Windows_NT)
	SHELL:=bash
	win_pwd := $(shell cygpath -w $(shell pwd))
	docker-run = docker run --rm --volume $(win_pwd):/code
	docker-kept-run = docker run --volume $(win_pwd):/code
else
	docker-run = docker run --rm --volume ${PWD}:/code
	docker-kept-run = docker run --volume ${PWD}:/code
endif

all: clean pytest-mb dredd

et-base:
	cp -f ./environment.yml ./docker/base/
	(cd ./docker/base/ && docker build --tag et-base .)

rm-dist:
	rm -rf ./dist

dist/dtop_energytransf-*.tar.gz:
	python setup.py sdist


x-build: rm-dist dist/dtop_energytransf-*.tar.gz

build: et-base x-build

et-deploy: build
	cp -fr ./dist ./docker/deploy/
	(cd ./docker/deploy/ && docker build --cache-from=et-deploy --tag et-deploy .)

et-deploy-docker: et-base
	(docker build -f deploy.dockerfile --cache-from=et-deploy --tag et-deploy .)

et-pytest: et-deploy-docker
	cp -fr ./test ./docker/pytest/
	cp -f ./testing.makefile ./docker/pytest/
	(cd ./docker/pytest/ && docker build --tag et-pytest .)

pytest: et-pytest
	$(docker-kept-run) --name et-pytest-cont et-pytest make --file testing.makefile pytest
	docker cp et-pytest-cont:/app/report.xml .
	docker container rm et-pytest-cont

pytest-mb: et-deploy-docker
	docker-compose \
	--file docker-compose.yml \
	--file docker-compose.test.yml \
	up --abort-on-container-exit --exit-code-from=pytest \
	et pytest mc sc ec

shell-pytest: et-pytest
	$(docker-run) --tty --interactive et-pytest bash

dredd: et-deploy-docker
	touch ./hooks-output.txt
	docker-compose --file docker-compose.yml --file docker-compose.test.yml down
	docker-compose --file docker-compose.yml --file docker-compose.test.yml up --build --abort-on-container-exit --exit-code-from=dredd  dredd

clean:
	rm -fr dist
	rm -fr ./docker/deploy/dist
	rm -fr ./docker/pytest/test
	docker image rm --force et-base et-deploy et-pytest et-dredd

cypress-run: et-deploy-docker
	docker-compose --file docker-compose.yml --file docker-compose.test.yml down
	docker-compose --file docker-compose.yml --file docker-compose.test.yml up --detach  et client nginx e2e-cypress
	docker exec e2e-cypress /usr/wait-for-it.sh --timeout=300 client:8080
	docker exec e2e-cypress /usr/wait-for-it.sh --timeout=300 et:5000
	docker exec e2e-cypress npx cypress run
	docker exec e2e-cypress npx nyc report --reporter=text-summary




## !
## Set DTOP domain
DTOP_DOMAIN=dto.opencascade.com


## !
## Set the value to cors urls
ALL_MODULES_NICKNAMES := si sg sc mc ec et ed sk lmo spey rams slc esa cm mm
CORS_URLS := $(foreach module,${ALL_MODULES_NICKNAMES},http://${module}.${DTOP_DOMAIN},https://${module}.${DTOP_DOMAIN},)


## !
## Set the required module nickname
MODULE_SHORT_NAME=et

## !
## Set the required docker image TAG
ET_IMAGE_TAG=v1.15.6.3

## !
## Update the values of ET_CI_REGISTRY to your module registry
#ET_CI_REGISTRY?=registry.gitlab.com/opencascade/dtocean/fork-ET
ET_CI_REGISTRY=registry.gitlab.com/dtoceanplus/dtop_et


login:
	echo ${CI_REGISTRY_PASSWORD} | docker login --username ${CI_REGISTRY_USER} --password-stdin ${ET_CI_REGISTRY}

logout:
	docker logout ${ET_CI_REGISTRY}

build-prod-be:
	docker pull ${ET_CI_REGISTRY}/${MODULE_SHORT_NAME}_backend:${ET_IMAGE_TAG} || true
	docker build --cache-from ${ET_CI_REGISTRY}/${MODULE_SHORT_NAME}_backend:${ET_IMAGE_TAG} \
	  --tag ${ET_CI_REGISTRY}/${MODULE_SHORT_NAME}_backend:${ET_IMAGE_TAG} \
	  --file ./${MODULE_SHORT_NAME}-prod.dockerfile \
          .
	docker push ${ET_CI_REGISTRY}/${MODULE_SHORT_NAME}_backend:${ET_IMAGE_TAG}
	docker images

build-prod-fe:
	docker pull ${ET_CI_REGISTRY}/${MODULE_SHORT_NAME}_frontend:${ET_IMAGE_TAG} || true
	docker build --build-arg DTOP_MODULE_SHORT_NAME="${MODULE_SHORT_NAME}" \
	--cache-from ${ET_CI_REGISTRY}/${MODULE_SHORT_NAME}_frontend:${ET_IMAGE_TAG} \
	  --tag ${ET_CI_REGISTRY}/${MODULE_SHORT_NAME}_frontend:${ET_IMAGE_TAG} \
      --file ./src/dtop_energytransf/service/gui/frontend/frontend-prod.dockerfile \
	  ./src/dtop_energytransf
	docker push ${ET_CI_REGISTRY}/${MODULE_SHORT_NAME}_frontend:${ET_IMAGE_TAG}
	docker images

