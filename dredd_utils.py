import sys

import requests


body = {
    "Description": "Another example",
    "Technology": "Tidal",
    "Type_sim": "Assessment",
    "ControlStrategy": "Passive",
    "Mech_type": "Air Turbine",
    "Elect_type": "SCIG",
    "Grid_type": "B2B2Level",
    "CpxMech": 1,
    "CpxElec": 1,
    "CpxGrid": 1,
    "Date": "2020-06-06",
}


sys.stdout = sys.stderr = open("hooks-output.txt", "w", buffering=1)


def if_not_skipped(func):
    def wrapper(transaction):
        if not transaction["skip"]:
            func(transaction)

    return wrapper


def remove_all_projects(tr):
    protocol = tr["protocol"]
    host = tr["host"]
    port = tr["port"]
    projects_ids = get_project_ids(tr)
    for etId in projects_ids:
        requests.delete(f"{protocol}//{host}:{port}/energy_transf/{etId}")


def get_project_ids(tr):
    existing_project = check_project(tr)
    ids = [pr["etId"] for pr in existing_project]
    return list(set(ids))


def check_project(tr):
    protocol = tr["protocol"]
    host = tr["host"]
    port = tr["port"]
    existing_project = requests.get(f"{protocol}//{host}:{port}/energy_transf").json()
    return existing_project


def post_a_project(tr):
    protocol = tr["protocol"]
    host = tr["host"]
    port = tr["port"]

    r = requests.post(f"{protocol}//{host}:{port}/energy_transf", json=body)
    print(r.json())
    tr["_et_id"] = r.json()["ID"]
    print(tr)

    print("POST A PROJECT")
    print(body)
    print(f"{protocol}//{host}:{port}/energy_transf")
    print(r.status_code)
    print(r)


def get_a_project(tr):
    print(tr)
    existing_project = check_project(tr)
    print(existing_project)
    if existing_project:  # if any project exist
        project = existing_project[0]
    else:
        post_a_project(tr)
        projects = check_project(tr)

        if projects:
            project = projects
        else:
            print("ERROR NO PROJECT AVAILABLE IN THE DB")
    print("project")

    return project


# # def get_a_project_wo_farm(tr):
# #     print('get_a_project_wo_farm')
# #     existing_project = check_project(tr)
# #     if existing_project:  # if any project exist
# #         for project in existing_project:
# #             if not project['farm']:
# #                 print('Found a project without a farm')
# #                 return project

# #     print('No project without a farm')
# #     print('posting a new project')
# #     post_a_project(tr)
# #     project = get_a_project_wo_farm(tr)
# #     print(project)

# #     return project


# # def get_project_w_device(tr):
# #     existing_project = check_project(tr)
# #     if existing_project:  # if any project exist
# #         for project in existing_project:
# #             if project['device']:
# #                 print('Found a project with a device')
# #                 return project
# #         else:
# #             post_device()

# #     post_a_project(tr)
